//
//  ErrorHandlingServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ErrorHandlingServiceTests: QuickSpec {
  override func spec() {
    describe("ErrorHandlingService") {
      var sut: ErrorHandlingService!
      var session: MockSessionService!

      beforeEach {
        session = MockSessionService()
        sut = ErrorHandlingService(
          session: session
        )
      }

      afterEach {
        session = nil
        sut = nil
      }
      
      it("should call session.clearSession once on handleAPIError when .httpUnauthorized error code is used") {
        expect(session.clearSessionCallCount).to(equal(0))
        expect(session.clearSessionShouldBroadcast).to(beNil())

        let testInfo = APIClientFailedRequestInfo(
          testErrorCode: .httpUnauthorized
        )
        sut.handleAPIError(.failedRequest(testInfo))

        expect(session.clearSessionCallCount).to(equal(1))
        expect(session.clearSessionShouldBroadcast).to(beTrue())
      }
    }
  }
}
