//
//  MockErrorHandlingService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockErrorHandlingService: ErrorHandlingServiceProtocol {
  var errorToReturn: Error?

  private(set) var handleAPIErrorCallCount: Int = 0
  private(set) var handleAPIErrorValue: APIClientError?

  private(set) var handleErrorCallCount: Int = 0
  private(set) var handleErrorValue: Error?
  private(set) var handleErrorInfo: [String: Any]?
}

// MARK: - Methods

extension MockErrorHandlingService {
  func handleAPIError(_ apiError: APIClientError) {
    handleAPIErrorCallCount += 1
    handleAPIErrorValue = apiError
  }

  func handleError(_ error: Error) {
    handleErrorCallCount += 1
    handleErrorValue = error
  }

  func handleError(
    _ error: Error,
    info: [String: Any]?
  ) {
    handleErrorCallCount += 1
    handleErrorValue = error
    handleErrorInfo = info
  }
}
