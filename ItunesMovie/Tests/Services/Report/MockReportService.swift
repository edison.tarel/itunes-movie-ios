//
//  MockReportService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockReportService: ReportServiceProtocol {
  var errorToReturn: Error?
  var categoriesToReturn: [ReportCategory] = []

  private(set) var fetchReportCategoriesCallCount: Int = 0

  private(set) var submitUserReportValue: Report?
  private(set) var submitUserReportUserId: Int?
  private(set) var submitUserReportCallCount: Int = 0
}

extension MockReportService {
  func fetchReportCategories(
    onSuccess: @escaping SingleResult<[ReportCategory]>,
    onError: @escaping ErrorResult
  ) {
    fetchReportCategoriesCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(categoriesToReturn)
    }
  }

  func reportUser(
    with userId: Int,
    report: Report,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    submitUserReportValue = report
    submitUserReportUserId = userId
    submitUserReportCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
