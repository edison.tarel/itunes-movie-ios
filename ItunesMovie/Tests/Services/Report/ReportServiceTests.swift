//
//  ReportServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ReportServiceTests: QuickSpec {
  override func spec() {
    describe("ReportService") {
      var sut: ReportService!
      var api: MockReportAPI!

      beforeEach {
        api = MockReportAPI()
        sut = ReportService(api: api)
      }

      afterEach {
        api = nil
        sut = nil
      }

      it("should call api.getReportCategories on fetchReportCategories") {
        expect(api.getReportCategoriesCallCount).to(equal(0))

        sut.fetchReportCategories(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getReportCategoriesCallCount).to(equal(1))
      }

      it("should call api.postReport and pass params on registerWithEmail") {
        let report = Report(
          reasonId: -1,
          description: "Random",
          attachments: []
        )
        expect(api.postReportValue).to(beNil())
        expect(api.postReportUserId).to(beNil())
        expect(api.postReportCallCount).to(equal(0))

        sut.reportUser(
          with: 11,
          report: report,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postReportValue).to(equal(report))
        expect(api.postReportUserId).to(equal(11))
        expect(api.postReportCallCount).to(equal(1))
      }

      it("should only call api.getReportCategories once even on multiple fetchReportCategories") {
        expect(api.getReportCategoriesCallCount).to(equal(0))

        sut.fetchReportCategories(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )
        sut.fetchReportCategories(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getReportCategoriesCallCount).to(equal(1))
      }
    }
  }
}
