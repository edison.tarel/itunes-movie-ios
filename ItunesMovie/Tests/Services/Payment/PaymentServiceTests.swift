//
//  PaymentServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PaymentServiceTests: QuickSpec {
  override func spec() {
    describe("PaymentService") {
      var sut: PaymentService!
      var api: MockAuthConnectAPI!

      beforeEach {
        api = MockAuthConnectAPI()
        sut = PaymentService(
          api: api
        )
      }

      afterEach {
        api = nil
        sut = nil
      }

      it("should call api.postEphemeralKey once on fetchEphemeralKey") {
        expect(api.postEphemeralKeyCallCount).to(equal(0))

        sut.fetchEphemeralKey(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postEphemeralKeyCallCount).to(equal(1))
      }

      it("should call api.getAuthConnectExternalAccounts once on fetchConnectedBanks") {
        expect(api.getAuthConnectExternalAccountsCallCount).to(equal(0))

        sut.fetchConnectedBanks(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getAuthConnectExternalAccountsCallCount).to(equal(1))
      }

      it("should pass correct params and call api.putAuthConnectExternalAccounts once on updateConnectedBanks") {
        let bankDetails: BankDetails = .init(testId: "BankDetailsId")
        expect(api.putAuthConnectExternalAccountsCallCount).to(equal(0))
        expect(api.putAuthConnectExternalAccountsAccountId).to(beNil())
        expect(api.putAuthConnectExternalAccountsParams).to(beNil())

        sut.updateConnectedBanks(
          accountId: "AccountId",
          bankDetails: bankDetails,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.putAuthConnectExternalAccountsCallCount).to(equal(1))
        expect(api.putAuthConnectExternalAccountsAccountId).to(equal("AccountId"))
        expect(api.putAuthConnectExternalAccountsParams?[stringOrNil: "id"]).to(equal("BankDetailsId"))
      }

      it("should call api.postAuthConnectAccount once on setupStripeConnect") {
        let stripeConnect: StripeConnect = .init(testExternalAccount: "ExternalAccount")
        expect(api.postAuthConnectAccountCallCount).to(equal(0))

        sut.setupStripeConnect(
          stripeConnectParams: stripeConnect,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthConnectAccountCallCount).to(equal(1))
        expect(api.postAuthConnectAccountParams?[stringOrNil: "external_account"]).to(equal("ExternalAccount"))
      }

      it("should call api.getAuthConnectAccount once on fetchStripeConnect") {
        expect(api.getAuthConnectAccountCallCount).to(equal(0))

        sut.fetchStripeConnect(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getAuthConnectAccountCallCount).to(equal(1))
      }

      it("should call api.deleteAuthConnectAccount once on deleteAuthConnectAccount") {
        expect(api.deleteAuthConnectAccountCallCount).to(equal(0))

        sut.deleteAuthConnectAccount(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.deleteAuthConnectAccountCallCount).to(equal(1))
      }

      it("should pass correct params and call api.postAuthConnectFileUpload once on postAuthConnectFileUpload") {
        let data = "Data".data(using: .utf8)!
        expect(api.postAuthConnectFileUploadCallCount).to(equal(0))
        expect(api.postAuthConnectFileUploadImageData).to(beNil())
        expect(api.postAuthConnectFileUploadPurpose).to(beNil())

        sut.postAuthConnectFileUpload(
          imageData: data,
          purpose: "Purpose",
          onProgress: DefaultClosure.singleResult(),
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthConnectFileUploadCallCount).to(equal(1))
        expect(api.postAuthConnectFileUploadImageData).to(equal(data))
        expect(api.postAuthConnectFileUploadPurpose).to(equal("Purpose"))
      }

      it("should pass correct params and call api.postAuthConnectAccountExternalAccount once on stripeConnectExternalAccount") {
        expect(api.postAuthConnectAccountExternalAccountCallCount).to(equal(0))
        expect(api.postAuthConnectAccountExternalAccountParams).to(beNil())

        sut.stripeConnectExternalAccount(
          externalAccount: .init(testAccountNumber: "AccountNumber"),
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthConnectAccountExternalAccountCallCount).to(equal(1))
        expect(api.postAuthConnectAccountExternalAccountParams?[stringOrNil: "account_number"]).to(equal("AccountNumber"))
      }

      // TODO: Add success and fail tests
      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

//        it("should call onSuccess closure once on someFunc") {
//          var onSuccessCallCount = 0
//
//          sut.someFunc(
//            param: "Param",
//            onSuccess: { onSuccessCallCount += 1 },
//            onError: DefaultClosure.singleResult()
//          )
//
//          expect(onSuccessCallCount).toEventually(equal(1))
//        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
        }

//        it("should return apiError and call onError closure once on someFunc") {
//          var passedError: Error?
//          var onErrorCallCount = 0
//
//          sut.someFunc(
//            param: "Param",
//            onSuccess: DefaultClosure.voidResult(),
//            onError: {
//              passedError = $0
//              onErrorCallCount += 1
//            }
//          )
//
//          expect(passedError).toEventually(be(apiError))
//          expect(onErrorCallCount).toEventually(equal(1))
//        }
      }
    }
  }
}
