//
//  MockPaymentService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockPaymentService: PaymentServiceProtocol {
  var errorToReturn: Error?
  var ephemeralKeyToReturn: EphemeralKey = .init()
  var connectedBanksToReturn: [BankDetails] = []
  var stripeConnectToReturn: StripeConnect = .init()

  private(set) var fetchEphemeralKeyCallCount: Int = 0
  private(set) var fetchConnectedBanksCallCount: Int = 0

  private(set) var updateConnectedBanksCallCount: Int = 0
  private(set) var updateConnectedBanksBankDetails: BankDetails?

  private(set) var setupStripeConnectCallCount: Int = 0
  private(set) var setupStripeConnectParams: StripeConnect?

  private(set) var fetchStripeConnectCallCount: Int = 0
  private(set) var deleteAuthConnectAccountCallCount: Int = 0

  private(set) var postAuthConnectFileUploadCallCount: Int = 0
  private(set) var postAuthConnectFileUploadImageData: Data?
  private(set) var postAuthConnectFileUploadPurpose: String?

  private(set) var stripeConnectExternalAccountCallCount: Int = 0
  private(set) var stripeConnectExternalAccountValue: StripeExternalAccountParams?
}

// MARK: - Methods

extension MockPaymentService {
  func fetchEphemeralKey(
    onSuccess: @escaping SingleResult<EphemeralKey>,
    onError: @escaping ErrorResult
  ) {
    fetchEphemeralKeyCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(ephemeralKeyToReturn)
    }
  }

  func fetchConnectedBanks(
    onSuccess: @escaping SingleResult<[BankDetails]>,
    onError: @escaping ErrorResult
  ) {
    fetchConnectedBanksCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(connectedBanksToReturn)
    }
  }

  func updateConnectedBanks(
    accountId: String,
    bankDetails: BankDetails,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    updateConnectedBanksCallCount += 1
    updateConnectedBanksBankDetails = bankDetails

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func setupStripeConnect(
    stripeConnectParams: StripeConnect,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    setupStripeConnectCallCount += 1
    setupStripeConnectParams = stripeConnectParams

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func fetchStripeConnect(
    onSuccess: @escaping SingleResult<StripeConnect>,
    onError: @escaping ErrorResult
  ) {
    fetchStripeConnectCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(stripeConnectToReturn)
    }
  }

  func deleteAuthConnectAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    deleteAuthConnectAccountCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func postAuthConnectFileUpload(
    imageData: Data,
    purpose: String?,
    onProgress: SingleResult<Progress>?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    postAuthConnectFileUploadCallCount += 1
    postAuthConnectFileUploadImageData = imageData
    postAuthConnectFileUploadPurpose = purpose

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess("")
    }
  }

  func stripeConnectExternalAccount(
    externalAccount: StripeExternalAccountParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    stripeConnectExternalAccountCallCount += 1
    stripeConnectExternalAccountValue = externalAccount

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
