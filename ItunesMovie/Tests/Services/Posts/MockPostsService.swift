//
//  MockPostsService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import NSObject_Rx

@testable import ItunesMovie

class MockPostsService: PostsServiceProtocol {
  var errorToReturn: Error?
  var postsToReturn: [Post] = []
  var postToReturn: Post?
  var commentsToReturn: [Comment] = []
  var progressToReturn: Progress?

  private(set) var postsCallCount: Int = 0

  private(set) var postWithIdCallCount: Int = 0
  private(set) var postWithIdValue: Int?

  private(set) var commentsInPostWithIdCallCount: Int = 0
  private(set) var commentsInPostWithIdValue: Int?

  private(set) var fetchPostsCallCount: Int = 0
  private(set) var fetchPostsPage: APIPage?
  private(set) var fetchPostsInclude: [PostsIncludes]?

  private(set) var likePostCallCount: Int = 0
  private(set) var likePostId: Int?

  private(set) var unlikePostCallCount: Int = 0
  private(set) var unlikePostId: Int?

  private(set) var createPostCallCount: Int = 0
  private(set) var createPostImageData: Data?
  private(set) var createPostDescription: String?

  private(set) var fetchPostCommentsCallCount: Int = 0
  private(set) var fetchPostCommentsPostId: Int?
  private(set) var fetchPostCommentsInclude: [PostsCommentIncludes]?

  private(set) var createPostCommentCallCount: Int = 0
  private(set) var createPostCommentPostId: Int?
  private(set) var createPostCommentBody: String?

  private(set) var replyCommentId: Int?
  private(set) var replyCommentValue: String?
  private(set) var replyCommentCallCount: Int = 0

  private(set) var bindPostsCallCount: Int = 0
  private(set) var bindPostsOnRefresh: VoidResult?

  private(set) var bindPostCallCount: Int = 0
  private(set) var bindPostOnRefresh: VoidResult?

  private(set) var bindCommentsInPostCallCount: Int = 0
  private(set) var bindCommentsInPostOnRefresh: VoidResult?
}

// MARK: - Methods

extension MockPostsService {
  func posts() -> [Post] {
    postsCallCount += 1

    return postsToReturn
  }

  func post(withId id: Int) -> Post? {
    postWithIdCallCount += 1
    postWithIdValue = id

    return postToReturn
  }

  func commentsInPost(withId id: Int) -> [Comment] {
    commentsInPostWithIdCallCount += 1
    commentsInPostWithIdValue = id

    return commentsToReturn
  }

  func fetchPosts(
    userId: Int?,
    page: APIPage,
    include: [PostsIncludes],
    onSuccess: @escaping SingleResult<[Post]>,
    onError: @escaping ErrorResult
  ) {
    fetchPostsCallCount += 1
    fetchPostsPage = page
    fetchPostsInclude = include

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(postsToReturn)
    }
  }

  func likePost(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    likePostCallCount += 1
    likePostId = id

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func unlikePost(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    unlikePostCallCount += 1
    unlikePostId = id

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func createPost(
    imageData: Data,
    description: String,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    createPostCallCount += 1
    createPostImageData = imageData
    createPostDescription = description

    if let e = errorToReturn {
      onError(e)
    } else if let p = progressToReturn {
      onProgress(p)
    } else {
      onSuccess()
    }
  }

  func fetchPostComments(
    postId: Int,
    include: [PostsCommentIncludes],
    onSuccess: @escaping SingleResult<[Comment]>,
    onError: @escaping ErrorResult
  ) {
    fetchPostCommentsCallCount += 1
    fetchPostCommentsPostId = postId
    fetchPostCommentsInclude = include

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(commentsToReturn)
    }
  }

  func createPostComment(
    postId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) {
    createPostCommentCallCount += 1
    createPostCommentPostId = postId
    createPostCommentBody = body

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }
  }

  func replyToComment(
    withId commentId: Int,
    comment: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) {
    replyCommentId = commentId
    replyCommentValue = comment
    replyCommentCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(Comment())
    }
  }

  func bindPosts(
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  ) {
    bindPostsCallCount += 1
    bindPostsOnRefresh = onRefresh
  }

  func bindPost(
    withId id: Int,
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  ) {
    bindPostCallCount += 1
    bindPostOnRefresh = onRefresh
  }

  func bindCommentsInPost(
    withId id: Int,
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  ) {
    bindCommentsInPostCallCount += 1
    bindCommentsInPostOnRefresh = onRefresh
  }
}
