//
//  PostsServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PostsServiceTests: QuickSpec {
  override func spec() {
    describe("PostsService") {
      var sut: PostsService!
      var api: MockPostsAPI!

      beforeEach {
        api = MockPostsAPI()
        sut = PostsService(
          api: api
        )
      }

      afterEach {
        api = nil
        sut = nil
      }

      it("should pass correct params and call api.getPosts once on fetchPosts") {
        expect(api.getPostsCallCount).to(equal(0))
        expect(api.getPostsUserId).to(beNil())
        expect(api.getPostsPage).to(beNil())

        sut.fetchPosts(
          userId: -1,
          page: APIPage(index: 2, size: 3),
          include: [.author, .commentsCount],
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getPostsCallCount).to(equal(1))
        expect(api.getPostsUserId).to(equal(-1))
        expect(api.getPostsPage?.index).to(equal(2))
        expect(api.getPostsPage?.size).to(equal(3))
        expect(api.getPostsInclude).to(equal([.author, .commentsCount]))
      }

      it("should pass correct params and call api.likePost once on likePost") {
        expect(api.likePostCallCount).to(equal(0))
        expect(api.likePostId).to(beNil())

        sut.likePost(
          with: -1,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.likePostCallCount).to(equal(1))
        expect(api.likePostId).to(equal(-1))
      }

      it("should pass correct params and call api.unlikePost once on unlikePost") {
        expect(api.unlikePostCallCount).to(equal(0))
        expect(api.unlikePostId).to(beNil())

        sut.unlikePost(
          with: -1,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.unlikePostCallCount).to(equal(1))
        expect(api.unlikePostId).to(equal(-1))
      }

      it("should pass correct params and call api.createPost once on createPost") {
        let data = "Data".data(using: .utf8)!
        expect(api.createPostCallCount).to(equal(0))
        expect(api.createPostImageData).to(beNil())
        expect(api.createPostDescription).to(beNil())

        sut.createPost(
          imageData: data,
          description: "Description",
          onProgress: DefaultClosure.singleResult(),
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.createPostCallCount).to(equal(1))
        expect(api.createPostImageData).to(equal(data))
        expect(api.createPostDescription).to(equal("Description"))
      }

      it("should pass correct params and call api.getPostsComments once on fetchPostComments") {
        expect(api.getPostsCommentsCallCount).to(equal(0))
        expect(api.getPostsCommentsPostId).to(beNil())
        expect(api.getPostsCommentsInclude).to(beNil())

        sut.fetchPostComments(
          postId: -1,
          include: [.author, .responses],
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getPostsCommentsCallCount).to(equal(1))
        expect(api.getPostsCommentsPostId).to(equal(-1))
        expect(api.getPostsCommentsInclude).to(equal([.author, .responses]))
      }

      it("should pass correct params and call api.createPostComment once on postPostsComments") {
        expect(api.postPostsCommentsCallCount).to(equal(0))
        expect(api.postPostsCommentsPostId).to(beNil())
        expect(api.postPostsCommentsBody).to(beNil())

        sut.createPostComment(
          postId: -1,
          body: "Body",
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postPostsCommentsCallCount).to(equal(1))
        expect(api.postPostsCommentsPostId).to(equal(-1))
        expect(api.postPostsCommentsBody).to(equal("Body"))
      }

      it("should pass parameters and call api.postCommentsResponses on replyToComment") {
        let testId = 5
        let testMessage = "Test Comment"

        expect(api.postCommentsId).to(beNil())
        expect(api.postCommentsBody).to(beNil())
        expect(api.postCommentsCallCount).to(equal(0))

        sut.replyToComment(
          withId: testId,
          comment: testMessage,
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postCommentsId).to(equal(testId))
        expect(api.postCommentsBody).to(equal(testMessage))
        expect(api.postCommentsCallCount).to(equal(1))
      }

      context("when initialized with in progress api") {
        var apiProgress: Progress!

        beforeEach {
          apiProgress = Progress(totalUnitCount: 1)
          api.progressToReturn = apiProgress
        }

        it("should pass api progress and call onProgress closure once on createPost") {
          var onProgressCallCount = 0
          var progressReturned: Progress?

          sut.createPost(
            imageData: Data(),
            description: "",
            onProgress: {
              progressReturned = $0
              onProgressCallCount += 1
            },
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(progressReturned).toEventually(be(apiProgress))
          expect(onProgressCallCount).toEventually(equal(1))
        }
      }

      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

        it("should call onSuccess closure once on fetchPosts") {
          var onSuccessCallCount = 0

          sut.fetchPosts(
            userId: -1,
            page: APIPage(index: 0, size: 0),
            include: [],
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on likePost") {
          var onSuccessCallCount = 0

          sut.likePost(
            with: -1,
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on unlikePost") {
          var onSuccessCallCount = 0

          sut.unlikePost(
            with: -1,
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on createPost") {
          var onSuccessCallCount = 0

          sut.createPost(
            imageData: Data(),
            description: "",
            onProgress: DefaultClosure.singleResult(),
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on fetchPostComments") {
          var onSuccessCallCount = 0

          sut.fetchPostComments(
            postId: -1,
            include: [],
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on createPostComment") {
          var onSuccessCallCount = 0

          sut.createPostComment(
            postId: -1,
            body: "",
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on replyToComment") {
          var onSuccessCallCount = 0

          sut.replyToComment(
            withId: 0,
            comment: "Test",
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
        }

        it("should pass api error and call onError closure once on fetchPosts") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.fetchPosts(
            userId: -1,
            page: APIPage(index: 0, size: 0),
            include: [],
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass api error and call onError closure once on likePost") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.likePost(
            with: -1,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass api error and call onError closure once on unlikePost") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.unlikePost(
            with: -1,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass api error and call onError closure once on createPost") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.createPost(
            imageData: Data(),
            description: "",
            onProgress: DefaultClosure.singleResult(),
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass api error and call onError closure once on fetchPostComments") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.fetchPostComments(
            postId: -1,
            include: [],
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass api error and call onError closure once on createPostComment") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.createPostComment(
            postId: -1,
            body: "",
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should return apiError and call onError closure once on replyToComment") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.replyToComment(
            withId: 0,
            comment: "Test",
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
