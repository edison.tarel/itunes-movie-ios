//
//  CredentialsServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class CredentialsServiceTests: QuickSpec {
  override func spec() {
    describe("CredentialsService") {
      var sut: CredentialsService!
      var authChangeAPI: MockAuthChangeAPI!
      var authVerificationAPI: MockAuthVerificationAPI!

      beforeEach {
        authChangeAPI = MockAuthChangeAPI()
        authVerificationAPI = MockAuthVerificationAPI()
        sut = CredentialsService(
          authChangeAPI: authChangeAPI,
          authVerificationAPI: authVerificationAPI
        )
      }

      afterEach {
        sut = nil
        authChangeAPI = nil
        authVerificationAPI = nil
      }

      it("should pass token and call api.postAuthVerificationToken once on requestVerificationToken") {
        expect(authVerificationAPI.postAuthVerificationTokenCallCount).to(equal(0))

        sut.requestVerificationToken(
          with: "Password",
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authVerificationAPI.postAuthVerificationTokenPassword).to(equal("Password"))
        expect(authVerificationAPI.postAuthVerificationTokenCallCount).to(equal(1))
      }

      it("should pass email and call api.postAuthChangeEmail once on requestChangeEmail") {
        expect(authChangeAPI.postAuthChangeEmailCallCount).to(equal(0))

        sut.requestChangeEmail(
          with: "NewEmail",
          token: "EmailToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangeEmailValue).to(equal("NewEmail"))
        expect(authChangeAPI.postAuthChangeEmailToken).to(equal("EmailToken"))
        expect(authChangeAPI.postAuthChangeEmailCallCount).to(equal(1))
      }

      it("should pass token and call api.postAuthChangeEmailVerify once on verifyChangeEmail") {
        expect(authChangeAPI.postAuthChangeEmailVerifyCallCount).to(equal(0))

        sut.verifyChangeEmail(
          with: "EmailToken",
          verificationToken: "EmailVerificationToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangeEmailVerifyToken).to(equal("EmailToken"))
        expect(authChangeAPI.postAuthChangeEmailVerifyVerificationToken).to(equal("EmailVerificationToken"))
        expect(authChangeAPI.postAuthChangeEmailVerifyCallCount).to(equal(1))
      }

      it("should pass phoneNumber and call api.postAuthChangePhoneNumber once on requestChangePhoneNumber") {
        expect(authChangeAPI.postAuthChangePhoneNumberCallCount).to(equal(0))

        sut.requestChangePhoneNumber(
          with: "NewPhoneNumber",
          token: "PhoneNumberToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangePhoneNumberValue).to(equal("NewPhoneNumber"))
        expect(authChangeAPI.postAuthChangePhoneNumberToken).to(equal("PhoneNumberToken"))
        expect(authChangeAPI.postAuthChangePhoneNumberCallCount).to(equal(1))
      }

      it("should pass token and call api.postAuthChangePhoneNumberVerify once on verifyChangePhoneNumber") {
        expect(authChangeAPI.postAuthChangePhoneNumberVerifyCallCount).to(equal(0))

        sut.verifyChangePhoneNumber(
          with: "PhoneNumberToken",
          verificationToken: "PhoneNumberVerificationToken",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangePhoneNumberVerifyToken).to(equal("PhoneNumberToken"))
        expect(authChangeAPI.postAuthChangePhoneNumberVerifyVerificationToken).to(equal("PhoneNumberVerificationToken"))
        expect(authChangeAPI.postAuthChangePhoneNumberVerifyCallCount).to(equal(1))
      }

      it("should pass newPassword, oldPassword and call api.postAuthChangePassword once on changePassword") {
        expect(authChangeAPI.postAuthChangePasswordCallCount).to(equal(0))

        sut.changePassword(
          to: "NewPassword",
          oldPassword: "OldPassword",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(authChangeAPI.postAuthChangePasswordNewValue).to(equal("NewPassword"))
        expect(authChangeAPI.postAuthChangePasswordOldValue).to(equal("OldPassword"))
        expect(authChangeAPI.postAuthChangePasswordCallCount).to(equal(1))
      }

      context("when initialized with successful api") {
        beforeEach {
          authChangeAPI.errorToReturn = nil
          authVerificationAPI.errorToReturn = nil
        }

        it("should call onSuccess closure once on requestVerificationToken") {
          var onSuccessCallCount = 0

          sut.requestVerificationToken(
            with: "Password",
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on requestChangeEmail") {
          var onSuccessCallCount = 0

          sut.requestChangeEmail(
            with: "NewEmail",
            token: "EmailToken",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on verifyChangeEmail") {
          var onSuccessCallCount = 0

          sut.verifyChangeEmail(
            with: "EmailToken",
            verificationToken: "EmailNumberVerificationToken",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on requestChangePhoneNumber") {
          var onSuccessCallCount = 0

          sut.requestChangePhoneNumber(
            with: "NewPhoneNumber",
            token: "PhoneNumberToken",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on verifyChangePhoneNumber") {
          var onSuccessCallCount = 0

          sut.verifyChangePhoneNumber(
            with: "PhoneNumberToken",
            verificationToken: "PhoneNumberVerificationToken",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on changePassword") {
          var onSuccessCallCount = 0

          sut.changePassword(
            to: "NewPassword",
            oldPassword: "OldPassword",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          authChangeAPI.errorToReturn = apiError
          authVerificationAPI.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
        }

        it("should pass apiError and call onError closure once on requestVerificationToken") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.requestVerificationToken(
            with: "Password",
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass apiError and call onError closure once on requestChangeEmail") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.requestChangeEmail(
            with: "NewEmail",
            token: "EmailToken",
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass apiError and call onError closure once on verifyChangeEmail") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.verifyChangeEmail(
            with: "EmailToken",
            verificationToken: "EmailNumberVerificationToken",
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass apiError and call onError closure once on requestChangePhoneNumber") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.requestChangePhoneNumber(
            with: "NewPhoneNumber",
            token: "PhoneNumberToken",
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass apiError and call onError closure once on verifyChangePhoneNumber") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.verifyChangePhoneNumber(
            with: "PhoneNumberToken",
            verificationToken: "PhoneNumberVerificationToken",
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass apiError and call onError closure once on changePassword") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.changePassword(
            to: "NewPassword",
            oldPassword: "OldPassword",
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
