//
//  MockAccountDeletionService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockAccountDeletionService: AccountDeletionServiceProtocol {
  var onDeAuth: BoolResult?
  var onError: ErrorResult?
  
  var errorToReturn: Error?

  private(set) var deleteAccountCallCount: Int = 0
  private(set) var deleteAccountId: Int?
}

// MARK: - Methods

extension MockAccountDeletionService {
  func deleteAccount(
    withId id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    deleteAccountCallCount += 1
    deleteAccountId = id

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
