//
//  AccountDeletionServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class AccountDeletionServiceTests: QuickSpec {
  override func spec() {
    describe("AccountDeletionService") {
      var sut: AccountDeletionService!
      var api: MockUsersAPI!

      beforeEach {
        api = MockUsersAPI()
        sut = AccountDeletionService(
          api: api
        )
      }

      afterEach {
        api = nil
        sut = nil
      }

      it("should pass correct params and call api.deleteUsers once on deleteAccount") {
        expect(api.deleteUsersCallCount).to(equal(0))
        expect(api.deleteUsersId).to(beNil())

        sut.deleteAccount(
          withId: -1,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.deleteUsersCallCount).to(equal(1))
        expect(api.deleteUsersId).to(equal(-1))
      }

      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

        it("should call onSuccess closure once on deleteAccount") {
          var onSuccessCallCount = 0

          sut.deleteAccount(
            withId: -1,
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
        }

        it("should return apiError and call onError closure once on deleteAccount") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.deleteAccount(
            withId: -1,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
