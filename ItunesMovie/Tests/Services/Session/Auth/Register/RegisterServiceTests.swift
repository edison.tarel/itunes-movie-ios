//
//  RegisterServiceTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class RegisterServiceTests: QuickSpec {
  override func spec() {
    describe("RegisterService") {
      var sut: RegisterService!
      var api: MockAuthRegisterAPI!

      beforeEach {
        api = MockAuthRegisterAPI()
        sut = RegisterService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.postAuthRegister on registerWithEmail") {
        expect(api.postAuthRegisterEmailCallCount).to(equal(0))

        sut.registerWithEmail(
          "Email",
          password: "Password",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthRegisterEmailCallCount).to(equal(1))
      }

      it("should pass the email to api on registerWithEmail") {
        sut.registerWithEmail(
          "Email",
          password: "Password",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthRegisterEmail).to(equal("Email"))
        expect(api.postAuthRegisterPassword).to(equal("Password"))
      }

      it("should call api.postAuthRegisterPhoneNumber on registerWithPhoneNumber") {
        expect(api.postAuthRegisterPhoneNumberCallCount).to(equal(0))

        sut.registerWithPhoneNumber(
          "PhoneNumber",
          password: "Password",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthRegisterPhoneNumberCallCount).to(equal(1))
      }

      it("should pass the phoneNumber and password to api on registerWithPhoneNumber") {
        sut.registerWithPhoneNumber(
          "PhoneNumber",
          password: "Password",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthRegisterPhoneNumber).to(equal("PhoneNumber"))
        expect(api.postAuthRegisterPassword).to(equal("Password"))
      }

      context("when api is set to succeed") {
        beforeEach {
          api.errorToReturn = nil
        }

        it("should call onAuth and onSuccess closures on registerWithEmail") {
          var onSuccessCalled = false
          var onAuthCalled = false
          sut.onAuth = { _ in
            onAuthCalled = true
          }

          sut.registerWithEmail(
            "Email",
            password: "Password",
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onAuthCalled).toEventually(beTrue())
          expect(onSuccessCalled).toEventually(beTrue())
        }

        it("should call onAuth and onSuccess closures on registerWithPhoneNumber") {
          var onSuccessCalled = false
          var onAuthCalled = false
          sut.onAuth = { _ in
            onAuthCalled = true
          }

          sut.registerWithPhoneNumber(
            "PhoneNumber",
            password: "Password",
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onAuthCalled).toEventually(beTrue())
          expect(onSuccessCalled).toEventually(beTrue())
        }
      }

      context("when api is set to fail") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
        }

        it("should call onError closure and pass error from api on registerWithEmail") {
          var passedError: Error?
          var onErrorCalled = false

          sut.registerWithEmail(
            "Email",
            password: "Password",
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCalled = true
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCalled).toEventually(beTrue())
        }

        it("should call onError closure and pass error from api on registerWithPhoneNumber") {
          var passedError: Error?
          var onErrorCalled = false

          sut.registerWithPhoneNumber(
            "PhoneNumber",
            password: "Password",
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCalled = true
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCalled).toEventually(beTrue())
        }
      }
    }
  }
}
