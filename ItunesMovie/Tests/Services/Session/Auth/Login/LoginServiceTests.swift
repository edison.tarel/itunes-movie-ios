//
//  LoginServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class LoginServiceTests: QuickSpec {
  override func spec() {
    describe("LoginService") {
      var sut: LoginService!
      var api: MockAuthLoginAPI!

      beforeEach {
        api = MockAuthLoginAPI()
        sut = LoginService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should pass parameters and call api.postAuthLogin on loginWithEmail") {
        expect(api.postAuthLoginCallCount).to(equal(0))
        expect(api.postAuthLoginUsername).to(beNil())
        expect(api.postAuthLoginPassword).to(beNil())

        sut.loginWithEmail(
          "Email",
          password: "Password",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthLoginCallCount).to(equal(1))
        expect(api.postAuthLoginUsername).to(equal("Email"))
        expect(api.postAuthLoginPassword).to(equal("Password"))
      }

      it("should pass parameters and call api.postAuthLogin on loginWithPhoneNumber") {
        expect(api.postAuthLoginCallCount).to(equal(0))
        expect(api.postAuthLoginUsername).to(beNil())
        expect(api.postAuthLoginPassword).to(beNil())

        sut.loginWithPhoneNumber(
          "PhoneNumber",
          password: "Password",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthLoginCallCount).to(equal(1))
        expect(api.postAuthLoginUsername).to(equal("PhoneNumber"))
        expect(api.postAuthLoginPassword).to(equal("Password"))
      }

      context("when api is set to succeed") {
        beforeEach {
          api.errorToReturn = nil
        }

        it("should call onAuth and onSuccess closures once on loginWithEmail") {
          var onSuccessCallCount = 0
          var onAuthCallCount = 0
          sut.onAuth = { _ in
            onAuthCallCount += 1
          }

          sut.loginWithEmail(
            "Email",
            password: "Password",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onAuthCallCount).toEventually(equal(1))
          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onAuth and onSuccess closures once on loginWithPhoneNumber") {
          var onSuccessCallCount = 0
          var onAuthCallCount = 0
          sut.onAuth = { _ in
            onAuthCallCount += 1
          }

          sut.loginWithPhoneNumber(
            "PhoneNumber",
            password: "Password",
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onAuthCallCount).toEventually(equal(1))
          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when api is set to fail") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
        }

        it("should pass back error from api and call onError closure once on loginWithEmail") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.loginWithEmail(
            "Email",
            password: "Password",
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass back error from api and call onError closure once on loginWithPhoneNumber") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.loginWithPhoneNumber(
            "PhoneNumber",
            password: "Password",
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
