//
//  MockSessionService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import ItunesMovie

class MockSessionService: SessionServiceProtocol {
  var user: User?
  
  var register: RegisterServiceProtocol = MockRegisterService()
  var login: LoginServiceProtocol = MockLoginService()
  var socialAuth: SocialAuthServiceProtocol = MockSocialAuthService()
  
  var accountVerification: AccountVerificationServiceProtocol = MockAccountVerificationService()
  var profileSetup: ProfileSetupServiceProtocol = MockProfileSetupService()
  var profileRefresh: ProfileRefreshServiceProtocol = MockProfileRefreshService()
  
  var credentials: CredentialsServiceProtocol = MockCredentialsService()
  
  var post: PostsServiceProtocol = MockPostsService()
  
  var logout: LogoutServiceProtocol = MockLogoutService()
  
  var isActive: Bool = false

  var errorToReturn: Error?

  private(set) var clearSessionCallCount: Int = 0
  private(set) var clearSessionShouldBroadcast: Bool?
}

// MARK: - Methods

extension MockSessionService {
  func clearSession(shouldBroadcast: Bool) {
    clearSessionCallCount += 1
    clearSessionShouldBroadcast = shouldBroadcast
  }
}
