//
//  MockEditProfileService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockEditProfileService: EditProfileServiceProtocol {
  var onUserResult: SingleResult<User>?
  var onAvatarResult: SingleResult<Photo>?

  var errorToReturn: Error?

  private(set) var setInfoFullName: String?
  private(set) var setInfoBirthdate: Date?
  private(set) var setInfoDescription: String?
  private(set) var setInfoCallCount: Int = 0

  private(set) var setPictureData: Data?
  private(set) var setPictureCallCount: Int = 0
}

// MARK: - Methods

extension MockEditProfileService {
  func setInfo(
    fullName: String,
    birthdate: Date?,
    description: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    setInfoFullName = fullName
    setInfoBirthdate = birthdate
    setInfoDescription = description
    setInfoCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    setPictureData = data
    setPictureCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
