//
//  EditProfileServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class EditProfileServiceTests: QuickSpec {
  override func spec() {
    describe("EditProfileService") {
      var sut: EditProfileService!
      var api: MockAuthProfileAPI!

      beforeEach {
        api = MockAuthProfileAPI()
        sut = EditProfileService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.putAuthProfile on setInfo") {
        expect(api.putAuthProfileCallCount).to(equal(0))

        sut.setInfo(
          fullName: "",
          birthdate: nil,
          description: nil,
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.putAuthProfileCallCount).to(equal(1))
      }

      it("should call api.postAuthProfileAvatar on setPicture") {
        expect(api.postAuthProfileAvatarCallCount).to(equal(0))

        sut.setPicture(
          with: Data(),
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthProfileAvatarCallCount).to(equal(1))
      }

      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

        afterEach {
          api.reset()
        }

        it("should call onSuccess and onUserResult closure each once on setInfo") {
          var onSuccessCallCount = 0
          var onUserResultCallCount = 0
          sut.onUserResult = { _ in
            onUserResultCallCount += 1
          }

          sut.setInfo(
            fullName: "",
            birthdate: nil,
            description: nil,
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
          expect(onUserResultCallCount).toEventually(equal(1))
        }

        it("should call onSuccess and onUserResult closure each once on setPicture") {
          var onSuccessCallCount = 0
          var onAvatarResultCallCount = 0
          sut.onAvatarResult = { _ in
            onAvatarResultCallCount += 1
          }

          sut.setPicture(
            with: Data(),
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
          expect(onAvatarResultCallCount).toEventually(equal(1))
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
          api.reset()
        }

        it("should pass apiError and call onError closure once on setInfo") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.setInfo(
            fullName: "",
            birthdate: nil,
            description: nil,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should pass apiError and call onError closure once on setPicture") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.setPicture(
            with: Data(),
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
