//
//  ProfileRefreshServiceTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ProfileRefreshServiceTests: QuickSpec {
  override func spec() {
    describe("ProfileRefreshService") {
      var sut: ProfileRefreshService!
      var api: MockAuthProfileAPI!

      beforeEach {
        api = MockAuthProfileAPI()
        sut = ProfileRefreshService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.getAuthProfile on refreshProfile") {
        expect(api.getAuthProfileCallCount).to(equal(0))

        sut.refreshProfile(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getAuthProfileCallCount).to(equal(1))
      }

      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

        afterEach {
          api.reset()
        }

        it("should call onSuccess closure on refreshProfile") {
          var onSuccessCalled = false

          sut.refreshProfile(
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }

        it("should call onUserResult closure on setName") {
          var onUserResultCalled = false
          sut.onUserResult = { _ in
            onUserResultCalled = true
          }

          sut.refreshProfile(
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(onUserResultCalled).toEventually(beTrue())
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
          api.reset()
        }

        it("should call onError closure and pass error from api on refreshProfile") {
          var passedError: Error?
          var onErrorCalled = false

          sut.refreshProfile(
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCalled = true
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCalled).toEventually(beTrue())
        }
      }
    }
  }
}
