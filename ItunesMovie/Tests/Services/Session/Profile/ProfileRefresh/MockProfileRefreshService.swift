//
//  MockProfileRefreshService.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockProfileRefreshService: ProfileRefreshServiceProtocol {
  var onUserResult: SingleResult<User>?

  var errorToReturn: Error?

  private(set) var refreshProfileCallCount: Int = 0

  func reset() {
    errorToReturn = nil

    refreshProfileCallCount = 0
  }
}

// MARK: - Methods

extension MockProfileRefreshService {
  func refreshProfile(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    refreshProfileCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
