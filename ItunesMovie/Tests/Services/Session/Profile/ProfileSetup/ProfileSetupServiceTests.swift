//
//  ProfileSetupServiceTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ProfileSetupServiceTests: QuickSpec {
  override func spec() {
    describe("ProfileSetupService") {
      var sut: ProfileSetupService!
      var api: MockAuthProfileAPI!

      beforeEach {
        api = MockAuthProfileAPI()
        sut = ProfileSetupService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.putAuthProfile on setName") {
        expect(api.putAuthProfileCallCount).to(equal(0))

        sut.setName(
          "Test",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.putAuthProfileCallCount).to(equal(1))
      }

      it("should call api.postAuthProfileAvatar on setPicture") {
        expect(api.postAuthProfileAvatarCallCount).to(equal(0))

        sut.setPicture(
          with: Data(),
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.postAuthProfileAvatarCallCount).to(equal(1))
      }

      it("should create and pass the correct params to api on setName") {
        sut.setName(
          "Test",
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.putAuthProfileParams?.fullName).to(equal("Test"))
      }

      it("should call onSetupFinish closure on finishSetup") {
        var onSetupFinishCalled = false
        sut.onSetupFinish = {
          onSetupFinishCalled = true
        }

        sut.finishSetup()

        expect(onSetupFinishCalled).toEventually(beTrue())
      }

      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

        afterEach {
          api.reset()
        }

        it("should call onSuccess closure on setName") {
          var onSuccessCalled = false

          sut.setName(
            "Test",
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }

        it("should call onUserResult closure on setName") {
          var onUserResultCalled = false
          sut.onUserResult = { _ in
            onUserResultCalled = true
          }

          sut.setName(
            "",
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(onUserResultCalled).toEventually(beTrue())
        }

        it("should call onSuccess closure on setPicture") {
          var onSuccessCalled = false

          sut.setPicture(
            with: Data(),
            onSuccess: { onSuccessCalled = true },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }

        it("should call onAvatarResult closure on setPicture") {
          var onAvatarResultCalled = false
          sut.onAvatarResult = { _ in
            onAvatarResultCalled = true
          }

          sut.setPicture(
            with: Data(),
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(onAvatarResultCalled).toEventually(beTrue())
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
          api.reset()
        }

        it("should call onError closure and pass error from api on setName") {
          var passedError: Error?
          var onErrorCalled = false

          sut.setName(
            "Test",
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCalled = true
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCalled).toEventually(beTrue())
        }

        it("should call onError closure and pass error from api on setName") {
          var passedError: Error?
          var onErrorCalled = false

          sut.setPicture(
            with: Data(),
            onSuccess: DefaultClosure.voidResult(),
            onError: { error in
              passedError = error
              onErrorCalled = true
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCalled).toEventually(beTrue())
        }
      }
    }
  }
}
