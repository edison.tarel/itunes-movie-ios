//
//  MockProfileSetupService.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockProfileSetupService: ProfileSetupServiceProtocol {
  var onUserResult: SingleResult<User>?
  var onAvatarResult: SingleResult<Photo>?
  var onSetupFinish: VoidResult?

  private(set) var setNameCallCount: Int = 0
  private(set) var setPictureCallCount: Int = 0
  private(set) var finishSetupCallCount: Int = 0

  private let shouldSucceed: Bool
  private let errorToReturn: Error

  init(
    shouldSucceed: Bool = true,
    errorToReturn: Error = NSError()
  ) {
    self.shouldSucceed = shouldSucceed
    self.errorToReturn = errorToReturn
  }
}

// MARK: - Methods

extension MockProfileSetupService {
  func setName(
    _ name: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    setNameCallCount += 1

    if shouldSucceed {
      onSuccess()
    } else {
      onError(errorToReturn)
    }
  }

  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    setPictureCallCount += 1

    if shouldSucceed {
      onSuccess()
    } else {
      onError(errorToReturn)
    }
  }

  func finishSetup() {
    finishSetupCallCount += 1
  }
}
