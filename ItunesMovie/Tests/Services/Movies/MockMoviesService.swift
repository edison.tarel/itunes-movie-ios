//
//  MockMoviesService.swift
//  Tests
//
//  Created by Edison Tarel on 12/23/20.
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import NSObject_Rx

@testable import ItunesMovie

class MockMoviesService: MoviesServiceProtocol {
  var errorToReturn: Error?
  var moviesToReturn: SearchResult<Movie> = SearchResult(resultCount: 0, results: [])

  var movieToReturn = Movie()

  private(set) var fetchMoviesCallCount: Int = 0
  private(set) var fetchMoviesFromApi: Bool?
  private(set) var fetchMoviesParamter: MovieParameter?

  private(set) var fetchMovieCallCount: Int = 0
  private(set) var fetchMovieId: Int?
}

// MARK: - Methods: Fetch

extension MockMoviesService {
  func fetchMovies(
    fromApi: Bool,
    with parameter: MovieParameter,
    onSuccess: @escaping SingleResult<SearchResult<Movie>>,
    onError: @escaping ErrorResult
  ) {
    fetchMoviesCallCount += 1

    fetchMoviesParamter = parameter

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(moviesToReturn)
    }
  }

  func fetchMovie(
    with id: Int,
    onSuccess: @escaping SingleResult<Movie?>,
    onError: @escaping ErrorResult
  ) {
    fetchMovieCallCount += 1
    fetchMovieId = id

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(movieToReturn)
    }
  }
}
