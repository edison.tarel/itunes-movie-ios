//
//  MoviesServiceTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class MoviesServiceTests: QuickSpec {
  override func spec() {
    describe("MoviesService") {
      var sut: MoviesService!
      var api: MockMoviesAPI!

      beforeEach {
        api = MockMoviesAPI()
        sut = MoviesService(
          api: api,
          dataStorage: MockMovieStorage()
        )
      }

      afterEach {
        api = nil
        sut = nil
      }

      it("should pass correct params and call api.getMovies once on fetchMovies") {
        expect(api.getMoviesCallCount).to(equal(0))
        expect(api.getMoviesParamter).to(beNil())

        let movieParameter = MovieParameter(
          term: "star",
          country: "au",
          media: "movie"
        )

        sut.fetchMovies(
          with: movieParameter,
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getMoviesCallCount).to(equal(1))
        expect(api.getMoviesParamter?.term).to(equal("star"))
        expect(api.getMoviesParamter?.country).to(equal("au"))
        expect(api.getMoviesParamter?.media).to(equal("movie"))
      }

      context("when initialized with successful api") {
        beforeEach {
          api.errorToReturn = nil
        }

        it("should call onSuccess closure once on fetchMovies") {
          var onSuccessCallCount = 0

          let movieParameter = MovieParameter(
            term: "star",
            country: "au",
            media: "movie"
          )

          sut.fetchMovies(
            with: movieParameter,
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when initialized with failing api") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
        }

        it("should pass api error and call onError closure once on fetchMovies") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          let movieParameter = MovieParameter(
            term: "star",
            country: "au",
            media: "movie"
          )

          sut.fetchMovies(
            with: movieParameter,
            onSuccess: DefaultClosure.singleResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
