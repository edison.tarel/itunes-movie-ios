//
//  NotificationsServiceTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class NotificationsServiceTests: QuickSpec {
  override func spec() {
    describe("NotificationsService") {
      var sut: NotificationsService!
      var api: MockNotificationsAPI!

      beforeEach {
        api = MockNotificationsAPI()
        sut = NotificationsService(api: api)
      }

      afterEach {
        sut = nil
        api = nil
      }

      it("should call api.getNotificationsToday once on fetchNotificationsForToday") {
        expect(api.getNotificationsTodayCallCount).to(equal(0))

        sut.fetchNotificationsForToday(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getNotificationsTodayCallCount).to(equal(1))
      }

      it("should call api.getNotificationsThisWeek once on fetchNotificationsForThisWeek") {
        expect(api.getNotificationsThisWeekCallCount).to(equal(0))

        sut.fetchNotificationsForThisWeek(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getNotificationsThisWeekCallCount).to(equal(1))
      }

      it("should call api.getNotificationsUnread once on fetchNotificationsUnread") {
        expect(api.getNotificationsUnreadCallCount).to(equal(0))

        sut.fetchNotificationsUnread(
          onSuccess: DefaultClosure.singleResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(api.getNotificationsUnreadCallCount).to(equal(1))
      }

      context("when api is set to succeed") {
        beforeEach {
          api.errorToReturn = nil
        }

        it("should call onSuccess closure once on fetchNotificationsForToday") {
          var onSuccessCallCount = 0

          sut.fetchNotificationsForToday(
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on fetchNotificationsForThisWeek") {
          var onSuccessCallCount = 0

          sut.fetchNotificationsForThisWeek(
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on fetchNotificationsUnread") {
          var onSuccessCallCount = 0

          sut.fetchNotificationsUnread(
            onSuccess: { _ in onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when api is set to fail") {
        var apiError: Error!

        beforeEach {
          apiError = NSError(domain: #function, code: 1, userInfo: nil)
          api.errorToReturn = apiError
        }

        afterEach {
          apiError = nil
        }

        it("should call onError closure once and pass api error on fetchNotificationsForToday") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.fetchNotificationsForToday(
            onSuccess: DefaultClosure.singleResult(),
            onError: { error in
              passedError = error
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should call onError closure once and pass api error on fetchNotificationsForThisWeek") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.fetchNotificationsForThisWeek(
            onSuccess: DefaultClosure.singleResult(),
            onError: { error in
              passedError = error
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }

        it("should call onError closure once and pass api error on fetchNotificationsUnread") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.fetchNotificationsUnread(
            onSuccess: DefaultClosure.singleResult(),
            onError: { error in
              passedError = error
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(apiError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
