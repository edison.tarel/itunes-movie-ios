//
//  MockNotificationsService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockNotificationsService: NotificationsServiceProtocol {

  var errorToReturn: Error?

  private(set) var fetchNotificationsForTodayCallCount: Int = 0
  private(set) var fetchNotificationsForThisWeekCallCount: Int = 0
  private(set) var fetchNotificationsUnreadCallCount: Int = 0
}

// MARK: - Methods

extension MockNotificationsService {
  func fetchNotificationsForToday(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) {
    fetchNotificationsForTodayCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }
  }

  func fetchNotificationsForThisWeek(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) {
    fetchNotificationsForThisWeekCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }
  }

  func fetchNotificationsUnread(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) {
    fetchNotificationsUnreadCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }
  }
}
