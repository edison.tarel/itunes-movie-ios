//
//  URL+Utils.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension URL {
  static var dummy: URL {
    URL(string: "https://appetiser.com.au")!
  }
}
