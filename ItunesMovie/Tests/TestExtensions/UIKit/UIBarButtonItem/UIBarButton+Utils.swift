//
//  UIBarButton+Utils.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIBarButtonItem {
  func tap() {
    _ = target?.perform(action, with: nil)
  }
}
