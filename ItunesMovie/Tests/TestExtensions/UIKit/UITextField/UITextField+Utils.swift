//
//  UITextField+Utils.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

extension UITextField {
  func setText(_ text: String, sendingAction action: UIControl.Event = .editingChanged) {
    self.text = text
    sendActions(for: action)
  }

  func tapReturnKey() {
    sendActions(for: .editingDidEndOnExit)
  }
}
