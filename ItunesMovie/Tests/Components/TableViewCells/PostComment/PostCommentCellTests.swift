//
//  PostCommentCellTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PostCommentCellTests: QuickSpec {
  override func spec() {
    describe("PostCommentCell") {
      var sut: PostCommentCell!
      var viewModel: MockPostCommentCellViewModel!

      var tableView: UITableView!

      beforeEach {
        viewModel = MockPostCommentCellViewModel()
        viewModel.authorName = "Author Name"
        viewModel.datePostedText = "Date Posted"
        viewModel.commentText = "Comment Text"

        tableView = UITableView()
        sut = tableView.cellInstance(for: R.nib.postCommentCell)
        sut.viewModel = viewModel
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      it("should have non-nil outlets") {
        expect(sut.authorImage).toNot(beNil())
        expect(sut.authorNameLabel).toNot(beNil())
        expect(sut.commentBodyLabel).toNot(beNil())
        expect(sut.datePostedLabel).toNot(beNil())
        expect(sut.optionButton).toNot(beNil())
        expect(sut.replyButtonContainer).toNot(beNil())
        expect(sut.mainStackViewLeadingConstraint).toNot(beNil())
        expect(sut.authorImageWidthConstraint).toNot(beNil())
      }

      it("should set authorNameLabel.text to correct value") {
        expect(sut.authorNameLabel.text).to(equal("Author Name"))
      }

      it("should set datePostedLabel.text to correct value") {
        expect(sut.datePostedLabel.text).to(equal("Date Posted"))
      }

      it("should set commentBodyLabel.text to correct value") {
        expect(sut.commentBodyLabel.text).to(equal("Comment Text"))
      }
    }
  }
}
