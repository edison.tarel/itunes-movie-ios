//
//  MockPostCommentCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import ItunesMovie

class MockPostCommentCellViewModel: PostCommentCellViewModelProtocol {
  var authorAvatarURL: URL?
  var authorName: String = ""
  var datePostedText: String = ""
  var commentText: String  = ""
  var level: Comment.Level  = .level1
}
