//
//  PostCommentCellViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick
import SwiftDate

@testable import ItunesMovie

class PostCommentCellViewModelTests: QuickSpec {
  override func spec() {
    describe("PostCommentCellViewModel") {
      var sut: PostCommentCellViewModel!
      var author: User!
      var comment: Comment!

      beforeEach {
        author = User(
          fullName: "Author Name",
          avatarPermanentThumbUrl: .dummy
        )
        comment = Comment(
          testAuthor: author,
          testBody: "Body",
          testLevel: .level2,
          testCreatedAt: Date() - 4.minutes
        )
        sut = PostCommentCellViewModel(
          comment: comment
        )
      }

      afterEach {
        sut = nil
        comment = nil
      }

      it("should return authorAvatarURL equal to author.avatarThumbURL") {
        expect(sut.authorAvatarURL?.absoluteString).to(equal(URL.dummy.absoluteString))
      }

      it("should return authorName equal to author.fullName") {
        expect(sut.authorName).to(equal("Author Name"))
      }

      it("should return datePostedText equal to relative comment.createdAt") {
        expect(sut.datePostedText).to(equal("4 minutes ago"))
      }

      it("should return commentText equal to comment.body") {
        expect(sut.commentText).to(equal("Body"))
      }

      it("should return level equal to comment.level") {
        expect(sut.level).to(equal(.level2))
      }
    }
  }
}
