//
//  MockNotificationCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockNotificationCellViewModel: NotificationCellViewModelProtocol {
  var mainImageURL: URL?
  var actorText: String = ""
  var actionText: String = ""
  var timeAgoText: String = ""
  var accessoryImageURL: URL?
}
