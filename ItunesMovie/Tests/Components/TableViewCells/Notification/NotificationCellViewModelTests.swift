//
//  NotificationCellViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick
import SwiftDate

@testable import ItunesMovie

class NotificationCellViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("NotificationCellViewModel") {
      var actor: User!
      var notification: AppNotification!
      var sut: NotificationCellViewModel!

      beforeEach {
        actor = User()
        notification = AppNotification(testActor: actor)
        sut = NotificationCellViewModel(notification: notification)
      }

      afterEach {
        actor = nil
        notification = nil
        sut = nil
      }

      it("should return mainImageURL equal to actor.avatarPermanentThumbUrl") {
        expect(sut.mainImageURL).to(equal(actor.avatarPermanentThumbUrl))
      }

      context("when type and message is for follow") {
        beforeEach {
          notification = AppNotification(
            testType: .follow,
            testMessage: "Jiin K. started following you"
          )
          sut = NotificationCellViewModel(notification: notification)
        }

        it("should return the correct actorText") {
          expect(sut.actorText).to(equal("Jiin K."))
        }

        it("should return the correct actionText") {
          expect(sut.actionText).to(equal("started following you"))
        }

        // TODO: Replace with notification.image check or something
        it("should return nil accessoryImageURL ") {
          expect(sut.accessoryImageURL).to(equal(actor.avatarPermanentThumbUrl))
        }
      }

      context("when type and message is for comment") {
        beforeEach {
          notification = AppNotification(
            testType: .comment,
            testMessage: "Jiin K. commented on your post"
          )
          sut = NotificationCellViewModel(notification: notification)
        }

        it("should return the correct actorText") {
          expect(sut.actorText).to(equal("Jiin K."))
        }

        it("should return the correct actionText") {
          expect(sut.actionText).to(equal("commented on your post"))
        }

        // TODO: Replace with notification.image check or something
        it("should return accessoryImageURL equal to actor.avatarPermanentThumbUrl") {
          expect(sut.accessoryImageURL).to(equal(actor.avatarPermanentThumbUrl))
        }
      }

      context("when type and message is for like") {
        beforeEach {
          notification = AppNotification(
            testType: .like,
            testMessage: "Jiin K. liked your post"
          )
          sut = NotificationCellViewModel(notification: notification)
        }

        it("should return the correct actorText") {
          expect(sut.actorText).to(equal("Jiin K."))
        }

        it("should return the correct actionText") {
          expect(sut.actionText).to(equal("liked your post"))
        }

        // TODO: Replace with notification.image check or something
        it("should return nil accessoryImageURL ") {
          expect(sut.accessoryImageURL).to(beNil())
        }
      }

      context("when type and message format does not match") {
        beforeEach {
          notification = AppNotification(
            testType: .like,
            testMessage: "Jiin K. commented in your post"
          )
          sut = NotificationCellViewModel(notification: notification)
        }

        it("should return the empty actorText") {
          expect(sut.actorText).to(equal(""))
        }

        it("should return the empty actionText") {
          expect(sut.actionText).to(equal(""))
        }
      }

      describe("timeAgoText") {
        it("should be correct 1") {
          notification = AppNotification(testCreatedAt: Date() - 1.seconds)
          sut = NotificationCellViewModel(notification: notification)
          expect(sut.timeAgoText).to(equal("now"))
        }

        it("should be correct 2") {
          notification = AppNotification(testCreatedAt: Date() - 2.minutes)
          sut = NotificationCellViewModel(notification: notification)
          expect(sut.timeAgoText).to(equal("2m"))
        }

        it("should be correct 3") {
          notification = AppNotification(testCreatedAt: Date() - 3.hours)
          sut = NotificationCellViewModel(notification: notification)
          expect(sut.timeAgoText).to(equal("3h"))
        }

        it("should be correct 4") {
          notification = AppNotification(testCreatedAt: Date() - 4.days)
          sut = NotificationCellViewModel(notification: notification)
          expect(sut.timeAgoText).to(equal("4d"))
        }
      }
    }
  }
}
