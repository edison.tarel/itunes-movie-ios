//
//  NotificationCellTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import AlamofireImage
import Nimble
import Quick

@testable import ItunesMovie

class NotificationCellTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("NotificationCell") {
      var sut: NotificationCell!
      var viewModel: MockNotificationCellViewModel!
      var tableView: UITableView!

      beforeEach {
        viewModel = MockNotificationCellViewModel()
        viewModel.actorText = "A"
        viewModel.actionText = "B"
        viewModel.timeAgoText = "C"

        tableView = UITableView()
        sut = tableView.cellInstance(for: R.nib.notificationCell)
        sut.viewModel = viewModel
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      it("should have non-nil outlets") {
        expect(sut.mainImageView).toNot(beNil())
        expect(sut.messageLabel).toNot(beNil())
        expect(sut.accessoryImageView).toNot(beNil())
      }

      it("should call mainImageView.setImageWithURL with viewModel.mainImageURL") {
        // TODO: Add tests for mainImageView
      }

      it("should set messageLabel.text to correct value") {
        expect(sut.messageLabel.attributedText?.string).to(equal("A B C"))
      }

      it("should call accessoryImageView.setImageWithURL with viewModel.accessoryImageURL") {
        // TODO: Add tests for accessoryImageView
      }
    }
  }
}
