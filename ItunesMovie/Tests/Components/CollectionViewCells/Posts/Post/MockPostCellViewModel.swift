//
//  MockPostCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

@testable import ItunesMovie

class MockPostCellViewModel: PostCellViewModelProtocol {
  var onRefresh: VoidResult?
  
  var id: Int = 0
  
  var itemVM: SimplePostItemViewModelProtocol = MockSimplePostItemViewModel()
  var authorVM: PostAuthorViewModelProtocol = MockPostAuthorViewModel()
}
