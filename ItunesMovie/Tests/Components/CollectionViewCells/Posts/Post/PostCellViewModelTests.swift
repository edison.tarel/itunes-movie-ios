//
//  PostCellViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class PostCellViewModelTests: QuickSpec {
  override func spec() {
    describe("PostCellViewModel") {
      var sut: PostCellViewModel!
      var post: Post!
      var service: MockPostsService!

      beforeEach {
        post = Post(testId: 1)
        
        service = MockPostsService()
        service.postToReturn = post
        
        sut = PostCellViewModel(
          post: post,
          service: service
        )
      }

      afterEach {
        sut = nil
        post = nil
      }
      
      it("should return the same id as itemVM and authorVM") {
        expect(sut.id).to(equal(1))
        expect(sut.id).to(equal(sut.itemVM.id))
        expect(sut.id).to(equal(sut.authorVM.id))
      }
      
      it("should have called service.bindPost once") {
        expect(service.bindPostCallCount).to(equal(1))
        expect(service.bindPostOnRefresh).toNot(beNil())
      }
      
      it("should update sut.post to service.postToReturn when service triggers post refresh") {
        service.postToReturn = Post(testId: 2)
        expect(sut.id).to(equal(1))
        
        service.bindPostOnRefresh?()
        
        expect(sut.id).to(equal(2))
      }
      
      it("should call onRefresh once when service triggers post refresh") {
        var onRefreshCallCount = 0
        sut.onRefresh = { onRefreshCallCount += 1 }
        
        service.bindPostOnRefresh?()
        
        expect(onRefreshCallCount).to(equal(1))
      }
    }
  }
}
