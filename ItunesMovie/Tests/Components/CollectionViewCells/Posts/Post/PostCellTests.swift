//
//  PostCellTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PostCellTests: QuickSpec {
  override func spec() {
    describe("PostCell") {
      var sut: PostCell!
      var viewModel: MockPostCellViewModel!

      var collectionView: UICollectionView!

      beforeEach {
        viewModel = MockPostCellViewModel()
        viewModel.authorVM = MockPostAuthorViewModel()
        viewModel.itemVM = MockSimplePostItemViewModel()

        collectionView = UICollectionView(
          frame: .zero,
          collectionViewLayout: UICollectionViewFlowLayout()
        )

        sut = collectionView.cellInstance(for: R.nib.postCell)
        sut.viewModel = viewModel
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      it("should have non-nil outlets") {
        expect(sut.postAuthorView).toNot(beNil())
        expect(sut.postView).toNot(beNil())
        expect(sut.mainStackView).toNot(beNil())
      }

      it("should set postAuthorView.viewModel to authorVM") {
        expect(sut.postAuthorView.viewModel).to(be(viewModel.authorVM))
      }

      it("should set postView.viewModel to itemVM") {
        expect(sut.postView.viewModel).to(be(viewModel.itemVM))
      }
    }
  }
}
