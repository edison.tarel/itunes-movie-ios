//
//  PostAuthorViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import SwiftDate

import Quick
import Nimble

@testable import ItunesMovie

class PostAuthorViewModelTests: QuickSpec {
  override func spec() {
    describe("PostAuthorViewModel") {
      var sut: PostAuthorViewModel!
      var service: MockPostsService!
      var post: Post!

      beforeEach {
        post = Post(testId: 0)
        
        service = MockPostsService()
        service.postToReturn = post
        
        sut = PostAuthorViewModel(
          post: post,
          service: service
        )
      }

      afterEach {
        sut = nil
        post = nil
      }
      
      it("should have called service.bindPost once") {
        expect(service.bindPostCallCount).to(equal(1))
        expect(service.bindPostOnRefresh).toNot(beNil())
      }
      
      it("should update sut.post to service.postToReturn when service triggers post refresh") {
        service.postToReturn = Post(testId: 1)
        expect(sut.id).to(equal(0))
        
        service.bindPostOnRefresh?()
        
        expect(sut.id).to(equal(1))
      }
      
      it("should call onRefresh once when service triggers post refresh") {
        var onRefreshCallCount = 0
        sut.onRefresh = { onRefreshCallCount += 1 }
        
        service.bindPostOnRefresh?()
        
        expect(onRefreshCallCount).to(equal(1))
      }

      it("should have the correct values based on post") {
        let author = User(
          fullName: "Dummy Author",
          avatar: Photo()
        )
        post = Post(
          testAuthor: author,
          testCreatedAt: Date() - 7.minutes
        )
        sut = PostAuthorViewModel(post: post)

        expect(sut.id).to(equal(post.id))
        expect(sut.avatarURL).to(equal(author.avatar?.thumbUrl))
        expect(sut.datePostedText).to(equal("7 minutes ago"))
        expect(sut.nameText).to(equal(author.fullName))
      }
    }
  }
}
