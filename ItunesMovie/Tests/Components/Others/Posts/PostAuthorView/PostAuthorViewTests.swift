//
//  PostAuthorViewTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PostAuthorViewTests: QuickSpec {
  override func spec() {
    describe("PostAuthorView") {
      var sut: PostAuthorView!
      var viewModel: MockPostAuthorViewModel!

      beforeEach {
        viewModel = MockPostAuthorViewModel()
        sut = PostAuthorView()
        sut.viewModel = viewModel
      }

      context("when view is loaded") {
        it("should have non-nil outlets") {
          expect(sut.contentView).toNot(beNil())
          expect(sut.imageView).toNot(beNil())
          expect(sut.nameLabel).toNot(beNil())
          expect(sut.datePostedLabel).toNot(beNil())
          expect(sut.optionButton).toNot(beNil())
        }

        it("should have the correct field values") {
          // TODO: Find way to test if image was loaded from url
          expect(sut.nameLabel.text).to(equal(viewModel.nameText))
          expect(sut.datePostedLabel.text).to(equal(viewModel.datePostedText))
        }

        it("should call onOption closure when optionButton is tapped") {
          var onOptionTapCallCount = 0
          sut.onOptionTap = {
            onOptionTapCallCount += 1
          }

          sut.optionButton.tap()
          expect(onOptionTapCallCount).to(equal(1))
        }

        it("should refresh view when viewModel.onRefresh is called") {
          expect(sut.nameLabel.text).to(beEmpty())
          expect(sut.datePostedLabel.text).to(beEmpty())

          viewModel.nameText = "Updated"
          viewModel.datePostedText = "Updated"
          viewModel.onRefresh?()

          expect(sut.nameLabel.text).to(equal("Updated"))
          expect(sut.datePostedLabel.text).to(equal("Updated"))
        }
      }
    }
  }
}
