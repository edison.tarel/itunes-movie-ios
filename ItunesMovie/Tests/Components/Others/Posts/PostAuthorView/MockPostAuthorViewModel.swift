//
//  MockPostAuthorViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockPostAuthorViewModel: PostAuthorViewModelProtocol {
  var onRefresh: VoidResult?
  
  var id: Int = 0
  var avatarURL: URL?
  var datePostedText: String = ""
  var nameText: String = ""
}
