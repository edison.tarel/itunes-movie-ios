//
//  SimplePostItemViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class SimplePostItemViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("SimplePostItemViewModel") {
      var sut: SimplePostItemViewModel!
      var service: MockPostsService!
      var post: Post!

      beforeEach {
        post = Post(testId: 0)
        
        service = MockPostsService()
        service.postToReturn = post
        
        sut = SimplePostItemViewModel(
          post: post,
          service: service
        )
      }

      afterEach {
        sut = nil
        post = nil
      }
      
      it("should have called service.bindPost once") {
        expect(service.bindPostCallCount).to(equal(1))
        expect(service.bindPostOnRefresh).toNot(beNil())
      }
      
      it("should update sut.post to service.postToReturn when service triggers post refresh") {
        service.postToReturn = Post(testId: 1)
        expect(sut.id).to(equal(0))
        
        service.bindPostOnRefresh?()
        
        expect(sut.id).to(equal(1))
      }
      
      it("should call onRefresh once when service triggers post refresh") {
        var onRefreshCallCount = 0
        sut.onRefresh = { onRefreshCallCount += 1 }
        
        service.bindPostOnRefresh?()
        
        expect(onRefreshCallCount).to(equal(1))
      }

      it("should have the correct values based on post") {
        expect(sut.id).to(equal(post.id))
        expect(sut.commentsCountText).to(equal("0 comments"))
        expect(sut.description).to(equal(post.body))
        expect(sut.photoURL).to(equal(post.photo.url))
      }

      context("when initialized with 1 comment") {
        beforeEach {
          post = Post(testCommentsCount: 1)
          sut = SimplePostItemViewModel(post: post)
        }

        it("should have the correct comment count text") {
          expect(sut.commentsCountText).to(equal("1 comment"))
        }
      }

      context("when initialized with many comments") {
        beforeEach {
          post = Post(testCommentsCount: 6)
          sut = SimplePostItemViewModel(post: post)
        }

        it("should have the correct comment count text") {
          expect(sut.commentsCountText).to(equal("6 comments"))
        }
      }

      context("when initialized as favorite") {
        beforeEach {
          post = Post(testIsFavorite: true)
          sut = SimplePostItemViewModel(post: post)
        }

        it("should have the correct icon displayed") {
          expect(sut.likeImage).to(equal(R.image.heartFilled()!))
        }
      }
    }
  }
}
