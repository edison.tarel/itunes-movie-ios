//
//  SimplePostItemViewTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Nimble
import Quick

@testable import ItunesMovie

class SimplePostItemViewTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("SimplePostItemView") {
      var sut: SimplePostItemView!
      var viewModel: MockSimplePostItemViewModel!

      beforeEach {
        viewModel = MockSimplePostItemViewModel()
        sut = SimplePostItemView()
        sut.viewModel = viewModel
      }

      afterEach {
        sut = nil
        viewModel = nil
      }

      context("when view is loaded") {
        it("should have non-nil outlets") {
          expect(sut.contentView).toNot(beNil())
          expect(sut.commentsCountLabel).toNot(beNil())
          expect(sut.descriptionLabel).toNot(beNil())
          expect(sut.descriptionMoreLabel).toNot(beNil())
          expect(sut.mainImageView).toNot(beNil())
          expect(sut.likeButton).toNot(beNil())
          expect(sut.likeCountLabel).toNot(beNil())
        }

        it("should call onLikeTap callback on tap of like button") {
          var likeTapCallCount: Int = 0
          sut.onLikeTap = {
            likeTapCallCount += 1
          }

          sut.likeButton.tap()

          expect(likeTapCallCount).to(equal(1))
        }

        it("should refresh view when viewModel.onRefresh is called") {
          expect(sut.descriptionLabel.text).to(beEmpty())
          expect(sut.commentsCountLabel.text).to(beEmpty())
          expect(sut.likeCountLabel.text).to(equal("0"))

          viewModel.description = "Updated"
          viewModel.commentsCountText = "Updated"
          viewModel.likeCount = 1
          viewModel.onRefresh?()

          expect(sut.descriptionLabel.text).to(equal("Updated"))
          expect(sut.commentsCountLabel.text).to(equal("Updated"))
          expect(sut.likeCountLabel.text).to(equal("1"))
        }
      }
    }
  }
}
