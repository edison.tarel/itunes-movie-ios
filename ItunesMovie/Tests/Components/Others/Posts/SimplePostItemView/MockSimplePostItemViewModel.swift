//
//  MockSimplePostItemViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

@testable import ItunesMovie

class MockSimplePostItemViewModel: SimplePostItemViewModelProtocol {
  var onRefresh: VoidResult?
  
  var id: Int = 0
  var commentsCountText: String = ""
  var description: String = ""
  var likeCount: Int = 0
  var likeImage: UIImage = .init()
  var likeText: String = ""
  var photoURL: URL?
}
