//
//  MockPostsAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockPostsAPI: PostsAPI {
  var errorToReturn: Error?
  var progressToReturn: Progress?
  var createdPostToReturn: Post = .init()

  private(set) var getPostsCallCount: Int = 0
  private(set) var getPostsUserId: Int?
  private(set) var getPostsPage: APIPage?
  private(set) var getPostsInclude: [PostsIncludes]?

  private(set) var likePostCallCount: Int = 0
  private(set) var likePostId: Int?

  private(set) var unlikePostCallCount: Int = 0
  private(set) var unlikePostId: Int?

  private(set) var createPostCallCount: Int = 0
  private(set) var createPostImageData: Data?
  private(set) var createPostDescription: String?

  private(set) var getPostsCommentsCallCount: Int = 0
  private(set) var getPostsCommentsPostId: Int?
  private(set) var getPostsCommentsInclude: [PostsCommentIncludes]?

  private(set) var postPostsCommentsCallCount: Int = 0
  private(set) var postPostsCommentsPostId: Int?
  private(set) var postPostsCommentsBody: String?

  private(set) var postCommentsCallCount: Int = 0
  private(set) var postCommentsId: Int?
  private(set) var postCommentsBody: String?
}

// MARK: - Methods

extension MockPostsAPI {
  @discardableResult
  func getPosts(
    userId: Int?,
    page: APIPage,
    include: [PostsIncludes],
    onSuccess: @escaping SingleResult<[Post]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getPostsCallCount += 1
    getPostsUserId = userId
    getPostsPage = page
    getPostsInclude = include

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }

    return DummyRequest()
  }

  @discardableResult
  func postPostsFavorite(
    with id: Int,
    onSuccess: VoidResult,
    onError: ErrorResult
  ) -> RequestProtocol {
    likePostCallCount += 1
    likePostId = id

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }

  @discardableResult
  func postPostsUnfavorite(
    with id: Int,
    onSuccess: VoidResult,
    onError: ErrorResult
  ) -> RequestProtocol {
    unlikePostCallCount += 1
    unlikePostId = id

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }

  func postPosts(
    imageData: Data,
    description: String,
    onProgress: SingleResult<Progress>,
    onSuccess: @escaping SingleResult<Post>,
    onError: @escaping ErrorResult
  ) {
    createPostCallCount += 1
    createPostImageData = imageData
    createPostDescription = description

    if let e = errorToReturn {
      onError(e)
    } else if let p = progressToReturn {
      onProgress(p)
    } else  {
      onSuccess(createdPostToReturn)
    }
  }

  @discardableResult
  func getPostsComments(
    postId: Int,
    include: [PostsCommentIncludes],
    onSuccess: @escaping SingleResult<[Comment]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getPostsCommentsCallCount += 1
    getPostsCommentsPostId = postId
    getPostsCommentsInclude = include

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }

    return DummyRequest()
  }

  @discardableResult
  func postPostsComments(
    postId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postPostsCommentsCallCount += 1
    postPostsCommentsPostId = postId
    postPostsCommentsBody = body

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postCommentsResponses(
    commentId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postCommentsId = commentId
    postCommentsBody = body
    postCommentsCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }
}
