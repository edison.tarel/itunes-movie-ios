//
//  GetPostsTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class GetPostsTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("GetPosts") {
      var apiResponse: APIResponse!
      var data: [Post]!

      context("when decoding status 200 response") {
        beforeEach {
          apiResponse = self.decodeResponseValue(statusCode: .ok)
        }

        it("should have non-nil decoded response") {
          expect(apiResponse).toNot(beNil())
        }

        it("should have 200 status code") {
          data = apiResponse.decodedValue()

          expect(data).toNot(beNil())
        }
      }

      afterEach {
        apiResponse = nil
        data = nil
      }
    }
  }
}
