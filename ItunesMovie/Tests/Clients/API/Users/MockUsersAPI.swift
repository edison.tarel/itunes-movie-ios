//
//  MockUsersAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockUsersAPI: UsersAPI {
  var errorToReturn: Error?

  private(set) var deleteUsersCallCount: Int = 0
  private(set) var deleteUsersId: Int?
}

// MARK: - Methods

extension MockUsersAPI {
  @discardableResult
  func deleteUsers(
    withId id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    deleteUsersCallCount += 1
    deleteUsersId = id

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
    
    return DummyRequest()
  }
}
