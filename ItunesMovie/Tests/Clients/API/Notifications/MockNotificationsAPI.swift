//
//  MockNotificationsAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockNotificationsAPI: NotificationsAPI {

  var errorToReturn: Error?

  private(set) var getNotificationsTodayCallCount = 0
  private(set) var getNotificationsThisWeekCallCount = 0
  private(set) var getNotificationsUnreadCallCount = 0

  func reset() {
    errorToReturn = nil
  }
}

extension MockNotificationsAPI {
  @discardableResult
  func getNotificationsToday(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getNotificationsTodayCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }

    return DummyRequest()
  }

  @discardableResult
  func getNotificationsThisWeek(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getNotificationsThisWeekCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }

    return DummyRequest()
  }

  @discardableResult
  func getNotificationsUnread(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getNotificationsUnreadCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }

    return DummyRequest()
  }
}
