//
//  MockAuthRegisterAPI.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockAuthRegisterAPI: AuthRegisterAPI {
  var errorToReturn: Error?

  private(set) var postAuthRegisterEmail: String?
  private(set) var postAuthRegisterPhoneNumber: String?
  private(set) var postAuthRegisterPassword: String?

  private(set) var postAuthRegisterEmailCallCount: Int = 0
  private(set) var postAuthRegisterPhoneNumberCallCount: Int = 0
}

extension MockAuthRegisterAPI {
  @discardableResult
  func postAuthRegisterEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthRegisterEmail = email
    postAuthRegisterPassword = password
    postAuthRegisterEmailCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthRegisterPhoneNumber(
    _ phoneNumber: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthRegisterPhoneNumber = phoneNumber
    postAuthRegisterPassword = password
    postAuthRegisterPhoneNumberCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }
}
