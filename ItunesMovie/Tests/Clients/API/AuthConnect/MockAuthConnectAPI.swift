//
//  MockAuthConnectAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockAuthConnectAPI: AuthConnectAPI {
  var errorToReturn: Error?

  private(set) var postEphemeralKeyCallCount: Int = 0
  private(set) var getAuthConnectExternalAccountsCallCount: Int = 0

  private(set) var putAuthConnectExternalAccountsCallCount: Int = 0
  private(set) var putAuthConnectExternalAccountsAccountId: String?
  private(set) var putAuthConnectExternalAccountsParams: JSONDictionary?

  private(set) var postAuthConnectAccountCallCount: Int = 0
  private(set) var postAuthConnectAccountParams: JSONDictionary?

  private(set) var getAuthConnectAccountCallCount: Int = 0
  private(set) var deleteAuthConnectAccountCallCount: Int = 0

  private(set) var postAuthConnectFileUploadCallCount: Int = 0
  private(set) var postAuthConnectFileUploadImageData: Data?
  private(set) var postAuthConnectFileUploadPurpose: String?

  private(set) var postAuthConnectAccountExternalAccountCallCount: Int = 0
  private(set) var postAuthConnectAccountExternalAccountParams: JSONDictionary?
}

// MARK: - Methods

extension MockAuthConnectAPI {
  @discardableResult
  func postEphemeralKey(
    onSuccess: @escaping SingleResult<EphemeralKey>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postEphemeralKeyCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func getAuthConnectExternalAccounts(
    onSuccess: @escaping SingleResult<[BankDetails]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getAuthConnectExternalAccountsCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }

    return DummyRequest()
  }

  @discardableResult
  func putAuthConnectExternalAccounts(
    accountId: String,
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    putAuthConnectExternalAccountsCallCount += 1
    putAuthConnectExternalAccountsAccountId = accountId
    putAuthConnectExternalAccountsParams = params

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthConnectAccount(
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthConnectAccountCallCount += 1
    postAuthConnectAccountParams = params

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }

  @discardableResult
  func getAuthConnectAccount(
    onSuccess: @escaping SingleResult<StripeConnect>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getAuthConnectAccountCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func deleteAuthConnectAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    deleteAuthConnectAccountCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }

  func postAuthConnectFileUpload(
    imageData: Data,
    purpose: String?,
    onProgress: SingleResult<Progress>?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    postAuthConnectFileUploadCallCount += 1
    postAuthConnectFileUploadImageData = imageData
    postAuthConnectFileUploadPurpose = purpose

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess("")
    }
  }

  @discardableResult
  func postAuthConnectAccountExternalAccount(
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthConnectAccountExternalAccountCallCount += 1
    postAuthConnectAccountExternalAccountParams = params

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }

    return DummyRequest()
  }
}
