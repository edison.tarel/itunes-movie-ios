//
//  MockReportAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockReportAPI: ReportAPI {
  var errorToReturn: Error?

  private(set) var getReportCategoriesCallCount: Int = 0

  private(set) var postReportValue: Report?
  private(set) var postReportUserId: Int?
  private(set) var postReportCallCount: Int = 0
}

extension MockReportAPI {
  @discardableResult
  func getReportCategories(
    onSuccess: @escaping SingleResult<[ReportCategory]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getReportCategoriesCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess([])
    }

    return DummyRequest()
  }

  func postReport(
    with userId: Int,
    report: Report,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    postReportValue = report
    postReportUserId = userId
    postReportCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
