//
//  StripeExternalAccountParams.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension StripeExternalAccountParams {
  init(
    testAccountNumber: String? = nil,
    testObject: String? = nil,
    testCountry: String? = nil,
    testCurrency: String? = nil,
    testAccountHolderName: String? = nil,
    testAccountHolderType: String? = nil,
    testRoutingNumber: String? = nil
  ) {
    self.init(
      accountNumber: testAccountNumber,
      object: testObject,
      country: testCountry,
      currency: testCurrency,
      accountHolderName: testAccountHolderName,
      accountHolderType: testAccountHolderType,
      routingNumber: testRoutingNumber
    )
  }
}
