//
//  MockAuthVerificationAPI.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockAuthVerificationAPI: AuthVerificationAPI {
  var errorToReturn: Error?

  private(set) var postAuthVerificationVerifyToken: String?
  private(set) var postAuthVerificationVerifyType: VerificationType?
  private(set) var postAuthVerificationVerifyCallCount: Int = 0

  private(set) var postAuthVerificationResendType: VerificationType?
  private(set) var postAuthVerificationResendCallCount: Int = 0

  private(set) var postAuthVerificationTokenPassword: String?
  private(set) var postAuthVerificationTokenCallCount: Int = 0

  func reset() {
    errorToReturn = nil

    postAuthVerificationVerifyToken = nil
    postAuthVerificationVerifyType = nil
    postAuthVerificationVerifyCallCount = 0

    postAuthVerificationResendType = nil
    postAuthVerificationResendCallCount = 0
  }
}

extension MockAuthVerificationAPI {
  @discardableResult
  func postAuthVerificationVerify(
    token: String,
    type: VerificationType,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthVerificationVerifyToken = token
    postAuthVerificationVerifyType = type
    postAuthVerificationVerifyCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthVerificationResend(
    type: VerificationType,
    onSuccess: @escaping SingleResult<User>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthVerificationResendType = type
    postAuthVerificationResendCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }

  @discardableResult
  func postAuthVerificationToken(
    password: String,
    onSuccess: @escaping SingleResult<VerificationToken>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthVerificationTokenPassword = password
    postAuthVerificationTokenCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }
}
