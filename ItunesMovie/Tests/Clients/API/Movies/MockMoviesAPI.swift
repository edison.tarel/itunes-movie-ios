//
//  MockMoviesAPI.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockMoviesAPI: MoviesAPI {
  var errorToReturn: Error?

  private(set) var getMoviesCallCount: Int = 0
  private(set) var getMoviesParamter: MovieParameter?
}

// MARK: - Methods

extension MockMoviesAPI {
  @discardableResult
  func getMovies(
    with parameter: MovieParameter,
    onSuccess: @escaping SingleResult<SearchResult<Movie>>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getMoviesCallCount += 1
    getMoviesParamter = parameter

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(SearchResult(resultCount: 0, results: []))
    }

    return DummyRequest()
  }
}
