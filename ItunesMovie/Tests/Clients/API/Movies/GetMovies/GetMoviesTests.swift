//
//  GetMoviesTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class GetMoviesTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("GetMovies") {
      var apiResponse: SearchResult<Movie>!
      var data: [Movie]!

      context("when decoding status 200 response") {
        beforeEach {
          apiResponse = self.decodeResponseValue(statusCode: .ok)
        }

        it("should have non-nil decoded response") {
          expect(apiResponse).toNot(beNil())
        }

        it("should have 200 status code") {
          data = apiResponse.results

          expect(data).toNot(beNil())
        }
      }

      afterEach {
        apiResponse = nil
        data = nil
      }
    }
  }
}
