//
//  MockAuthLoginAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockAuthLoginAPI: AuthLoginAPI {
  var errorToReturn: Error?

  private(set) var postAuthLoginUsername: String?
  private(set) var postAuthLoginPassword: String?
  private(set) var postAuthLoginCallCount: Int = 0
}

extension MockAuthLoginAPI {
  @discardableResult
  func postAuthLogin(
    with username: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    postAuthLoginUsername = username
    postAuthLoginPassword = password
    postAuthLoginCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(.init())
    }

    return DummyRequest()
  }
}
