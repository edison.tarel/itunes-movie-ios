//
//  MockMovieStorage.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockMovieStorage: MovieStorageProtocol {
  var errorToReturn: Error?

  var movieToReturn: Movie?
  var moviesToReturn: [Movie]?

  private(set) var saveMoviesCallCount: Int = 0
  private(set) var saveMoviesMovies: [Movie]?

  private(set) var getMovieCallCount: Int = 0
  private(set) var getMovieId: Int?

  private(set) var getSavedMoviesCallCount: Int = 0

  private(set) var deleteAllMoviesCallCount: Int = 0
}

// MARK: - Methods

extension MockMovieStorage {
  func saveMovies(_ movies: [Movie]) {
    saveMoviesCallCount += 1
    saveMoviesMovies = movies
  }

  func getMovie(
    with id: Int,
    onSuccess: @escaping SingleResult<Movie?>,
    onError: @escaping ErrorResult
  ) {
    getMovieCallCount += 1
    getMovieId = id

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(movieToReturn)
    }
  }

  func getSavedMovies(
    onSuccess: @escaping SingleResult<[Movie]?>,
    onError: @escaping ErrorResult
  ) {
    getSavedMoviesCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess(moviesToReturn)
    }
  }

  func deleteAllMovies() {
    deleteAllMoviesCallCount += 1
  }
}
