//
//  DummyStripeConnect.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension StripeConnect {
  init(
    testIndividual: IndividualAccount = .init(),
    testExternalAccount: String? = nil,
    testExternalAccounts: ExternalAccounts? = nil
  ) {
    self.init(
      individual: testIndividual,
      externalAccount: testExternalAccount,
      externalAccounts: testExternalAccounts
    )
  }
}

// MARK: - ExternalAccounts

extension StripeConnect.ExternalAccounts {
  init(
    testData: [BankDetails] = []
  ) {
    self.init(
      data: testData
    )
  }
}

// MARK: - IndividualAccount

extension StripeConnect.IndividualAccount {
  init(
    testFirstName: String? = nil,
    testLastName: String? = nil,
    testDob: StripeConnect.DateOfBirth? = nil,
    testAddress: StripeConnect.Address? = nil,
    testVerification: StripeConnect.VerificationDocument? = nil
  ) {
    self.init(
      firstName: testFirstName,
      lastName: testLastName,
      dob: testDob,
      address: testAddress,
      verification: testVerification
    )
  }
}

// MARK: - DateOfBirth

extension StripeConnect.DateOfBirth {
  init(
    testDay: Int? = nil,
    testMonth: Int? = nil,
    testYear: Int? = nil
  ) {
    self.init(
      day: testDay,
      month: testMonth,
      year: testYear
    )
  }
}

// MARK: - Address

extension StripeConnect.Address {
  init(
    testCity: String? = nil,
    testLine1: String? = nil,
    testState: String? = nil,
    testCountry: String? = nil,
    testPostalCode: String? = nil
  ) {
    self.init(
      city: testCity,
      line1: testLine1,
      state: testState,
      country: testCountry,
      postalCode: testPostalCode
    )
  }
}

// MARK: - VerificationDocument

extension StripeConnect.VerificationDocument {
  init(
    testDocument: StripeConnect.Document = .init()
  ) {
    self.init(
      document: testDocument
    )
  }
}

// MARK: - Document

extension StripeConnect.Document {
  init(
    testFront: String? = nil,
    testBack: String? = nil
  ) {
    self.init(
      front: testFront,
      back: testBack
    )
  }
}
