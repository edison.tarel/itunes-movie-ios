//
//  StripeConnectTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class StripeConnectTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("StripeConnect") {
      var sut: StripeConnect!

      afterEach {
        sut = nil
      }

      it("should return correct individual.dob.date when params are not nil") {
        sut = StripeConnect(
          testIndividual: .init(
            testDob: .init(
              day: 14,
              month: 10,
              year: 2020
            )
          )
        )

        let expectedDate = Date(
          year: 2020,
          month: 10,
          day: 14,
          hour: 0,
          minute: 0
        )
        expect(sut.individual.dob?.date).to(equal(expectedDate))
      }

      it("should return nil individual.dob.date when params are nil") {
        sut = StripeConnect(
          testIndividual: .init(
            testDob: .init(
              day: nil,
              month: nil,
              year: nil
            )
          )
        )

        expect(sut.individual.dob?.date).to(beNil())
      }
    }
  }
}
