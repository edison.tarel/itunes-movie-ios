//
//  BankDetails.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class BankDetailsTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("BankDetails") {
      var sut: BankDetails!

      afterEach {
        sut = nil
      }

      it("should return correct accountNumber when last4 is nil") {
        sut = BankDetails(testLast4: nil)

        expect(sut.accountNumber).to(equal("*"))
      }

      it("should return correct accountNumber when last4 is not nil") {
        sut = BankDetails(testLast4: "1234")

        expect(sut.accountNumber).to(equal("*1234"))
      }

      it("should return correct bsb when routingNumber is nil") {
        sut = BankDetails(testLast4: nil)

        expect(sut.bsb).to(beEmpty())
      }

      it("should return correct bsb when routingNumber is not nil") {
        sut = BankDetails(testRoutingNumber: "RoutingNumber")

        expect(sut.bsb).to(equal("RoutingNumber"))
      }
    }
  }
}
