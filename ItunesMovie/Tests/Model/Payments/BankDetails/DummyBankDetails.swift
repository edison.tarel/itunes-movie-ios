//
//  DummyBankDetails.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension BankDetails {
  init(
    testId: String? = nil,
    testAccountHolderName: String? = nil,
    testLast4: String? = nil,
    testRoutingNumber: String? = nil
  ) {
    self.init(
      id: testId,
      accountHolderName: testAccountHolderName,
      last4: testLast4,
      routingNumber: testRoutingNumber
    )
  }
}
