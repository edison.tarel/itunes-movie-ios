//
//  DummyEphemeralKey.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import ItunesMovie

extension EphemeralKey {
  init(
    testId: String = "",
    testObject: String = "",
    testSecret: String = "",
    testAssociatedObjects: [AssociatedObject] = [],
    testCreated: Int = 0,
    testExpires: Int = 0,
    testLivemode: Bool = false
  ) {
    self.init(
      id: testId,
      object: testObject,
      secret: testSecret,
      associatedObjects: testAssociatedObjects,
      created: testCreated,
      expires: testExpires,
      livemode: testLivemode
    )
  }
}
