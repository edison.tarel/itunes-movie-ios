//
//  DummyUser.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension User {
  init(
    email: String? = nil,
    phoneNumber: String? = nil,
    fullName: String? = nil,
    emailVerified: Bool = false,
    phoneNumberVerified: Bool = false,
    avatarPermanentUrl: URL = .dummy,
    avatarPermanentThumbUrl: URL = .dummy,
    avatar: Photo? = nil,
    birthdate: String? = nil,
    description: String? = nil
  ) {
    self.init(
      id: 0,
      email: email,
      phoneNumber: phoneNumber,
      fullName: fullName,
      gender: .male,
      emailVerified: emailVerified,
      phoneNumberVerified: phoneNumberVerified,
      avatarPermanentUrl: avatarPermanentUrl,
      avatarPermanentThumbUrl: avatarPermanentThumbUrl,
      avatar: avatar,
      birthdate: birthdate,
      description: description
    )
  }
}
