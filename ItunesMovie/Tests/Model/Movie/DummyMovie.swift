//
//  DummyMovie.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension Movie {
  init(
    testTrackId: Int32 = 0,
    testTrackName: String = "",
    testPrimaryGenreName: String = "",
    testArtworkUrl60: URL? = nil,
    testTrackPrice: Float = 0.0,
    testLongDescription: String = ""
  ) {
    self.init(
      trackId: testTrackId,
      trackName: testTrackName,
      primaryGenreName: testPrimaryGenreName,
      artworkUrl60: testArtworkUrl60,
      trackPrice: testTrackPrice,
      longDescription: testLongDescription
    )
  }
}
