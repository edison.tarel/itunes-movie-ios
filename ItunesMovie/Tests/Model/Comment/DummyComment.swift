//
//  DummyComment.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import ItunesMovie

extension Comment {
  init(
    testId: Int = 0,
    testResponses: [Comment]? = nil,
    testAuthor: User? = nil,
    testBody: String = "",
    testLevel: Level = .level1,
    testCreatedAt: Date = .init()
  ) {
    self.init(
      id: testId,
      author: testAuthor,
      authorId: 0,
      body: testBody,
      createdAt: testCreatedAt,
      updatedAt: Date(),
      responses: testResponses,
      level: testLevel
    )
  }
}
