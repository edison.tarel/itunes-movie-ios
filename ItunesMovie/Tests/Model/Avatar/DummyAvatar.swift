//
//  DummyAvatar.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension Photo {
  init(
    testURL: URL = URL(string: "https://appetiser.com.au/")!,
    testThumbURL: URL = URL(string: "https://appetiser.com.au/")!
  ) {
    self.init(
      url: testURL,
      thumbUrl: testThumbURL
    )
  }
}
