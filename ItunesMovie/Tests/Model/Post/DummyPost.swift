//
//  DummyPost.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension Post {
  init(
    testId: Int = 0,
    testBody: String = "",
    testCommentsCount: Int = 0,
    testFavoritesCount: Int = 0,
    testIsFavorite: Bool = false,
    testAuthor: User = .init(),
    testPhoto: Photo = .init(),
    testUpdatedAt: Date = .init(),
    testCreatedAt: Date = .init()
  ) {
    self.init(
      id: testId,
      body: testBody,
      commentsCount: testCommentsCount,
      favoritesCount: testFavoritesCount,
      isFavorite: testIsFavorite,
      author: testAuthor,
      photo: testPhoto,
      updatedAt: testUpdatedAt,
      createdAt: testCreatedAt
    )
  }
}
