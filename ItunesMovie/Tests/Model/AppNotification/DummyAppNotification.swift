//
//  DummyAppNotification.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension AppNotification {
  init(
    testType: Type = .comment,
    testMessage: String = "",
    testCreatedAt: Date = .init(),
    testActor: User = .init()
  ) {
    self.init(
      id: "Id",
      type: testType,
      actorId: 0,
      message: testMessage,
      createdAt: testCreatedAt,
      actor: testActor
    )
  }
}
