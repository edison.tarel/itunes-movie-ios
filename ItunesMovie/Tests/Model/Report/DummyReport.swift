//
//  DummyReport.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension Report {
  init(
    reasonId: Int = 0,
    description: String = ""
  ) {
    self.init(
      reasonId: reasonId,
      description: description,
      attachments: []
    )
  }
}
