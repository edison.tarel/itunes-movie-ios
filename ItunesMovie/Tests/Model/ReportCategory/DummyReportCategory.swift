//
//  DummyReportCategory.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

extension ReportCategory {
  init() {
    self.init(
      id: 0,
      label: ""
    )
  }
}
