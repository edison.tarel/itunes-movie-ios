//
//  MockNSCoder.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MockNSCoder: NSCoder {
  override var requiresSecureCoding: Bool { false }

  override var allowsKeyedCoding: Bool { true }

  override func decodeObject(forKey key: String) -> Any? { nil }

  override func decodeInt64(forKey key: String) -> Int64 { 0 }
}
