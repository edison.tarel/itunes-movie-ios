//
//  NotificationsControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

//
// TODO: Should update to correct test cases.
//
class NotificationsControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("NotificationsController") {
      var sut: NotificationsController!

      var viewModel: MockNotificationsViewModel!

      beforeEach {
        viewModel = MockNotificationsViewModel()
        sut = R.storyboard.notifications.notificationsController()!
        sut.viewModel = viewModel
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
//          expect(sut.headerView).toNot(beNil())
          expect(sut.tableView).toNot(beNil())
        }

        it("should set tableView.delegate to self") {
          expect(sut.tableView.delegate).to(be(sut))
        }
      }

      context("when viewModel.sections is set") {
        var sections: [NotificationsSection]!

        beforeEach {
          sections = [
            NotificationsSection(
              index: 0,
              name: "A",
              status: .loading,
              viewModels: [
                MockNotificationCellViewModel(),
                MockNotificationCellViewModel(),
                MockNotificationCellViewModel()
              ]
            ),
            NotificationsSection(
              index: 1,
              name: "B",
              status: .loading,
              viewModels: [
                MockNotificationCellViewModel()
              ]
            ),
            NotificationsSection(
              index: 2,
              name: "C",
              status: .loading,
              viewModels: [
                MockNotificationCellViewModel(),
                MockNotificationCellViewModel()
              ]
            ),
            NotificationsSection(
              index: 3,
              name: "D",
              status: .loading,
              viewModels: []
            )
          ]

          viewModel.sections = sections
          viewModel.errorToReturn = nil

          sut.loadViewIfNeeded()
        }

        afterEach {
          sections = nil
        }

        it("should return correct values when tableView.dataSource methods are called") {
          let t = sut.tableView!
          let d = t.dataSource!

//          expect(d.numberOfSections?(in: t)).to(equal(4))  // This errors
          expect(d.tableView(t, numberOfRowsInSection: 0)).to(equal(3))
          expect(d.tableView(t, numberOfRowsInSection: 1)).to(equal(1))
          expect(d.tableView(t, numberOfRowsInSection: 2)).to(equal(2))
        }

        it("should return correct values when tableView.delegate methods are called") {
          let t = sut.tableView!
          let d = t.delegate!

          expect(d.tableView?(t, viewForHeaderInSection: 0)).to(beAKindOf(NotificationTitleView.self))
          expect(d.tableView?(t, viewForHeaderInSection: 3)).to(beNil())
          expect(d.tableView?(t, heightForHeaderInSection: 0)).to(equal(NotificationTitleView.height))
          expect(d.tableView?(t, heightForHeaderInSection: 3)).to(equal(.zero))
        }
      }
    }
  }
}
