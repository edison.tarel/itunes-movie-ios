//
//  MockNotificationsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockNotificationsViewModel: NotificationsViewModelProtocol {
  var sections: [NotificationsSection] = []

  var errorToReturn: Error?

  var fetchNotificationsCallCount: Int = 0
}

// MARK: - Methods

extension MockNotificationsViewModel {
  func fetchNotifications(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    fetchNotificationsCallCount += 1

    if let e = errorToReturn {
      return onError(e)
    }

    onSuccess()
  }
}
