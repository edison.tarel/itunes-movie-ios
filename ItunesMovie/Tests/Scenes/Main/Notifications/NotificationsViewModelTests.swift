//
//  NotificationsViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class NotificationsViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("NotificationsViewModel") {
      var sut: NotificationsViewModel!
      var service: MockNotificationsService!

      beforeEach {
        service = MockNotificationsService()
        sut = NotificationsViewModel(service: service)
      }

      afterEach {
        service = nil
        sut = nil
      }

      it("should call service.fetchNotificationsForToday and service.fetchNotificationsForThisWeek each once on fetchNotifications") {
        expect(service.fetchNotificationsForTodayCallCount).to(equal(0))
        expect(service.fetchNotificationsForThisWeekCallCount).to(equal(0))

        sut.fetchNotifications(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.fetchNotificationsForTodayCallCount).to(equal(1))
        expect(service.fetchNotificationsForThisWeekCallCount).to(equal(1))
      }

      it("should eventually add sections up to 2 and sort by index") {
        expect(sut.sections).to(beEmpty())

        sut.fetchNotifications(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(sut.sections.count).toEventually(equal(3))

        let areSectionsSortedByIndex = sut.sections.isSorted(by: { $0.index < $1.index })
        expect(areSectionsSortedByIndex).toEventually(beTrue())
      }

      context("when service is set to succeed") {
        beforeEach {
          service.errorToReturn = nil
        }

        it("should call onSuccess closure once on fetchNotifications") {
          var onSuccessCallCount = 0

          sut.fetchNotifications(
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when service is set to fail") {
        var expectedError: Error!

        beforeEach {
          expectedError = NSError(domain: #function, code: 0, userInfo: nil)
          service.errorToReturn = expectedError
        }

        it("should call onError closure once and pass expected error on fetchNotifications") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.fetchNotifications(
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
        }
      }
    }
  }
}
