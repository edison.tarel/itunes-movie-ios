//
//  MockAccountViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import ItunesMovie

class MockAccountViewModel: AccountViewModelProtocol {
  var profileImageURL: URL?
  var fullNameText: String?
  var usernameText: String?
}
