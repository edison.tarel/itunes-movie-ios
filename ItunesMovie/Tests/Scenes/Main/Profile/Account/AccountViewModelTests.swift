//
//  AccountViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class AccountViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("AccountViewModel") {
      var sut: AccountViewModel!
      var session: MockSessionService!

      var user: User!

      beforeEach {
        user = .init(
          fullName: "FullName",
          avatarPermanentThumbUrl: URL(string: "https://appetiser.com.au/")!
        )
        session = MockSessionService()
        session.user = user

        sut = AccountViewModel(session: session)
      }

      afterEach {
        user = nil
        session = nil
        sut = nil
      }

      it("should use session.user.avatarPermanentThumbUrl as profileImageURL") {
        expect(sut.profileImageURL?.absoluteString).to(equal("https://appetiser.com.au/"))
      }

      it("should use session.user.fullName as fullNameText") {
        expect(sut.fullNameText).to(equal("FullName"))
      }

      context("when session.user has email") {
        beforeEach {
          user = .init(
            email: "Email"
          )
          session.user = user
        }

        afterEach {
          user = nil
        }

        it("should use session.user.email as usernameText") {
          expect(sut.usernameText).to(equal("Email"))
        }
      }

      context("when session.user has no email but has phoneNumber") {
        beforeEach {
          user = .init(
            email: nil,
            phoneNumber: "PhoneNumber"
          )
          session.user = user
        }

        afterEach {
          user = nil
        }

        it("should use session.user.email as usernameText") {
          expect(sut.usernameText).to(equal("PhoneNumber"))
        }
      }

      context("when session.user has no email and no phoneNumber") {
        beforeEach {
          user = .init(
            email: nil,
            phoneNumber: nil
          )
          session.user = user
        }

        afterEach {
          user = nil
        }

        it("should return nil usernameText") {
          expect(sut.usernameText).to(beNil())
        }
      }
    }
  }
}
