//
//  MockPayoutViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockPayoutViewModel: PayoutViewModelProtocol {
  var stripeAccount: StripeConnect! = .init()

  var errorToReturn: Error?

  private(set) var fetchStripeAccountCallCount: Int = 0
  private(set) var deleteStripeAccountCallCount: Int = 0
}

// MARK: - Methods

extension MockPayoutViewModel {
  func fetchStripeAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    fetchStripeAccountCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func deleteStripeAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    deleteStripeAccountCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
