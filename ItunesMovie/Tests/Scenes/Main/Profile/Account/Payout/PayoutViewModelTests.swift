//
//  PayoutViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PayoutViewModelTests: QuickSpec {
  override func spec() {
    describe("PayoutViewModel") {
      var sut: PayoutViewModel!
      var service: MockPaymentService!

      beforeEach {
        service = MockPaymentService()
        sut = PayoutViewModel(
          service: service
        )
      }

      afterEach {
        sut = nil
        service = nil
      }

      it("should call service.fetchStripeConnect once on fetchStripeAccount") {
        expect(service.fetchStripeConnectCallCount).to(equal(0))

        sut.fetchStripeAccount(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.fetchStripeConnectCallCount).to(equal(1))
      }

      it("should call service.deleteAuthConnectAccount once on deleteStripeAccount") {
        expect(service.deleteAuthConnectAccountCallCount).to(equal(0))

        sut.deleteStripeAccount(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.deleteAuthConnectAccountCallCount).to(equal(1))
      }

      context("when initialized with successful service") {
        beforeEach {
          service.errorToReturn = nil
        }

        it("should call onSuccess closure once on fetchStripeAccount") {
          var onSuccessCallCount = 0

          sut.fetchStripeAccount(
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }

        it("should call onSuccess closure once on deleteStripeAccount") {
          var onSuccessCallCount = 0

          sut.deleteStripeAccount(
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when initialized with failing service") {
        let serviceError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          service.errorToReturn = serviceError
        }

        it("should return service error and call onError closure once on fetchStripeAccount") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.fetchStripeAccount(
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              onErrorCallCount += 1
              errorReturned = $0
            }
          )

          expect(onErrorCallCount).toEventually(equal(1))
          expect(errorReturned).toEventually(be(serviceError))
        }
      }
    }
  }
}
