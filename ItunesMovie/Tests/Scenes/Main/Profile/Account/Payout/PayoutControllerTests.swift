//
//  PayoutControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PayoutControllerTests: QuickSpec {
  override func spec() {
    describe("PayoutController") {
      var sut: PayoutController!
      var viewModel: MockPayoutViewModel!

      beforeEach {
        viewModel = MockPayoutViewModel()
        viewModel.stripeAccount = .init(
          testExternalAccounts: .init(
            testData: [
              .init(
                testAccountHolderName: "AccountHolderName",
                testLast4: "7890",
                testRoutingNumber: "RoutingNumber"
              )
            ]
          )
        )
        sut = R.storyboard.payout.payoutController()!
        sut.viewModel = viewModel
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.accountNameLabel).toNot(beNil())
          expect(sut.bsbLabel).toNot(beNil())
          expect(sut.accountNumberLabel).toNot(beNil())
          expect(sut.editPayoutButton).toNot(beNil())
          expect(sut.removeAccountButton).toNot(beNil())
        }

        it("set correct title") {
          expect(sut.title).to(equal(S.payoutTitle()))
        }

        it("set label texts from viewModel") {
          expect(sut.accountNameLabel.text).to(equal("AccountHolderName"))
          expect(sut.bsbLabel.text).to(equal("RoutingNumber"))
          expect(sut.accountNumberLabel.text).to(equal("*7890"))
        }
      }
      
      context("when initialized in nav flow") {
        var nc: MockNavigationController!
        
        beforeEach {
          nc = MockNavigationController(rootViewController: sut)
          nc.loadViewIfNeeded()
          nc.addToViewHierarchy()
          
          sut.loadViewIfNeeded()
        }
        
        afterEach {
          nc = nil
        }
        
        it("should push PayoutEntryController once on tap of editPayoutButton") {
          let currentCallCount = nc.pushViewControllerCallCount
          
          sut.editPayoutButton.tap()
          
          expect(nc.pushedViewController).to(beAKindOf(PayoutEntryController.self))
          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
        }
        
        it("should present UIAlertController once on tap of removeAccountButton") {
          sut.removeAccountButton.tap()
          
          expect(nc.presentedViewController).toEventually(beAKindOf(UIAlertController.self))
        }
      }
    }
  }
}
