//
//  PaymentViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PaymentViewModelTests: QuickSpec {
  override func spec() {
    describe("PaymentViewModel") {
      var sut: PaymentViewModel!
      var service: MockPaymentService!

      beforeEach {
        service = MockPaymentService()
        sut = PaymentViewModel(
          service: service
        )
      }

      afterEach {
        sut = nil
        service = nil
      }

      it("should call service.fetchEphemeralKey once on createCustomerKey") {
        expect(service.fetchEphemeralKeyCallCount).to(equal(0))

        sut.createCustomerKey(
          withAPIVersion: "",
          completion: DefaultClosure.doubleResult()
        )

        expect(service.fetchEphemeralKeyCallCount).to(equal(1))
      }

      context("when initialized with successful service") {
        beforeEach {
          service.errorToReturn = nil
        }

        it("should pass non-nil dictionary and nil error then call completion closure once on createCustomerKey") {
          var completionCallCount = 0
          var dictionaryReturned: [AnyHashable: Any]?
          var errorReturned: Error?

          sut.createCustomerKey(
            withAPIVersion: "",
            completion: { dictionary, error in
              completionCallCount += 1
              dictionaryReturned = dictionary
              errorReturned = error
            }
          )

          expect(completionCallCount).toEventually(equal(1))
          expect(dictionaryReturned).toEventuallyNot(beNil())
          expect(errorReturned).toEventually(beNil())
        }
      }

      context("when initialized with failing service") {
        let serviceError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          service.errorToReturn = serviceError
        }

        it("should pass nil dictionary and non-nil error then call completion closure once on createCustomerKey") {
          var completionCallCount = 0
          var dictionaryReturned: [AnyHashable: Any]?
          var errorReturned: Error?

          sut.createCustomerKey(
            withAPIVersion: "",
            completion: { dictionary, error in
              completionCallCount += 1
              dictionaryReturned = dictionary
              errorReturned = error
            }
          )

          expect(completionCallCount).toEventually(equal(1))
          expect(dictionaryReturned).toEventually(beNil())
          expect(errorReturned).toEventually(be(serviceError))
        }
      }
    }
  }
}
