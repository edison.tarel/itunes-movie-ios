//
//  MockPaymentViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Stripe

@testable import ItunesMovie

class MockPaymentViewModel: NSObject, PaymentViewModelProtocol {
  var errorToReturn: Error?

  private(set) var createCustomerKeyCallCount: Int = 0
  private(set) var createCustomerKeyAPIVersion: String?
}

// MARK: - Methods

extension MockPaymentViewModel {
  func createCustomerKey(
    withAPIVersion apiVersion: String,
    completion: @escaping STPJSONResponseCompletionBlock
  ) {
    createCustomerKeyCallCount += 1
    createCustomerKeyAPIVersion = apiVersion

    if let e = errorToReturn {
      completion(nil, e)
    } else {
      completion([:], nil)
    }
  }
}
