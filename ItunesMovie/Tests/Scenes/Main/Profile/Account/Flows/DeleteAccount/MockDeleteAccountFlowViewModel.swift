//
//  MockDeleteAccountViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import ItunesMovie

class MockDeleteAccountFlowViewModel: DeleteAccountFlowViewModelProtocol {

  var verifyPasswordVM: VerifyPasswordViewModelProtocol = MockVerifyPasswordViewModel()
}
