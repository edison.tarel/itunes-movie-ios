//
//  DeleteAccountFlowCoordinatorTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class DeleteAccountFlowCoordinatorTests: QuickSpec {
  override func spec() {
    describe("DeleteAccountFlowCoordinator") {
      var sut: DeleteAccountFlowCoordinator!
      var viewModel: MockDeleteAccountFlowViewModel!
      var navigationController: MockNavigationController!
      
      beforeEach {
        navigationController = MockNavigationController(
          rootViewController: UIViewController()
        )
        
        viewModel = MockDeleteAccountFlowViewModel()
        
        sut = DeleteAccountFlowCoordinator(
          navigationController: navigationController
        )
        sut.viewModel = viewModel
      }
      
      afterEach {
        navigationController = nil
        viewModel = nil
        sut = nil
      }
      
      it("should push VerifyPasswordController as first page in flow on start") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        
        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyPasswordController.self))
      }
      
      it("should pop to controller before the flow on viewModel.verifyPasswordVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        
        expect(navigationController.viewControllers.count).to(equal(1))
      }
    }
  }
}
