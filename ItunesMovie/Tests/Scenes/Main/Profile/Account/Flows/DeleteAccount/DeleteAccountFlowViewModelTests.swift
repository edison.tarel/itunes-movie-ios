//
//  DeleteAccountFlowViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class DeleteAccountFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("DeleteAccountFlowViewModel") {
      var sut: DeleteAccountFlowViewModel!
      
      beforeEach {
        sut = DeleteAccountFlowViewModel()
      }
      
      afterEach {
        sut = nil
      }
      
      it("should return verifyPasswordVM with correct messageText") {
        let expectedText = S.verifyPasswordDeleteAccountMessage()
        expect(sut.verifyPasswordVM.messageText).to(equal(expectedText))
      }
    }
  }
}
