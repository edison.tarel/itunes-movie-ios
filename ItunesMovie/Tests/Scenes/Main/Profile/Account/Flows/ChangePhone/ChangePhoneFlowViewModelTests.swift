//
//  ChangePhoneFlowViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ChangePhoneFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("ChangePhoneFlowViewModel") {
      var sut: ChangePhoneFlowViewModel!
      
      beforeEach {
        sut = ChangePhoneFlowViewModel()
      }
      
      afterEach {
        sut = nil
      }
      
      it("should return verifyPasswordVM with correct messageText") {
        let expectedText = S.verifyPasswordChangePhoneMessage()
        expect(sut.verifyPasswordVM.messageText).to(equal(expectedText))
      }
      
      it("should return verifyChangeCredentialVM with correct type") {
        sut.save(verificationToken: "Token")
        sut.save(
          countryCode: "CountryCode",
          phoneNumber: "PhoneNumber"
        )
        
        let expectedType = VerifyChangePhoneViewModel.self
        expect(sut.verifyChangeCredentialVM).to(beAKindOf(expectedType))
      }
    }
  }
}

