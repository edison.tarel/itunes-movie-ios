//
//  ChangePhoneFlowCoordinatorTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ChangePhoneFlowCoordinatorTests: QuickSpec {
  override func spec() {
    describe("ChangePhoneFlowCoordinator") {
      var sut: ChangePhoneFlowCoordinator!
      var viewModel: MockChangePhoneFlowViewModel!
      var navigationController: MockNavigationController!
      
      beforeEach {
        navigationController = MockNavigationController(
          rootViewController: UIViewController()
        )
        
        viewModel = MockChangePhoneFlowViewModel()
        
        sut = ChangePhoneFlowCoordinator(
          navigationController: navigationController
        )
        sut.viewModel = viewModel
      }
      
      afterEach {
        navigationController = nil
        viewModel = nil
        sut = nil
      }
      
      it("should push VerifyPasswordController as first page in flow on start") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        
        expect(navigationController.viewControllers.count).to(equal(2))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyPasswordController.self))
      }
      
      it("should call viewModel.save(verificationToken:) once on viewModel.verifyPasswordVM.onComplete") {
        expect(viewModel.saveVerificationTokenCallCount).to(equal(0))
        expect(viewModel.saveVerificationTokenValue).to(beNil())
        
        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        
        expect(viewModel.saveVerificationTokenCallCount).to(equal(1))
        expect(viewModel.saveVerificationTokenValue).to(equal("Token"))
      }
      
      it("should push ChangePhoneController as second page in flow on viewModel.verifyPasswordVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        
        expect(navigationController.viewControllers.count).to(equal(3))
        expect(navigationController.pushedViewController).to(beAKindOf(ChangePhoneController.self))
      }
      
      it("should call viewModel.save(email:) once on viewModel.changePhoneVM.onComplete") {
        expect(viewModel.saveCountryCodePhoneNumberCallCount).to(equal(0))
        expect(viewModel.saveCountryCodeValue).to(beNil())
        
        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        viewModel.changePhoneVM.onComplete?("CountryCode", "PhoneNumber")
        
        expect(viewModel.saveCountryCodePhoneNumberCallCount).to(equal(1))
        expect(viewModel.saveCountryCodeValue).to(equal("CountryCode"))
        expect(viewModel.savePhoneNumberValue).to(equal("PhoneNumber"))
      }
      
      it("should push VerifyChangeCredentialController as third page in flow on viewModel.changePhoneVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        viewModel.changePhoneVM.onComplete?("CountryCode", "PhoneNumber")
        
        expect(navigationController.viewControllers.count).to(equal(4))
        expect(navigationController.pushedViewController).to(beAKindOf(VerifyChangeCredentialController.self))
      }
      
      
      it("should pop to controller before the flow on viewModel.verifyChangeCredentialVM.onComplete") {
        expect(navigationController.viewControllers.count).to(equal(1))
        
        sut.start()
        viewModel.verifyPasswordVM.onComplete?("Token", "Password")
        viewModel.changePhoneVM.onComplete?("CountryCode", "PhoneNumber")
        viewModel.verifyChangeCredentialVM.onComplete?()
        
        expect(navigationController.viewControllers.count).to(equal(1))
      }
    }
  }
}
