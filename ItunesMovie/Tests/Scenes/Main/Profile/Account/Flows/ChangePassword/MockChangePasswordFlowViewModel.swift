//
//  MockChangePasswordFlowViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockChangePasswordFlowViewModel: ChangePasswordFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol = MockVerifyPasswordViewModel()
  var changePasswordVM: ChangePasswordViewModelProtocol = MockChangePasswordViewModel()

  var savePasswordCallCount: Int = 0
  var savePasswordValue: String?
}

// MARK: - Methods

extension MockChangePasswordFlowViewModel {
  func save(password: String) {
    savePasswordCallCount += 1
    savePasswordValue = password
  }
}
