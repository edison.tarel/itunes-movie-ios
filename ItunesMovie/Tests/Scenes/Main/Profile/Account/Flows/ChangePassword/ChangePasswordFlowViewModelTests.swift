//
//  ChangePasswordFlowViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ChangePasswordFlowViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("ChangePasswordFlowViewModel") {
      var sut: ChangePasswordFlowViewModel!
      
      beforeEach {
        sut = ChangePasswordFlowViewModel()
      }
      
      afterEach {
        sut = nil
      }
      
      it("should return verifyPasswordVM with correct messageText") {
        let expectedText = S.verifyPasswordChangePasswordMessage()
        expect(sut.verifyPasswordVM.messageText).to(equal(expectedText))
      }
    }
  }
}

