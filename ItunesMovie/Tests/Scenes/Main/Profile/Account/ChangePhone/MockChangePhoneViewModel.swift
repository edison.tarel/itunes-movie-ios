//
//  MockChangePhoneViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockChangePhoneViewModel: ChangePhoneViewModelProtocol {
  var onComplete: DoubleResult<String, String>?

  var errorToReturn: Error?

  private(set) var changePhoneCountryCode: String?
  private(set) var changePhoneValue: String?
  private(set) var changePhoneCallCount: Int = 0
}

// MARK: - Methods

extension MockChangePhoneViewModel {
  func changePhone(
    countryCode: String,
    phoneNumber: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    changePhoneCountryCode = countryCode
    changePhoneValue = phoneNumber
    changePhoneCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
