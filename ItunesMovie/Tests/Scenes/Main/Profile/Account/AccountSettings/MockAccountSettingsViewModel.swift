//
//  MockAccountSettingsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import ItunesMovie

class MockAccountSettingsViewModel: AccountSettingsViewModelProtocol {
  var emailAddress: String = ""
  var mobileNumber: String = ""
  
  var errorToReturn: Error?

  private(set) var logoutCallCount: Int = 0
  private(set) var deleteAccountCallCount: Int = 0
}

// MARK: - Methods

extension MockAccountSettingsViewModel {
  func logout(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    logoutCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func deleteAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    deleteAccountCallCount += 1
    
    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
