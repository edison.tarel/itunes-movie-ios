//
//  AccountSettingsViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class AccountSettingsViewModelTests: QuickSpec {
  override func spec() {
    describe("AccountSettingsViewModel") {
      var sut: AccountSettingsViewModel!
      var sessionService: MockSessionService!
      var logoutService: MockLogoutService!
      var accountDeletionService: MockAccountDeletionService!

      beforeEach {
        sessionService = MockSessionService()
        sessionService.user = .init()
        
        logoutService = MockLogoutService()
        accountDeletionService = MockAccountDeletionService()
        sut = AccountSettingsViewModel(
          sessionService: sessionService,
          logoutService: logoutService,
          accountDeletionService: accountDeletionService
        )
      }

      afterEach {
        sut = nil
        sessionService = nil
        logoutService = nil
        accountDeletionService = nil
      }

      it("should return emailAddress from sessionService.user.email") {
        sessionService.user = .init(email: "Email")

        expect(sut.emailAddress).to(equal("Email"))
      }

      it("should return empty emailAddress when sessionService.user.email is nil") {
        sessionService.user = .init(email: nil)

        expect(sut.emailAddress).to(beEmpty())
      }

      it("should return mobileNumber from sessionService.user.phoneNumber") {
        sessionService.user = .init(phoneNumber: "PhoneNumber")

        expect(sut.mobileNumber).to(equal("PhoneNumber"))
      }

      it("should return empty mobileNumber when sessionService.user.phoneNumber is nil") {
        sessionService.user = .init(phoneNumber: nil)

        expect(sut.mobileNumber).to(beEmpty())
      }

      context("when logoutService is set to succeed") {
        beforeEach {
          logoutService.errorToReturn = nil
        }

        it("should call onSuccess closure once on logout") {
          var onSuccessCallCount = 0

          sut.logout(
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when logoutService is set to fail") {
        let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          logoutService.errorToReturn = expectedError
        }

        it("should pass service error and call onError closure once on logout") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.logout(
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }

      context("when accountDeletionService is set to succeed") {
        beforeEach {
          accountDeletionService.errorToReturn = nil
        }

        it("should call onSuccess closure once on deleteAccount") {
          var onSuccessCallCount = 0

          sut.deleteAccount(
            onSuccess: { onSuccessCallCount += 1 },
            onError: DefaultClosure.singleResult()
          )

          expect(onSuccessCallCount).toEventually(equal(1))
        }
      }

      context("when accountDeletionService is set to fail") {
        let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          accountDeletionService.errorToReturn = expectedError
        }

        it("should pass service error and call onError closure once on deleteAccount") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.deleteAccount(
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
