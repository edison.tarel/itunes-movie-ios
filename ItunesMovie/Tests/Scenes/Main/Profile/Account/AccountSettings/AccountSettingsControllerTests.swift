//
//  AccountSettingsControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class AccountSettingsControllerTests: QuickSpec {
  override func spec() {
    describe("AccountSettingsController") {
      var sut: AccountSettingsController!
      var viewModel: MockAccountSettingsViewModel!
      var navigationController: MockNavigationController!

      beforeEach {
        viewModel = MockAccountSettingsViewModel()
        sut = R.storyboard.profile.accountSettingsController()!
        sut.viewModel = viewModel
        navigationController = MockNavigationController(rootViewController: sut)
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.emailAddressLabel).toNot(beNil())
          expect(sut.mobileNumberLabel).toNot(beNil())
        }
        
        it("should present ChangeEmailFlow on tap of row 0") {
          sut.tableView.tap(row: 0)
          
          expect(navigationController.pushedViewController)
            .toEventually(beAKindOf(VerifyPasswordController.self))
        }
        
        it("should present ChangePhoneFlow on tap of row 1") {
          sut.tableView.tap(row: 1)
          
          expect(navigationController.pushedViewController)
            .toEventually(beAKindOf(VerifyPasswordController.self))
        }
        
        it("should present ChangePasswordFlow on tap of row 1") {
          sut.tableView.tap(row: 2)
          
          expect(navigationController.pushedViewController)
            .toEventually(beAKindOf(VerifyPasswordController.self))
        }
        
        // TODO: Add tests for delete and logout alerts
      }
    }
  }
}
