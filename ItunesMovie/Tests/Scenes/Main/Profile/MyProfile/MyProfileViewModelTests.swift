//
//  MyProfileViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class MyProfileViewModelTests: QuickSpec {
  override func spec() {
    describe("MyProfileViewModel") {
      var sut: MyProfileViewModel!
      var service: MockPostsService!

      beforeEach {
        service = MockPostsService()
        sut = MyProfileViewModel(
          service: service
        )
      }

      afterEach {
        sut = nil
        service = nil
      }

      it("should pass filters and call service.getPosts on fetchPosts") {
        expect(service.fetchPostsCallCount).to(equal(0))
        expect(service.fetchPostsPage).to(beNil())
        expect(service.fetchPostsInclude).to(beNil())

        sut.fetchPosts(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.fetchPostsCallCount).to(equal(1))
        expect(service.fetchPostsPage).to(equal(APIPage.default))
        expect(service.fetchPostsInclude).to(equal(PostsIncludes.allCases))
      }
    }
  }
}
