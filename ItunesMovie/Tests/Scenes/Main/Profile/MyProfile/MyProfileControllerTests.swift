//
//  MyProfileControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class MyProfileControllerTests: QuickSpec {
  override func spec() {
    describe("MyProfileController") {
      var sut: MyProfileController!
      var viewModel: MockMyProfileViewModel!

      beforeEach {
        viewModel = MockMyProfileViewModel()
        sut = R.storyboard.profile.myProfileController()!
        sut.viewModel = viewModel
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.collectionView).toNot(beNil())
          expect(sut.titleView).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.flowLayout).toNot(beNil())
        }
      }

      // TODO: Add Navigation tests
    }
  }
}
