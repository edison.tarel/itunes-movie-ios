//
//  MockMyProfileViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockMyProfileViewModel: MyProfileViewModelProtocol {
  var dataSource: [Post] = []

  var errorToReturn: Error?

  private(set) var fetchPostsCallCount: Int = 0
}

// MARK: - Methods

extension MockMyProfileViewModel {
  func fetchPosts(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    fetchPostsCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
