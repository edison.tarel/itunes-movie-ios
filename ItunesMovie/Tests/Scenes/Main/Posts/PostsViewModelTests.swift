//
//  PostsViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PostsViewModelTests: QuickSpec {
  override func spec() {
    describe("PostsViewModel") {
      var sut: PostsViewModel!
      var service: MockPostsService!

      beforeEach {
        service = MockPostsService()
        sut = PostsViewModel(
          service: service
        )
      }

      afterEach {
        sut = nil
        service = nil
      }
      
      it("should have called service.bindPosts once") {
        expect(service.bindPostsCallCount).to(equal(1))
        expect(service.bindPostsOnRefresh).toNot(beNil())
      }
      
      it("should update sut.post to service.postToReturn when service triggers posts refresh") {
        service.postsToReturn = [Post()]
        expect(sut.posts.count).to(equal(0))
        
        service.bindPostsOnRefresh?()
        
        expect(sut.posts.count).to(equal(1))
      }
      
      it("should call onRefresh once when service triggers posts refresh") {
        var onRefreshCallCount = 0
        sut.onRefresh = { onRefreshCallCount += 1 }
        
        service.bindPostsOnRefresh?()
        
        expect(onRefreshCallCount).to(equal(1))
      }
      
      it("should call service.reloadFeed on fetchPosts") {
        expect(service.fetchPostsCallCount).to(equal(0))

        sut.fetchPosts(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.fetchPostsCallCount).to(equal(1))
      }

      it("should call service.fetchPosts on fetchPosts") {
        expect(service.fetchPostsCallCount).to(equal(0))

        sut.fetchPosts(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.fetchPostsCallCount).to(equal(1))
      }

      it("should pass id and call service.likePost on likePost") {
        expect(service.likePostCallCount).to(equal(0))
        expect(service.likePostId).to(beNil())

        sut.likePost(with: -1)

        expect(service.likePostCallCount).to(equal(1))
        expect(service.likePostId).to(equal(-1))
      }

      it("should pass id and call service.unlikePost on unlikePost") {
        expect(service.unlikePostCallCount).to(equal(0))
        expect(service.unlikePostId).to(beNil())

        sut.unlikePost(with: -2)

        expect(service.unlikePostCallCount).to(equal(1))
        expect(service.unlikePostId).to(equal(-2))
      }
    }
  }
}
