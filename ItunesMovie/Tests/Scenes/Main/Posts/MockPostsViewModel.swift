//
//  MockPostsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockPostsViewModel: PostsViewModelProtocol {
  var onRefresh: VoidResult?
  
  var posts: [Post] = []
  var hasLoadedAllPosts: Bool = false

  var errorToReturn: Error?

  var reloadFeedCallCount: Int = 0
  var fetchPostsCallCount: Int = 0

  var setPostCallCount: Int = 0
  var setPostValue: Post?
  var setPostIndex: Int?

  var likePostCallCount: Int = 0
  var unlikePostCallCount: Int = 0
}

// MARK: - Methods

extension MockPostsViewModel {
  func reloadFeed(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    reloadFeedCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func fetchPosts(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    fetchPostsCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func setPost(
    _ post: Post,
    at index: Int
  ) {
    setPostCallCount += 1
    setPostValue = post
    setPostIndex = index
  }

  func likePost(with id: Int) {
    likePostCallCount += 1
  }

  func unlikePost(with id: Int) {
    unlikePostCallCount += 1
  }
}
