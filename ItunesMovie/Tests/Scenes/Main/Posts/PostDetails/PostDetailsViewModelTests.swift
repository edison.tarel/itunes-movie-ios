//
//  PostDetailsViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class PostDetailsViewModelTests: QuickSpec {
  override func spec() {
    describe("PostDetailsViewModel") {
      var sut: PostDetailsViewModel!
      var service: MockPostsService!
      var post: Post!

      beforeEach {
        post = Post(testId: 0)
        
        service = MockPostsService()
        
        sut = PostDetailsViewModel(
          post: post,
          service: service
        )
      }

      afterEach {
        sut = nil
        service = nil
      }
      
      it("should have called service.bindPost once") {
        expect(service.bindCommentsInPostCallCount).to(equal(1))
        expect(service.bindCommentsInPostOnRefresh).toNot(beNil())
      }
      
      it("should update sut.dataSource based on service.comments when service triggers comments in post refresh") {
        service.commentsToReturn = [Comment()]
        expect(sut.dataSource.count).to(equal(0))
        
        service.bindCommentsInPostOnRefresh?()
        
        expect(sut.dataSource.count).to(equal(1))
      }
      
      it("should call onRefresh once when service triggers comments in post refresh") {
        var onRefreshCallCount = 0
        sut.onRefresh = { onRefreshCallCount += 1 }
        
        service.bindCommentsInPostOnRefresh?()
        
        expect(onRefreshCallCount).to(equal(1))
      }

      it("should call service.fetchPostComments on fetchPostComments") {
        expect(service.fetchPostCommentsCallCount).to(equal(0))

        sut.fetchPostComments(
          onError: DefaultClosure.singleResult()
        )

        expect(service.fetchPostCommentsCallCount).to(equal(1))
      }

      it("should pass comment and call service.createPostComment on createPostComment") {
        let testComment = "Test comment"

        expect(service.createPostCommentBody).to(beNil())
        expect(service.createPostCommentCallCount).to(equal(0))

        sut.postComment(
          testComment,
          onError: DefaultClosure.singleResult()
        )

        expect(service.createPostCommentBody).to(equal(testComment))
        expect(service.createPostCommentCallCount).to(equal(1))
      }

      it("should pass feed id and call service.likePost when not liked yet on toggleLikeFeed") {
        expect(service.likePostId).to(beNil())
        expect(service.likePostCallCount).to(equal(0))

        sut.toggleLike(onError: DefaultClosure.singleResult())

        expect(service.likePostId).to(equal(0))
        expect(service.likePostCallCount).to(equal(1))
      }

      it("should pass feed id and call service.unlikePost when already liked on toggleLikeFeed") {
        post = Post(testIsFavorite: true)
        sut = PostDetailsViewModel(
          post: post,
          service: service
        )

        expect(service.unlikePostId).to(beNil())
        expect(service.unlikePostCallCount).to(equal(0))

        sut.toggleLike(onError: DefaultClosure.singleResult())

        expect(service.unlikePostId).to(equal(0))
        expect(service.unlikePostCallCount).to(equal(1))
      }
    }
  }
}
