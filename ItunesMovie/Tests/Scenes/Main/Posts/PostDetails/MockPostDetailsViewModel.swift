//
//  MockPostDetailsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockPostDetailsViewModel: PostDetailsViewModelProtocol {
  var onRefresh: VoidResult?

  var id: Int = 0

  var dataSource: [PostCommentCellViewModelProtocol] = []

  var itemVM: SimplePostItemViewModelProtocol = MockSimplePostItemViewModel()
  var authorVM: PostAuthorViewModelProtocol = MockPostAuthorViewModel()
  var reportVM: ReportViewModelProtocol = MockReportViewModel()

  var errorToReturn: Error?

  private(set) var fetchPostCommentsCallCount: Int = 0
  private(set) var postCommentCallCount: Int = 0
  private(set) var toggleLikeCallCount: Int = 0
}

// MARK: - Methods

extension MockPostDetailsViewModel {
  func fetchPostComments(onError: @escaping ErrorResult) {
    fetchPostCommentsCallCount += 1

    if let e = errorToReturn {
      onError(e)
    }
  }

  func toggleLike(onError: @escaping ErrorResult) {
    toggleLikeCallCount += 1
  }

  func postComment(
    _ comment: String,
    onError: @escaping ErrorResult
  ) {
    postCommentCallCount += 1

    if let e = errorToReturn {
      onError(e)
    }
  }
}
