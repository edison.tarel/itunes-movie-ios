//
//  PostDetailsControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class PostDetailsControllerTests: QuickSpec {
  override func spec() {
    describe("PostDetailsController") {
      var sut: PostDetailsController!
      var viewModel: MockPostDetailsViewModel!

      beforeEach {
        viewModel = MockPostDetailsViewModel()
        sut = R.storyboard.posts.postDetailsController()!
        sut.viewModel = viewModel
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.commentTextView).toNot(beNil())
          expect(sut.commentPlaceholderLabel).toNot(beNil())
          expect(sut.feedAuthorView).toNot(beNil())
          expect(sut.tableView).toNot(beNil())
          expect(sut.commentTextViewHeight).toNot(beNil())
          expect(sut.sendButton).toNot(beNil())
        }
        
        it("should refresh view when viewModel.onRefresh is called") {
          expect(sut.tableView.numberOfRows(inSection: 0)).to(equal(0))
          
          viewModel.dataSource = [
            MockPostCommentCellViewModel()
          ]
          viewModel.onRefresh?()
          
          expect(sut.tableView.numberOfRows(inSection: 0)).to(equal(1))
        }
      }
    }
  }
}
