//
//  PostsControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PostsControllerTests: QuickSpec {
  override func spec() {
    describe("PostsController") {
      var sut: PostsController!
      var viewModel: MockPostsViewModel!

      beforeEach {
        viewModel = MockPostsViewModel()
        sut = R.storyboard.posts.postsController()!
        sut.viewModel = viewModel
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.collectionView).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.refreshControl).toNot(beNil())
        }

        it("should refresh view when viewModel.onRefresh is called") {
          expect(sut.collectionView.numberOfItems(inSection: 0)).to(equal(0))

          viewModel.posts = [
            Post(),
            Post()
          ]
          viewModel.onRefresh?()

          expect(sut.collectionView.numberOfItems(inSection: 0)).to(equal(2))
        }
      }
    }
  }
}
