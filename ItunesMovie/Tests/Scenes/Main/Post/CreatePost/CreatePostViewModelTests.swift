//
//  CreatePostViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class CreatePostViewModelTests: QuickSpec {
  override func spec() {
    describe("CreatePostViewModel") {
      var sut: CreatePostViewModel!
      var service: MockPostsService!

      beforeEach {
        service = MockPostsService()
        sut = CreatePostViewModel(
          service: service
        )
      }

      afterEach {
        sut = nil
        service = nil
      }

      context("when invalid image data is passed") {
        it("should pass error with correct message and call onError closure once") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.sharePost(
            with: nil,
            description: "Description",
            onProgress: DefaultClosure.singleResult(),
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned?.localizedDescription).toEventually(be(S.createPostErrorImageRequired()))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }

      context("when invalid description is passed") {
        it("should pass error with correct message and call onError closure once") {
          var onErrorCallCount = 0
          var errorReturned: Error?

          sut.sharePost(
            with: Data(),
            description: nil,
            onProgress: DefaultClosure.singleResult(),
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              errorReturned = $0
              onErrorCallCount += 1
            }
          )

          expect(errorReturned?.localizedDescription).toEventually(be(S.createPostErrorDescriptionRequired()))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }

      context("when valid params are passed") {
        var validImageData: Data!
        var validDescription: String!

        beforeEach {
          validImageData = Data()
          validDescription = "Description"
        }

        afterEach {
          validImageData = nil
          validDescription = nil
        }

        it("should pass params and call service.createPost once on sharePost") {
          expect(service.createPostCallCount).to(equal(0))
          expect(service.createPostImageData).to(beNil())
          expect(service.createPostDescription).to(beNil())

          sut.sharePost(
            with: validImageData,
            description: validDescription,
            onProgress: DefaultClosure.singleResult(),
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.createPostCallCount).to(equal(1))
          expect(service.createPostImageData).to(equal(validImageData))
          expect(service.createPostDescription).to(equal(validDescription))
        }

        context("and initialized with in progress service") {
          var serviceProgress: Progress!

          beforeEach {
            serviceProgress = Progress(totalUnitCount: 1)
            service.progressToReturn = serviceProgress
          }

          it("should pass api progress and call onProgress closure once on createPost") {
            var onProgressCallCount = 0
            var progressReturned: Progress?

            sut.sharePost(
              with: validImageData,
              description: validDescription,
              onProgress: {
                progressReturned = $0
                onProgressCallCount += 1
              },
              onSuccess: DefaultClosure.voidResult(),
              onError: DefaultClosure.singleResult()
            )

            expect(progressReturned).toEventually(be(serviceProgress))
            expect(onProgressCallCount).toEventually(equal(1))
          }
        }

        context("and initialized with successful service") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure once on sharePost") {
            var onSuccessCallCount = 0

            sut.sharePost(
              with: validImageData,
              description: validDescription,
              onProgress: DefaultClosure.singleResult(),
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(service.createPostCallCount).to(equal(1))
            expect(service.createPostImageData).to(equal(validImageData))
            expect(service.createPostDescription).to(equal(validDescription))
          }
        }

        context("when initialized with failing service") {
          var serviceError: Error!

          beforeEach {
            serviceError = NSError(domain: #function, code: 1, userInfo: nil)
            service.errorToReturn = serviceError
          }

          afterEach {
            serviceError = nil
          }

          it("should pass api error and call onError closure once on fetchPosts") {
            var onErrorCallCount = 0
            var errorReturned: Error?

            sut.sharePost(
              with: validImageData,
              description: validDescription,
              onProgress: DefaultClosure.singleResult(),
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                errorReturned = $0
                onErrorCallCount += 1
              }
            )

            expect(errorReturned).toEventually(be(serviceError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }
    }
  }
}
