//
//  MockCreatePostViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockCreatePostViewModel: CreatePostViewModelProtocol {
  var errorToReturn: Error?
  var progressToReturn: Progress?

  private(set) var sharePostCallCount: Int = 0
  private(set) var sharePostImageData: Data?
  private(set) var sharePostDescription: String?
}

// MARK: - Methods

extension MockCreatePostViewModel {
  func sharePost(
    with imageData: Data?,
    description: String?,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    sharePostCallCount += 1
    sharePostImageData = imageData
    sharePostDescription = description

    if let e = errorToReturn {
      onError(e)
    } else if let p = progressToReturn {
      onProgress(p)
    } else {
      onSuccess()
    }
  }
}
