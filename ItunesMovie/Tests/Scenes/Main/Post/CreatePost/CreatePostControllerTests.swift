//
//  CreatePostControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class CreatePostControllerTests: QuickSpec {
  override func spec() {
    describe("CreatePostController") {
      var sut: CreatePostController!
      var viewModel: MockCreatePostViewModel!
      var progressPresenter: MockProgressPresenter!
      var infoPresenter: MockInfoPresenter!

      beforeEach {
        viewModel = MockCreatePostViewModel()
        progressPresenter = MockProgressPresenter()
        infoPresenter = MockInfoPresenter()

        sut = R.storyboard.posts.createPostController()!
        sut.viewModel = viewModel
        sut.progressPresenter = progressPresenter
        sut.infoPresenter = infoPresenter
      }

      afterEach {
        sut = nil
        viewModel = nil
        progressPresenter = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.descriptionField).toNot(beNil())
          expect(sut.imageButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.imagePicker).toNot(beNil())
          expect(sut.imagePickerConfig).toNot(beNil())
        }

        it("should pass correct params and call viewModel.sharePost once on tap of shareButton") {
          expect(viewModel.sharePostCallCount).to(equal(0))
          expect(viewModel.sharePostImageData).to(beNil())
          expect(viewModel.sharePostDescription).to(beNil())

          sut.descriptionField.text = "Description"
          sut.imageButton.setImage(R.image.account()!, for: .normal)
          sut.shareButton.tap()

          expect(viewModel.sharePostCallCount).to(equal(1))
          expect(viewModel.sharePostImageData).toNot(beNil())
          expect(viewModel.sharePostDescription).to(equal("Description"))
        }

        it("should pass correct params and call progressPresenter.presentIndefiniteProgress once on tap of shareButton") {
          expect(progressPresenter.presentIndefiniteProgressCallCount).to(equal(0))
          expect(progressPresenter.presentIndefiniteProgressMessage).to(beNil())
          expect(progressPresenter.presentIndefiniteProgressSource).to(beNil())

          sut.shareButton.tap()

          expect(progressPresenter.presentIndefiniteProgressCallCount).to(equal(1))
          expect(progressPresenter.presentIndefiniteProgressMessage).to(beNil())
          expect(progressPresenter.presentIndefiniteProgressSource).to(be(sut))
        }

        context("when initialized with in progress viewModel") {
          var viewModelProgress: Progress!

          beforeEach {
            viewModelProgress = Progress(expectedProgress: 0.5)
            viewModel.progressToReturn = viewModelProgress
          }

          it("should call progressPresenter.presentProgress once on tap of shareButton") {
            expect(progressPresenter.presentProgressCallCount).to(equal(0))
            expect(progressPresenter.presentProgressValue).to(beNil())
            expect(progressPresenter.presentProgressSource).to(beNil())
            expect(progressPresenter.presentProgressMessage).to(beNil())

            sut.shareButton.tap()

            expect(progressPresenter.presentProgressCallCount).to(equal(1))
            expect(progressPresenter.presentProgressValue).to(equal(0.5))
            expect(progressPresenter.presentProgressSource).to(be(sut))
            expect(progressPresenter.presentProgressMessage).to(beNil())
          }
        }

        context("and initialized with successful viewModel") {
          beforeEach {
            viewModel.errorToReturn = nil
          }

          it("should call call progressPresenter.dismiss on tap of continueButton") {
            expect(progressPresenter.dismissCallCount).to(equal(0))
            expect(progressPresenter.dismissOnComplete).to(beNil())

            sut.shareButton.tap()

            expect(progressPresenter.dismissCallCount).to(equal(1))
            expect(progressPresenter.dismissOnComplete).to(beNil())
          }

          it("should post notification .didFinishShareFeed on tap of continueButton") {
            var notificationPostedCallCount = 0

            NotificationCenter.default.rx.notification(.didFinishShareFeed)
              .bind(onNext: { _ in notificationPostedCallCount += 1 })
              .disposed(by: self.rx.disposeBag)

            expect(progressPresenter.dismissCallCount).toEventually(equal(notificationPostedCallCount))
          }

          it("should call pass message and call infoPresenter.presentSuccessInfo on tap of continueButton") {
            expect(infoPresenter.presentSuccessInfoCallCount).to(equal(0))
            expect(infoPresenter.presentSuccessInfoMessage).to(beNil())
            expect(infoPresenter.presentSuccessInfoOnComplete).to(beNil())

            sut.shareButton.tap()

            expect(infoPresenter.presentSuccessInfoCallCount).to(equal(1))
            expect(infoPresenter.presentSuccessInfoMessage).to(equal(S.statusSuccess()))
            expect(infoPresenter.presentSuccessInfoOnComplete).to(beNil())
          }
        }

        context("when initialized with failing viewModel") {
          var viewModelError: Error!

          beforeEach {
            viewModelError = NSError(domain: #function, code: 1, userInfo: nil)
            viewModel.errorToReturn = viewModelError
          }

          afterEach {
            viewModelError = nil
          }

          it("should call progressPresenter.dismiss once on tap of shareButton") {
            expect(progressPresenter.dismissCallCount).to(equal(0))
            expect(progressPresenter.dismissOnComplete).to(beNil())

            sut.shareButton.tap()

            expect(progressPresenter.dismissCallCount).to(equal(1))
            expect(progressPresenter.dismissOnComplete).to(beNil())
          }

          it("should pass viewModel error and call infoPresenter.presentErrorInfo once on tap of shareButton") {
            expect(infoPresenter.presentErrorInfoCallCount).to(equal(0))
            expect(infoPresenter.presentErrorInfoValue).to(beNil())
            expect(infoPresenter.presentErrorInfoOnComplete).to(beNil())

            sut.shareButton.tap()

            expect(infoPresenter.presentErrorInfoCallCount).to(equal(1))
            expect(infoPresenter.presentErrorInfoValue).to(be(viewModelError))
            expect(infoPresenter.presentErrorInfoOnComplete).to(beNil())
          }
        }

        context("and added to view hierarchy") {
          beforeEach {
            sut.addToViewHierarchy()
          }

          it("should present imagePicker on tap of imageButton") {
            expect(sut.presentedViewController).to(beNil())

            sut.imageButton.tap()

            expect(sut.presentedViewController).toEventually(be(sut.imagePicker))
          }
        }
      }
    }
  }
}
