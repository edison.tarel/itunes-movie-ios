//
//  MovieListViewModelTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class MovieListViewModelTests: QuickSpec {
  override func spec() {
    describe("MovieListViewModel") {
      var sut: MovieListViewModel!
      var service: MockMoviesService!

      beforeEach {
        service = MockMoviesService()
        sut = MovieListViewModel(service: service)
      }

      afterEach {
        sut = nil
        service = nil
      }

      it("should call onLoading on sut.reloadMovies") {
        expect(sut.isLoading).to(equal(false))
        var isLoadingDidChangeToTrue = false
        sut.onLoading = {
          if sut.isLoading == true {
            isLoadingDidChangeToTrue = true
          }
        }

        sut.reloadMovies(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(isLoadingDidChangeToTrue).to(equal(true))
        expect(sut.isLoading).to(equal(false))
      }

      it("should call onLoading on sut.getMovies") {
        expect(sut.isLoading).to(equal(false))
        var isLoadingDidChangeToTrue = false
        sut.onLoading = {
          if sut.isLoading == true {
            isLoadingDidChangeToTrue = true
          }
        }

        sut.getMovies(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(isLoadingDidChangeToTrue).to(equal(true))
        expect(sut.isLoading).to(equal(false))
      }

      it("should return correct Movie on sut.getMovie") {
        service.moviesToReturn = SearchResult(
          resultCount: 4,
          results: [
            Movie(testTrackId: 1),
            Movie(testTrackId: 2),
            Movie(testTrackId: 3),
            Movie(testTrackId: 4)
          ]
        )

        sut.getMovies(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        let returnedMovie = sut.getMovie(at: IndexPath(row: 1, section: 0))

        expect(returnedMovie).to(equal(Movie(testTrackId: 2)))
      }

      it("should return correct number of Movies on sut.numberOfMovies") {
        service.moviesToReturn = SearchResult(
          resultCount: 4,
          results: [
            Movie(testTrackId: 1),
            Movie(testTrackId: 2),
            Movie(testTrackId: 3),
            Movie(testTrackId: 4)
          ]
        )

        sut.getMovies(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        let movieCount = sut.numberOfMovies

        expect(movieCount).to(equal(4))
      }

      context("when service is set to fail") {
        var expectedError: Error!

        beforeEach {
          expectedError = NSError(domain: #function, code: 0, userInfo: nil)
          service.errorToReturn = expectedError
        }

        it("should call onError closure once and pass expected error on sut.reloadMovies") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.reloadMovies(
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
        }

        it("should call onError closure once and pass expected error on sut.getMovies") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.getMovies(
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(be(expectedError))
        }
      }
    }
  }
}
