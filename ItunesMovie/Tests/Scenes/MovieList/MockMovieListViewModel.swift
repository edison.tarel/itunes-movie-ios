//
//  MockMovieListViewModel.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockMovieListViewModel: MovieListViewModelProtocol {  
  var errorToReturn: Error?
  
  var movies: [Movie] = [Movie(testTrackId: 1), Movie(testTrackId: 2)]
  var lastVisitDate: String = ""
  var onLoading: VoidResult?

  var reloadMoviesCallCount: Int = 0
  var getMoviesCallCount: Int = 0
  
  var isLoading: Bool = false {
    didSet {
      onLoading?()
    }
  }
}

// MARK: - Methods

extension MockMovieListViewModel {
  func reloadMovies(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    reloadMoviesCallCount += 1
    isLoading = true
    
    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
    
    isLoading = false
  }

  func getMovies(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    getMoviesCallCount += 1
    isLoading = true

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
    
    isLoading = false
  }
}

extension MockMovieListViewModel {
  func getMovie(at indexPath: IndexPath) -> Movie? {
    guard movies.count > indexPath.row else {
      return nil
    }
    
    return movies[indexPath.row]
  }
  
  var numberOfMovies: Int {
    return movies.count
  }
}
