//
//  MovieListControllerTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

//import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class MovieListControllerTests: QuickSpec {
  override func spec() {
    describe("MovieListController") {
      var sut: MovieListController!
      
      var infoPresenter: MockInfoPresenter!
      var viewModel: MockMovieListViewModel!
      
      beforeEach {
        viewModel = MockMovieListViewModel()
        sut = R.storyboard.movieList.movieListController()!
        sut.viewModel = viewModel
        infoPresenter = MockInfoPresenter()
        sut.infoPresenter = infoPresenter
      }
      
      afterEach {
        viewModel = nil
        sut = nil
      }
      
      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }
        
        it("should have non-nil outlets") {
          expect(sut.tableView).toNot(beNil())
        }
        
        it("should have non-nil properties") {
          expect(sut.refreshControl).toNot(beNil())
          expect(sut.movieListAdapter).toNot(beNil())
          expect(sut.tableView.delegate).to(be(sut.movieListAdapter))
          expect(sut.tableView.dataSource).to(be(sut.movieListAdapter))
        }
      }
      
      context("when viewModel is set to succeed") {
        beforeEach {
          sut.loadViewIfNeeded()
        }
        
        it("should updateDisplay on loadMovies") {
          expect(sut.tableView.numberOfRows(inSection: 0)).to(equal(2))
        }
        
        it("should end refreshing") {
          expect(sut.refreshControl.isRefreshing).to(equal(false))
        }
      }
      
      context("when pulled to refresh") {
        beforeEach {
          viewModel.movies = []
          sut.loadViewIfNeeded()
        }
        
        it("should display on empty rows") {
          expect(sut.tableView.numberOfRows(inSection: 0)).to(equal(0))
        }
        
        it("should call updateDisplay") {
          viewModel.movies = [Movie(testTrackId: 1), Movie(testTrackId: 2)]
          sut.refreshControl.sendActions(for: .valueChanged)
          
          expect(sut.tableView.numberOfRows(inSection: 0)).to(equal(2))
        }
        
        it("should end refreshing") {
          expect(sut.refreshControl.isRefreshing).to(equal(false))
        }
      }
      
      context("when viewModel is set to fail") {
        var fetchError: Error!
        
        beforeEach {
          fetchError = NSError(domain: #function, code: 1, userInfo: nil)
          viewModel.errorToReturn = fetchError
          viewModel.movies = []
          sut.loadViewIfNeeded()
        }
        
        it("should updateDisplay on loadMovies") {
          expect(sut.tableView.numberOfRows(inSection: 0)).to(equal(0))
        }
        
        it("should end refreshing") {
          expect(sut.refreshControl.isRefreshing).to(equal(false))
        }
        
        it("should call infoPresenter.presentErrorInfo") {
          expect(infoPresenter.presentErrorInfoCallCount).to(equal(1))
          expect(infoPresenter.presentErrorInfoValue).to(be(fetchError))
        }
      }
      
      context("when initialized in nav flow") {
        var nc: MockNavigationController!
        
        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)
          
          sut.loadViewIfNeeded()
        }
        
        afterEach {
          nc = nil
        }
        
        it("should present MovieDetailsController on tap of row 0") {
          let currentCallCount = nc.pushViewControllerCallCount
          
          sut.tableView.tap(row: 0)
          
          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
          expect(nc.pushedViewController).to(beAKindOf(MovieDetailController.self))
        }
      }
    }
  }
}
