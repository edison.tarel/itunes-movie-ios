//
//  ReportControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ReportControllerTests: QuickSpec {
  override func spec() {
    describe("ReportController") {
      var sut: ReportController!
      var viewModel: MockReportViewModel!

      beforeEach {
        viewModel = MockReportViewModel()
        viewModel.optionCheckboxTitle = "Option Checkbox Title"
        
        sut = R.storyboard.report.reportController()!
        sut.viewModel = viewModel
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.reasonField).toNot(beNil())
          expect(sut.descriptionField).toNot(beNil())
          expect(sut.uploadImageLabel).toNot(beNil())
          expect(sut.addButton).toNot(beNil())
          expect(sut.checkBox).toNot(beNil())
          expect(sut.checkBoxLabel).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.reasonInputController).toNot(beNil())
          expect(sut.descriptionInputController).toNot(beNil())
          expect(sut.categoryPickerView).toNot(beNil())
          expect(sut.imagePicker).toNot(beNil())
          expect(sut.imagePickerConfig).toNot(beNil())
        }
        
        it("should properly setup categoryPickerView") {
          expect(sut.categoryPickerView.viewModel).to(be(viewModel.categoryPickerVM))
          expect(sut.categoryPickerView.textField).to(be(sut.reasonField))
        }
        
        it("should properly setup checkBoxLabel") {
          expect(sut.checkBoxLabel.text).to(equal("Option Checkbox Title"))
        }
      }
    }
  }
}
 
   
