//
//  ReportUserViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class ReportUserViewModelTests: QuickSpec {
  override func spec() {
    describe("ReportUserViewModel") {
      var sut: ReportUserViewModel!
      var service: MockReportService!
      var categoryPickerVM: MockGenericPickerViewModel!

      beforeEach {
        service = MockReportService()
        service.categoriesToReturn = [
          ReportCategory(id: 0, label: "A"),
          ReportCategory(id: 1, label: "B"),
          ReportCategory(id: 2, label: "C"),
        ]

        categoryPickerVM = MockGenericPickerViewModel()
        categoryPickerVM.options = service.categoriesToReturn.map { $0.label }

        sut = ReportUserViewModel(
          userId: -1,
          service: service,
          categoryPickerVM: categoryPickerVM
        )
      }

      afterEach {
        sut = nil
        service = nil
        categoryPickerVM = nil
      }

      it("should call service.fetchReportCategories once on fetchCategories") {
        expect(service.fetchReportCategoriesCallCount).to(equal(0))

        sut.fetchCategories(
          onSuccess: DefaultClosure.voidResult(),
          onError: DefaultClosure.singleResult()
        )

        expect(service.fetchReportCategoriesCallCount).to(equal(1))
      }

      context("while no category is selected") {
        it("should call onError once and return validation error on submitReport") {
          var onErrorCallCount = 0
          var error: Error?

          sut.submitReport(
            with: nil,
            attachments: [],
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              error = $0
              onErrorCallCount += 1
            }
          )

          expect(onErrorCallCount).toEventually(equal(1))
          expect(error).toEventually(beAKindOf(ReportInputValidator.ValidationError.self))
        }
      }

      context("after categories are fetched and category is selected") {
        beforeEach {
          sut.fetchCategories(
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )
          categoryPickerVM.onOptionIndexSelect?(2)
        }

        it("should pass correct params and call service.reportUser once on submitReport") {
          expect(service.submitUserReportCallCount).to(equal(0))
          expect(service.submitUserReportUserId).to(beNil())
          expect(service.submitUserReportValue).to(beNil())

          sut.submitReport(
            with: "Description",
            attachments: [],
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.submitUserReportCallCount).to(equal(1))
          expect(service.submitUserReportUserId).to(equal(-1))
          expect(service.submitUserReportValue?.reasonId).to(equal(2))
          expect(service.submitUserReportValue?.description).to(equal("Description"))
          expect(service.submitUserReportValue?.attachments).to(beEmpty())
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure once on submitReport") {
            var onSuccessCallCount = 0

            sut.submitReport(
              with: nil,
              attachments: [],
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          var error: Error!

          beforeEach {
            error = NSError(domain: #function, code: 1, userInfo: nil)
            service.errorToReturn = error
          }

          it("should call onError closure once and return service error on submitReport") {
            var onErrorCallCount = 0

            sut.submitReport(
              with: nil,
              attachments: [],
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                error = $0
                onErrorCallCount += 1
              }
            )

            expect(onErrorCallCount).toEventually(equal(1))
            expect(error).toEventually(be(error))
          }
        }
      }
    }
  }
}
