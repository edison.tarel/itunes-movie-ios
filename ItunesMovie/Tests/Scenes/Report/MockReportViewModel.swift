//
//  MockReportViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockReportViewModel: ReportViewModelProtocol {
  var optionCheckboxTitle: String?
  var optionNumberOfAttachments: Int = 0
  var categoryPickerVM: GenericPickerViewModelProtocol = MockGenericPickerViewModel()

  var errorToReturn: Error?

  private(set) var fetchCategoriesCallCount: Int = 0

  private(set) var submitReportCallCount: Int = 0
  private(set) var submitReportDescription: String?
  private(set) var submitReportAttachments: [Data]?
}

// MARK: - Methods

extension MockReportViewModel {
  func fetchCategories(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    fetchCategoriesCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }

  func submitReport(
    with description: String?,
    attachments: [Data],
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    submitReportCallCount += 1
    submitReportDescription = description
    submitReportAttachments = attachments

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
