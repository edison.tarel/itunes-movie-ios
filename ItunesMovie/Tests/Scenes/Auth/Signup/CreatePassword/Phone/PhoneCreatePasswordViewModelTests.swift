//
//  PhoneCreatePasswordViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PhoneCreatePasswordViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhoneCreatePasswordViewModel") {
      var sut: PhoneCreatePasswordViewModel!
      var service: MockRegisterService!

      beforeEach {
        service = MockRegisterService()

        sut = PhoneCreatePasswordViewModel(
          countryCode: "+63",
          phoneNumber: "9123456789",
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      it("should use init countryCode and init phoneNumber as usernameText") {
        expect(sut.usernameText).to(equal("+639123456789"))
      }

      context("when valid password is used") {
        var validPassword: String!

        beforeEach {
          validPassword = "Password"
        }

        afterEach {
          validPassword = nil
        }

        it("should pass correct parameters and call service.registerWithPhoneNumber once on register") {
          expect(service.registerWithPhoneNumberCallCount).to(equal(0))
          expect(service.registerWithPhoneNumberValue).to(beNil())
          expect(service.registerWithPhoneNumberPassword).to(beNil())

          sut.register(
            with: "Password",
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.registerWithPhoneNumberCallCount).to(equal(1))
          expect(service.registerWithPhoneNumberValue).to(equal("+639123456789"))
          expect(service.registerWithPhoneNumberPassword).to(equal("Password"))
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure once on register") {
            var onSuccessCallCount = 0

            sut.register(
              with: validPassword,
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          var expectedError: Error!

          beforeEach {
            expectedError = NSError(domain: #function, code: 1, userInfo: nil)
            service.errorToReturn = expectedError
          }

          afterEach {
            expectedError = nil
          }

          it("should receive expected error via onError callback on register") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.register(
              with: validPassword,
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }

      context("when invalid password is used") {
        var invalidPassword: String!

        beforeEach {
          invalidPassword = ""
        }

        afterEach {
          invalidPassword = nil
        }

        it("should pass back NewPasswordInputValidator.ValidationError to onError closure on register") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.register(
            with: invalidPassword,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(beAKindOf(NewPasswordInputValidator.ValidationError.self))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
