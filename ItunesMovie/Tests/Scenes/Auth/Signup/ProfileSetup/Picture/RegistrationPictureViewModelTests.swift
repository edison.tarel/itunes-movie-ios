//
//  RegistrationPictureViewModelTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class RegistrationPictureViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("RegistrationPictureViewModel") {
      var sut: RegistrationPictureViewModel!
      var profileSetup: MockProfileSetupService!

      context("when initialized with success validate") {
        beforeEach {
          profileSetup = MockProfileSetupService()

          sut = RegistrationPictureViewModel(profileSetup: profileSetup)
        }

        it("should call profileSetup.setPicture method on submitPicture") {
          expect(profileSetup.setPictureCallCount).to(equal(0))

          sut.submitPicture(
            with: Data(),
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(profileSetup.setPictureCallCount).to(equal(1))
        }

        it("should call profileSetup.finishSetup method on skip") {
          expect(profileSetup.finishSetupCallCount).to(equal(0))

          sut.skip()

          expect(profileSetup.finishSetupCallCount).to(equal(1))
        }
      }

      context("when initialized with failure profileSetup.setPicture") {
        var expectedError: Error!

        beforeEach {
          expectedError = NSError(domain: #function, code: 1, userInfo: nil)

          profileSetup = MockProfileSetupService(
            shouldSucceed: false,
            errorToReturn: expectedError
          )

          sut = RegistrationPictureViewModel(profileSetup: profileSetup)
        }

        afterEach {
          expectedError = nil
          profileSetup = nil
          sut = nil
        }

        it("should call onError closure callback and receive expected error on submitPicture") {
          var submitError: Error!
          var onErrorCalled = false

          sut.submitPicture(
            with: Data(),
            onSuccess: {},
            onError: { error in onErrorCalled.toggle()
              submitError = error
            }
          )

          expect(onErrorCalled).toEventually(beTrue())
          expect(submitError).toEventually(be(expectedError))
        }
      }

      context("when initialized with success profile.setName") {
        beforeEach {
          profileSetup = MockProfileSetupService(shouldSucceed: true)
          sut = RegistrationPictureViewModel(profileSetup: profileSetup)
        }

        afterEach {
          profileSetup = nil
          sut = nil
        }

        it("should call onSuccess closure callback on submitPicture") {
          var onSuccessCalled = false

          sut.submitPicture(
            with: Data(),
            onSuccess: { onSuccessCalled.toggle() },
            onError: { _ in }
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }

        it("should call profileSetup.finishSetup on submitPicture") {
          expect(profileSetup.finishSetupCallCount).to(equal(0))

          sut.skip()

          expect(profileSetup.finishSetupCallCount).to(equal(1))
        }
      }
    }
  }
}
