//
//  RegistrationNameViewModelTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class RegistrationNameViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("RegistrationNameViewModel") {
      var sut: RegistrationNameViewModel!
      var validator: MockRegistrationNameInputValidator!
      var profileSetup: MockProfileSetupService!

      let name = ""

      context("when initialized with success validate") {
        beforeEach {
          validator = MockRegistrationNameInputValidator(shouldSucceed: true)
          profileSetup = MockProfileSetupService()

          sut = RegistrationNameViewModel(
            validate: validator.validate,
            profileSetup: profileSetup
          )
        }

        it("should call validate method on submit") {
          expect(validator.validateCallCount).to(equal(0))

          sut.submit(
            name: name,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(validator.validateCallCount).to(equal(1))
        }

        it("should call setName method on submit") {
          expect(profileSetup.setNameCallCount).to(equal(0))

          sut.submit(
            name: name,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(profileSetup.setNameCallCount).to(equal(1))
        }
      }

      context("when initialized with failure validate") {
        beforeEach {
          validator = MockRegistrationNameInputValidator(shouldSucceed: false)
          profileSetup = MockProfileSetupService()

          sut = RegistrationNameViewModel(
            validate: validator.validate,
            profileSetup: profileSetup
          )
        }

        it("should call validate method on submit") {
          expect(validator.validateCallCount).to(equal(0))

          sut.submit(
            name: name,
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(validator.validateCallCount).to(equal(1))
        }

        it("should call onError closure callback on submit") {
          var onErrorCalled = false

          sut.submit(
            name: name,
            onSuccess: {},
            onError: { _ in onErrorCalled.toggle() }
          )

          expect(onErrorCalled).toEventually(beTrue())
        }
      }

      context("when initialized with success validate and failure setName") {
        let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

        beforeEach {
          validator = MockRegistrationNameInputValidator(shouldSucceed: true)
          profileSetup = MockProfileSetupService(
            shouldSucceed: false,
            errorToReturn: expectedError
          )

          sut = RegistrationNameViewModel(
            validate: validator.validate,
            profileSetup: profileSetup
          )
        }

        it("should call onError closure callback and receive expected error on submit") {
          var submitError: Error!
          var onErrorCalled = false

          sut.submit(
            name: name,
            onSuccess: {},
            onError: { error in
              onErrorCalled.toggle()
              submitError = error
            }
          )

          expect(onErrorCalled).toEventually(beTrue())
          expect(submitError).toEventually(be(expectedError))
        }
      }

      context("when initialized with success validate and success setName") {
        beforeEach {
          validator = MockRegistrationNameInputValidator(shouldSucceed: true)
          profileSetup = MockProfileSetupService(shouldSucceed: true)

          sut = RegistrationNameViewModel(
            validate: validator.validate,
            profileSetup: profileSetup
          )
        }

        it("should call onSuccess closure callback on submit") {
          var onSuccessCalled = false

          sut.submit(
            name: name,
            onSuccess: { onSuccessCalled.toggle() },
            onError: { _ in }
          )

          expect(onSuccessCalled).toEventually(beTrue())
        }
      }

      afterEach {
        sut = nil
        validator = nil
        profileSetup = nil
      }
    }
  }
}
