//
//  MockRegistrationNameInputValidator.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockRegistrationNameInputValidator {
  private let shouldSucceed: Bool

  init(shouldSucceed: Bool) {
    self.shouldSucceed = shouldSucceed
  }

  var validate: RegistrationNameInputValidator.ValidateMethod {
    return { [unowned self] _ in
      self.validateCallCount += 1

      if self.shouldSucceed {
        return .success("")
      } else {
        return .failure(.requiredName)
      }
    }
  }

  private(set) var validateCallCount: Int = 0
}
