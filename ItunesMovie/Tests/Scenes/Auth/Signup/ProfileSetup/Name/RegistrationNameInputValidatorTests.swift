//
//  RegistrationNameInputValidatorTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class RegistrationNameInputValidatorTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("RegistrationNameInputValidator") {
      typealias Inputs = RegistrationNameInputValidator.Inputs
      typealias ValidInputs = RegistrationNameInputValidator.ValidInputs
      typealias ValidationError = RegistrationNameInputValidator.ValidationError

      var inputs: RegistrationNameInputValidator.Inputs!
      var expectedResult: Result<ValidInputs, ValidationError>!

      describe("ValidationError") {
        context("requiredName") {
          it("should have correct errorDescription") {
            let expectedValue = R.string.localizable.nameFormErrorNameRequired()
            expect(ValidationError.requiredName.localizedDescription).to(equal(expectedValue))
          }
        }
      }

      context("when validate method called with nil input") {
        beforeEach {
          inputs = Inputs(nil)
        }

        it("should return failure with requiredName error") {
          expectedResult = .failure(ValidationError.requiredName)

          let result = RegistrationNameInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      context("when validate method called with empty input") {
        beforeEach {
          inputs = Inputs("")
        }

        it("should return failure with requiredName error") {
          expectedResult = .failure(ValidationError.requiredName)

          let result = RegistrationNameInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      context("when validate method called with non-nil input") {
        let testName = "Test Name"

        beforeEach {
          inputs = Inputs(testName)
        }

        it("should return success") {
          expectedResult = .success(testName)

          let result = RegistrationNameInputValidator.validate(inputs)

          expect(result).to(equal(expectedResult))
        }
      }

      afterEach {
        inputs = nil
      }
    }
  }
}
