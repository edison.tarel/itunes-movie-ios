//
//  EmailCheckerControllerTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class EmailCheckerControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("EmailCheckerController") {
      var sut: EmailCheckerController!
      var nc: MockNavigationController!

      var infoPresenter: MockInfoPresenter!
      var viewModel: MockEmailCheckerViewModel!

      beforeEach {
        viewModel = MockEmailCheckerViewModel()
        viewModel.initialEmail = "ios@appetiser.com.au"
        infoPresenter = MockInfoPresenter()

        sut = R.storyboard.auth.emailCheckerController()!
        sut.viewModel = viewModel
        sut.infoPresenter = infoPresenter
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.field).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }

        it("should return viewModel as singleFormInputVM") {
          expect(sut.singleFormInputVM).to(be(viewModel))
        }

        it("should set viewModel.initialEmail to field.text") {
          expect(sut.field.text).to(equal("ios@appetiser.com.au"))
        }

        it("should enable continueButton when valid email is provided") {
          sut.field.setText("ios@appetiser.com.au")

          expect(sut.continueButton.isEnabled).to(beTrue())
        }

        it("should not enable continueButton when an empty email is provided") {
          sut.field.setText("")

          expect(sut.continueButton.isEnabled).to(beFalse())
        }

        it("should not enable continueButton when an invalid email is provided") {
          sut.field.setText("Invalid")

          expect(sut.continueButton.isEnabled).to(beFalse())
        }

        it("should pass field values and call viewModel.checkPhoneAvailability once on tap of continueButton") {
          sut.field.text = "Email"
          expect(viewModel.checkEmailAvailabilityEmail).to(beNil())
          expect(viewModel.checkEmailAvailabilityCallCount).to(equal(0))

          sut.continueButton.tap()

          expect(viewModel.checkEmailAvailabilityEmail).to(equal("Email"))
          expect(viewModel.checkEmailAvailabilityCallCount).to(equal(1))
        }
      }

      context("when initialized in nav flow") {
        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)

          sut.loadViewIfNeeded()
        }

        afterEach {
          nc = nil
        }

        it("should show the navigation bar on viewWillAppear") {
          sut.viewWillAppear(false)

          expect(sut.navigationController?.navigationBar.isHidden).to(beFalse())
        }

        it("should call navigationController.pushViewController once on tap of continueButton") {
          let currentCallCount = nc.pushViewControllerCallCount

          sut.continueButton.tap()

          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
        }

        it("should push CreatePasswordController once when email is available") {
          let currentCallCount = nc.pushViewControllerCallCount
          viewModel.isEmailAvailable = true

          sut.continueButton.tap()

          expect(nc.pushedViewController).to(beAKindOf(CreatePasswordController.self))
          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
        }

        it("should push LoginFormController once when email is not available ") {
          let currentCallCount = nc.pushViewControllerCallCount
          viewModel.isEmailAvailable = false

          sut.continueButton.tap()

          expect(nc.pushedViewController).to(beAKindOf(LoginFormController.self))
          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
        }
      }

      context("when viewModel is set to fail") {
        var submitError: Error!

        beforeEach {
          submitError = NSError(domain: #function, code: 1, userInfo: nil)
          viewModel.errorToReturn = submitError

          sut.loadViewIfNeeded()
        }

        afterEach {
          submitError = nil
        }

        it("should call infoPresenter.presentErrorInfo once on tap of continueButton") {
          expect(infoPresenter.presentErrorInfoCallCount).to(equal(0))
          expect(infoPresenter.presentErrorInfoValue).to(beNil())
          
          sut.continueButton.tap()
          
          expect(infoPresenter.presentErrorInfoCallCount).to(equal(1))
          expect(infoPresenter.presentErrorInfoValue).to(be(submitError))
        }
      }

      context("when viewModel is set to succeed") {
        beforeEach {
          viewModel.errorToReturn = nil
          sut.loadViewIfNeeded()
        }

        it("should clear fieldInputController.errorText on tap of primary button") {
          sut.fieldInputController.setErrorText("test", errorAccessibilityValue: nil)

          sut.continueButton.tap()

          expect(sut.fieldInputController.errorText).to(beNil())
        }
      }
    }
  }
}
