//
//  MockEmailCheckerViewModel.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockEmailCheckerViewModel: EmailCheckerViewModelProtocol {
  var initialEmail: String = ""

  var errorToReturn: Error?

  var isEmailAvailable = false

  private(set) var checkEmailAvailabilityEmail: String?
  private(set) var checkEmailAvailabilityCallCount: Int = 0
}

// MARK: - Methods

extension MockEmailCheckerViewModel {
  func checkEmailAvailability(
    email: String?,
    onAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    checkEmailAvailabilityEmail = email
    checkEmailAvailabilityCallCount += 1

    if let e = errorToReturn {
      return onError(e)
    }

    if isEmailAvailable {
      onAvailable(MockCreatePasswordViewModel())
    } else {
      onUnavailable(MockLoginFormViewModel())
    }
  }
}
