//
//  EmailCheckerViewModelTests.swift
//  Tests
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class EmailCheckerViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("EmailCheckerViewModel") {
      var sut: EmailCheckerViewModel!
      var usernameChecker: MockUsernameCheckerService!

      beforeEach {
        usernameChecker = MockUsernameCheckerService()

        sut = EmailCheckerViewModel(
          usernameChecker: usernameChecker
        )
      }

      afterEach {
        usernameChecker = nil
        sut = nil
      }

      context("when valid email is used") {
        var validEmail: String!

        beforeEach {
          validEmail = "ios@appetiser.com.au"
        }

        afterEach {
          validEmail = nil
        }

        it("should call usernameChecker.checkEmailAvailability once on checkEmailAvailability") {
          expect(usernameChecker.checkUsernameParamUsername).to(beNil())
          expect(usernameChecker.checkUsernameCallCount).to(equal(0))

          sut.checkEmailAvailability(
            email: validEmail,
            onAvailable: DefaultClosure.singleResult(),
            onUnavailable: DefaultClosure.singleResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(usernameChecker.checkUsernameParamUsername).to(equal(validEmail))
          expect(usernameChecker.checkUsernameCallCount).to(equal(1))
        }

        context("and usernameChecker is set to have available username") {
          beforeEach {
            usernameChecker.isUsernameAvailable = true
          }

          it("should call onAvailable callback on checkEmailAvailability") {
            var onAvailableCalled = false

            sut.checkEmailAvailability(
              email: validEmail,
              onAvailable: { _ in onAvailableCalled.toggle() },
              onUnavailable: DefaultClosure.singleResult(),
              onError: DefaultClosure.singleResult()
            )

            expect(onAvailableCalled).toEventually(beTrue())
          }
        }

        context("and usernameChecker is set to have unavailable username") {
          beforeEach {
            usernameChecker.isUsernameAvailable = false
          }

          it("should call onUnavailable closure callback on checkEmailAvailability") {
            var onUnavailableCalled = false

            sut.checkEmailAvailability(
              email: validEmail,
              onAvailable: DefaultClosure.singleResult(),
              onUnavailable: { _ in onUnavailableCalled.toggle() },
              onError: DefaultClosure.singleResult()
            )

            expect(onUnavailableCalled).toEventually(beTrue())
          }
        }

        context("and usernameChecker is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            usernameChecker.errorToReturn = expectedError
          }

          it("should receive expected error via onError callback on checkEmailAvailability") {
            var submitError: Error?

            sut.checkEmailAvailability(
              email: validEmail,
              onAvailable: DefaultClosure.singleResult(),
              onUnavailable: DefaultClosure.singleResult(),
              onError: { error in
                submitError = error
              }
            )

            expect(submitError).toEventually(be(expectedError))
          }
        }
      }

      context("when invalid email is used") {
        var invalidEmail: String!

        beforeEach {
          invalidEmail = "InvalidEmail"
        }

        afterEach {
          invalidEmail = nil
        }

        it("should call onError callback on checkEmailAvailability") {
          var onErrorCalled = false

          sut.checkEmailAvailability(
            email: invalidEmail,
            onAvailable: DefaultClosure.singleResult(),
            onUnavailable: DefaultClosure.singleResult(),
            onError: { _ in onErrorCalled.toggle() }
          )

          expect(onErrorCalled).toEventually(beTrue())
        }
      }
    }
  }
}
