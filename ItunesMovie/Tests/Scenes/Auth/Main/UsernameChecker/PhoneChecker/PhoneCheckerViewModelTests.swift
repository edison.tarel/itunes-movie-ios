//
//  PhoneCheckerViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PhoneCheckerViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhoneCheckerViewModel") {
      var sut: PhoneCheckerViewModel!
      var usernameChecker: MockUsernameCheckerService!
      var countryCode: String!

      beforeEach {
        countryCode = "+639"
        usernameChecker = MockUsernameCheckerService()

        sut = PhoneCheckerViewModel(
          usernameChecker: usernameChecker
        )
      }

      afterEach {
        countryCode = nil
        usernameChecker = nil
        sut = nil
      }

      context("when valid phoneNumber is used") {
        var validPhoneNumber: String!

        beforeEach {
          validPhoneNumber = "012345689"
        }

        afterEach {
          validPhoneNumber = nil
        }

        it("should call usernameChecker.checkUsernameAvailability once on checkPhoneNumberAvailability") {
          expect(usernameChecker.checkUsernameParamUsername).to(beNil())
          expect(usernameChecker.checkUsernameCallCount).to(equal(0))

          sut.checkPhoneNumberAvailability(
            countryCode: countryCode,
            phoneNumber: validPhoneNumber,
            onAvailable: DefaultClosure.singleResult(),
            onUnavailable: DefaultClosure.singleResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(usernameChecker.checkUsernameParamUsername).to(equal(countryCode + validPhoneNumber))
          expect(usernameChecker.checkUsernameCallCount).to(equal(1))
        }

        context("and usernameChecker is set to have available username") {
          beforeEach {
            usernameChecker.isUsernameAvailable = true
          }

          it("should call onSuccess callback on checkPhoneNumberAvailability") {
            var onAvailableCalled = false

            sut.checkPhoneNumberAvailability(
              countryCode: countryCode,
              phoneNumber: validPhoneNumber,
              onAvailable: { _ in onAvailableCalled.toggle() },
              onUnavailable: DefaultClosure.singleResult(),
              onError: DefaultClosure.singleResult()
            )

            expect(onAvailableCalled).toEventually(beTrue())
          }
        }

        context("and usernameChecker is set to have unavailable username") {
          beforeEach {
            usernameChecker.isUsernameAvailable = false
          }

          it("should call onUnavailable closure callback on checkPhoneNumberAvailability") {
            var onUnavailableCalled = false

            sut.checkPhoneNumberAvailability(
              countryCode: countryCode,
              phoneNumber: validPhoneNumber,
              onAvailable: DefaultClosure.singleResult(),
              onUnavailable: { _ in onUnavailableCalled.toggle() },
              onError: DefaultClosure.singleResult()
            )

            expect(onUnavailableCalled).toEventually(beTrue())
          }
        }

        context("and usernameChecker is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            usernameChecker.errorToReturn = expectedError
          }

          it("should receive expected error via onError callback on checkPhoneNumberAvailability") {
            var submitError: Error?

            sut.checkPhoneNumberAvailability(
              countryCode: countryCode,
              phoneNumber: validPhoneNumber,
              onAvailable: DefaultClosure.singleResult(),
              onUnavailable: DefaultClosure.singleResult(),
              onError: { submitError = $0 }
            )

            expect(submitError).toEventually(be(expectedError))
          }
        }
      }

      context("when invalid phoneNumber is used") {
        var invalidPhoneNumber: String!

        beforeEach {
          invalidPhoneNumber = ""
        }

        afterEach {
          invalidPhoneNumber = nil
        }

        it("should call onError callback on checkPhoneNumberAvailability") {
          var onErrorCalled = false

          sut.checkPhoneNumberAvailability(
            countryCode: countryCode,
            phoneNumber: invalidPhoneNumber,
            onAvailable: DefaultClosure.singleResult(),
            onUnavailable: DefaultClosure.singleResult(),
            onError: { _ in onErrorCalled.toggle() }
          )

          expect(onErrorCalled).toEventually(beTrue())
        }
      }
    }
  }
}
