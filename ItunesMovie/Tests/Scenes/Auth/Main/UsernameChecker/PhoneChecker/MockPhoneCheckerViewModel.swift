//
//  MockPhoneCheckerViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockPhoneCheckerViewModel: PhoneCheckerViewModelProtocol {
  var initialCountryCode: String = ""
  var initialPhoneNumber: String = ""

  var errorToReturn: Error?

  var isPhoneNumberAvailable = false

  private(set) var checkPhoneNumberAvailabilityCountryCode: String?
  private(set) var checkPhoneNumberAvailabilityPhoneNumber: String?
  private(set) var checkPhoneNumberAvailabilityCallCount: Int = 0
}

// MARK: - Methods

extension MockPhoneCheckerViewModel {
  func checkPhoneNumberAvailability(
    countryCode: String,
    phoneNumber: String?,
    onAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    checkPhoneNumberAvailabilityCountryCode = countryCode
    checkPhoneNumberAvailabilityPhoneNumber = phoneNumber
    checkPhoneNumberAvailabilityCallCount += 1

    if let e = errorToReturn {
      return onError(e)
    }

    if isPhoneNumberAvailable {
      onAvailable(MockCreatePasswordViewModel())
    } else {
      onUnavailable(MockLoginFormViewModel())
    }
  }
}
