//
//  PhoneCheckerControllerTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PhoneCheckerControllerTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhoneCheckerController") {
      var sut: PhoneCheckerController!
      var nc: MockNavigationController!

      var infoPresenter: MockInfoPresenter!
      var viewModel: MockPhoneCheckerViewModel!

      beforeEach {
        viewModel = MockPhoneCheckerViewModel()
        viewModel.initialCountryCode = "+63"
        viewModel.initialPhoneNumber = "9123456789"

        infoPresenter = MockInfoPresenter()

        sut = R.storyboard.auth.phoneCheckerController()!
        sut.viewModel = viewModel
        sut.infoPresenter = infoPresenter
      }

      afterEach {
        viewModel = nil
        sut = nil
      }

      context("when view is loaded") {
        beforeEach {
          sut.loadViewIfNeeded()
        }

        it("should have non-nil outlets") {
          expect(sut.field).toNot(beNil())
          expect(sut.continueButton).toNot(beNil())
        }

        it("should have non-nil properties") {
          expect(sut.fieldInputController).toNot(beNil())
        }

        it("should return viewModel as singleFormInputVM") {
          expect(sut.singleFormInputVM).to(be(viewModel))
        }

        // TODO: Implement this. Needs countryCode and phoneNumber to be split in backend data
        // it("should set viewModel.initialCountryCode to field.text") {
        //  expect(sut.countryField.text).to(equal("+63"))
        // }

        // TODO: Implement this. Needs countryCode and phoneNumber to be split in backend data
        // it("should set viewModel.initialPhone to field.text") {
        // expect(sut.field.text).to(equal("9123456789"))
        // }

        it("should enable continueButton when valid phoneNumber is provided") {
          sut.field.setText("9123456789")

          expect(sut.continueButton.isEnabled).to(beTrue())
        }

        it("should not enable continueButton when an empty phoneNumber is provided") {
          sut.field.setText("")

          expect(sut.continueButton.isEnabled).to(beFalse())
        }

        it("should pass field values and call viewModel.checkPhoneAvailability once on tap of continueButton") {
          sut.countryField.text = "CountryCode"
          sut.field.text = "PhoneNumber"
          expect(viewModel.checkPhoneNumberAvailabilityCountryCode).to(beNil())
          expect(viewModel.checkPhoneNumberAvailabilityPhoneNumber).to(beNil())
          expect(viewModel.checkPhoneNumberAvailabilityCallCount).to(equal(0))

          sut.continueButton.tap()

          expect(viewModel.checkPhoneNumberAvailabilityCountryCode).to(equal("CountryCode"))
          expect(viewModel.checkPhoneNumberAvailabilityPhoneNumber).to(equal("PhoneNumber"))
          expect(viewModel.checkPhoneNumberAvailabilityCallCount).to(equal(1))
        }
      }

      context("when initialized in nav flow") {
        beforeEach {
          nc = MockNavigationController(rootViewController: UIViewController())
          nc.loadViewIfNeeded()
          nc.pushViewController(sut, animated: false)

          sut.loadViewIfNeeded()
        }

        afterEach {
          nc = nil
        }

        it("should unhide the navigationBar on viewWillAppear") {
          sut.viewWillAppear(false)

          expect(sut.navigationController?.navigationBar.isHidden).to(beFalse())
        }

        it("should push CreatePasswordController once when phoneNumber is available") {
          let currentCallCount = nc.pushViewControllerCallCount
          viewModel.isPhoneNumberAvailable = true

          sut.continueButton.tap()

          expect(nc.pushedViewController).to(beAKindOf(CreatePasswordController.self))
          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
        }

        it("should push LoginFormController once when phoneNumber is not available ") {
          let currentCallCount = nc.pushViewControllerCallCount
          viewModel.isPhoneNumberAvailable = false

          sut.continueButton.tap()

          expect(nc.pushedViewController).to(beAKindOf(LoginFormController.self))
          expect(nc.pushViewControllerCallCount).to(equal(currentCallCount + 1))
        }
      }

      context("when viewModel is set to fail") {
        var submitError: Error!

        beforeEach {
          submitError = NSError(domain: #function, code: 1, userInfo: nil)
          viewModel.errorToReturn = submitError

          sut.loadViewIfNeeded()
        }

        afterEach {
          submitError = nil
        }

        it("should call infoPresenter.presentErrorInfo once on tap of continueButton") {
          expect(infoPresenter.presentErrorInfoCallCount).to(equal(0))
          expect(infoPresenter.presentErrorInfoValue).to(beNil())

          sut.continueButton.tap()

          expect(infoPresenter.presentErrorInfoCallCount).to(equal(1))
          expect(infoPresenter.presentErrorInfoValue).to(be(submitError))
        }
      }

      context("when viewModel is set to succeed") {
        beforeEach {
          viewModel.errorToReturn = nil
          sut.loadViewIfNeeded()
        }

        it("should clear fieldInputController.errorText on tap of continueButton") {
          sut.fieldInputController.setErrorText("test", errorAccessibilityValue: nil)

          sut.continueButton.tap()

          expect(sut.fieldInputController.errorText).to(beNil())
        }
      }
    }
  }
}
