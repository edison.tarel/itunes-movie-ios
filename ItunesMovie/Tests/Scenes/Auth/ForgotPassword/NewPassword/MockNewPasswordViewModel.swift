//
//  MockNewPasswordViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

@testable import ItunesMovie

class MockNewPasswordViewModel: NewPasswordViewModelProtocol {
  var usernameText: String = ""

  var errorToReturn: Error?

  private(set) var resetPasswordCallCount: Int = 0
}

extension MockNewPasswordViewModel {
  func resetPassword(
    with newPassword: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    resetPasswordCallCount += 1

    if let e = errorToReturn {
      onError(e)
    } else {
      onSuccess()
    }
  }
}
