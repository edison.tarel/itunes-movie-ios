//
//  PhoneLoginFormViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Nimble
import Quick

@testable import ItunesMovie

class PhoneLoginFormViewModelTests: QuickSpec, EndpointResponseSpec {
  override func spec() {
    describe("PhoneLoginFormViewModel") {
      var sut: PhoneLoginFormViewModel!
      var service: MockLoginService!

      beforeEach {
        service = MockLoginService()

        sut = PhoneLoginFormViewModel(
          countryCode: "+63",
          phoneNumber: "9123456789",
          service: service
        )
      }

      afterEach {
        service = nil
        sut = nil
      }

      it("should use init countryCode plus init phoneNumber as usernameText") {
        expect(sut.usernameText).to(equal("+639123456789"))
      }

      it("should pass init countryCode plus init phoneNumber to passwordResetVM as initialUsername") {
        expect(sut.passwordResetVM.initialUsername).to(equal("+639123456789"))
      }

      it("should have passwordResetVM with type PhonePasswordResetViewModel") {
        expect(sut.passwordResetVM is PhonePasswordResetViewModel).to(beTrue())
      }

      context("when valid password is used") {
        var validPassword: String!

        beforeEach {
          validPassword = "Password"
        }

        afterEach {
          validPassword = nil
        }

        it("should pass correct parameters and call service.loginWithEmail once on login") {
          expect(service.loginWithPhoneNumberCallCount).to(equal(0))
          expect(service.loginWithPhoneNumberValue).to(beNil())
          expect(service.loginWithPhoneNumberPassword).to(beNil())

          sut.login(
            with: "Password",
            onSuccess: DefaultClosure.voidResult(),
            onError: DefaultClosure.singleResult()
          )

          expect(service.loginWithPhoneNumberCallCount).to(equal(1))
          expect(service.loginWithPhoneNumberValue).to(equal("+639123456789"))
          expect(service.loginWithPhoneNumberPassword).to(equal("Password"))
        }

        context("and service is set to succeed") {
          beforeEach {
            service.errorToReturn = nil
          }

          it("should call onSuccess closure once on login") {
            var onSuccessCallCount = 0

            sut.login(
              with: validPassword,
              onSuccess: { onSuccessCallCount += 1 },
              onError: DefaultClosure.singleResult()
            )

            expect(onSuccessCallCount).toEventually(equal(1))
          }
        }

        context("and service is set to fail") {
          let expectedError: Error = NSError(domain: #function, code: 1, userInfo: nil)

          beforeEach {
            service.errorToReturn = expectedError
          }

          it("should receive expected error via onError callback on login") {
            var passedError: Error?
            var onErrorCallCount = 0

            sut.login(
              with: validPassword,
              onSuccess: DefaultClosure.voidResult(),
              onError: {
                passedError = $0
                onErrorCallCount += 1
              }
            )

            expect(passedError).toEventually(be(expectedError))
            expect(onErrorCallCount).toEventually(equal(1))
          }
        }
      }

      context("when invalid password is used") {
        var invalidPassword: String!

        beforeEach {
          invalidPassword = ""
        }

        afterEach {
          invalidPassword = nil
        }

        it("should pass back PasswordInputValidator.ValidationError to onError closure once on login") {
          var passedError: Error?
          var onErrorCallCount = 0

          sut.login(
            with: invalidPassword,
            onSuccess: DefaultClosure.voidResult(),
            onError: {
              passedError = $0
              onErrorCallCount += 1
            }
          )

          expect(passedError).toEventually(beAKindOf(PasswordInputValidator.ValidationError.self))
          expect(onErrorCallCount).toEventually(equal(1))
        }
      }
    }
  }
}
