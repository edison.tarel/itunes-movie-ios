//
//  OnboardingViewModelTests.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import Quick
import Nimble

@testable import ItunesMovie

class OnboardingViewModelTests: QuickSpec {
  override func spec() {
    describe("OnboardingViewModel") {
      var sut: OnboardingViewModel!

      beforeEach {
        sut = OnboardingViewModel()
      }

      afterEach {
        sut = nil
      }
      
      it("should return correct pageVMs") {
        expect(sut.pageVMs.count).to(equal(4))
        
        expect(sut.pageVMs[0]).to(beAKindOf(OnboardingStepOneViewModel.self))
        expect(sut.pageVMs[1]).to(beAKindOf(OnboardingStepTwoViewModel.self))
        expect(sut.pageVMs[2]).to(beAKindOf(OnboardingStepThreeViewModel.self))
        expect(sut.pageVMs[3]).to(beAKindOf(OnboardingStepFourViewModel.self))
      }
      
      it("should return false for shouldHideNextButton") {
        expect(sut.shouldHideNextButton).to(beFalse())
      }
      
      it("should return false for shouldHideSkipButton") {
        expect(sut.shouldHideSkipButton).to(beFalse())
      }
    }
  }
}
