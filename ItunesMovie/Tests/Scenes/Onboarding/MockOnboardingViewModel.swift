//
//  MockOnboardingViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

@testable import ItunesMovie

class MockOnboardingViewModel: OnboardingViewModelProtocol {  
  var onContinue: VoidResult?
  var onSkip: VoidResult?
  
  var pageVMs: [PageContentViewModelProtocol] = []
  
  var shouldHideNextButton: Bool = false
  var shouldHideSkipButton: Bool = false
}
