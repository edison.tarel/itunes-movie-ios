//
//  CoreDataManagerProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CoreData
import Foundation

protocol CoreDataManagerProtocol {
  var managedObjectContext: NSManagedObjectContext { get }
}
