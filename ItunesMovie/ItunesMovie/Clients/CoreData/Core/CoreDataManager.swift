//  CoreDataManager.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CoreData

class CoreDataManager: CoreDataManagerProtocol {
  private(set) lazy var managedObjectContext: NSManagedObjectContext = {
    let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
    return managedObjectContext
  }()

  private lazy var managedObjectModel: NSManagedObjectModel = {
    guard let modelURL = Bundle.main.url(
      forResource: self.modelName,
      withExtension: "momd"
    ) else {
      fatalError("Cannot find Data model")
    }

    guard let manageObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
      fatalError("Cannot load Data model")
    }

    return manageObjectModel
  }()

  private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
    let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)

    let fileManager = FileManager.default
    let storeName = "\(self.modelName).sqlite"

    let documentsDirectoryURL = fileManager.urls(
      for: .documentDirectory,
      in: .userDomainMask
    ).first

    let persistenStoreURL = documentsDirectoryURL?.appendingPathComponent(storeName)

    do {
      let options = [NSMigratePersistentStoresAutomaticallyOption: true,
                     NSInferMappingModelAutomaticallyOption: true]

      try persistentStoreCoordinator.addPersistentStore(
        ofType: NSSQLiteStoreType,
        configurationName: nil,
        at: persistenStoreURL,
        options: options
      )
    } catch {
      fatalError("Cannot add Persistent Store")
    }

    return persistentStoreCoordinator
  }()

  private let modelName: String

  init(modelName: String) {
    self.modelName = modelName
  }
}
