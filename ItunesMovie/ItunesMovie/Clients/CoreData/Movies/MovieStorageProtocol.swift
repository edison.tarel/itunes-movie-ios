//  CoreDataManager.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieStorageProtocol {
  func saveMovies(_ movies: [Movie])
  func deleteAllMovies()

  func getMovie(
    with id: Int,
    onSuccess: @escaping SingleResult<Movie?>,
    onError: @escaping ErrorResult
  )

  func getSavedMovies(
    onSuccess: @escaping SingleResult<[Movie]?>,
    onError: @escaping ErrorResult
  )
}

// MARK: - APIClientError

enum StorageError: Error {
  case dataNotFound
  case unknown
}

extension StorageError: LocalizedError {
  var errorDescription: String? {
    switch self {
    case .dataNotFound:
      return S.errorStorageDataNotFound()
    default:
      return S.errorDevUnknown()
    }
  }

  var failureReason: String? {
    switch self {
    case .dataNotFound:
      return S.errorStorageDataNotFound()
    default:
      return S.errorDevUnknown()
    }
  }
}
