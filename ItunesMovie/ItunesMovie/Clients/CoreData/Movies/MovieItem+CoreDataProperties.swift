//  MovieItem+CoreDataProperties.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CoreData
import Foundation

public extension MovieItem {
  @NSManaged var trackId: Int32
  @NSManaged var trackName: String
  @NSManaged var genre: String
  @NSManaged var artwork: URL?
  @NSManaged var price: Float
  @NSManaged var longDescription: String
  
  @nonobjc class func fetchRequest() -> NSFetchRequest<MovieItem> {
    return NSFetchRequest<MovieItem>(entityName: "MovieItem")
  }
}
