//
//  MovieStorage.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CoreData
import Foundation

class MovieStorage {
  private let dataManager: CoreDataManagerProtocol

  init(dataManager: CoreDataManagerProtocol = CoreDataManager(modelName: "Movies")) {
    self.dataManager = dataManager
  }
}

extension MovieStorage: MovieStorageProtocol {
  func saveMovies(_ movies: [Movie]) {
    self.deleteAllMovies()

    let fetchRequest: NSFetchRequest<MovieItem> = MovieItem.fetchRequest()
    let context = dataManager.managedObjectContext

    context.performAndWait {
      do {
        try fetchRequest.execute()
      } catch {
        print("Unable to Execure Fetch Request")
        print("\(error.localizedDescription)")
      }

      movies.forEach { item in
        let movie = MovieItem(context: context)
        movie.trackId = item.trackId
        movie.trackName = item.trackName
        movie.genre = item.primaryGenreName
        movie.price = item.trackPrice
        movie.artwork = item.artworkUrl60
        movie.longDescription = item.longDescription
      }

      if context.hasChanges {
        try? context.save()
      }
    }
  }

  // Removed all saved movies
  func deleteAllMovies() {
    let managedContext = dataManager.managedObjectContext
    let fetchRequest: NSFetchRequest<MovieItem> = MovieItem.fetchRequest()
    fetchRequest.includesPropertyValues = false
    do {
      let movies = try managedContext.fetch(fetchRequest)
      for movie in movies {
        managedContext.delete(movie)
      }
      try managedContext.save()
    } catch {
      print("Could not perform delete. \(error)")
    }
  }

  // Get particular Item
  func getMovie(
    with id: Int,
    onSuccess: @escaping SingleResult<Movie?>,
    onError: @escaping ErrorResult
  ) {
    let fetchRequest: NSFetchRequest<MovieItem> = MovieItem.fetchRequest()
    fetchRequest.predicate = NSPredicate(format: "trackId = %d", id)

    dataManager.managedObjectContext.performAndWait {
      do {
        let result = try fetchRequest.execute()
        if let movie = result.first {
          let itunesItem = Movie(
            trackId: movie.trackId,
            trackName: movie.trackName,
            primaryGenreName: movie.genre,
            artworkUrl60: movie.artwork,
            trackPrice: movie.price,
            longDescription: movie.longDescription
          )
          onSuccess(itunesItem)
        }
      } catch {
        print("Unable to Execure Fetch Request")
        print("\(error.localizedDescription)")
        onError(error)
      }
    }
  }

  // Get all saved movies
  func getSavedMovies(
    onSuccess: @escaping SingleResult<[Movie]?>,
    onError: @escaping ErrorResult
  ) {
    let fetchRequest: NSFetchRequest<MovieItem> = MovieItem.fetchRequest()
    dataManager.managedObjectContext.performAndWait {
      do {
        let movies = try fetchRequest.execute()
        let searchedItems = movies.map { movie -> Movie in
          Movie(
            trackId: movie.trackId,
            trackName: movie.trackName,
            primaryGenreName: movie.genre,
            artworkUrl60: movie.artwork,
            trackPrice: movie.price,
            longDescription: movie.longDescription
          )
        }
        onSuccess(searchedItems)
      } catch {
        print("Unable to Execure Fetch Request")
        print("\(error.localizedDescription)")
        onError(error)
      }
    }
  }
}
