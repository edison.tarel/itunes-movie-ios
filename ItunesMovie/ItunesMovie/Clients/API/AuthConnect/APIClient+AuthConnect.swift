//
//  APIClient+Payment.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthConnectAPI {
  @discardableResult
  func postEphemeralKey(
    onSuccess: @escaping SingleResult<EphemeralKey>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/ephemeral-key",
      method: .post,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func getAuthConnectExternalAccounts(
    onSuccess: @escaping SingleResult<[BankDetails]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/connect/external-accounts",
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func putAuthConnectExternalAccounts(
    accountId: String,
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/connect/external-accounts/\(accountId)",
      method: .put,
      parameters: params,
      encoding: JSONEncoding.default,
      success: { _ in onSuccess() },
      failure: onError
    )
  }

  @discardableResult
  func postAuthConnectAccount(
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/connect/account",
      method: .post,
      parameters: params,
      encoding: JSONEncoding.default,
      success: { _ in onSuccess() },
      failure: onError
    )
  }

  @discardableResult
  func getAuthConnectAccount(
    onSuccess: @escaping SingleResult<StripeConnect>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/connect/account",
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  func deleteAuthConnectAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/connect/account",
      method: .delete,
      success: { _ in onSuccess() },
      failure: onError
    )
  }

  func postAuthConnectFileUpload(
    imageData: Data,
    purpose: String? = nil,
    onProgress: SingleResult<Progress>?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    let purpseStr = purpose ?? "identity_document"
    let multipartFormData: SingleResult<MultipartFormData> = { multipartFormData in
      multipartFormData.append(["file": imageData, "purpose": purpseStr])
    }
    upload(
      to: "auth/connect/file-upload",
      with: multipartFormData,
      uploadProgress: onProgress,
      success: { response in
        guard let fileId: String = response.decodedValue(forKeyPath: "id") else {
          return onError(APIClientError.unknown)
        }
        onSuccess(fileId)
      },
      failure: onError
    )
  }

  @discardableResult
  func postAuthConnectAccountExternalAccount(
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "auth/connect/external-accounts",
      method: .post,
      parameters: ["external_account": params],
      encoding: JSONEncoding.default,
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
