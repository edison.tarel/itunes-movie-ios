//
//  AuthConnectAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AuthConnectAPI {
  @discardableResult
  func postEphemeralKey(
    onSuccess: @escaping SingleResult<EphemeralKey>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func getAuthConnectExternalAccounts(
    onSuccess: @escaping SingleResult<[BankDetails]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func putAuthConnectExternalAccounts(
    accountId: String,
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postAuthConnectAccount(
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func getAuthConnectAccount(
    onSuccess: @escaping SingleResult<StripeConnect>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func deleteAuthConnectAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  func postAuthConnectFileUpload(
    imageData: Data,
    purpose: String?,
    onProgress: SingleResult<Progress>?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  )

  @discardableResult
  func postAuthConnectAccountExternalAccount(
    params: JSONDictionary,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}
