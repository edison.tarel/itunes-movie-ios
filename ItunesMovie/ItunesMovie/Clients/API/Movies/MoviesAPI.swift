//
//  MoviesAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MoviesAPI {
  @discardableResult
  func getMovies(
    with parameter: MovieParameter,
    onSuccess: @escaping SingleResult<SearchResult<Movie>>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}

struct MovieParameter {
  var term: String
  var country: String
  var media: String
}
