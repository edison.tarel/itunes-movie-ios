//
//  APIClient+Movies.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Alamofire
import Foundation

extension APIClient: MoviesAPI {
  @discardableResult
  func getMovies(
    with parameter: MovieParameter,
    onSuccess: @escaping SingleResult<SearchResult<Movie>>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    var params: Parameters = [:]
    params["term"] = parameter.term
    params["country"] = parameter.country
    params["media"] = parameter.media

    return request(
      "search",
      parameters: params,
      success: { (responseValue: SearchResult<Movie>) in
        onSuccess(responseValue)
      },
      failure: onError
    )
  }
}
