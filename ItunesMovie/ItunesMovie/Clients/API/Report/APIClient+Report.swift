//
//  APIClient+Report.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: ReportAPI {
  @discardableResult
  func getReportCategories(
    onSuccess: @escaping SingleResult<[ReportCategory]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "report/categories",
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  func postReport(
    with userId: Int,
    report: Report,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let multipartFormData: SingleResult<MultipartFormData> = { multipartFormData in
      multipartFormData.append(report.customDictionary)
    }

    upload(
      to: "users/\(userId)/report",
      with: multipartFormData,
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
