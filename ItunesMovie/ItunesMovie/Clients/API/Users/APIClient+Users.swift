//
//  APIClient+Users.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Alamofire

extension APIClient: UsersAPI {
  @discardableResult
  func deleteUsers(
    withId id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "users/\(id)",
      method: .delete,
      success: { _ in onSuccess() },
      failure: onError
    )
  }
}
