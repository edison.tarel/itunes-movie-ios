//
//  APIClient+Posts.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: PostsAPI {
  @discardableResult
  func getPosts(
    userId: Int? = nil,
    page: APIPage,
    include: [PostsIncludes],
    onSuccess: @escaping SingleResult<[Post]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    var params: Parameters = [:]
    params["include"] = include.map { $0.rawValue }
    params["page"] = page.index
    params["per_page"] = page.size
    if let userId = userId {
      params["filter[author_id]"] = userId
    }
    return request(
      "posts",
      parameters: params,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postPostsFavorite(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "posts/\(id)/favorite",
      method: .post,
      success: { _ in onSuccess() },
      failure: onError
    )
  }

  @discardableResult
  func postPostsUnfavorite(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "posts/\(id)/unfavorite",
      method: .post,
      success: { _ in onSuccess() },
      failure: onError
    )
  }

  func postPosts(
    imageData: Data,
    description: String,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping SingleResult<Post>,
    onError: @escaping ErrorResult
  ) {
    let multipartFormData: SingleResult<MultipartFormData> = { multipartFormData in
      multipartFormData.append(["photo": imageData, "body": description])
    }
    upload(
      to: "posts",
      with: multipartFormData,
      uploadProgress: onProgress,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func getPostsComments(
    postId: Int,
    include: [PostsCommentIncludes],
    onSuccess: @escaping SingleResult<[Comment]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    var params: Parameters = [:]
    params["include"] = include.map { $0.rawValue }

    return request(
      "posts/\(postId)/comments",
      parameters: params,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postPostsComments(
    postId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    var params: Parameters = [:]
    params["body"] = body

    return request(
      "posts/\(postId)/comments",
      method: .post,
      parameters: params,
      encoding: JSONEncoding.default,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }

  @discardableResult
  func postCommentsResponses(
    commentId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    var params: Parameters = [:]
    params["body"] = body

    return request(
      "comments/\(commentId)/responses",
      method: .post,
      parameters: params,
      encoding: JSONEncoding.default,
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
