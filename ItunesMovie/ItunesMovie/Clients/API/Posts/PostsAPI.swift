//
//  PostsAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PostsAPI {
  @discardableResult
  func getPosts(
    userId: Int?,
    page: APIPage,
    include: [PostsIncludes],
    onSuccess: @escaping SingleResult<[Post]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postPostsFavorite(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postPostsUnfavorite(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  func postPosts(
    imageData: Data,
    description: String,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping SingleResult<Post>,
    onError: @escaping ErrorResult
  )

  @discardableResult
  func getPostsComments(
    postId: Int,
    include: [PostsCommentIncludes],
    onSuccess: @escaping SingleResult<[Comment]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postPostsComments(
    postId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func postCommentsResponses(
    commentId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}

// MARK: - Param Types

enum PostsIncludes: String, CaseIterable {
  case photo
  case author
  case favoritesCount = "favorites_count"
  case commentsCount = "comments_count"
}

enum PostsCommentIncludes: String, CaseIterable {
  case author
  case responses
}
