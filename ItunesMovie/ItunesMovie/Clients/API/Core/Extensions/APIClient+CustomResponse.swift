//
//  APIClient+CustomResponse.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Alamofire

extension APIClient {
  
  /// Request with custom API Response
  /// This wraps the call to `Alamofire.request(...).apiResponse(result:)`.
  ///
  /// - parameters:
  ///   - resourcePath: The path of the API resource.
  ///   - method: The HTTP method for this API resource.
  ///   - version: Optional. Defaults to whatever the value of `self.version` property is.
  ///   - parameters: The parameters for this API resource. `nil` by default.
  ///   - encoding: The parameter encoding to use. Defaults to `URLEncoding.default`.
  ///   - headers: The HTTP headers. Defaults to calling `httpRequestHeaders(withAuth:)`.
  ///   - success: Accepts `APIResponse` instance.
  ///   - failure: Accepts `Error` instance.
  ///
  func request<T: Codable>(
    _ resourcePath: String,
    method: HTTPMethod = .get,
    version: String? = nil,
    parameters: Parameters? = nil,
    encoding: ParameterEncoding = URLEncoding.default,
    headers: HTTPHeaders? = nil,
    success: @escaping (T) -> Void,
    failure: @escaping (Error) -> Void
  ) -> DataRequest {
    return sessionManager
      .request(
        endpointURL(resourcePath, version: version),
        method: method,
        parameters: parameters,
        encoding: encoding,
        headers: headers ?? httpRequestHeaders(withAuth: true)
      )
      .apiResponse(completion: { (result: Result<T>)  in
        switch result {
        case .success(let responseValue):
          success(responseValue)
        case .failure(let error):
          failure(error)
        }
      })
  }
}

// MARK: - Alamofire.DataRequest

extension DataRequest {
  @discardableResult
  func apiResponse<T: Codable>(
    queue: DispatchQueue? = nil,
    completion: @escaping (Result<T>) -> Void
  ) -> DataRequest {
    return responseData(queue: queue, completionHandler: { (response: DataResponse<Data>) in

      if let responseError = response.result.error {
        App.shared.errorHandling.handleError(responseError)
        return completion(.failure(responseError))
      }

      guard let responseData = response.value else {
        return completion(.failure(APIClientError.dataNotFound(Data.self)))
      }

      do {
        let resp = try JSONDecoder().decode(T.self, from: self.utf8Data(from: responseData))

        if let code = HTTPStatusCode(rawValue: response.response!.statusCode),
          code.isRequestError || code.isServerError {
          let info = APIClientFailedRequestInfo(
            status: code,
            message: S.generalErrorLabelsTypeUnknown(),
            errorCode: APIErrorCode.unknown
            //response.response!.statusCode
          )
          let apiError = APIClientError.failedRequest(info)
          App.shared.errorHandling.handleAPIError(apiError)
          completion(.failure(apiError))
        } else {
          completion(.success(resp))
        }

      } catch {
        App.shared.errorHandling.handleError(error)
        completion(.failure(error))
      }
    })
  }
  
  // TODO: Throw error
  private func utf8Data(from data: Data) -> Data {
    let encoding = detectEncoding(of: data)
    guard encoding != .utf8 else { return data }
    guard let responseString = String(data: data, encoding: encoding) else {
      preconditionFailure("Could not convert data to string with encoding \(encoding.rawValue)")
    }
    guard let utf8Data = responseString.data(using: .utf8) else {
      preconditionFailure("Could not convert data to UTF-8 format")
    }
    return utf8Data
  }

  private func detectEncoding(of data: Data) -> String.Encoding {
    var convertedString: NSString?
    let encoding = NSString.stringEncoding(
      for: data,
      encodingOptions: nil,
      convertedString: &convertedString,
      usedLossyConversion: nil
    )
    debugLog("~~> \(encoding)")
    return String.Encoding(rawValue: encoding)
  }
}
