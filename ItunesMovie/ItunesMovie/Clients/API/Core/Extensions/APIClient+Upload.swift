//
//  APIClient+Upload.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Alamofire
import Foundation

extension APIClient {
  /// Uploads files and other info to the server with one request using `MultiparFormData`.
  ///
  /// It is **not recommended** to use this method directly on your `ViewControllers/Models`, instead create a wrapper method that is tailored for the action you want to perform.
  /// For example, if your app requires you to upload a profile image, create a wrapper function called `uploadProfileImage(image:)`. This way you have control on
  /// what parameters you want required, and the intent is much clearer for anyone reading your code.
  ///
  /// - Parameter endpointURL: Upload endpoint
  /// - Parameter payload: A closure for handling MultipartFormData
  /// - Parameter requiresAuth: Whether the endpoint needs auth or not
  /// - Parameter completion: A closure for handling the request
  func upload(
    to resourcePath: String,
    version: String? = nil,
    with payload: @escaping (MultipartFormData) -> Void,
    requiresAuth: Bool = true,
    uploadProgress: SingleResult<Progress>? = nil,
    success: @escaping (APIResponse) -> Void,
    failure: @escaping (Error) -> Void
  ) {
    Alamofire.upload(
      multipartFormData: payload,
      to: endpointURL(resourcePath, version: version),
      method: .post,
      headers: httpRequestHeaders(withAuth: requiresAuth),
      encodingCompletion: { result in
        switch result {
        case .success(let request, _, _):
          request.uploadProgress(closure: { progress in
            uploadProgress?(progress)
          })
          request.apiResponse { result in
            switch result {
            case let .success(resp):
              success(resp)
            case let .failure(error):
              failure(error)
            }
          }
        case let .failure(error):
          App.shared.errorHandling.handleError(error)
          return failure(error)
        }
      }
    )
  }
}

extension MultipartFormData {
  func append(_ params: [String: Any]) {
    for (key, value) in params {
      if let stringValue = value as? String {
        append(stringValue.data(using: .utf8)!, withName: key)
      } else if let intValue = value as? Int {
        append("\(intValue)".data(using: .utf8)!, withName: key)
      } else if let boolValue = value as? Bool {
        append("\(boolValue.intValue())".data(using: .utf8)!, withName: key)
      } else if let data = value as? Data {
        append(
          data,
          withName: key,
          fileName: UUID().uuidString + ".jpg",
          mimeType: "image/jpeg"
        )
      } else if let data = value as? [Data] {
        for (index, datum) in data.enumerated() {
          append(
            datum,
            withName: "\(key)[\(index)]",
            fileName: "\(Date().timeIntervalSince1970).jpg",
            mimeType: "image/jpeg"
          )
        }
      }
    }
  }
}
