//
//  StripeExternalAccountParams.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct StripeExternalAccountParams: APIRequestParameters {
  let accountNumber: String?
  let object: String?
  let country: String?
  let currency: String?
  let accountHolderName: String?
  let accountHolderType: String?
  let routingNumber: String?
}
