//
//  APIClient+Notifications.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: NotificationsAPI {
  @discardableResult
  func getNotificationsToday(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getNotifications(
      queryType: .today,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  @discardableResult
  func getNotificationsThisWeek(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getNotifications(
      queryType: .thisWeek,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  @discardableResult
  func getNotificationsUnread(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    getNotifications(
      queryType: .unread,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  @discardableResult
  func getNotifications(
    queryType: NotificationQueryType,
    includes: [NotificationIncludes] = [.actor],
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    request(
      "notifications/\(queryType.rawValue)",
      parameters: ["include": includes.map { $0.rawValue }],
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
