//
//  NotificationsAPI.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol NotificationsAPI {
  @discardableResult
  func getNotificationsToday(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func getNotificationsThisWeek(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol

  @discardableResult
  func getNotificationsUnread(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol
}

// MARK: - Param Types

enum NotificationQueryType: String {
  case today, unread
  case thisWeek = "this-week"
}

enum NotificationIncludes: String {
  case actor, notifiable
}
