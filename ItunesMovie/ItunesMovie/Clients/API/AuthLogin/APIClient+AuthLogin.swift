//
//  APIClient+AuthLogin.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire

extension APIClient: AuthLoginAPI {
  @discardableResult
  func postAuthLogin(
    with username: String,
    password: String,
    onSuccess: @escaping SingleResult<UserAuthResponse>,
    onError: @escaping ErrorResult
  ) -> RequestProtocol {
    return request(
      "auth/login",
      method: .post,
      parameters: ["username": username, "password": password],
      encoding: JSONEncoding.default,
      headers: httpRequestHeaders(withAuth: false),
      success: decodeModel(
        onSuccess: onSuccess,
        onError: onError
      ),
      failure: onError
    )
  }
}
