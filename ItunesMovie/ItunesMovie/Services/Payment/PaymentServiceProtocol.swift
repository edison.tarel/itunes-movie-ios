//
//  PaymentServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PaymentServiceProtocol {
  func fetchEphemeralKey(
    onSuccess: @escaping SingleResult<EphemeralKey>,
    onError: @escaping ErrorResult
  )

  func fetchConnectedBanks(
    onSuccess: @escaping SingleResult<[BankDetails]>,
    onError: @escaping ErrorResult
  )

  func updateConnectedBanks(
    accountId: String,
    bankDetails: BankDetails,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func setupStripeConnect(
    stripeConnectParams: StripeConnect,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func fetchStripeConnect(
    onSuccess: @escaping SingleResult<StripeConnect>,
    onError: @escaping ErrorResult
  )

  func deleteAuthConnectAccount(    
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func postAuthConnectFileUpload(
    imageData: Data,
    purpose: String?,
    onProgress: SingleResult<Progress>?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  )

  func stripeConnectExternalAccount(
    externalAccount: StripeExternalAccountParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
