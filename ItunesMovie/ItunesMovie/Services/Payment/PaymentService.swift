//
//  PaymentService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PaymentService: PaymentServiceProtocol {
  private let api: AuthConnectAPI

  init(api: AuthConnectAPI) {
    self.api = api
  }
}

extension PaymentService {
  func fetchEphemeralKey(
    onSuccess: @escaping SingleResult<EphemeralKey>,
    onError: @escaping ErrorResult
  ) {
    api.postEphemeralKey(
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func fetchConnectedBanks(
    onSuccess: @escaping SingleResult<[BankDetails]>,
    onError: @escaping ErrorResult
  ) {
    api.getAuthConnectExternalAccounts(
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func updateConnectedBanks(
    accountId: String,
    bankDetails: BankDetails,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    guard let params = bankDetails.dictionary() else {
      return onError(APIClientError.unknown)
    }
    api.putAuthConnectExternalAccounts(
      accountId: accountId,
      params: params,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func setupStripeConnect(
    stripeConnectParams: StripeConnect,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    guard let params = stripeConnectParams.dictionary() else {
      return onError(APIClientError.unknown)
    }
    api.postAuthConnectAccount(
      params: params,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func fetchStripeConnect(
    onSuccess: @escaping SingleResult<StripeConnect>,
    onError: @escaping ErrorResult
  ) {
    api.getAuthConnectAccount(
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func deleteAuthConnectAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.deleteAuthConnectAccount(
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func postAuthConnectFileUpload(
    imageData: Data,
    purpose: String? = nil,
    onProgress: SingleResult<Progress>?,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    api.postAuthConnectFileUpload(
      imageData: imageData,
      purpose: purpose,
      onProgress: onProgress,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func stripeConnectExternalAccount(
    externalAccount: StripeExternalAccountParams,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    guard let params = externalAccount.dictionary() else {
      return onError(APIClientError.unknown)
    }
    api.postAuthConnectAccountExternalAccount(
      params: params,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
