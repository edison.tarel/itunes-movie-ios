//
//  ErrorHandlingService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import FirebaseCrashlytics

class ErrorHandlingService: ErrorHandlingServiceProtocol {
  private let session: SessionServiceProtocol
  private let crashlytics: Crashlytics

  init(
    session: SessionServiceProtocol,
    crashlytics: Crashlytics = .crashlytics()
  ) {
    self.session = session
    self.crashlytics = crashlytics
  }
}

// MARK: - Methods

extension ErrorHandlingService {
  func handleAPIError(_ apiError: APIClientError) {
    handleError(apiError)

    switch apiError {
    case let .failedRequest(info):
      handleFailedRequest(withInfo: info)
    case .dataNotFound, .unknown:
      break
    }
  }

  func handleError(_ error: Error) {
    handleError(
      error,
      info: nil
    )
  }

  func handleError(
    _ error: Error,
    info: [String: Any]?
  ) {
    recordError(
      error,
      info: info
    )
  }
}

// MARK: - Helpers

private extension ErrorHandlingService {
  func handleFailedRequest(
    withInfo info: APIClientFailedRequestInfoType
  ) {
    switch info.errorCode {
    case .httpUnauthorized:
      session.clearSession(shouldBroadcast: true)
    case .emailNotFound,
         .usernameNotFound,
         .passwordNotSupported,
         .unknown:
      break
    }
  }

  func recordError(
    _ error: Error,
    info: [String: Any]?
  ) {
    debugLog(String(describing: error))

    if let userInfo = info {
      debugLog("other info: \(String(describing: userInfo))")

      crashlytics.setCustomValue(
        userInfo as Any,
        forKey: "additionalUserInfo"
      )
    }
    crashlytics.record(error: error)
  }
}
