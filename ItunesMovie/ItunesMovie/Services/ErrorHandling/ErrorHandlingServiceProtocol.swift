//
//  ErrorHandlingServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ErrorHandlingServiceProtocol {
  func handleAPIError(_ apiError: APIClientError)
  
  func handleError(_ error: Error)

  func handleError(
    _ error: Error,
    info: [String: Any]?
  )
}
