//
//  MoviesService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import NSObject_Rx
import RxSwift

class MoviesService {
  private let api: MoviesAPI
  private let dataStorage: MovieStorageProtocol

  init(
    api: MoviesAPI,
    dataStorage: MovieStorageProtocol
  ) {
    self.api = api
    self.dataStorage = dataStorage
  }
}

// MARK: - Methods: Fetch

extension MoviesService: MoviesServiceProtocol {
  func fetchMovies(
    fromApi: Bool = false,
    with parameter: MovieParameter,
    onSuccess: @escaping SingleResult<SearchResult<Movie>>,
    onError: @escaping ErrorResult
  ) {
    if fromApi {
      self.fetchMoviesFromApi(
        with: parameter,
        onSuccess: onSuccess,
        onError: onError
      )
    } else {
      self.fetchMoviesFromStorage(
        onSuccess: onSuccess,
        onError: { _ in
          self.fetchMoviesFromApi(
            with: parameter,
            onSuccess: onSuccess,
            onError: onError
          )
        }
      )
    }
  }

  private func fetchMoviesFromApi(
    with parameter: MovieParameter,
    onSuccess: @escaping SingleResult<SearchResult<Movie>>,
    onError: @escaping ErrorResult
  ) {
    self.api.getMovies(
      with: parameter,
      onSuccess: { response in
        if let movies = response.results {
          self.dataStorage.saveMovies(movies)
        }
        onSuccess(response)
      },
      onError: onError
    )
  }

  private func fetchMoviesFromStorage(
    onSuccess: @escaping SingleResult<SearchResult<Movie>>,
    onError: @escaping ErrorResult
  ) {
    self.dataStorage.getSavedMovies(
      onSuccess: { savedMovies in
        if savedMovies.isNilOrEmpty {
          onError(StorageError.dataNotFound)
        } else {
          let result = SearchResult(resultCount: savedMovies?.count, results: savedMovies)
          onSuccess(result)
        }
      },
      onError: onError
    )
  }

  func fetchMovie(
    with id: Int,
    onSuccess: @escaping SingleResult<Movie?>,
    onError: @escaping ErrorResult
  ) {
    self.dataStorage.getMovie(
      with: id,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
