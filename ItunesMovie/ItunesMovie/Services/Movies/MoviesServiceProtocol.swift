//
//  MoviesServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MoviesServiceProtocol: AppServiceProtocol {
  func fetchMovies(
    fromApi: Bool,
    with parameter: MovieParameter,
    onSuccess: @escaping SingleResult<SearchResult<Movie>>,
    onError: @escaping ErrorResult
  )

  func fetchMovie(
    with id: Int,
    onSuccess: @escaping SingleResult<Movie?>,
    onError: @escaping ErrorResult
  )
}
