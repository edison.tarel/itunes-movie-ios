//
//  SessionService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import Alamofire
import GoogleSignIn

class SessionService: SessionServiceProtocol {
  private(set) var user: User?

  // MARK: Auth

  private(set) var register: RegisterServiceProtocol
  private(set) var login: LoginServiceProtocol
  private(set) var socialAuth: SocialAuthServiceProtocol

  // MARK: Profile

  private(set) var accountVerification: AccountVerificationServiceProtocol
  private(set) var profileSetup: ProfileSetupServiceProtocol
  private(set) var profileRefresh: ProfileRefreshServiceProtocol
  private(set) var editProfile: EditProfileServiceProtocol

  // MARK: Credentials

  private(set) var credentials: CredentialsServiceProtocol

  // MARK: DeAuth

  private(set) var accountDeletion: AccountDeletionServiceProtocol
  private(set) var logout: LogoutServiceProtocol

  private let api: APIClientProtocol
  private let userDefaults: UserDefaults

  init(
    api: APIClientProtocol,
    register: RegisterServiceProtocol,
    login: LoginServiceProtocol,
    socialAuth: SocialAuthServiceProtocol,
    accountVerification: AccountVerificationServiceProtocol,
    profileSetup: ProfileSetupServiceProtocol,
    profileRefresh: ProfileRefreshServiceProtocol,
    editProfile: EditProfileServiceProtocol,
    credentials: CredentialsServiceProtocol,
    accountDeletion: AccountDeletionServiceProtocol,
    logout: LogoutServiceProtocol,
    userDefaults: UserDefaults = .standard
  ) {
    self.api = api
    self.register = register
    self.login = login
    self.socialAuth = socialAuth
    self.accountVerification = accountVerification
    self.profileSetup = profileSetup
    self.profileRefresh = profileRefresh
    self.editProfile = editProfile
    self.credentials = credentials
    self.accountDeletion = accountDeletion
    self.logout = logout
    self.userDefaults = userDefaults

    setup()
  }
}

// MARK: - Setup

private extension SessionService {
  func setup() {
    setupSubServices()
    resumeSession()
  }

  func setupSubServices() {
    setupAuthServices()
    setupProfileServices()
    setupUsersServices()
    setupPostsServices()
    setupCredentialsService()
    setupDeAuthServices()
  }

  func setupAuthServices() {
    register.onAuth = handleAuthResult()
    login.onAuth = handleAuthResult()
    socialAuth.onAuth = handleAuthResult()
  }

  func setupProfileServices() {
    accountVerification.onUserResult = handleUserResult()

    profileSetup.onUserResult = handleUserResult()
    profileSetup.onAvatarResult = handleAvatarResult()
    profileSetup.onSetupFinish = handleProfileSetupFinish()

    profileRefresh.onUserResult = handleUserResult()

    editProfile.onUserResult = handleUserResult()
    editProfile.onAvatarResult = handleAvatarResult()
  }

  func setupUsersServices() {
    // TODO:
  }

  func setupPostsServices() {
    // TODO:
  }

  func setupCredentialsService() {
    credentials.onUserResult = handleUserResult()
  }

  func setupDeAuthServices() {
    accountDeletion.onDeAuth = handleDeAuth()
    accountDeletion.onError = handleDeAuthError()

    logout.onDeAuth = handleDeAuth()
    logout.onError = handleDeAuthError()
  }

  func resumeSession() {
    guard let userInfo = encodedUserData else { return }
    do {
      debugLog("<-- \(try JSONSerialization.jsonObject(with: userInfo))")
      user = try User.decode(userInfo)
    } catch DecodingError.typeMismatch {
      // Force remove cached user info since we can't decode it anymore.
      encodedUserData = nil
    } catch {
      App.shared.errorHandling.handleError(error)
    }
  }
}

// MARK: - User Data Management

private extension SessionService {
  var encodedUserData: Data? {
    set { userDefaults.set(newValue, forKey: #function) }
    get { userDefaults.data(forKey: #function) }
  }

  func cacheUser(_ user: User) {
    self.user = user

    if let newEncodedUserData = try? user.encode() {
      encodedUserData = newEncodedUserData
    }
  }

  func resetUser() {
    user = nil
    encodedUserData = nil
  }
}

// MARK: - Auth Handlers

private extension SessionService {
  func handleAuthResult() -> SingleResult<UserAuthResponse> {
    return { [unowned self] authResult in
      self.cacheUser(authResult.user)
      self.app.api.accessToken = authResult.accessToken
      self.postSesssionNotification(name: .didLogin)
    }
  }
}

// MARK: - Profile Handlers

private extension SessionService {
  func handleUserResult() -> SingleResult<User> {
    return { [unowned self] user in
      self.cacheUser(user)
      self.postSesssionNotification(name: .didRefreshUser)
    }
  }

  func handleAvatarResult() -> SingleResult<Photo> {
    return { [unowned self] avatar in
      guard let currentUser = self.user else { return }
      let updatedUser = User(user: currentUser, avatar: avatar)
      self.cacheUser(updatedUser)
      self.postSesssionNotification(name: .didRefreshUser)
    }
  }

  func handleProfileSetupFinish() -> VoidResult {
    return { [unowned self] in
      self.postSesssionNotification(name: .didFinishProfileSetup)
    }
  }
}

// MARK: - DeAuth Handlers

extension SessionService {
  func clearSession(shouldBroadcast: Bool = false) {
    resetUser()
    app.api.reset()

    if shouldBroadcast {
      postSesssionNotification(name: .didLogout)
    }
  }

  private func handleDeAuth() -> BoolResult {
    return { [unowned self] shouldBroadcast in
      self.clearSession(shouldBroadcast: shouldBroadcast)
    }
  }

  private func handleDeAuthError() -> ErrorResult {
    return { error in
      if case let .failedRequest(info) = error as? APIClientError,
        info.status == .unauthorized {
        // Just let the user log out since they no longer have valid session anyway.
      }
    }
  }
}

// MARK: - Utils

private extension SessionService {
  func postSesssionNotification(name: Notification.Name) {
    NotificationCenter.default.post(name: name, object: self)
  }
}

// MARK: - Getters

extension SessionService {
  var isActive: Bool {
    return (user != nil) && (api.accessToken != nil)
  }
}
