//
//  AccountDeletionService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class AccountDeletionService: AccountDeletionServiceProtocol {
  var onDeAuth: BoolResult?
  var onError: ErrorResult?

  private let api: UsersAPI

  init(api: UsersAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension AccountDeletionService {
  func deleteAccount(
    withId id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.deleteUsers(
      withId: id,
      onSuccess: handleDeAuth(
        shouldBroadcast: true,
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }
}
