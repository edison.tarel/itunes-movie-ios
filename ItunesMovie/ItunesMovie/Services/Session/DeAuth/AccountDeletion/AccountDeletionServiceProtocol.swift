//
//  AccountDeletionServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol AccountDeletionServiceProtocol: SessionDeAuthServiceProtocol {
  func deleteAccount(
    withId id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
