//
//  SessionDeAuthServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol SessionDeAuthServiceProtocol {
  var onDeAuth: BoolResult? { get set }
  var onError: ErrorResult? { get set }
}

extension SessionDeAuthServiceProtocol {
  func handleDeAuth(
    shouldBroadcast: Bool,
    thenExecute handler: @escaping VoidResult
  ) -> VoidResult {
    return { [onDeAuth] in
      onDeAuth?(shouldBroadcast)
      handler()
    }
  }

  func handleDeAuthError(thenExecute handler: @escaping ErrorResult) -> ErrorResult {
    return { [onError] error in
      onError?(error)
      handler(error)
    }
  }
}
