//
//  SocialAuthServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol LoginServiceProtocol: SessionAuthServiceProtocol {
  func loginWithEmail(
    _ email: String,
    password: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func loginWithPhoneNumber(
    _ phoneNumber: String,
    password: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
