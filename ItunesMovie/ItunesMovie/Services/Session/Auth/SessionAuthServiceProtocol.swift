//
//  SessionAuthServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol SessionAuthServiceProtocol {
  var onAuth: SingleResult<UserAuthResponse>? { get set }
}

extension SessionAuthServiceProtocol {
  func handleAuthResponseResult(thenExecute handler: @escaping VoidResult) -> SingleResult<UserAuthResponse> {
    return { [onAuth] authResponse in
      onAuth?(authResponse)
      handler()
    }
  }
}
