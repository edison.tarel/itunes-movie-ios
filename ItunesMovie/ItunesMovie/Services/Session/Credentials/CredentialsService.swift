//
//  CredentialsService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class CredentialsService: CredentialsServiceProtocol {
  var onUserResult: SingleResult<User>?

  private let authChangeAPI: AuthChangeAPI
  private let authVerificationAPI: AuthVerificationAPI

  init(authChangeAPI: AuthChangeAPI,
       authVerificationAPI: AuthVerificationAPI) {
    self.authChangeAPI = authChangeAPI
    self.authVerificationAPI = authVerificationAPI
  }
}

// MARK: - Methods

extension CredentialsService {
  func requestVerificationToken(
    with password: String,
    onSuccess: @escaping SingleResult<String>,
    onError: @escaping ErrorResult
  ) {
    authVerificationAPI.postAuthVerificationToken(
      password: password,
      onSuccess: { onSuccess($0.token) },
      onError: onError
    )
  }

  func requestChangeEmail(
    with newEmail: String,
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authChangeAPI.postAuthChangeEmail(
      email: newEmail,
      token: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func verifyChangeEmail(
    with token: String,
    verificationToken: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authChangeAPI.postAuthChangeEmailVerify(
      token: token,
      verificationToken: verificationToken,
      onSuccess: handleVerifySuccess(
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }

  func requestChangePhoneNumber(
    with newPhoneNumber: String,
    token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authChangeAPI.postAuthChangePhoneNumber(
      phoneNumber: newPhoneNumber,
      token: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func verifyChangePhoneNumber(
    with token: String,
    verificationToken: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authChangeAPI.postAuthChangePhoneNumberVerify(
      token: token,
      verificationToken: verificationToken,
      onSuccess: handleVerifySuccess(
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }

  func changePassword(
    to newPassword: String,
    oldPassword: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    authChangeAPI.postAuthChangePassword(
      newPassword: newPassword,
      oldPassword: oldPassword,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
