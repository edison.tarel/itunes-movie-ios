//
//  ProfileSetupService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ProfileSetupService: ProfileSetupServiceProtocol {
  var onUserResult: SingleResult<User>?
  var onAvatarResult: SingleResult<Photo>?
  var onSetupFinish: VoidResult?

  private let api: AuthProfileAPI

  init(api: AuthProfileAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension ProfileSetupService {
  func setName(
    _ name: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let params = AuthProfileParams(fullName: name)
    api.putAuthProfile(
      with: params,
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthProfileAvatar(
      with: data,
      onSuccess: handleAvatarResult(thenExecute: onSuccess),
      onError: onError
    )
  }

  func finishSetup() {
    onSetupFinish?()
  }
}
