//
//  ProfileSetupServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ProfileSetupServiceProtocol: SessionProfileServiceProtocol, AvatarServiceProtocol {
  var onSetupFinish: VoidResult? { get set }

  func setName(
    _ name: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func finishSetup()
}
