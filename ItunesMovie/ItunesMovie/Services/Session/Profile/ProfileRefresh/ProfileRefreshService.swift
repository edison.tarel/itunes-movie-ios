//
//  ProfileRefreshService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ProfileRefreshService: ProfileRefreshServiceProtocol {
  var onUserResult: SingleResult<User>?

  private let api: AuthProfileAPI

  init(api: AuthProfileAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension ProfileRefreshService {
  func refreshProfile(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.getAuthProfile(
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }
}
