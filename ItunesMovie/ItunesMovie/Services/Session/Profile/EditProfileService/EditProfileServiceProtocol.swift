//
//  EditProfileServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol EditProfileServiceProtocol: SessionProfileServiceProtocol, AvatarServiceProtocol {
  func setInfo(
    fullName: String,
    birthdate: Date?,
    description: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
