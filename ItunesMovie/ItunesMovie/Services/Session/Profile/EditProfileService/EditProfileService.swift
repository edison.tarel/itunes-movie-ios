//
//  EditProfileService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

class EditProfileService: EditProfileServiceProtocol {
  var onUserResult: SingleResult<User>?
  var onAvatarResult: SingleResult<Photo>?
  
  private let api: AuthProfileAPI
  
  init(api: AuthProfileAPI) {
    self.api = api
  }
}

extension EditProfileService {
  func setInfo(
    fullName: String,
    birthdate: Date?,
    description: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    var birthdateText: String?
    if let bd = birthdate {
      birthdateText = Constants.Formatters.birthdateFormatter.string(from: bd)
    }
    
    let params = AuthProfileParams(
      fullName: fullName,
      birthdate: birthdateText,
      description: description
    )
    api.putAuthProfile(
      with: params,
      onSuccess: handleUserResult(thenExecute: onSuccess),
      onError: onError
    )
  }
  
  func setPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postAuthProfileAvatar(
      with: data,
      onSuccess: handleAvatarResult(thenExecute: onSuccess),
      onError: onError
    )
  }
}
