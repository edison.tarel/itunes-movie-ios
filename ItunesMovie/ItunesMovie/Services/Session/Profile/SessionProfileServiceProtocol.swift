//
//  SessionProfileServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol SessionProfileServiceProtocol {
  var onUserResult: SingleResult<User>? { get set }
}

extension SessionProfileServiceProtocol {
  func handleUserResult(thenExecute handler: @escaping VoidResult) -> SingleResult<User> {
    return { [onUserResult] user in
      onUserResult?(user)
      handler()
    }
  }
}
