//
//  AvatarServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AvatarServiceProtocol {
  var onAvatarResult: SingleResult<Photo>? { get set }
}

extension AvatarServiceProtocol {
  func handleAvatarResult(thenExecute handler: @escaping VoidResult) -> SingleResult<Photo> {
    return { [onAvatarResult] avatar in
      onAvatarResult?(avatar)
      handler()
    }
  }
}
