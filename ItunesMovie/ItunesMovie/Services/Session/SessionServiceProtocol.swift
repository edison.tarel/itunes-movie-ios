//
//  SessionServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol SessionServiceProtocol: AppServiceProtocol {
  var isActive: Bool { get }
  var user: User? { get }

  var register: RegisterServiceProtocol { get }
  var login: LoginServiceProtocol { get }
  var socialAuth: SocialAuthServiceProtocol { get }

  var accountVerification: AccountVerificationServiceProtocol { get }
  var profileSetup: ProfileSetupServiceProtocol { get }
  var profileRefresh: ProfileRefreshServiceProtocol { get }

  var credentials: CredentialsServiceProtocol { get }

  var logout: LogoutServiceProtocol { get }

  func clearSession(shouldBroadcast: Bool)
}
