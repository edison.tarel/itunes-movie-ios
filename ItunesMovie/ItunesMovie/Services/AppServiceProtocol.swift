//
//  AppServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import RxSwift
import NSObject_Rx

protocol AppServiceProtocol: AnyObject {
}

// MARK: - Defaults

extension AppServiceProtocol {
  // TODO: Remove. Avoid using
  var app: App { .shared }
}

// MARK: - Notifications

extension AppServiceProtocol {
  private var notificationCenter: NotificationCenter { .default }

  func postNotification(withName name: Notification.Name) {
    notificationCenter.post(
      name: name,
      object: self
    )
  }

  func bindNotification(
    withName name: Notification.Name,
    onPost: @escaping VoidResult,
    toObserver observer: HasDisposeBag
  ) {
    notificationCenter.rx
      .notification(
        name,
        object: self
      )
      .mapToVoid()
      .bind(onNext: onPost)
      .disposed(by: observer.disposeBag)
  }

}
