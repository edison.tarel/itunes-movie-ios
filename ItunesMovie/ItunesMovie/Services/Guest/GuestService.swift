//
//  GuestService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class GuestService: AppServiceProtocol {
  private(set) var usernameChecker: UsernameCheckerServiceProtocol
  private(set) var password: PasswordServiceProtocol

  init(
    usernameChecker: UsernameCheckerServiceProtocol,
    password: PasswordServiceProtocol
  ) {
    self.usernameChecker = usernameChecker
    self.password = password
  }
}
