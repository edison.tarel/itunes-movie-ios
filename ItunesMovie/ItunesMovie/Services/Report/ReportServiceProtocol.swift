//
//  ReportServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ReportServiceProtocol {
  func fetchReportCategories(
    onSuccess: @escaping SingleResult<[ReportCategory]>,
    onError: @escaping ErrorResult
  )

  func reportUser(
    with userId: Int,
    report: Report,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
