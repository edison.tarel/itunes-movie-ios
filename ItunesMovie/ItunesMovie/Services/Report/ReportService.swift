//
//  ReportService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ReportService: ReportServiceProtocol {
  private var reportCategories: [ReportCategory]?

  private let api: ReportAPI

  init(api: ReportAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension ReportService {
  func fetchReportCategories(
    onSuccess: @escaping SingleResult<[ReportCategory]>,
    onError: @escaping ErrorResult
  ) {
    if let cached = reportCategories {
      return onSuccess(cached)
    }

    api.getReportCategories(
      onSuccess: handleFetchReportCategoriesSuccess(
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }

  func reportUser(
    with userId: Int,
    report: Report,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postReport(
      with: userId,
      report: report,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Handlers

private extension ReportService {
  func handleFetchReportCategoriesSuccess(
    thenExecute handler: @escaping SingleResult<[ReportCategory]>
  ) -> SingleResult<[ReportCategory]> {
    return { [weak self] reportCategories in
      guard let s = self else { return }
      s.reportCategories = reportCategories
      handler(reportCategories)
    }
  }
}
