//
//  PostsService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import NSObject_Rx
import RxSwift

class PostsService: PostsServiceProtocol {
  private let api: PostsAPI

  private var cachedPosts: [Post] = []
  private var cachedComments: [Int: [Comment]] = [:]

  init(api: PostsAPI) {
    self.api = api
  }
}

// MARK: - Methods: Retrieval

extension PostsService {
  func posts() -> [Post] {
    cachedPosts
  }

  func post(withId id: Int) -> Post? {
    cachedPosts.first(where: { $0.id == id })
  }

  func commentsInPost(withId id: Int) -> [Comment] {
    cachedComments[id] ?? []
  }
}

// MARK: - Methods: Fetch

extension PostsService {
  func fetchPosts(
    userId: Int?,
    page: APIPage,
    include: [PostsIncludes],
    onSuccess: @escaping SingleResult<[Post]>,
    onError: @escaping ErrorResult
  ) {
    api.getPosts(
      userId: userId,
      page: page,
      include: include,
      onSuccess: { [unowned self] posts in
        if page.index == 1 {
          self.cachedPosts.removeAll()
        }
        self.cachedPosts.append(contentsOf: posts)

        onSuccess(posts)

        self.postNotification(
          withName: .didRefreshPosts
        )
      },
      onError: onError
    )
  }

  func likePost(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postPostsFavorite(
      with: id,
      onSuccess: { [unowned self] in
        self.replacePost(
          withPostId: id,
          usingCopier: { $0.toggleLikeCopy() }
        )

        onSuccess()

        self.postNotification(
          withName: .didRefreshPost(withId: id)
        )
      },
      onError: onError
    )
  }

  func unlikePost(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postPostsUnfavorite(
      with: id,
      onSuccess: { [unowned self] in
        self.replacePost(
          withPostId: id,
          usingCopier: { $0.toggleLikeCopy() }
        )

        onSuccess()

        self.postNotification(
          withName: .didRefreshPost(withId: id)
        )
      },
      onError: onError
    )
  }

  func createPost(
    imageData: Data,
    description: String,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    api.postPosts(
      imageData: imageData,
      description: description,
      onProgress: onProgress,
      onSuccess: { [unowned self] post in
        self.cachedPosts.insert(post, at: 0)

        onSuccess()

        self.postNotification(
          withName: .didRefreshPosts
        )
      },
      onError: onError
    )
  }

  func fetchPostComments(
    postId: Int,
    include: [PostsCommentIncludes],
    onSuccess: @escaping SingleResult<[Comment]>,
    onError: @escaping ErrorResult
  ) {
    api.getPostsComments(
      postId: postId,
      include: include,
      onSuccess: { [unowned self] comments in
        self.cachedComments[postId] = comments

        onSuccess(comments)

        self.postNotification(
          withName: .didRefreshCommentsForPost(withId: postId)
        )
      },
      onError: onError
    )
  }

  func createPostComment(
    postId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) {
    api.postPostsComments(
      postId: postId,
      body: body,
      onSuccess: { [unowned self] comment in
        self.replacePost(
          withPostId: postId,
          usingCopier: { $0.incrementCommentCountCopy() }
        )
        self.cachedComments[postId]?.append(comment)

        onSuccess(comment)

        self.postNotification(
          withName: .didRefreshPost(withId: postId)
        )
        self.postNotification(
          withName: .didRefreshCommentsForPost(withId: postId)
        )
      },
      onError: onError
    )
  }

  func replyToComment(
    withId commentId: Int,
    comment: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  ) {
    api.postCommentsResponses(
      commentId: commentId,
      body: comment,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Bind Methods

extension PostsService {
  func bindPosts(
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  ) {
    bindNotification(
      withName: .didRefreshPosts,
      onPost: onRefresh,
      toObserver: observer
    )
  }

  func bindPost(
    withId id: Int,
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  ) {
    bindNotification(
      withName: .didRefreshPost(withId: id),
      onPost: onRefresh,
      toObserver: observer
    )
  }

  func bindCommentsInPost(
    withId id: Int,
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  ) {
    bindNotification(
      withName: .didRefreshCommentsForPost(withId: id),
      onPost: onRefresh,
      toObserver: observer
    )
  }
}

// MARK: - Helpers

private extension PostsService {
  func replacePost(
    withPostId postId: Int,
    usingCopier copier: SingleResultWithReturn<Post, Post>
  ) {
    guard let index = cachedPosts.firstIndex(where: { $0.id == postId }) else {
      return
    }
    let oldPost = cachedPosts[index]
    cachedPosts[index] = copier(oldPost)
  }
}

// MARK: - Notifications

private extension Notification.Name {
  static var prefix: String { .init(describing: PostsService.self) }

  static var didRefreshPosts: Notification.Name {
    .init("\(prefix).didRefreshPosts")
  }

  static func didRefreshPost(withId id: Int) -> Notification.Name {
    .init("\(prefix).didRefreshPost.\(id)")
  }

  static func didRefreshCommentsForPost(withId id: Int) -> Notification.Name {
    .init("\(prefix).didRefreshCommentsForPost.\(id)")
  }
}
