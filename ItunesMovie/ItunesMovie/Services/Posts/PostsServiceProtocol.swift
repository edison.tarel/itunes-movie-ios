//
//  PostsServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import NSObject_Rx

protocol PostsServiceProtocol: AppServiceProtocol {
  func posts() -> [Post]
  
  func post(withId id: Int) -> Post?
  
  func commentsInPost(withId id: Int) -> [Comment]

  func fetchPosts(
    userId: Int?,
    page: APIPage,
    include: [PostsIncludes],
    onSuccess: @escaping SingleResult<[Post]>,
    onError: @escaping ErrorResult
  )

  func likePost(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func unlikePost(
    with id: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func createPost(
    imageData: Data,
    description: String,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func fetchPostComments(
    postId: Int,
    include: [PostsCommentIncludes],
    onSuccess: @escaping SingleResult<[Comment]>,
    onError: @escaping ErrorResult
  )

  func createPostComment(
    postId: Int,
    body: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  )

  func replyToComment(
    withId commentId: Int,
    comment: String,
    onSuccess: @escaping SingleResult<Comment>,
    onError: @escaping ErrorResult
  )

  func bindPosts(
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  )

  func bindPost(
    withId id: Int,
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  )

  func bindCommentsInPost(
    withId id: Int,
    onRefresh: @escaping VoidResult,
    observer: HasDisposeBag
  )
}
