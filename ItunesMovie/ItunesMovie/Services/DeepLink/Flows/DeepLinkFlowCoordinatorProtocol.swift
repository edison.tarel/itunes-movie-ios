//
//  UnopenedAppDeeplinkFlowCoordinator.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol DeepLinkFlowCoordinatorProtocol {}

extension DeepLinkFlowCoordinatorProtocol {
  func dismissToTabBar(animated: Bool) {
    tabBarController?.dismiss(animated: animated)
  }

  func popToRootInSelectedTab(animated: Bool) {
    selectedTabItemController?.popToRootViewController(animated: animated)
  }
}

// MARK: - Getters

extension DeepLinkFlowCoordinatorProtocol {
  var tabBarController: TabBarController? {
    AppDelegate.shared.window?.rootViewController as? TabBarController
  }

  var selectedTabItemController: UINavigationController? {
    guard let tabBarController = tabBarController,
      let tabItemControllers = tabBarController.viewControllers else { return nil }

    return tabItemControllers[tabBarController.selectedIndex] as? UINavigationController
  }
}
