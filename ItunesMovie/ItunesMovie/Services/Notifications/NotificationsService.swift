//
//  NotificationsService.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class NotificationsService: NotificationsServiceProtocol {
  private let api: NotificationsAPI

  init(api: NotificationsAPI) {
    self.api = api
  }
}

// MARK: - Methods

extension NotificationsService {
  func fetchNotificationsForToday(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) {
    api.getNotificationsToday(
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func fetchNotificationsForThisWeek(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) {
    api.getNotificationsThisWeek(
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func fetchNotificationsUnread(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  ) {
    api.getNotificationsUnread(
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
