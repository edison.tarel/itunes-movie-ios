//
//  NotificationsServiceProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol NotificationsServiceProtocol {
  func fetchNotificationsForToday(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  )

  func fetchNotificationsForThisWeek(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  )

  func fetchNotificationsUnread(
    onSuccess: @escaping SingleResult<[AppNotification]>,
    onError: @escaping ErrorResult
  )
}
