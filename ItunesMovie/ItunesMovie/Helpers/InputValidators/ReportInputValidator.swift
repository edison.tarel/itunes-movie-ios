//
//  ReportInputValidator.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct ReportInputValidator: InputValidator {
  typealias ValidateMethod = (Inputs) -> Result<ValidInputs, ValidationError>

  static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let category = inputs.category else {
      return .failure(.requiredCategory)
    }

    let validInputs = ValidInputs(
      reasonId: category.id,
      description: inputs.description ?? "",
      attachments: inputs.attachments
    )

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

extension ReportInputValidator {
  typealias Inputs = (
    category: ReportCategory?,
    description: String?,
    attachments: [Data]
  )

  typealias ValidInputs = Report

  enum ValidationError: LocalizedError {
    case requiredCategory

    var errorDescription: String? {
      switch self {
      case .requiredCategory:
        return S.reportErrorReasonRequired()
      }
    }
  }
}
