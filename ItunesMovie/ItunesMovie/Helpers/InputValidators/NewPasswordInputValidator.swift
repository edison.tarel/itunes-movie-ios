//
//  NewPasswordInputValidator.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct NewPasswordInputValidator: InputValidator {
  private static let minLength: Int = 8
  private static let maxLength: Int = 32

  static func validate(_ inputs: Inputs) -> Result<ValidInputs, ValidationError> {
    guard let password = inputs, !password.isEmpty else {
      return .failure(.requiredPassword)
    }

    let isValid = InputValidatorUtil.isValidPassword(
      password,
      minLength: minLength,
      maxLenth: maxLength
    )

    guard isValid else {
      return .failure(.invalidPassword)
    }

    let validInputs = ValidInputs(password)

    return .success(validInputs)
  }
}

// MARK: - Type Declarations

extension NewPasswordInputValidator {
  typealias Inputs = String?
  typealias ValidInputs = String

  enum ValidationError: LocalizedError {
    case requiredPassword, invalidPassword

    var errorDescription: String? {
      switch self {
      case .requiredPassword:
        return S.passwordFieldErrorMissingValue()
      case .invalidPassword:
        return S.passwordFieldErrorInvalidValue(
          NewPasswordInputValidator.minLength,
          NewPasswordInputValidator.maxLength
        )
      }
    }
  }
}
