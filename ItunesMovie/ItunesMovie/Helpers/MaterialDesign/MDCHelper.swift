//
//  MDCHelper.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents.MaterialTextFields_ColorThemer
import MaterialComponents.MaterialTextFields_TypographyThemer
import UIKit

/// MaterialDesignComponents Helper class
///
/// Theming guidelines:
/// - https://material.io/design/components/text-fields.html#theming
///
class MDCHelper {
  static let shared = MDCHelper()

  private(set) lazy var containerScheme: MDCContainerScheme = {
    let container = MDCContainerScheme()
    container.typographyScheme = self.typographyScheme
    container.colorScheme = self.colorScheme

    return container
  }()

  private(set) lazy var typographyScheme: MDCTypographyScheme = {
    let scheme = MDCTypographyScheme()
    scheme.headline1 = UIFont.largeTitle
    scheme.subtitle1 = UIFont.systemFont(ofSize: 17, weight: .regular)
    scheme.caption = UIFont.systemFont(ofSize: 12, weight: .regular)

    return scheme
  }()

  private(set) lazy var colorScheme: MDCSemanticColorScheme = {
    let scheme = MDCSemanticColorScheme()
    scheme.primaryColor = Styles.Colors.primaryColor
    scheme.errorColor = Styles.Colors.Form.errorColor

    return scheme
  }()
}

// MARK: - Filled TextField Style

extension MDCHelper {
  static func inputController(for field: MDCTextField) -> MDCTextInputControllerFilled {
    let controller = MDCTextInputControllerFilled(textInput: field)
    controller.applyTheme(withScheme: MDCHelper.shared.containerScheme)

    return controller
  }

  static func inputController(for field: MDCMultilineTextField) -> MDCTextInputControllerFilled {
    let controller = MDCTextInputControllerFilled(textInput: field)
    controller.applyTheme(withScheme: MDCHelper.shared.containerScheme)
    return controller
  }
}

// MARK: - Underline TextField Style

extension MDCHelper {
  static func underlineInputController(for field: MDCTextField) -> MDCTextInputControllerUnderline {
    let controller = MDCTextInputControllerUnderline(textInput: field)

    controller.isFloatingEnabled = true
    controller.underlineHeightNormal = 1
    controller.underlineHeightActive = 2

    controller.applyTheme(withScheme: MDCHelper.shared.containerScheme)

    // This is because the new theming doesn't apply this to the Underline Textfields anymore
    // as it's already being deprecated.
    field.font = MDCHelper.shared.typographyScheme.subtitle1
    
    field.leadingUnderlineLabel.numberOfLines = 0
    field.leadingUnderlineLabel.lineBreakMode = .byWordWrapping

    return controller
  }

  static func underlineInputController(for field: MDCMultilineTextField) -> MDCTextInputControllerUnderline {
    let controller = MDCTextInputControllerUnderline(textInput: field)

    controller.isFloatingEnabled = true

    controller.applyTheme(withScheme: MDCHelper.shared.containerScheme)

    return controller
  }
}
