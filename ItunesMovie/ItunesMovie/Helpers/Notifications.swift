//
//  Notifications.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

// MARK: - Session

extension Notification.Name {
  private static var prefix: String { "com.ItunesMovie.notification.name.session" }

  static let didLogin = Notification.Name(rawValue: "\(prefix).didLogin")
  static let didLogout = Notification.Name(rawValue: "\(prefix).didLogout")
  static let didRefreshUser = Notification.Name(rawValue: "\(prefix).didRefreshUser")
  static let didFinishProfileSetup = Notification.Name(rawValue: "\(prefix).didFinishProfileSetup")
  static let didFinishShareFeed = Notification.Name(rawValue: "\(prefix).didFinishShareFeed")
}
