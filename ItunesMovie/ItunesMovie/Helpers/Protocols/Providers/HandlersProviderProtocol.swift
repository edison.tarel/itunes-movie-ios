//
//  HandlersProviderProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol HandlersProviderProtocol where Self: UIViewController {
  func handleSuccess(
    then onComplete: VoidResult?
  ) -> VoidResult

  func handleSuccessWithMessage(
    then onComplete: SingleResult<String>?
  ) -> SingleResult<String>
  
  func handleProgress() -> SingleResult<Progress>

  func handleError(
    then onComplete: ErrorResult?
  ) -> ErrorResult
}

// MARK: - Defaults

extension HandlersProviderProtocol where Self: PresentersProviderProtocol {
  func handleSuccess(
    then onComplete: VoidResult? = nil
  ) -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      onComplete?()
    }
  }

  func handleSuccessWithMessage(
    then onComplete: SingleResult<String>? = nil
  ) -> SingleResult<String> {
    return { [weak self] message in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.infoPresenter.presentSuccessInfo(message: message)
      onComplete?(message)
    }
  }
  
  func handleProgress() -> SingleResult<Progress> {
    return { [weak self] progress in
      guard let s = self else { return }
      s.progressPresenter.presentProgress(
        value: Float(progress.fractionCompleted),
        from: s
      )
    }
  }

  func handleError(
    then onComplete: ErrorResult? = nil
  ) -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.infoPresenter.presentErrorInfo(error: error)
      onComplete?(error)
    }
  }
}
