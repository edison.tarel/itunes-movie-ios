//
//  InAppPurchaseFlowPresenter.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol InAppPurchaseFlowPresenterProtocol {
  func presentInAppPurchaseFlow(from controller: UIViewController)
}

class InAppPurchaseFlowPresenter: InAppPurchaseFlowPresenterProtocol {
  func presentInAppPurchaseFlow(from controller: UIViewController) {
    let vc = R.storyboard.inAppPurchase.subscriptionController()!

    let nc = UINavigationController(rootViewController: vc)
    nc.modalPresentationStyle = .overFullScreen

    controller.present(nc, animated: true)
  }
}
