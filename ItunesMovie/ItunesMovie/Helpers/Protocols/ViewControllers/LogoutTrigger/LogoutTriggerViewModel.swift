//
//  LogoutTriggerViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class LogoutTriggerViewModel: LogoutTriggerViewModelProtocol {
  
  private let session: SessionService
  
  init(session: SessionService = App.shared.session) {
    self.session = session
  }
  
}

extension LogoutTriggerViewModel {

  func logout() {
    session.logout.logout(
      shouldBroadcast: false,
      onSuccess: DefaultClosure.voidResult(),
      onError: DefaultClosure.singleResult()
    )
  }
  
}
