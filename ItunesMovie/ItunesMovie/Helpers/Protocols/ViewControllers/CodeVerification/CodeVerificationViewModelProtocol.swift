//
//  CodeVerificationViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol CodeVerificationViewModelProtocol {
  var titleText: String { get }
  var messageText: String { get }
  var subMessageText: String? { get }
  
  func verify(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
  
  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
