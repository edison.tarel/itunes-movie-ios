//
//  ReportController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import ImageViewer_swift
import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SimpleCheckbox
import SVProgressHUD
import YPImagePicker

class ReportController: ViewController {
  var viewModel: ReportViewModelProtocol!

  @IBOutlet private(set) var reasonField: MDCTextField!
  @IBOutlet private(set) var descriptionField: MDCMultilineTextField!
  @IBOutlet private(set) var uploadImageLabel: UILabel!
  @IBOutlet private(set) var addButton: FormButton!
  @IBOutlet private(set) var checkBox: Checkbox!
  @IBOutlet private(set) var checkBoxLabel: UILabel!

  private(set) var reasonInputController: MDCTextInputControllerOutlined!
  private(set) var descriptionInputController: MDCTextInputControllerOutlinedTextArea!

  private(set) var categoryPickerView: GenericPickerView!

  private(set) var imagePicker: YPImagePicker!
  private(set) var imagePickerConfig: YPImagePickerConfiguration!
  private(set) var attachImages: [YPMediaItem] = []

  private let numberOfAttachmentsPerRow = 3
}

// MARK: - LifeCycle

extension ReportController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }
}

// MARK: - Setup

private extension ReportController {
  func setup() {
    title = S.reportTitle()

    setupCategoryField()
    setupComponents()
    setupImagePicker()
    setupCheckbox()

    viewModel.fetchCategories(
      onSuccess: handleSuccess(),
      onError: handleError(
        then: handleFetchError()
      )
    )
  }

  func setupCategoryField() {
    categoryPickerView = GenericPickerView()
    categoryPickerView.viewModel = viewModel.categoryPickerVM
    categoryPickerView.textField = reasonField
  }

  func setupComponents() {
    reasonField.rightView = UIImageView(image: R.image.chevronDownGray())
    reasonField.rightViewMode = .always
    reasonInputController = MDCTextInputControllerOutlined(textInput: reasonField)

    descriptionField.placeholder = S.description()
    descriptionInputController = MDCTextInputControllerOutlinedTextArea(textInput: descriptionField)

    addButton.backgroundColor = .clear
    addButton.isHidden = viewModel.areAttachmentsNotAllowed
    uploadImageLabel.isHidden = addButton.isHidden
  }

  func setupImagePicker() {
    let numOfAttachments = min(viewModel.optionNumberOfAttachments, numberOfAttachmentsPerRow)
    imagePickerConfig = YPImagePickerConfiguration()
    imagePickerConfig.showsPhotoFilters = false
    imagePickerConfig.startOnScreen = YPPickerScreen.library
    imagePickerConfig.library.defaultMultipleSelection = true
    imagePickerConfig.library.mediaType = YPlibraryMediaType.photo
    imagePickerConfig.library.skipSelectionsGallery = true
    imagePickerConfig.library.maxNumberOfItems = Int(numOfAttachments)
    imagePicker = YPImagePicker(configuration: imagePickerConfig)
  }

  func setupCheckbox() {
    guard let optionCheckboxTitle = viewModel.optionCheckboxTitle else { return }

    checkBox.isHidden = false
    checkBox.borderCornerRadius = 5
    checkBox.borderLineWidth = 1
    checkBox.uncheckedBorderColor = .darkGray
    checkBox.checkedBorderColor = .darkGray
    checkBox.checkmarkStyle = .tick
    checkBoxLabel.isHidden = false
    checkBoxLabel.text = optionCheckboxTitle

    if viewModel.optionNumberOfAttachments == 0 {
      checkBox.autoPinEdge(.top, to: .top, of: uploadImageLabel)
      checkBoxLabel.autoPinEdge(.top, to: .top, of: uploadImageLabel)
    }
  }
}

// MARK: - Actions

private extension ReportController {
  @IBAction
  func addButtonTapped(_ sender: AnyObject) {
    imagePicker.didFinishPicking { [weak self] items, _ in
      guard let s = self else { return }
      s.imagePicker.dismiss(animated: true)
      if items.isEmpty { return }
      s.attachImages = items
      s.processAttachPhotos(items)
    }
    present(imagePicker, animated: true)
  }

  @IBAction
  func reportButtonTapped(_ sender: AnyObject) {
    let attachments = generateAttachmentsData()
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.submitReport(
      with: descriptionField.text,
      attachments: attachments,
      onSuccess: handleSubmitSuccess(),
      onError: handleError()
    )
  }

  @objc
  func onThumbnailCloseTapped(_ button: UIButton) {
    let index: Int = button.tag - 1
    guard index < attachImages.count else { return }
    attachImages.remove(at: index)
    processAttachPhotos(attachImages)
    imagePickerConfig.library.preselectedItems = attachImages
    imagePicker = YPImagePicker(configuration: imagePickerConfig)
  }
}

// MARK: - Handlers

private extension ReportController {
  func handleFetchError() -> ErrorResult {
    return { [weak self] _ in
      guard let s = self else { return }
      s.dismissModal()
    }
  }

  func handleSubmitSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.infoPresenter.presentSuccessInfo(message: S.statusSuccess())
      s.dismissModal()
    }
  }
}

// MARK: - Utils

private extension ReportController {
  func generateAttachmentsData() -> [Data] {
    var images: [UIImage] = []
    for case let .photo(photo) in attachImages {
      images.append(photo.image)
    }
    return images.compactMap { $0.jpegData(compressionQuality: 0.7) }
  }

  func processAttachPhotos(_ photos: [YPMediaItem]) {
    let attachmentMargin: CGFloat = 10

    let frame = addButton.frame
    let yAxis = frame.origin.y
    var xAxis = frame.origin.x
    if photos.count < 3 {
      xAxis += frame.size.width + attachmentMargin
    }
    for view in view.subviews where view.tag > 0 {
      view.removeFromSuperview()
    }
    var index: Int = 1
    var images: [UIImage] = []
    for case let .photo(photo) in photos {
      images.append(photo.image)
    }
    for image in images {
      let thumbnail = UIView()
      thumbnail.tag = index
      thumbnail.frame = CGRect(origin: CGPoint(x: xAxis, y: yAxis), size: frame.size)
      thumbnail.clipsToBounds = true
      thumbnail.layer.cornerRadius = 8
      view.addSubview(thumbnail)

      let imageView = UIImageView(image: image)
      thumbnail.addSubview(imageView)
      imageView.autoPinEdgesToSuperviewEdges()
      imageView.setupImageViewer(
        images: images,
        initialIndex: index - 1,
        options: [.closeIcon(R.image.close()!)]
      )

      let closeButton = UIButton()
      closeButton.tag = index
      closeButton.addTarget(self, action: #selector(onThumbnailCloseTapped(_:)), for: .touchUpInside)
      closeButton.frame = CGRect(origin: .zero, size: CGSize(width: 24, height: 24))
      closeButton.setImage(R.image.remove(), for: .normal)
      thumbnail.addSubview(closeButton)
      closeButton.autoPinEdge(toSuperviewEdge: .top)
      closeButton.autoPinEdge(toSuperviewEdge: .right)

      index += 1
      xAxis += frame.size.width + attachmentMargin
    }
  }
}
