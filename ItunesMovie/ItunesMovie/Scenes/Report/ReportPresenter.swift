//
//  ReportPresenter.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

protocol ReportPresenter where Self: ViewController {}

// MARK: Methods

extension ReportPresenter {
  func presentReportPage(
    with viewModel: ReportViewModelProtocol,
    modalPresentationStyle: UIModalPresentationStyle = .fullScreen
  ) {
    let vc = R.storyboard.report.reportController()!
    vc.viewModel = viewModel

    let nc = NavigationController(rootViewController: vc)
    nc.modalPresentationStyle = modalPresentationStyle
    self.present(nc, animated: true)
  }
}
