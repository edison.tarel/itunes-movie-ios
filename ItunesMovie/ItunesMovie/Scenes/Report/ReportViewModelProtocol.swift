//
//  ReportViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ReportViewModelProtocol {
  var optionCheckboxTitle: String? { get }
  var optionNumberOfAttachments: Int { get }

  var categoryPickerVM: GenericPickerViewModelProtocol { get }

  func fetchCategories(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func submitReport(
    with description: String?,
    attachments: [Data],
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - Defaults: Overridable

extension ReportViewModelProtocol {
  var optionCheckboxTitle: String? { nil }
  var optionNumberOfAttachments: Int { 3 }
}

// MARK: - Utils

extension ReportViewModelProtocol {
  var areAttachmentsNotAllowed: Bool { optionNumberOfAttachments == 0 }
}
