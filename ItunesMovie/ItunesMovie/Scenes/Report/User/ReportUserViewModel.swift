//
//  ReportUserViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import RxSwift

class ReportUserViewModel: ReportViewModelProtocol {
  let categoryPickerVM: GenericPickerViewModelProtocol

  private let userId: Int
  private let service: ReportServiceProtocol

  private var categories: [ReportCategory]?
  private var selectedCategory: ReportCategory?

  init(
    userId: Int,
    service: ReportServiceProtocol = App.shared.report,
    categoryPickerVM: GenericPickerViewModelProtocol = GenericPickerViewModel()
  ) {
    self.userId = userId
    self.service = service
    self.categoryPickerVM = categoryPickerVM

    categoryPickerVM.onOptionIndexSelect = handleCategoryIndexSelect()
  }
}

// MARK: - Methods

extension ReportUserViewModel {
  func fetchCategories(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.fetchReportCategories(
      onSuccess: handleCategoriesFetchSuccess(),
      onError: onError
    )
  }

  func submitReport(
    with description: String?,
    attachments: [Data],
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let inputs = ReportInputValidator.Inputs(
      category: selectedCategory,
      description: description,
      attachments: attachments
    )

    let result = ReportInputValidator.validate(inputs)
    switch result {
    case let .success(report):
      service.reportUser(
        with: userId,
        report: report,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Handlers

private extension ReportUserViewModel {
  func handleCategoriesFetchSuccess() -> SingleResult<[ReportCategory]> {
    return { [weak self] categories in
      guard let s = self else { return }
      s.categories = categories
      let categoryLabels = categories.map { $0.label }
      s.categoryPickerVM.setOptions(categoryLabels)
    }
  }

  func handleCategoryIndexSelect() -> SingleResult<Int> {
    return { [weak self] index in
      guard let s = self else { return }

      if let selectedCategory = s.categories?[index] {
        s.selectedCategory = selectedCategory
      }
    }
  }
}
