//
//  SubscriptionController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class SubscriptionController: UIViewController {
  @IBOutlet private var continueButton: UIButton!
}

// MARK: - Lifecycle

extension SubscriptionController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupNavBar()
  }
}

// MARK: - Setup

private extension SubscriptionController {
  func setup() {
    continueButton.layer.cornerRadius = 8
  }

  func setupNavBar() {
    guard let nb = navigationController?.navigationBar else { return }
    nb.setTransparent()
  }
}
