//
//  PostsViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PostsViewModelProtocol: ViewModelProtocol {
  var posts: [Post] { get }

  func reloadFeed(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func fetchPosts(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func likePost(with id: Int)

  func unlikePost(with id: Int)
}
