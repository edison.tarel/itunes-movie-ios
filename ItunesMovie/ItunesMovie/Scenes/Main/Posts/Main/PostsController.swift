//
//  PostsController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Material
import SkeletonView
import UIKit

final class PostsController: ViewController, ReportPresenter {
  lazy var viewModel: PostsViewModelProtocol! = PostsViewModel()

  @IBOutlet private(set) var collectionView: UICollectionView!

  private(set) var refreshControl: UIRefreshControl!
}

// MARK: - LifeCycle

extension PostsController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }
}

// MARK: - Setup

private extension PostsController {
  func setup() {
    setupVM()
    setupTitle()
    setupCollectionView()
  }
  
  func setupVM() {
    viewModel.onRefresh = trigger(type(of: self).refresh)
  }

  func setupTitle() {
    let titleLabel = UILabel()
    titleLabel.text = S.appName()
    titleLabel.font = R.font.sfProDisplayBold(size: 28)
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: titleLabel)
  }

  func setupCollectionView() {
    refreshControl = UIRefreshControl()
    
    collectionView.register(
      UINib(resource: R.nib.postCell),
      forCellWithReuseIdentifier: PostCell.reuseIdentifier
    )
    collectionView.addSubview(refreshControl)
    refreshControl.addTarget(self, action: #selector(onRefreshCollectionView), for: .valueChanged)

    let layout = collectionView.collectionViewLayout
    if let flowLayout = layout as? UICollectionViewFlowLayout {
      flowLayout.estimatedItemSize = CGSize(
        width: Screen.width,
        height: Screen.width + 196 // 196 is estimated height of other components except main image
      )
    }

    view.showAnimatedGradientSkeleton()
    fetchPosts()
  }
}

// MARK: - Refresh

private extension PostsController {
  func refresh() {
    collectionView.reloadData()
  }
}

// MARK: - Handlers

private extension PostsController {
  func handleFetchSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.refreshControl.endRefreshing()
      s.collectionView.reloadData()
      s.view.hideSkeleton()
    }
  }

  func handleFetchError() -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }
      s.infoPresenter.presentErrorInfo(error: error)
    }
  }

  func handleLikeTap(for index: Int) -> VoidResult? {
    return { [weak self] in
      guard
        let self = self,
        let post = self.viewModel.posts[safe: index]
      else { return }

      if post.isFavorite {
        self.viewModel.unlikePost(with: post.id)
      } else {
        self.viewModel.likePost(with: post.id)
      }
    }
  }

  func handleOptionTap(for index: Int) -> VoidResult {
    return { [weak self] in
      guard
        let self = self,
        let post = self.viewModel.posts[safe: index]
      else { return }

      DispatchQueue.main.async {
        self.presentReportPage(with: ReportUserViewModel(userId: post.author.id))
      }
    }
  }
}

// MARK: - Actions

private extension PostsController {
  func fetchPosts() {
    viewModel.fetchPosts(
      onSuccess: handleFetchSuccess(),
      onError: handleFetchError()
    )
  }

  @objc
  func onRefreshCollectionView() {
    viewModel.reloadFeed(
      onSuccess: handleFetchSuccess(),
      onError: handleFetchError()
    )
  }
}

// MARK: - UICollectionViewDelegate

extension PostsController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let post = viewModel.posts[safe: indexPath.row] else { return }
    let vc = R.storyboard.posts.postDetailsController()!
    vc.viewModel = PostDetailsViewModel(post: post)
    vc.hidesBottomBarWhenPushed = true
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - UICollectionViewDataSourcePrefetching

extension PostsController: UICollectionViewDataSourcePrefetching {
  func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
    for indexPath in indexPaths {
      let imageView = UIImageView()
      let post = viewModel.posts[safe: indexPath.row]
      imageView.setImageWithURL(post?.photo.url)
    }
  }
}

// MARK: - UICollectionViewDataSource

extension PostsController: UICollectionViewDataSource {
  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    viewModel.posts.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    guard
      let cell = collectionView.dequeueReusableCell(
        withReuseIdentifier: PostCell.reuseIdentifier,
        for: indexPath
      ) as? PostCell,
      let post = viewModel.posts[safe: indexPath.row]
    else { return PostCell() }

    cell.viewModel = PostCellViewModel(post: post)
    cell.postAuthorView.onOptionTap = handleOptionTap(for: indexPath.row)
    cell.postView.onLikeTap = handleLikeTap(for: indexPath.row)

    return cell
  }

  func collectionView(
    _ collectionView: UICollectionView,
    willDisplay cell: UICollectionViewCell,
    forItemAt indexPath: IndexPath
  ) {
    if indexPath.row == viewModel.posts.count - 3 {
      fetchPosts()
    }
  }
}

// MARK: - SkeletonCollectionViewDataSource

extension PostsController: SkeletonCollectionViewDataSource {
  func collectionSkeletonView(
    _ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath
  ) -> ReusableCellIdentifier {
    PostCell.reuseIdentifier
  }
}
