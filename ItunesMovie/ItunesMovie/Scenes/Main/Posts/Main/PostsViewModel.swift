//
//  PostsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PostsViewModel: PostsViewModelProtocol {
  var onRefresh: VoidResult?
  
  private(set) var posts: [Post] = []
  private(set) var hasLoadedAllPosts: Bool = false

  private var page: APIPage
  private var isLoading: Bool = false

  private let userId: Int?
  private let service: PostsServiceProtocol

  init(
    userId: Int? = nil,
    page: APIPage = .default,
    service: PostsServiceProtocol = App.shared.posts
  ) {
    self.userId = userId
    self.page = page
    self.service = service
    
    bind()
  }
}

// MARK: - Bind

private extension PostsViewModel {
  func bind() {
    service.bindPosts(
      onRefresh: trigger(type(of: self).refreshPosts),
      observer: self
    )
  }
}

// MARK: - Methods: Fetch

extension PostsViewModel {
  func reloadFeed(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    hasLoadedAllPosts = false
    isLoading = false
    page = APIPage(index: 1, size: page.size)
    fetchPosts(
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func fetchPosts(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    guard !hasLoadedAllPosts && !isLoading else { return }
    isLoading = true
    service.fetchPosts(
      userId: userId,
      page: page,
      include: [.photo, .author, .favoritesCount, .commentsCount],
      onSuccess: { [weak self] newPosts in
        guard let s = self else { return }

        if s.page.index == 1 {
          s.posts = []
        }

        s.page = APIPage(index: s.page.index + 1, size: s.page.size)
        s.isLoading = false

        if newPosts.isEmpty {
          s.hasLoadedAllPosts = true
        }

        onSuccess()
      },
      onError: { [weak self] error in
        guard let s = self else { return }

        s.hasLoadedAllPosts = true
        s.isLoading = false
        onError(error)
      }
    )
  }
}

// MARK: - Methods: Action

extension PostsViewModel {
  func likePost(with id: Int) {
    service.likePost(
      with: id,
      onSuccess: DefaultClosure.voidResult(),
      onError: DefaultClosure.singleResult()
    )
  }

  func unlikePost(with id: Int) {
    service.unlikePost(
      with: id,
      onSuccess: DefaultClosure.voidResult(),
      onError: DefaultClosure.singleResult()
    )
  }
}

// MARK: - Refresh

private extension PostsViewModel {
  func refreshPosts() {
    self.posts = service.posts()
    onRefresh?()
  }
}
