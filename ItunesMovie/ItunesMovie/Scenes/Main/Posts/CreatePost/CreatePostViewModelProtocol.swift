//
//  CreatePostViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol CreatePostViewModelProtocol {
  func sharePost(
    with imageData: Data?,
    description: String?,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
