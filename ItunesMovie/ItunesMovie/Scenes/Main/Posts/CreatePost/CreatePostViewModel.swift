//
//  CreatePostViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class CreatePostViewModel: CreatePostViewModelProtocol {
  private let service: PostsServiceProtocol

  init(service: PostsServiceProtocol = App.shared.posts) {
    self.service = service
  }
}

// MARK: - Methods

extension CreatePostViewModel {
  func sharePost(
    with imageData: Data?,
    description: String?,
    onProgress: @escaping SingleResult<Progress>,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let inputs = CreatePostInputValidator.Inputs(
      imageData: imageData,
      description: description
    )

    let result = CreatePostInputValidator.validate(inputs)
    switch result {
    case let .success(feedInput):
      service.createPost(
        imageData: feedInput.imageData,
        description: feedInput.description,
        onProgress: onProgress,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}
