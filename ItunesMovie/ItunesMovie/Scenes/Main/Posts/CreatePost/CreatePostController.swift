//
//  CreatePostController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import Material
import SVProgressHUD
import YPImagePicker

final class CreatePostController: ViewController {
  var viewModel: CreatePostViewModelProtocol!

  @IBOutlet private(set) var descriptionField: UITextView!
  @IBOutlet private(set) var imageButton: UIButton!
  @IBOutlet private(set) var shareButton: UIButton!

  private(set) var imagePicker: YPImagePicker!
  private(set) var imagePickerConfig: YPImagePickerConfiguration!
}

// MARK: - LifeCycle

extension CreatePostController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }
}

// MARK: - Setup

private extension CreatePostController {
  func setup() {
    shareButton.hero.id = HeroAnimation.createPost.id
    setupDescriptionField()
    setupImagePicker()
  }

  func setupDescriptionField() {
    let bottomLine = CALayer()
    bottomLine.frame = CGRect(
      x: 0,
      y: descriptionField.frame.height - 0.5,
      width: Screen.width - (descriptionField.frame.origin.x * 2),
      height: 0.5
    )
    bottomLine.backgroundColor = UIColor.black.cgColor
    descriptionField.layer.addSublayer(bottomLine)
  }

  func setupImagePicker() {
    imagePickerConfig = YPImagePickerConfiguration()
    imagePickerConfig.showsPhotoFilters = false
    imagePickerConfig.startOnScreen = YPPickerScreen.photo
    imagePickerConfig.shouldSaveNewPicturesToAlbum = false
    imagePickerConfig.library.mediaType = YPlibraryMediaType.photo
    imagePickerConfig.library.skipSelectionsGallery = true

    imagePicker = YPImagePicker(configuration: imagePickerConfig)
  }
}

// MARK: - Handlers

private extension CreatePostController {
  func handleShareSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      NotificationCenter.default.post(name: .didFinishShareFeed, object: self)
      s.dismiss(animated: true) {
        s.infoPresenter.presentSuccessInfo(message: S.statusSuccess())
      }
    }
  }
}

// MARK: - Actions

private extension CreatePostController {
  @IBAction
  func imageButtonTapped() {
    imagePicker.didFinishPicking(completion: handleImagePick())
    imagePicker.hero.isEnabled = true
    imagePicker.hero.modalAnimationType = .autoReverse(presenting: .fade)
    present(imagePicker, animated: true)
  }

  @IBAction
  func shareButtonTapped() {
    progressPresenter.presentIndefiniteProgress(from: self)
    let imageData = imageButton.imageView?.image?.jpegData(compressionQuality: 0.7)

    viewModel.sharePost(
      with: imageData,
      description: descriptionField.text.replacingOccurrences(of: S.addDescriptionPlaceholder(), with: ""),
      onProgress: handleProgress(),
      onSuccess: handleShareSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

private extension CreatePostController {
  func handleImagePick() -> DoubleResult<[YPMediaItem], Bool> {
    return { [weak self] items, isCancelled in
      if isCancelled { self?.imagePicker.dismiss(animated: true) }

      guard
        let s = self,
        let photo = items.singlePhoto?.image
      else { return }

      let resizedPhoto = photo.resize(toWidth: 512)
      s.imageButton.setImage(resizedPhoto, for: .normal)
      s.imagePicker.dismiss(animated: true)
    }
  }
}

// MARK: - Delegates

extension CreatePostController: UITextViewDelegate {
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.text == S.addDescriptionPlaceholder() {
      textView.text = ""
    }
  }

  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text.isEmpty {
      textView.text = S.addDescriptionPlaceholder()
    }
  }
}
