//
//  PostDetailsViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PostDetailsViewModelProtocol: ViewModelProtocol {
  var id: Int { get }
  var dataSource: [PostCommentCellViewModelProtocol] { get }

  var itemVM: SimplePostItemViewModelProtocol { get }
  var authorVM: PostAuthorViewModelProtocol { get }
  var reportVM: ReportViewModelProtocol { get }

  func fetchPostComments(
    onError: @escaping ErrorResult
  )

  func toggleLike(
    onError: @escaping ErrorResult
  )

  func postComment(
    _ comment: String,
    onError: @escaping ErrorResult
  )
}
