//
//  PostDetailsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import RxSwift
import NSObject_Rx

class PostDetailsViewModel: PostDetailsViewModelProtocol {
  var onRefresh: VoidResult?
  
  let itemVM: SimplePostItemViewModelProtocol
  let authorVM: PostAuthorViewModelProtocol
  let reportVM: ReportViewModelProtocol

  private(set) var dataSource: [PostCommentCellViewModelProtocol] = []

  private var post: Post!

  private let service: PostsServiceProtocol

  init(
    post: Post,
    service: PostsServiceProtocol = App.shared.posts
  ) {
    self.post = post
    self.service = service

    itemVM = SimplePostItemViewModel(post: post)
    authorVM = PostAuthorViewModel(post: post)
    reportVM = ReportUserViewModel(userId: post.author.id)
    
    bind()
  }
}

// MARK: - Bind

extension PostDetailsViewModel {
  func bind() {
    service.bindPost(
      withId: post.id,
      onRefresh: trigger(type(of: self).refreshPost),
      observer: self
    )
    service.bindCommentsInPost(
      withId: post.id,
      onRefresh: trigger(type(of: self).refreshPostComments),
      observer: self
    )
  }
}

// MARK: - Methods

extension PostDetailsViewModel {
  func fetchPostComments(
    onError: @escaping ErrorResult
  ) {
    service.fetchPostComments(
      postId: post.id,
      include: PostsCommentIncludes.allCases,
      onSuccess: handleFetchSuccess(),
      onError: onError
    )
  }

  func toggleLike(
    onError: @escaping ErrorResult
  ) {
    if post.isFavorite {
      service.unlikePost(
        with: post.id,
        onSuccess: DefaultClosure.voidResult(),
        onError: onError
      )
    } else {
      service.likePost(
        with: post.id,
        onSuccess: DefaultClosure.voidResult(),
        onError: onError
      )
    }
  }

  func postComment(
    _ comment: String,
    onError: @escaping ErrorResult
  ) {
    service.createPostComment(
      postId: post.id,
      body: comment,
      onSuccess: DefaultClosure.singleResult(),
      onError: onError
    )
  }
}

// MARK: - Helpers

private extension PostDetailsViewModel {
  func handleFetchSuccess() -> SingleResult<[Comment]> {
    return { [weak self] _ in
      guard let self = self else { return }
      // TODO: Handle pagination state
    }
  }
}

// MARK: - Refresh

private extension PostDetailsViewModel {
  func refreshPost() {
    guard let post = service.post(withId: post.id) else { return }
    self.post = post
    onRefresh?()
  }

  func refreshPostComments() {
    let comments = service.commentsInPost(withId: post.id)
    
    var allComments: [Comment] = []
    comments
      .forEach { comment in
        allComments.append(comment)
        comment.responses?.forEach { response in
          var c = response
          c.level = .level2
          allComments.append(c)
        }
      }

    dataSource = comments
      .sorted(by: { $0.createdAt > $1.createdAt })
      .map { PostCommentCellViewModel(comment: $0) }
    
    onRefresh?()
  }
}

// MARK: - Getters

extension PostDetailsViewModel {
  var id: Int { post.id }
  var isFavorite: Bool { post.isFavorite }
  var favoritesCount: Int { post.favoritesCount }
  var body: String { post.body }
  var createdAt: Date { post.createdAt }
}
