//
//  PostDetailsController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CoreLocation
import Foundation
import UIKit

class PostDetailsController: ViewController, ReportPresenter {
  var viewModel: PostDetailsViewModelProtocol!

  /// Maximum number of lines the comment text field can expand to.
  private(set) var maxNumLines = 3

  @IBOutlet private(set) var feedAuthorView: PostAuthorView!
  @IBOutlet private(set) var tableView: UITableView!

  // TODO: make comment textbox a custom view
  @IBOutlet private(set) var commentTextView: UITextView!
  @IBOutlet private(set) var commentPlaceholderLabel: UILabel!
  @IBOutlet private(set) var sendButton: UIButton!

  @IBOutlet private(set) var commentTextViewHeight: NSLayoutConstraint!

  private var postView: SimplePostItemView!
  private var commentTextViewMinHeight: CGFloat!
}

// MARK: - Lifecycle

extension PostDetailsController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()

    viewModel.fetchPostComments(
      onError: handleError()
    )
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: true)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    if isMovingFromParent {
      navigationController?.setNavigationBarHidden(false, animated: true)
    }
  }
}

// MARK: - Setup

private extension PostDetailsController {
  func setup() {
    setupVM()
    setupPostAuthorView()
    setupItemView()
    setupTableView()
    setupCommentTextView()
  }

  func setupVM() {
    viewModel.onRefresh = trigger(type(of: self).refresh)
  }

  func setupPostAuthorView() {
    feedAuthorView.viewModel = viewModel.authorVM
    feedAuthorView.onOptionTap = handleOptionTap()
  }

  func setupItemView() {
    guard postView != nil else { return }
    postView.canPreviewImage = true
    postView.viewModel = viewModel.itemVM
    postView.onLikeTap = trigger(type(of: self).likeToggled)
  }

  func setupTableView() {
    tableView.register(R.nib.postCommentCell)

    tableView.allowsSelection = false
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .none

    tableView.tableFooterView = UIView()
  }

  func setupCommentTextView() {
    commentTextView.delegate = self
    commentTextViewMinHeight = commentTextViewHeight.constant
  }
}

// MARK: - Refresh

private extension PostDetailsController {
  func refresh() {
    tableView.reloadData()
  }
}

// MARK: - Actions

private extension PostDetailsController {
  func likeToggled() {
    viewModel.toggleLike(
      onError: handleError()
    )
  }

  @IBAction
  func sendButtonTapped(_ sender: Any) {
    guard
      let comment = commentTextView.text,
      !comment.isEmpty
    else {
      return
    }

    commentTextView.text = nil
    commentTextView.resignFirstResponder()
    commentPlaceholderLabel.isHidden = false
    commentTextViewHeight.constant = commentTextViewMinHeight

    viewModel.postComment(
      comment,
      onError: handleError()
    )
  }
}

// MARK: - Handlers

private extension PostDetailsController {
  func handleOptionTap() -> VoidResult {
    return { [weak self] in
      guard let self = self else { return }
      self.presentReportPage(with: self.viewModel.reportVM)
    }
  }
}

// MARK: - UITableViewDelegate

extension PostDetailsController: UITableViewDelegate {
  func tableView(
    _ tableView: UITableView,
    viewForHeaderInSection section: Int
  ) -> UIView? {
    if postView != nil { return postView }
    postView = SimplePostItemView()
    setupItemView()
    return postView
  }
}

// MARK: - UITableViewDataSource

extension PostDetailsController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }

  func tableView(
    _ tableView: UITableView,
    numberOfRowsInSection section: Int
  ) -> Int {
    return viewModel.dataSource.count
  }

  func tableView(
    _ tableView: UITableView,
    cellForRowAt indexPath: IndexPath
  ) -> UITableViewCell {
    let identifier = R.reuseIdentifier.postCommentCell.identifier
    let cell = tableView.dequeueReusableCell(withIdentifier: identifier)

    guard
      let commentCell = cell as? PostCommentCell,
      let cellVM = viewModel.dataSource[safe: indexPath.row]
    else {
      return UITableViewCell()
    }

    commentCell.viewModel = cellVM

    return commentCell
  }
}

// MARK: - UITextViewDelegate

extension PostDetailsController: UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    let content = textView.text ?? ""
    commentPlaceholderLabel.isHidden = !content.isEmpty
    sendButton.isHidden = content.trimmed.isEmpty

    if let lineHeight = textView.font?.lineHeight {
      let numLines = Int(floor(textView.contentSize.height / lineHeight))
      var textViewHeight: CGFloat = commentTextViewMinHeight
      let lines = min(maxNumLines, numLines)
      textViewHeight += CGFloat(lines - 1) * lineHeight
      commentTextViewHeight.constant = textViewHeight
    }
  }
}
