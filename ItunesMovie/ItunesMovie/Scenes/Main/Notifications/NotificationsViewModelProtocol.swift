//
//  NotificationsViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol NotificationsViewModelProtocol {
  var sections: [NotificationsSection] { get }

  func fetchNotifications(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - TypeAliases

enum LoadStatus: Int {
  case loading, done
}

typealias NotificationsSection = (
  index: Int,
  name: String,
  status: LoadStatus,
  viewModels: [NotificationCellViewModelProtocol]
)
