//
//  NotificationTitleView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class NotificationTitleView: UITableViewHeaderFooterView {
  @IBOutlet private(set) var titleLabel: UILabel!

  func setTitle(_ title: String) {
    titleLabel.text = title
  }
}

extension NotificationTitleView {
  static var height: CGFloat { 56.0 }
}
