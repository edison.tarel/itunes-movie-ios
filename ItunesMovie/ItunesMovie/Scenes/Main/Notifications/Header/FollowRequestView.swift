//
//  FollowRequestView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class FollowRequestView: UITableViewHeaderFooterView {
  static let height: CGFloat = 56.0

  @IBOutlet var requestCount: UILabel!
}
