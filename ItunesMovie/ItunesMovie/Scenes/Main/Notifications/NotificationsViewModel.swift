//
//  NotificationsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class NotificationsViewModel: NotificationsViewModelProtocol {
  private(set) var sections: [NotificationsSection] = []

  private let service: NotificationsServiceProtocol

  init(service: NotificationsServiceProtocol = App.shared.notifications) {
    self.service = service
  }
}

// MARK: - Methods

extension NotificationsViewModel {
  func fetchNotifications(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    if !sections.isEmpty {
      var newSections: [NotificationsSection] = []
      for section in sections {
        newSections.append(toggleSectionStatus(section))
      }
      sections = newSections
    }

    fetchNotificationsToday(
      onSuccess: onSuccess,
      onError: onError
    )
    fetchNotificationsThisWeek(
      onSuccess: onSuccess,
      onError: onError
    )
    fetchNotificationsUnread(
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Utils

private extension NotificationsViewModel {
  func fetchNotificationsToday(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.fetchNotificationsForToday(
      onSuccess: handleFetchSuccess(
        for: .today,
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }

  func fetchNotificationsThisWeek(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.fetchNotificationsForThisWeek(
      onSuccess: handleFetchSuccess(
        for: .thisWeek,
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }

  func fetchNotificationsUnread(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.fetchNotificationsUnread(
      onSuccess: handleFetchSuccess(
        for: .unread,
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }
}

// MARK: - Handlers

private extension NotificationsViewModel {
  func handleFetchSuccess(
    for section: Section,
    thenExecute completionHandler: @escaping VoidResult
  ) -> SingleResult<[AppNotification]> {
    return { [weak self] newNotif in
      guard let s = self else { return }
      s.saveNotifications(newNotif, for: section)

      if s.isFetchComplete { completionHandler() }
    }
  }
}

// MARK: - Utils

private extension NotificationsViewModel {
  func saveNotifications(
    _ notifications: [AppNotification],
    for section: Section
  ) {
    let vms = notifications.map(NotificationCellViewModel.init)
    let section = NotificationsSection(
      index: section.index,
      name: section.name,
      status: .done,
      viewModels: vms
    )
    if expectedNumberOfSections == sections.count {
      sections[section.index] = section
    } else {
      sections.append(section)
      sections.sort(by: { $0.index < $1.index })
    }
  }

  func toggleSectionStatus(_ section: NotificationsSection) -> NotificationsSection {
    NotificationsSection(
      index: section.index,
      name: section.name,
      status: .loading,
      viewModels: section.viewModels
    )
  }
}

// MARK: - Getters

extension NotificationsViewModel {
  private var expectedNumberOfSections: Int { Section.allCases.count }
  private var isFetchComplete: Bool {
    let val = sections.map { $0.status.rawValue }
    return expectedNumberOfSections == val.reduce(0, +)
  }
}

// MARK: - Types

private enum Section: Int, CaseIterable {
  case today = 0
  case thisWeek
  case unread

  var index: Int { rawValue }

  var name: String {
    switch self {
    case .today:
      return S.notificationLabelsToday()
    case .thisWeek:
      return S.notificationLabelsThisWeek()
    case .unread:
      return S.notificationLabelsUnread()
    }
  }
}
