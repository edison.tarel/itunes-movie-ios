//
//  NotificationsController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import RxCocoa
import RxSwift
import SkeletonView
import SVProgressHUD

class NotificationsController: ViewController {
  lazy var viewModel: NotificationsViewModelProtocol! = { NotificationsViewModel() }()

  @IBOutlet private(set) var headerView: UIView!
  @IBOutlet private(set) var tableView: UITableView!

  private let refreshControl = UIRefreshControl()
}

// MARK: - Lifecycle

extension NotificationsController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    fetchNotifications()
  }
}

// MARK: Setup

private extension NotificationsController {
  func setup() {
    setupNavBar()
    setupTable()
  }

  func setupNavBar() {
    let titleLabel = UILabel()
    titleLabel.text = S.notifications()
    titleLabel.font = R.font.sfProDisplayBold(size: 28)
    navigationItem.leftBarButtonItem = UIBarButtonItem(customView: titleLabel)

    let filterButton = UIBarButtonItem(
      image: R.image.filter()!,
      style: .plain,
      target: self,
      action: #selector(filterButtonTapped(_:))
    )

    navigationItem.rightBarButtonItem = filterButton
  }

  func setupTable() {
    tableView.estimatedRowHeight = 68

    tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 20))

    tableView.register(
      UINib(resource: R.nib.notificationCell),
      forCellReuseIdentifier: R.nib.notificationCell.name
    )
    tableView.register(
      UINib(resource: R.nib.notificationTitleView),
      forHeaderFooterViewReuseIdentifier: R.nib.notificationTitleView.name
    )

    tableView.addSubview(refreshControl)
    refreshControl.addTarget(self, action: #selector(fetchNotifications), for: .valueChanged)
  }
}

// MARK: - Handlers

private extension NotificationsController {
  func handleFetchSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.view.hideSkeleton()
      s.refreshControl.endRefreshing()
      s.tableView.reloadData()
    }
  }

  func handleError() -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }

      s.view.hideSkeleton()
      s.refreshControl.endRefreshing()
      s.infoPresenter.presentErrorInfo(error: error)
    }
  }
}

// MARK: - Actions

private extension NotificationsController {
  @objc
  func filterButtonTapped(_ sender: Any) {
    // TODO: Temporary placement to showcase shimmer
    if view.isSkeletonActive {
      view.hideSkeleton()
      tableView.reloadData()
    } else {
      view.showAnimatedGradientSkeleton()
    }
  }

  @objc
  func fetchNotifications() {
    if viewModel.sections.isEmpty { view.showAnimatedGradientSkeleton() }
    
    viewModel.fetchNotifications(
      onSuccess: handleFetchSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Util

private extension NotificationsController {
  func headerView(with title: String) -> NotificationTitleView? {
    guard let view = tableView.dequeueReusableHeaderFooterView(
      withIdentifier: R.nib.notificationTitleView.name
    ) as? NotificationTitleView else {
      return nil
    }
    view.setTitle(title)
    view.contentView.backgroundColor = .white
    return view
  }
}

// MARK: UITableViewDataSource

extension NotificationsController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    let sections = viewModel.sections
    let todaySection = sections[safe: 0]?.viewModels.isEmpty == false
    let thisWeekSection = sections[safe: 1]?.viewModels.isEmpty == false
    if todaySection || thisWeekSection {
      return 2
    }
    return viewModel.sections.count
  }

  func tableView(
    _ tableView: UITableView,
    numberOfRowsInSection section: Int
  ) -> Int {
    let sectionData = viewModel.sections[section]
    return sectionData.viewModels.count
  }

  func tableView(
    _ tableView: UITableView,
    cellForRowAt indexPath: IndexPath
  ) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(
      withIdentifier: NotificationCell.reuseIdentifier,
      for: indexPath
    ) as? NotificationCell,
      let section = viewModel.sections[safe: indexPath.section] else {
      return NotificationCell()
    }

    let viewModel = section.viewModels[safe: indexPath.row]
    cell.viewModel = viewModel

    return cell
  }
}

extension NotificationsController: SkeletonTableViewDataSource {
  func numSections(in collectionSkeletonView: UITableView) -> Int {
    viewModel.sections.count
  }

  func collectionSkeletonView(
    _ skeletonView: UITableView,
    cellIdentifierForRowAt indexPath: IndexPath
  ) -> ReusableCellIdentifier {
    NotificationCell.reuseIdentifier
  }
}

// MARK: - UITableViewDelegate

extension NotificationsController: UITableViewDelegate {
  func tableView(
    _ tableView: UITableView,
    didSelectRowAt indexPath: IndexPath
  ) {
    // TODO: redirect notification
    tableView.deselectRow(at: indexPath, animated: true)
  }

  func tableView(
    _ tableView: UITableView,
    viewForHeaderInSection section: Int
  ) -> UIView? {
    let sectionData = viewModel.sections[section]
    if sectionData.viewModels.isEmpty { return nil }
    return headerView(with: sectionData.name)
  }

  func tableView(
    _ tableView: UITableView,
    heightForHeaderInSection section: Int
  ) -> CGFloat {
    let sectionData = viewModel.sections[section]
    if sectionData.viewModels.isEmpty { return .zero }
    return NotificationTitleView.height
  }
}

// MARK: - ShimmerPresenterProtocol

// extension NotificationsController: ShimmerPresenterProtocol {}
