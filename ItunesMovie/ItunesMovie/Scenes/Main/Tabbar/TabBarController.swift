//
//  TabBarController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Hero
import UIKit

class TabBarController: UITabBarController {
  private var hasSetUpTabbar: Bool = false
}

// MARK: - Lifecycle

extension TabBarController {
  override func viewDidLoad() {
    super.viewDidLoad()
    delegate = self
    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupTabBarIfNeeded()
  }
}

// MARK: - Setup

private extension TabBarController {
  func setup() {
    setupVMs()
  }

  func setupVMs() {
    setupMyProfileTab()
  }

  func setupMyProfileTab() {
    guard let vc = initialController(for: .account) as? MyProfileController else {
      preconditionFailure("Expecting viewController of type MyProfileController")
    }
    vc.viewModel = MyProfileViewModel()
  }

  func initialController(for tab: Tab) -> UIViewController {
    guard let nc = viewControllers?[tab.index] as? UINavigationController else {
      preconditionFailure("Expecting all viewControllers to be of type UINavigationController")
    }
    guard let vc = nc.viewControllers.first else {
      preconditionFailure("Expecting navigationController to have rootViewController")
    }
    return vc
  }
}

// MARK: - Tabbar Setup

private extension TabBarController {
  func setupTabBarIfNeeded() {
    if hasSetUpTabbar { return }

    setupTabbarImageInsets()
    setupTabbarCenterImage()

    hasSetUpTabbar = true
  }

  func setupTabbarImageInsets() {
    tabBar.items?.forEach {
      $0.imageInsets = UIEdgeInsets(top: -3, left: 0, bottom: 3, right: 0)
    }
  }

  func setupTabbarCenterImage() {
    let centerIndex = floor(Double(tabBarItemViews.count / 2))
    let tabBarItemView = tabBarItemViews[Int(centerIndex)]
    let imageSubviews = tabBarItemView.subviews.compactMap { $0 as? UIImageView }

    guard let itemImageView = imageSubviews.first else { return }

    // Set clear background
    itemImageView.backgroundColor = .clear

    // Add icon image
    let icnView = UIImageView(image: R.image.postOn()!)
    icnView.contentMode = .scaleAspectFill
    icnView.tintColor = .clear
    icnView.hero.id = HeroAnimation.createPost.id
    itemImageView.addSubview(icnView)

    // Center vertical alignment
    let dimension: CGFloat = 48
    let dimensions = CGSize(width: dimension, height: dimension)
    icnView.autoSetDimensions(to: dimensions)
    icnView.autoAlignAxis(.horizontal, toSameAxisOf: itemImageView, withOffset: 0.5)
  }
}

// MARK: - Getters

private extension TabBarController {
  var tabBarItemViews: [UIView] {
    let interactionControls = tabBar.subviews.filter { $0 is UIControl }
    return interactionControls.sorted(by: { $0.frame.minX < $1.frame.minX })
  }
}

// MARK: - UITabBarControllerDelegate

extension TabBarController: UITabBarControllerDelegate {
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    let tappedIndex = viewControllers?.firstIndex(of: viewController) ?? 0
    guard let tab = Tab(rawValue: tappedIndex) else { return false }

    switch tab {
    case .newPost:
      presentCreatePostViewController()
      return false
    default:
      break
    }

    return viewController != tabBarController.selectedViewController
  }
}

// MARK: - Routing

private extension TabBarController {
  func presentCreatePostViewController() {
    let vc = R.storyboard.posts.createPostController()!
    vc.viewModel = CreatePostViewModel()
    let nc = NavigationController(rootViewController: vc)
    nc.modalPresentationStyle = .fullScreen
    nc.hero.isEnabled = true
    nc.hero.modalAnimationType = .autoReverse(presenting: .pageIn(direction: .up))
    present(nc, animated: true)
  }
}

private enum Tab: Int {
  case feed = 0
  case chat
  case newPost
  case notifications
  case account

  var index: Int { rawValue }
}
