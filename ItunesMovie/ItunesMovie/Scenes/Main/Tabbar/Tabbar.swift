//
//  Tabbar.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class Tabbar: UITabBar {
  private let tabBarHeightShapeLayer = CAShapeLayer()
  private let extraHeight: CGFloat = 2.0

  override func draw(_ rect: CGRect) {
    super.draw(rect)
    setupTabBar()
  }

  func setupTabBar() {
    isTranslucent = false
    barTintColor = .clear
    backgroundColor = .white
    backgroundImage = UIImage()
    shadowImage = UIImage()
    clipsToBounds = false

    tabBarHeightShapeLayer.frame = CGRect(
      x: 0,
      y: -extraHeight,
      width: bounds.width,
      height: bounds.height + extraHeight
    )
    tabBarHeightShapeLayer.fillColor = UIColor.white.cgColor
    tabBarHeightShapeLayer.strokeColor = R.color.grey_F7F7F7()!.cgColor
    tabBarHeightShapeLayer.path = createPathForTabBar().cgPath
    layer.insertSublayer(tabBarHeightShapeLayer, at: 0)
  }

  private func createPathForTabBar() -> UIBezierPath {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 0, y: -extraHeight))
    path.addLine(to: CGPoint(x: bounds.width, y: -extraHeight))
    path.addLine(to: CGPoint(x: bounds.width, y: bounds.height + safeAreaInsets.bottom))
    path.addLine(to: CGPoint(x: 0, y: bounds.height + safeAreaInsets.bottom))
    path.addLine(to: CGPoint(x: 0, y: -extraHeight))

    return path
  }
}
