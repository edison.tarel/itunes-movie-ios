//
//  TabbarViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

enum TabIndex: Int {
  case feed = 0
  case chat
  case post
  case notification
  case profile
}
