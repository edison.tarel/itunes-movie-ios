//
//  PayoutEntryViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PayoutEntryViewModelProtocol {
  var sections: [PayoutSection] { get }

  func uploadFrontIdImage(
    onSuccess: @escaping VoidResult,
    onProgress: SingleResult<Progress>?,
    onError: @escaping ErrorResult
  )

  func uploadBackIdImage(
    onSuccess: @escaping VoidResult,
    onProgress: SingleResult<Progress>?,
    onError: @escaping ErrorResult
  )

  func setupStripeConnect(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func setupExternalAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func updateExternalAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - Types

enum PayoutEntrySection: Int, CaseIterable {
  case bankDetails = 0
  case ownerDetails
  case ownerAddress
  case idVerification

  var index: Int { rawValue }

  var name: String {
    switch self {
    case .bankDetails:
      return S.payoutEntryBankDetails()
    case .ownerDetails:
      return S.payoutEntryOwnerDetails()
    case .ownerAddress:
      return S.payoutEntryOwnerAddress()
    case .idVerification:
      return S.payoutEntryIdVerification()
    }
  }
}

// MARK: - TypeAliases

typealias PayoutSection = (
  index: PayoutEntrySection,
  viewModels: [PayoutTextInputCellViewModelProtocol]
)
