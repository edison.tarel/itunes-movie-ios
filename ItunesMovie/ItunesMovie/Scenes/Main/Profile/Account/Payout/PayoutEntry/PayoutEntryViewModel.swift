//
//  PayoutEntryViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CountryPickerView
import Foundation

class PayoutEntryViewModel: PayoutEntryViewModelProtocol {
  var stripeAccount: StripeConnect!
  var frontImageFileId: String!
  var backImageFileId: String!
  var sections: [PayoutSection] = []

  private let service: PaymentServiceProtocol

  init(
    service: PaymentServiceProtocol = App.shared.payment,
    stripeAccount: StripeConnect? = nil
  ) {
    self.service = service
    self.stripeAccount = stripeAccount

    if stripeAccount == nil {
      setupDefaultEntry()
    } else {
      setupEditEntry()
    }
  }
}

// MARK: - Setup

extension PayoutEntryViewModel {
  func setupDefaultEntry() {
    sections = [
      PayoutSection(index: .bankDetails, viewModels: [
        PayoutTextInputCellViewModel(input1Placeholder: S.payoutEntryAccountHolder(), input1ParamKey: "account_holder_name"),
        PayoutTextInputCellViewModel(input1Placeholder: S.payoutEntryBsb(), inputType: .number, input1ParamKey: "routing_number"),
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryAccountNumber(),
          inputType: .number,
          input1ParamKey: "account_number"
        )
      ]),
      PayoutSection(index: .ownerDetails, viewModels: [
        PayoutTextInputCellViewModel(
          input1Placeholder: S.firstName(),
          input2Placeholder: S.lastName(),
          input1ParamKey: "first_name",
          input2ParamKey: "last_name"
        ),
        PayoutTextInputCellViewModel(input1Placeholder: S.payoutEntryDob(), inputType: .date, input1ParamKey: "date_of_birth")
      ]),
      PayoutSection(index: .ownerAddress, viewModels: [
        PayoutTextInputCellViewModel(input1Placeholder: S.payoutEntryStreetAddress(), input1ParamKey: "street"),
        PayoutTextInputCellViewModel(input1Placeholder: S.payoutEntryCity(), input1ParamKey: "city"),
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryState(),
          input2Placeholder: S.payoutEntryPostCode(),
          inputType2: .number,
          input1ParamKey: "state",
          input2ParamKey: "post_code"
        ),
        PayoutTextInputCellViewModel(input1Placeholder: S.payoutEntryCountry(), inputType: .country, input1ParamKey: "country")
      ]),
      PayoutSection(index: .idVerification, viewModels: [
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryFront(),
          input2Placeholder: S.payoutEntryBack(),
          inputType: .imageAttachment,
          input1ParamKey: "front",
          input2ParamKey: "back"
        )
      ])
    ]
  }

  func setupEditEntry() {
    guard let externalDetails = stripeAccount.externalAccounts?.data.first else { return }
    let userDetails = stripeAccount.individual
    let address = userDetails.address
    sections = [
      PayoutSection(index: .bankDetails, viewModels: [
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryAccountHolder(),
          input1ParamKey: "account_holder_name",
          input1Value: externalDetails.accountHolderName
        ),
      ]),
      PayoutSection(index: .ownerDetails, viewModels: [
        PayoutTextInputCellViewModel(
          input1Placeholder: S.firstName(),
          input2Placeholder: S.lastName(),
          input1ParamKey: "first_name",
          input2ParamKey: "last_name",
          input1Value: userDetails.firstName,
          input2Value: userDetails.lastName
        ),
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryDob(),
          inputType: .date,
          input1ParamKey: "date_of_birth",
          input1Value: userDetails.dob?.date
        )
      ]),
      PayoutSection(index: .ownerAddress, viewModels: [
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryStreetAddress(),
          input1ParamKey: "street",
          input1Value: address?.line1
        ),
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryCity(),
          input1ParamKey: "city",
          input1Value: address?.city
        ),
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryState(),
          input2Placeholder: S.payoutEntryPostCode(),
          inputType2: .number,
          input1ParamKey: "state",
          input2ParamKey: "post_code",
          input1Value: address?.state,
          input2Value: address?.postalCode
        ),
        PayoutTextInputCellViewModel(
          input1Placeholder: S.payoutEntryCountry(),
          inputType: .country,
          input1ParamKey: "country",
          input1Value: address?.countryObject
        )
      ])
    ]
  }
}

// MARK: - Actions

extension PayoutEntryViewModel {
  func validateInputtedValues() -> String? {
    for section in sections {
      for viewModel in section.viewModels {
        let errorVerb = (viewModel.inputType == .imageAttachment) ? S.payoutEntryAttachImage() : S.payoutEntryInput()
        if viewModel.input1Value == nil {
          return S.payoutErrorMessage(S.payoutEntryPlease(), errorVerb, viewModel.input1Placeholder)
        }
        if viewModel.input2ParamKey != nil && viewModel.input2Value == nil {
          return S.payoutErrorMessage(S.payoutEntryPlease(), errorVerb, viewModel.input2Placeholder!)
        }
      }
    }
    return nil
  }

  func uploadFrontIdImage(
    onSuccess: @escaping VoidResult,
    onProgress: SingleResult<Progress>?,
    onError: @escaping ErrorResult
  ) {
    guard let imageData = fetchInputtedValues()[dataOrNil: "front"] else {
      return onError(APIClientError.unknown)
    }

    uploadStripeDocument(
      imageData, onSuccess: { [weak self] fileId in
        guard let s = self else { return onError(APIClientError.unknown) }
        s.frontImageFileId = fileId
        onSuccess()
      },
      onProgress: onProgress,
      onError: onError
    )
  }

  func uploadBackIdImage(
    onSuccess: @escaping VoidResult,
    onProgress: SingleResult<Progress>?,
    onError: @escaping ErrorResult
  ) {
    guard let imageData = fetchInputtedValues()[dataOrNil: "back"] else {
      return onError(APIClientError.unknown)
    }

    uploadStripeDocument(
      imageData, onSuccess: { [weak self] fileId in
        guard let s = self else { return onError(APIClientError.unknown) }
        s.backImageFileId = fileId
        onSuccess()
      },
      onProgress: onProgress,
      onError: onError
    )
  }

  func setupStripeConnect(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let stripeConnectParams = generateStripeConnectParam()
    service.setupStripeConnect(stripeConnectParams: stripeConnectParams, onSuccess: onSuccess, onError: onError)
  }

  func setupExternalAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let bank = generateExternalAccountParam()

    service.stripeConnectExternalAccount(externalAccount: bank, onSuccess: onSuccess, onError: onError)
  }

  func updateExternalAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    guard let accountId = stripeAccount.externalAccounts?.data.first?.id else { return }
    let inputtedValues = fetchInputtedValues()
    let bank = BankDetails(
      accountHolderName: inputtedValues[string: "account_holder_name"]
    )

    service.updateConnectedBanks(accountId: accountId, bankDetails: bank, onSuccess: onSuccess, onError: onError)
  }
}

// MARK: - Helpers

private extension PayoutEntryViewModel {
  func uploadStripeDocument(
    _ imageData: Data,
    onSuccess: @escaping SingleResult<String>,
    onProgress: SingleResult<Progress>?,
    onError: @escaping ErrorResult
  ) {
    service.postAuthConnectFileUpload(
      imageData: imageData,
      purpose: nil,
      onProgress: onProgress,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Utils

private extension PayoutEntryViewModel {
  func fetchInputtedValues() -> JSONDictionary {
    var paramDict: JSONDictionary = [:]
    for section in sections {
      for viewModel in section.viewModels {
        paramDict[viewModel.input1ParamKey] = viewModel.input1Value
        if let key = viewModel.input2ParamKey {
          paramDict[key] = viewModel.input2Value
        }
      }
    }

    return paramDict
  }

  func generateStripeConnectParam() -> StripeConnect {
    let params = fetchInputtedValues()
    let birthdate = params[date: "date_of_birth"]
    let stripeParams = StripeConnect(
      individual: StripeConnect.IndividualAccount(
        firstName: params[string: "first_name"],
        lastName: params[string: "last_name"],
        dob: StripeConnect.DateOfBirth(day: birthdate.day, month: birthdate.month, year: birthdate.year),
        address: StripeConnect.Address(
          city: params[string: "city"],
          line1: params[string: "street"],
          state: params[string: "state"],
          country: params[string: "country"],
          postalCode: params[string: "post_code"]
        ),
        verification: StripeConnect.VerificationDocument(
          document: StripeConnect.Document(front: frontImageFileId, back: backImageFileId)
        )
      ),
      externalAccount: "",
      externalAccounts: nil
    )
    return stripeParams
  }

  func generateExternalAccountParam() -> StripeExternalAccountParams {
    let params = fetchInputtedValues()
    let countryCode = params[string: "country"]

    let bank = StripeExternalAccountParams(
      accountNumber: params[string: "account_number"],
      object: "bank_account",
      country: countryCode,
      currency: Locale.currency[countryCode]?.code ?? "",
      accountHolderName: params[string: "account_holder_name"],
      accountHolderType: "individual",
      routingNumber: params[string: "routing_number"]
    )

    return bank
  }
}

// MARK: - Getters

extension PayoutEntryViewModel {
  var title: String! {
    stripeAccount != nil ?
      S.payoutEditTitle() :
      S.payoutAddTitle()
  }
}
