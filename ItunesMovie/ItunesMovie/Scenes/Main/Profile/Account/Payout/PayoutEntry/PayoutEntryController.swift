//
//  PayoutEntryController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import SwiftDate
import UIKit

class PayoutEntryController: ViewController {
  var viewModel: PayoutEntryViewModel!

  @IBOutlet private(set) var tableView: UITableView!
}

// MARK: - Lifecycle

extension PayoutEntryController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }
}

// MARK: Setup

private extension PayoutEntryController {
  func setup() {
    setupNavBar()
    setupTable()
  }

  func setupNavBar() {
    title = viewModel.title
    let saveButton = UIBarButtonItem(title: S.save(), style: .plain, target: self, action: #selector(saveButtonTapped))

    navigationItem.rightBarButtonItem = saveButton
  }

  func setupTable() {
    tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 20))

    tableView.register(
      UINib(resource: R.nib.payoutPhotoAttachmentCell),
      forCellReuseIdentifier: R.nib.payoutPhotoAttachmentCell.name
    )
    tableView.register(
      UINib(resource: R.nib.payoutTextInputCell),
      forCellReuseIdentifier: R.nib.payoutTextInputCell.name
    )
    tableView.register(
      UINib(resource: R.nib.payoutHeaderView),
      forHeaderFooterViewReuseIdentifier: R.nib.payoutHeaderView.name
    )
  }
}

// MARK: - Handlers

private extension PayoutEntryController {
  func handleSetupSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.infoPresenter.presentSuccessInfo(message: S.statusSuccess())
      if let vcs =  s.navigationController?.viewControllers,
        let accountVC = vcs.first(where: { $0 is AccountController }) {
        s.navigationController?.popToViewController(accountVC, animated: true)
      }
    }
  }

  func handleError() -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.infoPresenter.presentErrorInfo(error: error)
    }
  }

  func handleProgress(_ message: String? = nil) -> SingleResult<Progress> {
    return { [weak self] progress in
      guard let s = self else { return }
      s.progressPresenter.presentProgress(value: Float(CGFloat(progress.fractionCompleted)), message: message, from: s)
    }
  }

  func handleValueUpdated() -> DoubleResult<IndexPath, PayoutTextInputCellViewModelProtocol> {
    return { [weak self] indexPath, viewModel in
      guard let s = self else { return }
      s.viewModel.sections[indexPath.section].viewModels[indexPath.row] = viewModel
    }
  }
}

// MARK: - Actions

private extension PayoutEntryController {
  @objc
  func saveButtonTapped() {
    if let firstResponder = view.window?.firstResponder {
      firstResponder.resignFirstResponder()
    }

    let errorMessage = viewModel.validateInputtedValues()
    guard errorMessage == nil else {
      return infoPresenter.presentErrorMessage(message: errorMessage!)
    }

    viewModel.stripeAccount != nil ? editPayoutDetails() : addPayoutDetails()
  }

  func editPayoutDetails() {
    setupStripeAccount { [weak self] in
      guard let s = self else { return }
      s.updateExternalAccount()
    }
  }

  func addPayoutDetails() {
    uploadFrontImage { [weak self] in
      guard let s = self else { return }
      s.uploadBackImage {
        s.setupStripeAccount {
          s.createExternalAccount()
        }
      }
    }
  }

  func updateExternalAccount() {
    progressPresenter.presentIndefiniteProgress(message: S.payoutUpdateAccount(), from: self)
    viewModel.updateExternalAccount(
      onSuccess: handleSetupSuccess(),
      onError: handleError()
    )
  }

  func createExternalAccount() {
    progressPresenter.presentIndefiniteProgress(message: S.payoutAddAccount(), from: self)
    viewModel.setupExternalAccount(onSuccess: handleSetupSuccess(), onError: handleError())
  }

  func setupStripeAccount(_ onSuccess: @escaping VoidResult) {
    let message = viewModel.stripeAccount != nil ? S.updating() : S.creating()
    progressPresenter.presentIndefiniteProgress(message: "\(message) \(S.payoutStripeAccount())", from: self)
    viewModel.setupStripeConnect(
      onSuccess: onSuccess,
      onError: handleError()
    )
  }

  func uploadFrontImage(_ onSuccess: @escaping VoidResult) {
    viewModel.uploadFrontIdImage(
      onSuccess: onSuccess,
      onProgress: handleProgress(S.payoutUploadFrontImage()),
      onError: handleError()
    )
  }

  func uploadBackImage(_ onSuccess: @escaping VoidResult) {
    viewModel.uploadBackIdImage(
      onSuccess: onSuccess,
      onProgress: handleProgress(S.payoutUploadBackImage()),
      onError: handleError()
    )
  }
}

// MARK: - Util

private extension PayoutEntryController {
  func headerView(with title: String) -> UIView? {
    guard let view = tableView.dequeueReusableHeaderFooterView(
      withIdentifier: R.nib.payoutHeaderView.name
    ) as? PayoutHeaderView else {
      return nil
    }
    view.setTitle(title)
    view.contentView.backgroundColor = .white
    return view
  }
}

// MARK: UITableViewDataSource

extension PayoutEntryController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    viewModel.sections.count
  }

  func tableView(
    _ tableView: UITableView,
    numberOfRowsInSection section: Int
  ) -> Int {
    viewModel.sections[section].viewModels.count
  }

  func tableView(
    _ tableView: UITableView,
    cellForRowAt indexPath: IndexPath
  ) -> UITableViewCell {
    guard let section = viewModel.sections[safe: indexPath.section],
      let viewModel = section.viewModels[safe: indexPath.row] else {
      return PayoutTextInputCell()
    }

    if viewModel.inputType == .imageAttachment {
      guard let cell = tableView.dequeueReusableCell(
        withIdentifier: PayoutPhotoAttachmentCell.reuseIdentifier,
        for: indexPath
      ) as? PayoutPhotoAttachmentCell else { return PayoutPhotoAttachmentCell() }

      cell.viewModel = viewModel
      cell.indexPath = indexPath
      cell.valueUpdated = handleValueUpdated()

      return cell
    } else {
      guard let cell = tableView.dequeueReusableCell(
        withIdentifier: PayoutTextInputCell.reuseIdentifier,
        for: indexPath
      ) as? PayoutTextInputCell else { return PayoutTextInputCell() }

      cell.viewModel = viewModel
      cell.indexPath = indexPath
      cell.valueUpdated = handleValueUpdated()

      return cell
    }
  }
}

// MARK: - UITableViewDelegate

extension PayoutEntryController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    guard let section = viewModel.sections[safe: indexPath.section],
      let viewModel = section.viewModels[safe: indexPath.row] else {
      return 0
    }
    return viewModel.inputType == .imageAttachment ? PayoutPhotoAttachmentCell.height : PayoutTextInputCell.height
  }

  func tableView(
    _ tableView: UITableView,
    viewForHeaderInSection section: Int
  ) -> UIView? {
    let sectionData = viewModel.sections[section]
    return headerView(with: sectionData.index.name)
  }
}
