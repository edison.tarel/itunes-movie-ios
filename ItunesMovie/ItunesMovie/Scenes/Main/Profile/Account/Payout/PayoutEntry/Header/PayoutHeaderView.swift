//
//  PayoutHeaderView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import UIKit

class PayoutHeaderView: UITableViewHeaderFooterView {
  @IBOutlet private(set) var titleLabel: UILabel!

  func setTitle(_ title: String) {
    titleLabel.text = title
  }
}

extension PayoutHeaderView {
  static var height: CGFloat { 56.0 }
}
