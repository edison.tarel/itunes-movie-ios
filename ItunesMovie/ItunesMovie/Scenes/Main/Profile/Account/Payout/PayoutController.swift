//
//  PayoutController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

final class PayoutController: ViewController {
  var viewModel: PayoutViewModelProtocol!

  @IBOutlet private(set) var accountNameLabel: UILabel!
  @IBOutlet private(set) var bsbLabel: UILabel!
  @IBOutlet private(set) var accountNumberLabel: UILabel!
  @IBOutlet private(set) var editPayoutButton: FormButton!
  @IBOutlet private(set) var removeAccountButton: FormButton!
}

// MARK: - LifeCycle

extension PayoutController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }
}

// MARK: - Setup

private extension PayoutController {
  func setup() {
    title = S.payoutTitle()
    setupBankDetails()
  }

  func setupBankDetails() {
    guard let externalAccount = viewModel.stripeAccount.externalAccounts?.data.first else { return }
    accountNameLabel.text = externalAccount.accountHolderName
    bsbLabel.text = externalAccount.bsb
    accountNumberLabel.text = externalAccount.accountNumber
  }
}

// MARK: - Handlers

private extension PayoutController {
  func handleRemoveAccountConfirmed() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let s = self else { return }
      s.progressPresenter.presentIndefiniteProgress(from: s)
      s.viewModel.deleteStripeAccount(
        onSuccess: s.handleRemoveAccountSuccess(),
        onError: s.handleError()
      )
    }
  }

  func handleRemoveAccountSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.navigationController?.popViewController(animated: true)
    }
  }
}

// MARK: - Actions

private extension PayoutController {
  @IBAction
  func editPayoutButtonTapped() {
    let vc = R.storyboard.payout.payoutEntryController()!
    vc.viewModel = PayoutEntryViewModel(stripeAccount: viewModel.stripeAccount)
    navigationController?.pushViewController(vc, animated: true)
  }

  @IBAction
  func removeAccountButtonTapped() {
    // TODO: Replace with dialogPresenter call
    showAlertDialog(
      title: S.payoutRemoveAccountNotice(),
      positiveButtonText: S.payoutRemoveAccount(),
      positiveHandler: handleRemoveAccountConfirmed(),
      negativeButtonText: S.cancel()
    )
  }
}

// MARK: - Utils

private extension PayoutController {
  func showAlertDialog(
    title: String,
    positiveButtonText: String,
    positiveHandler: SingleResult<UIAlertAction>?,
    negativeButtonText: String
  ) {
    let dialog = UIAlertController(
      title: title,
      message: nil,
      preferredStyle: .alert
    )

    dialog.addAction(UIAlertAction(
      title: negativeButtonText,
      style: .cancel,
      handler: nil
    ))

    dialog.addAction(UIAlertAction(
      title: positiveButtonText,
      style: .destructive,
      handler: positiveHandler
    ))

    present(dialog, animated: true)
  }
}
