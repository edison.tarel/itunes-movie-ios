//
//  PayoutViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PayoutViewModel: PayoutViewModelProtocol {
  private(set) var stripeAccount: StripeConnect!

  private let service: PaymentServiceProtocol

  init(service: PaymentServiceProtocol = App.shared.payment) {
    self.service = service
  }
}

// MARK: - Methods

extension PayoutViewModel {
  func fetchStripeAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.fetchStripeConnect(
      onSuccess: { [weak self] stripeConnect in
        guard let s = self else { return onError(APIClientError.unknown) }
        s.stripeAccount = stripeConnect
        onSuccess()
      },
      onError: onError
    )
  }

  func deleteStripeAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.deleteAuthConnectAccount(
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
