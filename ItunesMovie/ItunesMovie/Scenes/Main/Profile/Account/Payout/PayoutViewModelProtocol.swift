//
//  PayoutViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PayoutViewModelProtocol {
  var stripeAccount: StripeConnect! { get }

  func fetchStripeAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func deleteStripeAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
   
