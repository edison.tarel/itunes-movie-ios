//
//  AccountViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class AccountViewModel: AccountViewModelProtocol {
  private let session: SessionServiceProtocol!

  init(session: SessionServiceProtocol = App.shared.session) {
    self.session = session
  }
}

// MARK: - Getters

extension AccountViewModel {
  var profileImageURL: URL? { user.avatarThumbURL }
  var fullNameText: String? { user.fullName }
  var usernameText: String? { user.email ?? user.phoneNumber }

  private var user: User { session.user! }
}
