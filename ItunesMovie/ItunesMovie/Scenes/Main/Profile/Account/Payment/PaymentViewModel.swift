//
//  PaymentViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Stripe

class PaymentViewModel: NSObject, PaymentViewModelProtocol {
  private let service: PaymentServiceProtocol

  init(service: PaymentServiceProtocol = App.shared.payment) {
    self.service = service
  }

  func createCustomerKey(
    withAPIVersion apiVersion: String,
    completion: @escaping STPJSONResponseCompletionBlock
  ) {
    service.fetchEphemeralKey(
      onSuccess: { completion($0.dictionary(), nil) },
      onError: { completion(nil, $0) }
    )
  }
}
