//
//  ChangePhoneViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ChangePhoneViewModel: ChangePhoneViewModelProtocol {
  var onComplete: DoubleResult<String, String>?

  private let token: String
  private let service: CredentialsServiceProtocol

  init(
    token: String,
    service: CredentialsServiceProtocol = App.shared.session.credentials
  ) {
    self.token = token
    self.service = service
  }
}

// MARK: - Methods

extension ChangePhoneViewModel {
  func changePhone(
    countryCode: String,
    phoneNumber: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(phoneNumber)
    switch result {
    case let .success(phoneNumber):
      let fullPhoneNumber = countryCode + phoneNumber
      service.requestChangePhoneNumber(
        with: fullPhoneNumber,
        token: token,
        onSuccess: handleSuccess(
          with: countryCode,
          phoneNumber: phoneNumber,
          thenExecute: onSuccess
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Util Handlers

private extension ChangePhoneViewModel {
  func handleSuccess(
    with countryCode: String,
    phoneNumber: String,
    thenExecute completionHandler: @escaping VoidResult
  ) -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.onComplete?(countryCode, phoneNumber)
      completionHandler()
    }
  }
}
