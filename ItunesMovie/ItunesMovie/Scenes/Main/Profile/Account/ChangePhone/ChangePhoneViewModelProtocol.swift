//
//  ChangePhoneViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ChangePhoneViewModelProtocol: SingleFormInputViewModelProtocol {
  var onComplete: DoubleResult<String, String>? { get set }
  
  func changePhone(
    countryCode: String,
    phoneNumber: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension ChangePhoneViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return PhoneNumberInputValidator.validate(input).genericResult
  }
}
