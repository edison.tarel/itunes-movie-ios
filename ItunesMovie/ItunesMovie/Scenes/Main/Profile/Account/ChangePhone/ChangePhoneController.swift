//
//  ChangePhoneController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class ChangePhoneController: ScrollViewController {
  var viewModel: ChangePhoneViewModelProtocol!

  @IBOutlet private(set) var countryField: APCountryCodeField!

  @IBOutlet private(set) var field: MDCTextField!
  @IBOutlet private(set) var continueButton: MDCButton!

  private(set) var countryInputController: MDCTextInputControllerBase!
  private(set) var fieldInputController: MDCTextInputControllerBase!

  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension ChangePhoneController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    progressPresenter.dismiss()

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true

    view.endEditing(true)
  }
}

// MARK: - Setup

private extension ChangePhoneController {
  func setup() {
    setupCountryField()
    setupPhoneField()

    continueButton.applyStyle(.primary)
  }

  func setupCountryField() {
    countryInputController = MDCHelper.underlineInputController(for: countryField)

    countryField.isUserInteractionEnabled = false
    countryField.pickerDelegate = self
  }

  func setupPhoneField() {
    field.applyAttribute(.phoneNumber)

    fieldInputController = MDCHelper.underlineInputController(for: field)

    field.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension ChangePhoneController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

private extension ChangePhoneController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    guard let countryCode = countryField.text else {
      return debugPrint("countryField.text should not be nil at this point")
    }

    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.changePhone(
      countryCode: countryCode,
      phoneNumber: field.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - SingleFormInputControllerProtocol

extension ChangePhoneController: SingleFormInputControllerProtocol {
  var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }
}

// MARK: - UsernameCheckerControllerProtocol

extension ChangePhoneController: UsernameCheckerControllerProtocol {}
