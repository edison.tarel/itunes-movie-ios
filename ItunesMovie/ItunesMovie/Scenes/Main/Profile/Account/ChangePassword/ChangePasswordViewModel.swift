//
//  ChangePasswordViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ChangePasswordViewModel: ChangePasswordViewModelProtocol {
  var onComplete: VoidResult?

  private let password: String
  private let service: CredentialsServiceProtocol

  init(
    password: String,
    service: CredentialsServiceProtocol = App.shared.session.credentials
  ) {
    self.password = password
    self.service = service
  }
}

// MARK: - Methods

extension ChangePasswordViewModel {
  func changePassword(
    with newPassword: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(newPassword)
    switch result {
    case let .success(newPassword):
      service.changePassword(
        to: newPassword,
        oldPassword: password,
        onSuccess: handleSuccess(
          thenExecute: onSuccess
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Util Handlers

private extension ChangePasswordViewModel {
  func handleSuccess(
    thenExecute completionHandler: @escaping VoidResult
  ) -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.onComplete?()
      completionHandler()
    }
  }
}
