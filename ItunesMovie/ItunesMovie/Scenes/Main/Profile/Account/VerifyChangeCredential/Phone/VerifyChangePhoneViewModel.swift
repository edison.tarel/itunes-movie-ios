//
//  VerifyChangePhoneViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

class VerifyChangePhoneViewModel: VerifyChangeCredentialViewModelProtocol {
  var onComplete: VoidResult?
  
  private let countryCode: String
  private let phoneNumber: String
  private let verificationToken: String
  private let service: CredentialsServiceProtocol
  
  init(
    countryCode: String,
    phoneNumber: String,
    verificationToken: String,
    service: CredentialsServiceProtocol = App.shared.session.credentials
  ) {
    self.countryCode = countryCode
    self.phoneNumber = phoneNumber
    self.verificationToken = verificationToken
    self.service = service
  }
}

// MARK: - Methods

extension VerifyChangePhoneViewModel {
  func verify(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.verifyChangePhoneNumber(
      with: token,
      verificationToken: verificationToken,
      onSuccess: handleSuccess(
        thenExecute: onSuccess
      ),
      onError: onError
    )
  }
  
  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.requestChangePhoneNumber(
      with: fullPhoneNumber,
      token: verificationToken,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}

// MARK: - Util Handlers

private extension VerifyChangePhoneViewModel {
  func handleSuccess(
    thenExecute completionHandler: @escaping VoidResult
  ) -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.onComplete?()
      completionHandler()
    }
  }
}

// MARK: - Getters

extension VerifyChangePhoneViewModel {
  var titleText: String { S.verifyCredentialChangePhoneTitle() }
  var messageText: String { S.verifyCredentialChangePhoneMessage() }
  var subMessageText: String? { fullPhoneNumber }
  
  private var fullPhoneNumber: String { countryCode + phoneNumber }
}
