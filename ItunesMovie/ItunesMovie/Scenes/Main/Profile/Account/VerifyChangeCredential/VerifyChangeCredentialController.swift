//
//  VerifyChangeCredentialController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class VerifyChangeCredentialController: ScrollViewController {
  var viewModel: CodeVerificationViewModelProtocol!

  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var subMessageLabel: UILabel!
  @IBOutlet private(set) var resendCodeButton: UIButton!

  // Make sure that each field has its own designated Tag value (from 1 to N, left to right.)
  @IBOutlet private(set) var textFields: [VerificationCodeField]!

  private(set) var inputControllers: [MDCTextInputControllerUnderline] = []

  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - View LifeCycle

extension VerifyChangeCredentialController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    textFields.first!.becomeFirstResponder()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    view.endEditing(true)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension VerifyChangeCredentialController {
  func setup() {
    setupLabels()
    setupFields()
    setupFieldInputControllers()
  }

  func setupLabels() {
    titleLabel.text = viewModel.titleText
    messageLabel.text = viewModel.messageText
    subMessageLabel.text = viewModel.subMessageText
  }
}

// MARK: - Bind

private extension VerifyChangeCredentialController {
  func bind() {
    bindFields()
  }
}

// MARK: - Actions

private extension VerifyChangeCredentialController {
  @IBAction
  func resendCodeButtonTapped(_ sender: Any) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.resendCode(
      onSuccess: handleResendCodeSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

private extension VerifyChangeCredentialController {
  func handleVerificationSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
    }
  }

  func handleVerificationError() -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.infoPresenter.presentErrorInfo(error: error)
      s.clearFields(makeFirstFieldFirstResponder: true)
    }
  }

  func handleResendCodeSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.infoPresenter.presentSuccessInfo(message: S.accountVerifAlertsCodeSent())
      s.clearFields(makeFirstFieldFirstResponder: true)
    }
  }
}

// MARK: - Routing

private extension VerifyChangeCredentialController {
  func presentNextPage() {
    let vc = R.storyboard.authSignup.registrationNameController()!
    vc.viewModel = RegistrationNameViewModel()
    vc.logoutTriggerVM = LogoutTriggerViewModel()
    vc.inputCache = ProfileSetupInputCache()
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - CodeVerificationControllerProtocol

extension VerifyChangeCredentialController: CodeVerificationControllerProtocol {
  func setupFieldInputControllers() {
    inputControllers = generateInputControllers(for: textFields)
  }

  func handleCodeFillCompletion() -> SingleResult<String> {
    return { [weak self] code in
      guard let s = self else { return }
      s.progressPresenter.presentIndefiniteProgress(from: s)
      s.viewModel.verify(
        using: code,
        onSuccess: s.handleSuccess(),
        onError: s.handleVerificationError()
      )
    }
  }
}

// MARK: - VerificationCodeFieldDelegate

extension VerifyChangeCredentialController: VerificationCodeFieldDelegate {}

// MARK: - UITextFieldDelegate

extension VerifyChangeCredentialController: UITextFieldDelegate {
  func textField(
    _ textField: UITextField,
    shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    shouldChangeCharacters(
      for: textField,
      in: range,
      replacementString: string
    )
  }
}
