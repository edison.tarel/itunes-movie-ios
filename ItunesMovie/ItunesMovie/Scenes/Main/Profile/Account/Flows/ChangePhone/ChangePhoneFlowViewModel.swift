//
//  ChangePhoneFlowViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ChangePhoneFlowViewModel: ChangePhoneFlowViewModelProtocol {
  private(set) var verificationToken: String!
  private(set) var countryCode: String!
  private(set) var phoneNumber: String!
}

// MARK: - Methods

extension ChangePhoneFlowViewModel {
  func save(verificationToken: String) {
    self.verificationToken = verificationToken
  }

  func save(
    countryCode: String,
    phoneNumber: String
  ) {
    self.countryCode = countryCode
    self.phoneNumber = phoneNumber
  }
}

// MARK: - Getters

extension ChangePhoneFlowViewModel {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol {
    VerifyPasswordViewModel(
      messageText: S.verifyPasswordChangePhoneMessage()
    )
  }

  var changePhoneVM: ChangePhoneViewModelProtocol {
    ChangePhoneViewModel(
      token: verificationToken
    )
  }

  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol {
    VerifyChangePhoneViewModel(
      countryCode: countryCode,
      phoneNumber: phoneNumber,
      verificationToken: verificationToken
    )
  }
}
