//
//  ChangePhoneFlowViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol ChangePhoneFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol { get }
  var changePhoneVM: ChangePhoneViewModelProtocol { get }
  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol { get }
  
  func save(verificationToken: String)
  func save(
    countryCode: String,
    phoneNumber: String
  )
}
