//
//  ChangePhoneFlowCoordinator.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class ChangePhoneFlowCoordinator {
  lazy var viewModel: ChangePhoneFlowViewModelProtocol! = { ChangePhoneFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: UIViewController

  private var verificationToken: String!

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension ChangePhoneFlowCoordinator {
  func start() {
    navigateToVerifyPasswordPage()
  }
}

// MARK: - Routing

private extension ChangePhoneFlowCoordinator {
  func navigateToVerifyPasswordPage() {
    let vm = viewModel.verifyPasswordVM
    vm.onComplete = handleVerifyPassword()

    let vc = R.storyboard.accountSettings.verifyPasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func navigateToChangePhonePage() {
    let vm = viewModel.changePhoneVM
    vm.onComplete = handleChangePhone()

    let vc = R.storyboard.accountSettings.changePhoneController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func navigateToVerifyChangePhonePage() {
    var vm = viewModel.verifyChangeCredentialVM
    vm.onComplete = handleVerifyChangePhone()

    let vc = R.storyboard.accountSettings.verifyChangeCredentialController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }
  
  func presentSuccessMessage() {
    guard let vc = baseController as? PresentersProviderProtocol else { return }
    vc.infoPresenter.presentSuccessInfo(message: S.verifyCredentialChangePhoneSuccessMessage())
  }

  func dismiss() {
    navigationController.popToViewController(
      baseController,
      animated: true
    )
  }
}

// MARK: - Handlers

private extension ChangePhoneFlowCoordinator {
  func handleVerifyPassword() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let s = self else { return }
      s.viewModel.save(verificationToken: token)
      s.navigateToChangePhonePage()
    }
  }

  func handleChangePhone() -> DoubleResult<String, String> {
    return { [weak self] countryCode, phoneNumber in
      guard let s = self else { return }
      s.viewModel.save(
        countryCode: countryCode,
        phoneNumber: phoneNumber
      )
      s.navigateToVerifyChangePhonePage()
    }
  }

  func handleVerifyChangePhone() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.presentSuccessMessage()
      s.dismiss()
    }
  }
}
