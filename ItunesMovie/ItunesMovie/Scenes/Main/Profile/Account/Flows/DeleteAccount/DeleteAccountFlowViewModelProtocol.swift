//
//  DeleteAccountFlowViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol DeleteAccountFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol { get }
}
