//
//  DeleteAccountFlowViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class DeleteAccountFlowViewModel: DeleteAccountFlowViewModelProtocol {
}

// MARK: - Getters

extension DeleteAccountFlowViewModel {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol {
    VerifyPasswordViewModel(
      messageText: S.verifyPasswordDeleteAccountMessage()
    )
  }
}
