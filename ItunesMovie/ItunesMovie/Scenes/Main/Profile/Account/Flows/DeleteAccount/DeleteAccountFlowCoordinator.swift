//
//  DeleteAccountFlowCoordinator.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

class DeleteAccountFlowCoordinator {
  lazy var viewModel: DeleteAccountFlowViewModelProtocol! = { DeleteAccountFlowViewModel() }()
  
  private let navigationController: UINavigationController
  private let baseController: UIViewController

  private var verificationToken: String!

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension DeleteAccountFlowCoordinator {
  func start() {
    navigateToVerifyPasswordPage()
  }
}

// MARK: - Routing

private extension DeleteAccountFlowCoordinator {
  func navigateToVerifyPasswordPage() {
    let vm = viewModel.verifyPasswordVM
    vm.onComplete = handleVerifyPassword()

    let vc = R.storyboard.accountSettings.verifyPasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }
}

// MARK: - Handlers

private extension DeleteAccountFlowCoordinator {
  func handleVerifyPassword() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let s = self else { return }
      if let vc = s.baseController as? AccountSettingsController {
        vc.onViewWillAppear = s.handleBaseControllerOnViewWillAppear(vc)
      }

      s.navigationController.popToViewController(
        s.baseController,
        animated: true
      )
    }
  }

  func handleBaseControllerOnViewWillAppear(_ vc: AccountSettingsController) -> VoidResult {
    return {
      vc.deleteAccount()
    }
  }
}
