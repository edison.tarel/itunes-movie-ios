//
//  ChangePasswordFlowCoordinator.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordFlowCoordinator {
  lazy var viewModel: ChangePasswordFlowViewModelProtocol! = { ChangePasswordFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: UIViewController

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension ChangePasswordFlowCoordinator {
  func start() {
    navigateToVerifyPasswordPage()
  }
}

// MARK: - Routing

private extension ChangePasswordFlowCoordinator {
  func navigateToVerifyPasswordPage() {
    let vm = viewModel.verifyPasswordVM
    vm.onComplete = handleVerifyPassword()

    let vc = R.storyboard.accountSettings.verifyPasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func navigateToChangePasswordPage() {
    let vm = viewModel.changePasswordVM
    vm.onComplete = handleChangePassword()

    let vc = R.storyboard.accountSettings.changePasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }
  
  func presentSuccessMessage() {
    guard let vc = baseController as? PresentersProviderProtocol else { return }
    vc.infoPresenter.presentSuccessInfo(message: S.changePasswordSuccessMessage())
  }
  
  func dismiss() {
    navigationController.popToViewController(
      baseController,
      animated: true
    )
  }
}

// MARK: - Handlers

private extension ChangePasswordFlowCoordinator {
  func handleVerifyPassword() -> DoubleResult<String, String> {
    return { [weak self] _, password in
      guard let s = self else { return }
      s.viewModel.save(password: password)
      s.navigateToChangePasswordPage()
    }
  }

  func handleChangePassword() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.presentSuccessMessage()
      s.dismiss()
    }
  }
}
