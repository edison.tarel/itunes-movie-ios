//
//  ChangePasswordFlowViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ChangePasswordFlowViewModel: ChangePasswordFlowViewModelProtocol {
  private(set) var password: String!
}

// MARK: - Methods

extension ChangePasswordFlowViewModel {
  func save(password: String) {
    self.password = password
  }
}

// MARK: - Getters

extension ChangePasswordFlowViewModel {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol {
    VerifyPasswordViewModel(
      messageText: S.verifyPasswordChangePasswordMessage()
    )
  }

  var changePasswordVM: ChangePasswordViewModelProtocol {
    ChangePasswordViewModel(
      password: password
    )
  }
}
