//
//  ChangePasswordFlowViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol ChangePasswordFlowViewModelProtocol {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol { get }
  var changePasswordVM: ChangePasswordViewModelProtocol { get }

  func save(password: String)
}
