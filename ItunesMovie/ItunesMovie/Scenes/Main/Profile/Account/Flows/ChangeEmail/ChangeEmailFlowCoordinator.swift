//
//  ChangeEmailFlowCoordinator.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class ChangeEmailFlowCoordinator {
  lazy var viewModel: ChangeEmailFlowViewModelProtocol! = { ChangeEmailFlowViewModel() }()

  private let navigationController: UINavigationController
  private let baseController: UIViewController

  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    guard let baseController = navigationController.viewControllers.last else {
      preconditionFailure("UINavigationController should have at least one controller")
    }
    self.baseController = baseController
  }
}

// MARK: - Methods

extension ChangeEmailFlowCoordinator {
  func start() {
    navigateToVerifyPasswordPage()
  }
}

// MARK: - Routing

private extension ChangeEmailFlowCoordinator {
  func navigateToVerifyPasswordPage() {
    let vm = viewModel.verifyPasswordVM
    vm.onComplete = handleVerifyPassword()

    let vc = R.storyboard.accountSettings.verifyPasswordController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func navigateToChangeEmailPage() {
    let vm = viewModel.changeEmailVM
    vm.onComplete = handleChangeEmail()

    let vc = R.storyboard.accountSettings.changeEmailController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }

  func navigateToVerifyChangeEmailPage() {
    var vm = viewModel.verifyChangeCredentialVM
    vm.onComplete = handleVerifyChangeEmail()

    let vc = R.storyboard.accountSettings.verifyChangeCredentialController()!
    vc.viewModel = vm

    navigationController.pushViewController(vc, animated: true)
  }
  
  func presentSuccessMessage() {
    guard let vc = baseController as? PresentersProviderProtocol else { return }
    vc.infoPresenter.presentSuccessInfo(message: S.verifyCredentialChangeEmailSuccessMessage())
  }

  func dismiss() {
    navigationController.popToViewController(
      baseController,
      animated: true
    )
  }
}

// MARK: - Handlers

private extension ChangeEmailFlowCoordinator {
  func handleVerifyPassword() -> DoubleResult<String, String> {
    return { [weak self] token, _ in
      guard let s = self else { return }
      s.viewModel.save(verificationToken: token)
      s.navigateToChangeEmailPage()
    }
  }

  func handleChangeEmail() -> SingleResult<String> {
    return { [weak self] email in
      guard let s = self else { return }
      s.viewModel.save(email: email)
      s.navigateToVerifyChangeEmailPage()
    }
  }

  func handleVerifyChangeEmail() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.presentSuccessMessage()
      s.dismiss()
    }
  }
}
