//
//  ChangeEmailFlowViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class ChangeEmailFlowViewModel: ChangeEmailFlowViewModelProtocol {
  private(set) var verificationToken: String!
  private(set) var email: String!
}

// MARK: - Methods

extension ChangeEmailFlowViewModel {
  func save(verificationToken: String) {
    self.verificationToken = verificationToken
  }

  func save(email: String) {
    self.email = email
  }
}

// MARK: - Getters

extension ChangeEmailFlowViewModel {
  var verifyPasswordVM: VerifyPasswordViewModelProtocol {
    VerifyPasswordViewModel(
      messageText: S.verifyPasswordChangeEmailMessage()
    )
  }

  var changeEmailVM: ChangeEmailViewModelProtocol {
    ChangeEmailViewModel(
      token: verificationToken
    )
  }

  var verifyChangeCredentialVM: VerifyChangeCredentialViewModelProtocol {
    VerifyChangeEmailViewModel(
      email: email,
      verificationToken: verificationToken
    )
  }
}
