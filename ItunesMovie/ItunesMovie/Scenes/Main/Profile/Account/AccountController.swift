//
//  AccountController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Stripe
import UIKit

class AccountController: StaticTableViewController {
  var viewModel: AccountViewModelProtocol!
  lazy var paymentViewModel: PaymentViewModel! = { PaymentViewModel() }()
  lazy var payoutViewModel: PayoutViewModelProtocol! = { PayoutViewModel() }()

  @IBOutlet private(set) var profileImageView: ImageView!
  @IBOutlet private(set) var fullnameLabel: UILabel!
  @IBOutlet private(set) var usernameLabel: UILabel!
}

// MARK: - Lifecycle

extension AccountController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bind()
  }
}

// MARK: - Setup

private extension AccountController {
  func setup() {
    setupTable()
    refresh()
  }

  func setupTable() {
    tableView.tableFooterView = UIView()
  }
}

// MARK: - Bind

private extension AccountController {
  func bind() {
    bindPostNotification(
      notificationName: .didRefreshUser,
      onPost: handleUserRefresh()
    )
  }
}

// MARK: - Helpers

private extension AccountController {
  func refresh() {
    profileImageView.setImageWithURL(viewModel.profileImageURL)
    fullnameLabel.text = viewModel.fullNameText
    usernameLabel.text = viewModel.usernameText
  }
}

// MARK: - Handlers

private extension AccountController {
  func handleFetchSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      guard let stripeAccount = s.payoutViewModel.stripeAccount else {
        s.navigateToPayoutsEntryPage()
        return
      }

      s.navigateToPayoutsPage(stripeAccount)
    }
  }

  func handleFetchError() -> ErrorResult {
    return { [weak self] _ in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.navigateToPayoutsEntryPage()
    }
  }

  func handleUserRefresh() -> SingleResult<Notification> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.refresh()
    }
  }
}

// MARK: - Actions

extension AccountController {
  func fetchStripeAccount() {
    progressPresenter.presentIndefiniteProgress(from: self)

    payoutViewModel.fetchStripeAccount(
      onSuccess: handleFetchSuccess(),
      onError: handleFetchError()
    )
  }
}

// MARK: - UITableViewDelegate

extension AccountController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)

    guard let row = Row(rawValue: indexPath.row) else { return }
    navigateToPage(at: row)
  }
}

// MARK: - Routing

private extension AccountController {
  func navigateToPage(at row: Row) {
    switch row {
    case .editProfile:
      navigateToEditProfilePage()
    case .payments:
      navigateToPaymentsPage()
    case .payouts:
      fetchStripeAccount()
    case .about:
      navigateToAboutPage()
    case .accountSettings:
      navigateToAccountSettingsPage()
    }
  }

  func navigateToEditProfilePage() {
    let vc = R.storyboard.editProfile.editProfileController()!
    vc.viewModel = EditProfileViewModel()
    navigationController?.pushViewController(vc, animated: true)
  }

  func navigateToPaymentsPage() {
    let customerContext = STPCustomerContext(keyProvider: paymentViewModel)
    let paymentContext = STPPaymentContext(customerContext: customerContext)
    paymentContext.hostViewController = self
    paymentContext.presentPaymentOptionsViewController()
  }

  func navigateToPayoutsPage(_ stripeAccount: StripeConnect) {
    let vc = R.storyboard.payout.payoutController()!
    vc.viewModel = payoutViewModel
    navigationController?.pushViewController(vc, animated: true)
  }

  func navigateToPayoutsEntryPage() {
    let vc = R.storyboard.payout.payoutEntryController()!
    vc.viewModel = PayoutEntryViewModel()
    navigationController?.pushViewController(vc, animated: true)
  }

  func navigateToAboutPage() {
    let vc = R.storyboard.about.aboutController()!
    navigationController?.pushViewController(vc, animated: true)
  }

  func navigateToAccountSettingsPage() {
    if let vc = R.storyboard.profile.accountSettingsController() {
      vc.viewModel = AccountSettingsViewModel()
      navigationController?.pushViewController(vc, animated: true)
    }
  }
}

// MARK: - Types

private enum Row: Int {
  case editProfile = 0
  case payments
  case payouts
  case about
  case accountSettings
}
