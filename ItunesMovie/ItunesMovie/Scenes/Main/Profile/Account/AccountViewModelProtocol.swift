//
//  AccountViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol AccountViewModelProtocol {
  var profileImageURL: URL? { get }
  var fullNameText: String? { get }
  var usernameText: String? { get }
}
