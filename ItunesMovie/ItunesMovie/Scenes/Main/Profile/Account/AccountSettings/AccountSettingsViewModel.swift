//
//  AccountSettingsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class AccountSettingsViewModel: AccountSettingsViewModelProtocol {
  private let sessionService: SessionServiceProtocol
  private let logoutService: LogoutServiceProtocol
  private let accountDeletionService: AccountDeletionServiceProtocol

  init(
    sessionService: SessionServiceProtocol = App.shared.session,
    logoutService: LogoutServiceProtocol = App.shared.session.logout,
    accountDeletionService: AccountDeletionServiceProtocol = App.shared.session.accountDeletion
  ) {
    self.sessionService = sessionService
    self.logoutService = logoutService
    self.accountDeletionService = accountDeletionService
  }
}

// MARK: - Getters

extension AccountSettingsViewModel {
  var emailAddress: String { sessionService.user?.email ?? "" }
  var mobileNumber: String { sessionService.user?.phoneNumber ?? "" }
}

// MARK: - API

extension AccountSettingsViewModel {
  func logout(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    logoutService.logout(
      shouldBroadcast: true,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func deleteAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    guard let user = sessionService.user else { return }

    accountDeletionService.deleteAccount(
      withId: user.id,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
