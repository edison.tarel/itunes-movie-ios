//
//  AccountSettingsController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import SVProgressHUD

class AccountSettingsController: StaticTableViewController {
  var viewModel: AccountSettingsViewModelProtocol!

  var onViewWillAppear: VoidResult?

  @IBOutlet private(set) var emailAddressLabel: UILabel!
  @IBOutlet private(set) var mobileNumberLabel: UILabel!

  private var changeEmailFlowCoordinator: ChangeEmailFlowCoordinator!
  private var changePhoneFlowCoordinator: ChangePhoneFlowCoordinator!
  private var changePasswordFlowCoordinator: ChangePasswordFlowCoordinator!
  private var deleteAccountFlowCoordinator: DeleteAccountFlowCoordinator!
}

// MARK: - Lifecycle

extension AccountSettingsController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
    bind()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    onViewWillAppear?()
  }
}

// MARK: - Setup

private extension AccountSettingsController {
  func setup() {
    setupTable()
    setupFlowCoordinators()
    refresh()
  }

  func setupTable() {
    tableView.tableFooterView = UIView()
  }

  func setupFlowCoordinators() {
    guard let nc = navigationController else {
      preconditionFailure("Expecting non-nil navigationController got nil")
    }
    changeEmailFlowCoordinator = ChangeEmailFlowCoordinator(navigationController: nc)
    changePhoneFlowCoordinator = ChangePhoneFlowCoordinator(navigationController: nc)
    changePasswordFlowCoordinator = ChangePasswordFlowCoordinator(navigationController: nc)
    deleteAccountFlowCoordinator = DeleteAccountFlowCoordinator(navigationController: nc)
  }
}

// MARK: - Bind

private extension AccountSettingsController {
  func bind() {
    bindPostNotification(
      notificationName: .didRefreshUser,
      onPost: handleUserRefresh()
    )
  }
}

// MARK: - Actions

private extension AccountSettingsController {
  func deleteCellTapped() {
    showAlertDialog(
      title: S.dialogsDeleteAccountTitle(),
      positiveButtonText: S.dialogsDeleteAccountButtonsPositive(),
      positiveHandler: handleDeleteAccountConfirmed(),
      negativeButtonText: S.dialogsDeleteAccountButtonsNegative()
    )
  }

  func logoutCellTapped() {
    showAlertDialog(
      title: S.dialogsLogoutTitle(),
      positiveButtonText: S.dialogsLogoutButtonsPositive(),
      positiveHandler: handleLogoutConfirmed(),
      negativeButtonText: S.dialogsLogoutButtonsNegative()
    )
  }
}

// MARK: - Helpers

extension AccountSettingsController {
  private func refresh() {
    emailAddressLabel.text = viewModel.emailAddress
    mobileNumberLabel.text = viewModel.mobileNumber
  }
  
  func deleteAccount() {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.deleteAccount(
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }

  private func showAlertDialog(
    title: String,
    positiveButtonText: String,
    positiveHandler: SingleResult<UIAlertAction>?,
    negativeButtonText: String
  ) {
    let dialog = UIAlertController(
      title: title,
      message: nil,
      preferredStyle: .alert
    )

    dialog.addAction(UIAlertAction(
      title: negativeButtonText,
      style: .cancel,
      handler: nil
    ))

    dialog.addAction(UIAlertAction(
      title: positiveButtonText,
      style: .destructive,
      handler: positiveHandler
    ))

    present(dialog, animated: true)
  }
}

// MARK: - Event Handlers

private extension AccountSettingsController {
  // MARK: Logout

  func handleLogoutConfirmed() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let s = self else { return }

      s.progressPresenter.presentIndefiniteProgress(from: s)
      s.viewModel.logout(
        onSuccess: s.handleSuccess(),
        onError: s.handleError()
      )
    }
  }

  // MARK: Delete Account

  func handleDeleteAccountConfirmed() -> SingleResult<UIAlertAction> {
    return { [weak self] _ in
      guard let s = self else { return }
      s.deleteAccountFlowCoordinator.start()
    }
  }
  
  func handleUserRefresh() -> SingleResult<Notification> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.refresh()
    }
  }
}

// MARK: - UITableViewDelegate

extension AccountSettingsController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)

    guard let row = Row(rawValue: indexPath.row) else { return }
    navigateToPage(at: row)
  }
}

// MARK: - Routing

private extension AccountSettingsController {
  func navigateToPage(at row: Row) {
    switch row {
    case .emailAddress:
      changeEmailFlowCoordinator.start()
    case .mobileNumber:
      changePhoneFlowCoordinator.start()
    case .password:
      changePasswordFlowCoordinator.start()
    case .deleteAccount:
      deleteCellTapped()
    case .logout:
      logoutCellTapped()
    }
  }

  func navigateToChangePasswordPage() {
    if let vc = R.storyboard.accountSettings.changePasswordController() {
      navigationController?.pushViewController(vc, animated: true)
    }
  }
}

// MARK: - Types

private enum Row: Int {
  case emailAddress = 0
  case mobileNumber
  case password
  case deleteAccount
  case logout
}
