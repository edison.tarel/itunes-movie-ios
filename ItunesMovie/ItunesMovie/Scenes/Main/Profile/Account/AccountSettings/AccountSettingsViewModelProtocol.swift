//
//  AccountSettingsViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AccountSettingsViewModelProtocol {
  var emailAddress: String { get }
  var mobileNumber: String { get }

  func logout(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func deleteAccount(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
