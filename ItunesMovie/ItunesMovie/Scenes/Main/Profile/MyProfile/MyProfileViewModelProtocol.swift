//
//  MyProfileViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol MyProfileViewModelProtocol {
  var dataSource: [Post] { get }

  func fetchPosts(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
