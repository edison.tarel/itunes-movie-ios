//
//  MyProfilePostCell.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class MyProfilePostCell: UICollectionViewCell {
  var viewModel: MyProfilePostCellViewModelProtocol! {
    didSet { refresh() }
  }

  @IBOutlet private(set) var imageView: UIImageView!
}

// MARK: - Helpers

private extension MyProfilePostCell {
  func refresh() {
    imageView.af_setImage(withURL: viewModel.thumbUrl)
  }
}
