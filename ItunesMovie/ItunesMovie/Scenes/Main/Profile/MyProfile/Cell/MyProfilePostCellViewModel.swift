//
//  MyProfilePostCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

class MyProfilePostCellViewModel: MyProfilePostCellViewModelProtocol {
  let thumbUrl: URL

  init(post: Post) {
    self.thumbUrl = post.photo.thumbUrl
  }
}
