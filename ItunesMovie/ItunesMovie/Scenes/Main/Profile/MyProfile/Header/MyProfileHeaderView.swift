//
//  MyProfileHeaderView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class MyProfileHeaderView: UICollectionReusableView {
  var onEditProfileButtonTap: VoidResult?
  var viewModel: MyProfileHeaderViewModelProtocol! {
    didSet { refresh() }
  }

  @IBOutlet private(set) var avatarImageView: UIImageView!
  @IBOutlet private(set) var descriptionLabel: UILabel!
  @IBOutlet private(set) var followersCountLabel: UILabel!
  @IBOutlet private(set) var followingCountLabel: UILabel!
  @IBOutlet private(set) var postsCountLabel: UILabel!
  @IBOutlet private(set) var stackView: UIStackView!
}

// MARK: - Lifecycle

extension MyProfileHeaderView {
  override func awakeFromNib() {
    super.awakeFromNib()
    setup()
    bind()
  }
}

// MARK: - Setup

private extension MyProfileHeaderView {
  func setup() {
    setupAvatarImageView()
  }

  func setupAvatarImageView() {
    avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height / 2
    avatarImageView.layer.masksToBounds = true
  }
}

// MARK: - Bind

private extension MyProfileHeaderView {
  func bind() {
    bindPostNotification(
      notificationName: .didRefreshUser,
      onPost: handleUserRefresh()
    )
  }
}

// MARK: - Actions

private extension MyProfileHeaderView {
  @IBAction
  func editProfileButtonTapped(_ sender: Any) {
    onEditProfileButtonTap?()
  }
}

// MARK: - Helpers

private extension MyProfileHeaderView {
  func refresh() {
    refreshAvatarImageVew()
    refreshCounts()
    refreshDescriptionLabel()
  }

  func refreshAvatarImageVew() {
    avatarImageView.af_setImage(withURL: viewModel.avatarUrl)
  }

  func refreshCounts() {
    followersCountLabel.text = "\(viewModel.followersCount)"
    followingCountLabel.text = "\(viewModel.followingCount)"
    postsCountLabel.text = "\(viewModel.postsCount)"
  }

  func refreshDescriptionLabel() {
    descriptionLabel.text = viewModel.descriptionText
  }
}

// MARK: - Handlers

private extension MyProfileHeaderView {
  func handleUserRefresh() -> SingleResult<Notification> {
    return { [weak self] _ in
      guard let self = self else { return }
      self.refresh()
    }
  }
}
