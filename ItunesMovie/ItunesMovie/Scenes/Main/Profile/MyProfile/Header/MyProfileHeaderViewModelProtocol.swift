//
//  MyProfileHeaderViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol MyProfileHeaderViewModelProtocol {
  var avatarUrl: URL { get }
  var descriptionText: String { get }
  var followersCount: Int { get }
  var followingCount: Int { get }
  var postsCount: Int { get }
}
