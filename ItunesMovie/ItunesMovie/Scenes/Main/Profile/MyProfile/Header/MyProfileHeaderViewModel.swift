//
//  MyProfileHeaderViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MyProfileHeaderViewModel: MyProfileHeaderViewModelProtocol {
  private let session: SessionServiceProtocol

  init(
    session: SessionServiceProtocol = App.shared.session
  ) {
    self.session = session
  }
}

// MARK: - Getters

extension MyProfileHeaderViewModel {
  private var user: User { session.user! }

  var avatarUrl: URL { user.avatarThumbURL }
  var descriptionText: String { user.description ?? "" }
  var followersCount: Int { 0 }
  var followingCount: Int { 0 }
  var postsCount: Int { 0 }
}
