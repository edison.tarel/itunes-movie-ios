//
//  MyProfileViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MyProfileViewModel: MyProfileViewModelProtocol {
  private let service: PostsServiceProtocol
  private(set) var dataSource: [Post] = []

  init(
    service: PostsServiceProtocol = App.shared.posts
  ) {
    self.service = service
  }
}

// MARK: - Getters

extension MyProfileViewModel {
  func fetchPosts(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.fetchPosts(
      userId: nil,
      page: .default,
      include: PostsIncludes.allCases,
      onSuccess: handleFetchPostsSuccess(thenExecute: onSuccess),
      onError: onError
    )
  }
}

// MARK: - Handlers

private extension MyProfileViewModel {
  func handleFetchPostsSuccess(
    thenExecute callback: @escaping VoidResult
  ) -> SingleResult<[Post]> {
    return { [weak self] posts in
      guard let s = self else { return }
      s.dataSource = posts
      callback()
    }
  }
}
