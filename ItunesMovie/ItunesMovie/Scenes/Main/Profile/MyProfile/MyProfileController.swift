//
//  MyProfileController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class MyProfileController: ViewController {
  var viewModel: MyProfileViewModelProtocol!

  @IBOutlet private(set) var collectionView: UICollectionView!
  @IBOutlet var titleView: UIBarButtonItem!

  var flowLayout: UICollectionViewFlowLayout!
}

// MARK: - Lifecycle

extension MyProfileController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    if viewModel.dataSource.isEmpty {
      fetchPosts()
    }
  }

  override func viewWillLayoutSubviews() {
    let itemWidth = (collectionView.bounds.size.width / 3)
    let rounded = itemWidth.rounded(.down)
    flowLayout.itemSize = CGSize(width: rounded, height: rounded)
  }
}

// MARK: - Setup

private extension MyProfileController {
  func setup() {
    setupCollectionView()
    setupTitleView()
  }

  func setupCollectionView() {
    flowLayout = UICollectionViewFlowLayout()
    flowLayout.minimumLineSpacing = 0
    flowLayout.minimumInteritemSpacing = 0
    flowLayout.sectionInset = .zero
    flowLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 300)

    collectionView.delegate = self
    collectionView.dataSource = self
    collectionView.collectionViewLayout = flowLayout
  }

  func setupTitleView() {
    titleView.setTitleTextAttributes(
      [
        .foregroundColor: UIColor.label,
        .font: R.font.sfProTextBold(size: 28)!
      ],
      for: .disabled
    )
  }
}

// MARK: - Actions

private extension MyProfileController {
  @IBAction
  func gearButtonTapped(_ sender: Any?) {
    navigateToAccountPage()
  }
}

// MARK: - Routing

private extension MyProfileController {
  func navigateToAccountPage() {
    let vc = R.storyboard.profile.accountController()!
    vc.viewModel = AccountViewModel()
    vc.hidesBottomBarWhenPushed = true
    navigationController?.pushViewController(vc, animated: true)
  }

  func navigateToEditProfilePage() {
    let vc = R.storyboard.editProfile.editProfileController()!
    vc.viewModel = EditProfileViewModel()    
    vc.hidesBottomBarWhenPushed = true
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - API Methods

private extension MyProfileController {
  func fetchPosts() {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.fetchPosts(
      onSuccess: handleFetchPostsSuccess(),
      onError: handleFetchPostsError()
    )
  }
}

// MARK: - Handlers

private extension MyProfileController {
  func handleEditProfileButtonTap() -> VoidResult {
    return { [weak self] in
      self?.navigateToEditProfilePage()
    }
  }

  // MARK: Fetch Posts

  func handleFetchPostsSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.collectionView.reloadData()
    }
  }

  func handleFetchPostsError() -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.infoPresenter.presentErrorInfo(error: error)
    }
  }
}

// MARK: - UICollectionViewDelegate

extension MyProfileController: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    // noop
  }

  func collectionView(
    _ collectionView: UICollectionView,
    viewForSupplementaryElementOfKind kind: String,
    at indexPath: IndexPath
  ) -> UICollectionReusableView {
    let id = R.reuseIdentifier.myProfileHeader.identifier
    guard
      let header = collectionView.dequeueReusableSupplementaryView(
        ofKind: UICollectionView.elementKindSectionHeader,
        withReuseIdentifier: id,
        for: indexPath
      ) as? MyProfileHeaderView
    else {
      return UICollectionReusableView()
    }

    header.viewModel = MyProfileHeaderViewModel()
    header.onEditProfileButtonTap = handleEditProfileButtonTap()

    // Calculate the expected height of the label. This so that the label is not cut-off
    // when the description is long.
    // See: https://stackoverflow.com/questions/25642493/dynamic-uicollectionview-header-size-based-on-uilabel/55467666#answer-55467666
    let horizontalMargins: CGFloat = 32
    let initialLabelHeight = header.descriptionLabel.frame.size.height
    let preferredLabelSize = header.descriptionLabel.sizeThatFits(CGSize(
      width: collectionView.frame.width - horizontalMargins,
      height: .greatestFiniteMagnitude
    ))
    let updatedHeight = preferredLabelSize.height + header.frame.size.height - initialLabelHeight

    flowLayout.headerReferenceSize = CGSize(
      width: collectionView.bounds.width,
      height: updatedHeight
    )

    return header
  }
}

// MARK: - UICollectionViewDataSource

extension MyProfileController: UICollectionViewDataSource {
  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    return viewModel.dataSource.count
  }

  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    let id = R.reuseIdentifier.myProfilePostCell.identifier
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id, for: indexPath)

    guard let postCell = cell as? MyProfilePostCell else {
      return UICollectionViewCell()
    }

    let post = viewModel.dataSource[indexPath.item]
    postCell.viewModel = MyProfilePostCellViewModel(post: post)

    return postCell
  }
}
