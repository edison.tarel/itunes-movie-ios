//
//  HomeController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright (c) 2020 Appetiser Pty Ltd. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

class HomeController: UIViewController {
  @IBOutlet private var welcomeLabel: UILabel!
  // @IBOutlet private weak var <# Property #>: <# PropertyType #>!

  var viewModel: HomeViewModelType!

  // override var preferredStatusBarStyle: UIStatusBarStyle {
  //  return .darkContent
  // }

  // private let <# Property #>: <# PropertyType #> = <# InitialValue #>
  // private let <# Property #>: <# PropertyType #> = <# InitialValue #>
}

// MARK: - View LifeCycle

extension HomeController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setupView()
    setupBindings()

    guard let user = App.shared.session.user else {
      preconditionFailure("Expected User model. Got nil.")
    }
    updateView(with: user)
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: animated)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
}

// MARK: - View Setups & Bindings

private extension HomeController {
  func setupView() {
  }

  func setupBindings() {
    //
  }

  func updateView(with user: User) {
    welcomeLabel.text = S.homeLabelsWelcomeUser(user.fullName ?? "")
  }
}

// MARK: - Actions

private extension HomeController {
}
