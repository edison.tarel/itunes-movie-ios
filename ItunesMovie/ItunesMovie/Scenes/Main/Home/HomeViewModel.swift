//
//  HomeViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright (c) 2020 Appetiser Pty Ltd. All rights reserved.
//

import NSObject_Rx
import RxSwift

protocol HomeViewModelType: AnyObject {}

extension HomeViewModelType {}

class HomeViewModel: HomeViewModelType, HasDisposeBag {
  init() {}
}
