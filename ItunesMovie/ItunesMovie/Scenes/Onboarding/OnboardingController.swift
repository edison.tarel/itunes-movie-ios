//
//  OnboardingController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Pageboy
import UIKit

class OnboardingController: ViewController {
  var viewModel: OnboardingViewModelProtocol!

  @IBOutlet private(set) var pageContentView: UIView!
  @IBOutlet private(set) var pageControl: PageControl!
  @IBOutlet private(set) var skipButton: UIButton!
  @IBOutlet private(set) var nextButton: UIButton!
  @IBOutlet private(set) var continueButton: FormButton!
  @IBOutlet private(set) var continueBtnHeight: NSLayoutConstraint!

  private(set) var pageController: PageController!
  private var origContinueBtnHeight: CGFloat = 0
}

// MARK: - Lifecycle

extension OnboardingController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)
  }
}

// MARK: - Setup

private extension OnboardingController {
  func setupViews() {
    guard viewModel != nil else { return }

    setupPageControl()
    setupPageController()

    skipButton.isHidden = viewModel.shouldHideSkipButton
    nextButton.isHidden = viewModel.shouldHideNextButton

    origContinueBtnHeight = continueButton.frame.size.height
    continueBtnHeight.constant = 0.0
  }

  func setupPageControl() {
    pageControl.numberOfPages = viewModel.pageVMs.count
    pageControl.addTarget(
      self,
      action: #selector(pageControlValueChanged(_:)),
      for: .valueChanged
    )
  }

  func setupPageController() {
    pageController = PageController()
    pageController.delegate = self
    pageController.dataSource = self
    pageController.transition = .init(style: .fade, duration: 0.4)

    pageContentView.backgroundColor = .clear
    addChild(pageController)
    pageContentView.addSubview(pageController.view)
    pageController.view.autoPinEdgesToSuperviewEdges()
    pageController.didMove(toParent: self)
  }

  func setupUIForPage(index: Int) {
    let lastPageIndex = (pageController.pageCount ?? 0) - 1
    let didScrollToLastPage = (index == lastPageIndex)
    let isContinueButtonHidden = continueBtnHeight.constant.isZero
    let shouldToggleContinueButtonVisibility = (didScrollToLastPage && isContinueButtonHidden) || !isContinueButtonHidden

    guard shouldToggleContinueButtonVisibility else { return }

    toggleContinueButton(shouldShow: didScrollToLastPage)
  }

  func toggleContinueButton(shouldShow: Bool) {
    let height = shouldShow ? origContinueBtnHeight : 0.0
    continueBtnHeight.constant = CGFloat(height)
    UIView.animate(withDuration: 0.3) { [unowned self] in
      self.view.layoutIfNeeded()
      self.continueButton.alpha = shouldShow.cgFloatValue()
      self.skipButton.alpha = (!shouldShow).cgFloatValue()
      self.nextButton.alpha = (!shouldShow).cgFloatValue()
    }
  }
}

// MARK: - Actions

private extension OnboardingController {
  @objc
  func pageControlValueChanged(_ sender: AnyObject) {
    pageController.scrollToPage(
      .at(index: pageControl.currentPage),
      animated: true
    )
  }

  @IBAction
  func onSkipTapped(_ sender: AnyObject) {
    viewModel.onSkip?()
  }

  @IBAction
  func onNextTapped(_ sender: AnyObject) {
    let nextIndex = pageControl.currentPage + 1
    guard nextIndex < pageControl.numberOfPages else { return }

    pageControl.setCurrentPage(nextIndex, animated: true)
    pageController.scrollToPage(.at(index: pageControl.currentPage), animated: true)
  }

  @IBAction
  func onContinueTapped(_ sender: AnyObject) {
    viewModel.onContinue?()
  }
}

// MARK: - PageboyViewControllerDataSource

extension OnboardingController: PageboyViewControllerDataSource {
  func numberOfViewControllers(in pageboyViewController: PageboyViewController) -> Int {
    viewModel.pageVMs.count
  }

  func viewController(
    for pageboyViewController: PageboyViewController,
    at index: PageboyViewController.PageIndex
  ) -> UIViewController? {
    let vc = R.storyboard.onboarding.pageContentController()!
    if let vm = viewModel.pageVMs[safe: index] {
      vc.viewModel = vm
    }
    return vc
  }

  func defaultPage(for pageboyViewController: PageboyViewController) -> PageboyViewController.Page? {
    .first
  }
}

// MARK: - PageboyViewControllerDelegate

extension OnboardingController: PageboyViewControllerDelegate {
  func pageboyViewController(
    _ pageboyViewController: PageboyViewController,
    didScrollTo position: CGPoint,
    direction: PageboyViewController.NavigationDirection,
    animated: Bool
  ) {
    guard pageControl.isUserInteractionEnabled else { return }

    let targetOffset = UIScreen.main.bounds.width * position.x
    let index = Int((targetOffset / UIScreen.main.bounds.width).rounded())
    pageControl.setCurrentPage(index, animated: true)
  }

  func pageboyViewController(
    _ pageboyViewController: PageboyViewController,
    willScrollToPageAt index: PageboyViewController.PageIndex,
    direction: PageboyViewController.NavigationDirection,
    animated: Bool
  ) {}

  func pageboyViewController(
    _ pageboyViewController: PageboyViewController,
    didScrollToPageAt index: PageboyViewController.PageIndex,
    direction: PageboyViewController.NavigationDirection,
    animated: Bool
  ) {
    DispatchQueue.main.async {
      self.setupUIForPage(index: index)
    }
  }

  func pageboyViewController(
    _ pageboyViewController: PageboyViewController,
    didReloadWith currentViewController: UIViewController,
    currentPageIndex: PageboyViewController.PageIndex
  ) {}
}
