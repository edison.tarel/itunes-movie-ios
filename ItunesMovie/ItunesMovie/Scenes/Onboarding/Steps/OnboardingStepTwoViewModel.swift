//
//  OnboardingStepTwoViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class OnboardingStepTwoViewModel: PageContentViewModelProtocol {}

// MARK: - Getters

extension OnboardingStepTwoViewModel {
  var title: String { S.onboardingLabelsStep(2) }
  var description: String { S.onboardingLabelsDescriptions() }
  var image: UIImage { R.image.intro2()! }
  var imageBackgroundColor: UIColor { R.color.blue_2C66D5()! }
}
