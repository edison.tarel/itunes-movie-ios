//
//  OnboardingStepOneViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class OnboardingStepOneViewModel: PageContentViewModelProtocol {}

// MARK: - Getters

extension OnboardingStepOneViewModel {
  var title: String { S.onboardingLabelsStep(1) }
  var description: String { S.onboardingLabelsDescriptions() }
  var image: UIImage { R.image.intro1()! }
  var imageBackgroundColor: UIColor { R.color.purple_5A489B()! }
}
