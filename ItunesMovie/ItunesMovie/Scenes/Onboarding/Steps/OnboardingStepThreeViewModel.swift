//
//  OnboardingStepThreeViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class OnboardingStepThreeViewModel: PageContentViewModelProtocol {}

// MARK: - Getters

extension OnboardingStepThreeViewModel {
  var title: String { S.onboardingLabelsStep(3) }
  var description: String { S.onboardingLabelsDescriptions() }
  var image: UIImage { R.image.intro3()! }
  var imageBackgroundColor: UIColor { R.color.purple_5A489B()! }
}
