//
//  OnboardingStepFourViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

class OnboardingStepFourViewModel: PageContentViewModelProtocol {}

// MARK: - Getters

extension OnboardingStepFourViewModel {
  var title: String { S.onboardingLabelsStep(4) }
  var description: String { S.onboardingLabelsDescriptions() }
  var image: UIImage { R.image.intro4()! }
  var imageBackgroundColor: UIColor { R.color.blue_2C66D5()! }
}
