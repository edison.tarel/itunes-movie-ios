//
//  OnboardingViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class OnboardingViewModel: OnboardingViewModelProtocol {
  var onContinue: VoidResult?
  var onSkip: VoidResult?
}

// MARK: - Getters

extension OnboardingViewModel {
  var pageVMs: [PageContentViewModelProtocol] {
    [
      OnboardingStepOneViewModel(),
      OnboardingStepTwoViewModel(),
      OnboardingStepThreeViewModel(),
      OnboardingStepFourViewModel()
    ]
  }
  
  var shouldHideNextButton: Bool { false }
  var shouldHideSkipButton: Bool { false }
}
