//
//  UsernameCheckerControllerProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import SVProgressHUD

protocol UsernameCheckerControllerProtocol where Self: SingleFormInputControllerProtocol {
  func presentCreatePasswordPage(with viewModel: CreatePasswordViewModelProtocol)
  func presentLoginPage(with viewModel: LoginFormViewModelProtocol)

  func handleAvailable() -> SingleResult<CreatePasswordViewModelProtocol>
  func handleUnavailable() -> SingleResult<LoginFormViewModelProtocol>
}

// MARK: - Handlers Defaults

extension UsernameCheckerControllerProtocol {
  func handleAvailable() -> SingleResult<CreatePasswordViewModelProtocol> {
    return { [weak self] passwordVM in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.clearFormErrors()
      s.presentCreatePasswordPage(with: passwordVM)
    }
  }

  func handleUnavailable() -> SingleResult<LoginFormViewModelProtocol> {
    return { [weak self] loginVM in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.clearFormErrors()
      s.presentLoginPage(with: loginVM)
    }
  }
}

// MARK: - Utils

private extension UsernameCheckerControllerProtocol {
  func clearFormErrors() {
    fieldInputController.setErrorText(nil, errorAccessibilityValue: nil)
  }
}

// MARK: - Routing Defaults

extension UsernameCheckerControllerProtocol {
  func presentCreatePasswordPage(with viewModel: CreatePasswordViewModelProtocol) {
    let vc = R.storyboard.authSignup.createPasswordController()!
    vc.viewModel = viewModel
    navigationController?.pushViewController(vc, animated: true)
  }

  func presentLoginPage(with viewModel: LoginFormViewModelProtocol) {
    let vc = R.storyboard.authLogin.loginFormController()!
    vc.viewModel = viewModel
    navigationController?.pushViewController(vc, animated: true)
  }
}
