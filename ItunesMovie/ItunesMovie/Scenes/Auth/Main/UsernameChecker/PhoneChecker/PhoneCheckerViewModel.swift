//
//  PhoneCheckerViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhoneCheckerViewModel: PhoneCheckerViewModelProtocol {
  let initialCountryCode: String
  let initialPhoneNumber: String

  private let usernameChecker: UsernameCheckerServiceProtocol

  init(
    initialCountryCode: String = "",
    initialPhoneNumber: String = "",
    usernameChecker: UsernameCheckerServiceProtocol = App.shared.guest.usernameChecker
  ) {
    self.initialCountryCode = initialCountryCode
    self.initialPhoneNumber = initialPhoneNumber
    self.usernameChecker = usernameChecker
  }
}

// MARK: - Methods

extension PhoneCheckerViewModel {
  func checkPhoneNumberAvailability(
    countryCode: String,
    phoneNumber: String?,
    onAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    let result = validate(phoneNumber)
    switch result {
    case let .success(phoneNumber):
      let fullPhoneNumber = countryCode + phoneNumber
      usernameChecker.checkUsernameAvailability(
        fullPhoneNumber,
        onAvailable: handleAvailable(
          with: countryCode,
          phoneNumber: phoneNumber,
          thenExecute: onAvailable
        ),
        onUnavailable: handleUnavailable(
          with: countryCode,
          phoneNumber: phoneNumber,
          thenExecute: onUnavailable
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Utils

private extension PhoneCheckerViewModel {
  func handleAvailable(
    with countryCode: String,
    phoneNumber: String,
    thenExecute handler: @escaping SingleResult<CreatePasswordViewModelProtocol>
  ) -> VoidResult {
    return {
      let vm = PhoneCreatePasswordViewModel(
        countryCode: countryCode,
        phoneNumber: phoneNumber
      )
      handler(vm)
    }
  }

  func handleUnavailable(
    with countryCode: String,
    phoneNumber: String,
    thenExecute handler: @escaping SingleResult<LoginFormViewModelProtocol>
  ) -> VoidResult {
    return {
      let vm = PhoneLoginFormViewModel(
        countryCode: countryCode,
        phoneNumber: phoneNumber
      )
      handler(vm)
    }
  }
}
