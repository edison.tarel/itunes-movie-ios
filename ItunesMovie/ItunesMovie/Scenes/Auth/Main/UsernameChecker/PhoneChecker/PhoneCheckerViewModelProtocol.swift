//
//  PhoneCheckerViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol PhoneCheckerViewModelProtocol: SingleFormInputViewModelProtocol {
  var initialCountryCode: String { get }
  var initialPhoneNumber: String { get }

  func checkPhoneNumberAvailability(
    countryCode: String,
    phoneNumber: String?,
    onAvailable: @escaping SingleResult<CreatePasswordViewModelProtocol>,
    onUnavailable: @escaping SingleResult<LoginFormViewModelProtocol>,
    onError: @escaping ErrorResult
  )
}

// MARK: - SingleFormInputViewModelProtocol

extension PhoneCheckerViewModelProtocol {
  func validate(_ input: String?) -> Result<String, Error> {
    return PhoneNumberInputValidator.validate(input).genericResult
  }
}
