//
//  PhoneCheckerController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class PhoneCheckerController: ViewController {
  var viewModel: PhoneCheckerViewModelProtocol!
  var singleFormInputVM: SingleFormInputViewModelProtocol! { viewModel }

  @IBOutlet private(set) var scrollContentView: UIView!
  @IBOutlet private(set) var countryField: APCountryCodeField!

  @IBOutlet private(set) var field: MDCTextField!
  @IBOutlet private(set) var continueButton: MDCButton!

  private(set) var countryInputController: MDCTextInputControllerBase!
  private(set) var fieldInputController: MDCTextInputControllerBase!
  
  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - LifeCycle

extension PhoneCheckerController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension PhoneCheckerController {
  func setup() {
    setupCountryField()
    setupPhoneField()

    continueButton.applyStyle(.primary)
  }

  func setupCountryField() {
    countryInputController = MDCHelper.underlineInputController(for: countryField)

    countryField.isUserInteractionEnabled = false
    countryField.pickerDelegate = self
  }

  func setupPhoneField() {
    field.applyAttribute(.phoneNumber)

    fieldInputController = MDCHelper.underlineInputController(for: field)

    field.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension PhoneCheckerController {
  func bind() {
    bindField()
    bindContinueButton()
  }
}

// MARK: - Actions

extension PhoneCheckerController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    guard let countryCode = countryField.text else {
      return debugPrint("countryField.text should not be nil at this point")
    }

    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.checkPhoneNumberAvailability(
      countryCode: countryCode,
      phoneNumber: field.text,
      onAvailable: handleAvailable(),
      onUnavailable: handleUnavailable(),
      onError: handleError()
    )
  }
}

// MARK: - SingleFormInputControllerProtocol

extension PhoneCheckerController: SingleFormInputControllerProtocol {}

// MARK: - UsernameCheckerControllerProtocol

extension PhoneCheckerController: UsernameCheckerControllerProtocol {}
