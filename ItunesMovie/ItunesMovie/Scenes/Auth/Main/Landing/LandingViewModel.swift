//
//  LandingViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class LandingViewModel: LandingViewModelProtocol {
  private let socialAuth: SocialAuthServiceProtocol

  init(socialAuth: SocialAuthServiceProtocol = App.shared.session.socialAuth) {
    self.socialAuth = socialAuth
  }
}

// MARK: Methods

extension LandingViewModel {
  func authenticateWithApple(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    socialAuth.authenticateWithApple(
      using: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func authenticateWithGoogle(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    socialAuth.authenticateWithGoogle(
      using: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func authenticateWithFacebook(
    using token: String,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    socialAuth.authenticateWithFacebook(
      using: token,
      onSuccess: onSuccess,
      onError: onError
    )
  }
}
