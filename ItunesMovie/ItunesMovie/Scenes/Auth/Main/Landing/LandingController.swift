//
//  LandingController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import ActiveLabel
import AuthenticationServices
import GoogleSignIn
import MaterialComponents.MDCButton
import PureLayout
import RxSwift
import SVProgressHUD
import UIKit

class LandingController: ScrollViewController {
  var viewModel: LandingViewModelProtocol!

  lazy var appDocumentPresenter: AppDocumentPresenterProtocol = AppDocumentPresenter()
  lazy var applePresenter: AppleSignInPresenterProtocol = AppleSignInPresenter()
  lazy var googlePresenter: GoogleSignInPresenterProtocol = GoogleSignInPresenter()
  lazy var fbPresenter: FacebookLoginPresenterProtocol = FacebookLoginPresenter()

  @IBOutlet private(set) var rootStackView: UIStackView!
  @IBOutlet private(set) var headerView: UIView!
  @IBOutlet private(set) var appNameLabel: UILabel!
  @IBOutlet private(set) var continueButton: MDCButton!
  @IBOutlet private(set) var continueWithPhoneButton: MDCButton!
  @IBOutlet private(set) var appleSignInButton: LeftAlignedIconMDCButton!
  @IBOutlet private(set) var appleSignInButtonSpacer: UIView!
  @IBOutlet private(set) var googleSignInButton: LeftAlignedIconMDCButton!
  @IBOutlet private(set) var facebookSignInButton: LeftAlignedIconMDCButton!
  @IBOutlet private(set) var tosAcceptanceLabel: ActiveLabel!

  private(set) var onSuccess: SingleResult<String>?
  private(set) var onError: SingleResult<GoogleSignInPresenterError>?

  override var preferredStatusBarStyle: UIStatusBarStyle { .lightContent }
}

// MARK: - View LifeCycle

extension LandingController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(true, animated: animated)

    applyStatusBarColor(Styles.Colors.primaryColor)
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    progressPresenter.dismiss()
  }
}

// MARK: - View Setups & Bindings

private extension LandingController {
  func setup() {
    setupHandlers()
    setupViews()
  }

  func setupHandlers() {
    applePresenter.presentationAnchorProvider = { [weak self] in self?.view.window }
    applePresenter.onSuccess = handleAppleSignInSuccess()
    applePresenter.onError = handleError()
    applePresenter.onCancel = DefaultClosure.voidResult()

    googlePresenter.controllerProvider = { [unowned self] in self }
    googlePresenter.onSuccess = handleGoogleSignInSuccess()
    googlePresenter.onError = handleError()
    googlePresenter.onCancel = DefaultClosure.voidResult()
  }

  func setupViews() {
    headerView.layer.cornerRadius = 50
    headerView.layer.maskedCorners = [.layerMinXMaxYCorner]

    appNameLabel.text = S.appName()
    scrollView.delegate = self

    setupButtons()
    setupTermsOfServiceLabel()
  }

  func setupButtons() {
    continueButton.applyStyle(.primary)
    continueWithPhoneButton.applyStyle(.primary)
    googleSignInButton.applyStyle(.white)
    facebookSignInButton.applyStyle(.white)

    if #available(iOS 13.0, *) {
      appleSignInButton.applyStyle(.black)
    } else {
      appleSignInButton.removeFromSuperview()
      appleSignInButtonSpacer.removeFromSuperview()
    }
  }
}

// MARK: - Actions

private extension LandingController {
  @IBAction
  func emailCheckerButtonTapped(_ sender: AnyObject) {
    let emailChecker = R.storyboard.auth.emailCheckerController()!
    emailChecker.viewModel = EmailCheckerViewModel()

    navigationController?.pushViewController(emailChecker, animated: true)
  }

  @IBAction
  func phoneCheckerButtonTapped(_ sender: AnyObject) {
    let phoneChecker = R.storyboard.auth.phoneCheckerController()!
    phoneChecker.viewModel = PhoneCheckerViewModel()

    navigationController?.pushViewController(phoneChecker, animated: true)
  }

  @IBAction
  func appleSignInButtonTapped(_ sender: AnyObject) {
    if #available(iOS 13.0, *) {
      applePresenter.presentAppleSignIn()
    }
  }

  @IBAction
  func googleSignInButtonTapped(_ sender: AnyObject) {
    googlePresenter.presentGoogleSignIn()
  }

  @IBAction
  func facebookLoginButtonTapped(_ sender: AnyObject) {
    fbPresenter.presentFacebookLogin(
      onSuccess: handleFBLoginSuccess(),
      onError: handleError(),
      onCancel: DefaultClosure.voidResult()
    )
  }
}

// MARK: - UIScrollViewDelegate

extension LandingController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    scrollView.contentOffset.y = max(scrollView.contentOffset.y, 0)
  }
}

// MARK: - Handlers

private extension LandingController {
  func handleAppleSignInSuccess() -> SingleResult<String> {
    return { [weak self] token in
      guard let s = self else { return }
      s.viewModel.authenticateWithApple(
        using: token,
        onSuccess: s.handleSocialAuth(),
        onError: s.handleError()
      )
    }
  }

  func handleGoogleSignInSuccess() -> SingleResult<String> {
    return { [weak self] token in
      guard let s = self else { return }
      s.viewModel.authenticateWithGoogle(
        using: token,
        onSuccess: s.handleSocialAuth(),
        onError: s.handleError()
      )
    }
  }

  func handleFBLoginSuccess() -> SingleResult<String> {
    return { [weak self] token in
      guard let s = self else { return }
      s.viewModel.authenticateWithFacebook(
        using: token,
        onSuccess: s.handleSocialAuth(),
        onError: s.handleError()
      )
    }
  }

  func handleSocialAuth() -> VoidResult {
    return {
      // noop
    }
  }
}

// MARK: - Terms And Conditions

private extension LandingController {
  func setupTermsOfServiceLabel() {
    let tosType = ActiveType.custom(pattern: "\\s\(S.legalTermsOfService())\\b")
    let ppType = ActiveType.custom(pattern: "\\s\(S.legalPrivacyPolicy())\\b")

    tosAcceptanceLabel.enabledTypes.append(tosType)
    tosAcceptanceLabel.enabledTypes.append(ppType)

    let attrStr = tosAcceptanceLabel.attributedText

    tosAcceptanceLabel.customize { [weak self] label in
      guard let self = self else { return }

      label.attributedText = attrStr
      label.textColor = Styles.Colors.Form.normalColor

      label.configureLinkAttribute = { type, attributes, _ in
        var _attrs = attributes
        if [tosType, ppType].contains(type) {
          _attrs[.font] = UIFont.systemFont(ofSize: label.font.pointSize, weight: .semibold)
          _attrs[.foregroundColor] = Styles.Colors.primaryColor
        }
        return _attrs
      }

      label.handleCustomTap(for: tosType) { _ in
        self.appDocumentPresenter.presentTermsOfServicePage(
          fromController: self
        )
      }

      label.handleCustomTap(for: ppType) { [unowned self] _ in
        self.appDocumentPresenter.presentPrivacyPolicyPage(
          fromController: self
        )
      }
    }
  }
}
