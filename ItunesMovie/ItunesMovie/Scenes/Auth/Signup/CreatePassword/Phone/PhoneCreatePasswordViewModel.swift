//
//  PhoneCreatePasswordViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PhoneCreatePasswordViewModel: CreatePasswordViewModelProtocol {
  private let countryCode: String
  private let phoneNumber: String
  private let service: RegisterServiceProtocol

  init(
    countryCode: String,
    phoneNumber: String,
    service: RegisterServiceProtocol = App.shared.session.register
  ) {
    self.countryCode = countryCode
    self.phoneNumber = phoneNumber
    self.service = service
  }
}

// MARK: - Methods

extension PhoneCreatePasswordViewModel {
  func register(
    with password: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(password)
    switch result {
    case let .success(password):
      service.registerWithPhoneNumber(
        fullPhoneNumber,
        password: password,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension PhoneCreatePasswordViewModel {
  var usernameText: String { fullPhoneNumber }
  private var fullPhoneNumber: String { countryCode + phoneNumber }
}
