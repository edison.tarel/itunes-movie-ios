//
//  RegistrationNameViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import NSObject_Rx
import RxSwift

protocol RegistrationNameViewModelProtocol {
  var nextPageVM: RegistrationPictureViewModelProtocol { get }

  func submit(
    name: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}

class RegistrationNameViewModel: RegistrationNameViewModelProtocol {
  private let validate: RegistrationNameInputValidator.ValidateMethod
  private let profileSetup: ProfileSetupServiceProtocol

  init(
    validate: @escaping RegistrationNameInputValidator.ValidateMethod = RegistrationNameInputValidator.validate,
    profileSetup: ProfileSetupServiceProtocol = App.shared.session.profileSetup
  ) {
    self.validate = validate
    self.profileSetup = profileSetup
  }
}

// MARK: - Methods

extension RegistrationNameViewModel {
  func submit(
    name: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(name)
    switch result {
    case let .success(name):
      profileSetup.setName(
        name,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension RegistrationNameViewModel {
  var nextPageVM: RegistrationPictureViewModelProtocol {
    RegistrationPictureViewModel()
  }
}
