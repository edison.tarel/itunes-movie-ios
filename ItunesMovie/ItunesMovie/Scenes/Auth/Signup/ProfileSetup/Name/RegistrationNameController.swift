//
//  RegistrationNameController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class RegistrationNameController: ViewController, LogoutTriggerControllerProtocol {
  var viewModel: RegistrationNameViewModelProtocol!
  var logoutTriggerVM: LogoutTriggerViewModelProtocol!
  var inputCache: ProfileSetupInputCacheProtocol!

  @IBOutlet private(set) var nameField: MDCTextField!
  @IBOutlet private(set) var primaryButton: MDCButton!

  private(set) var nameInputController: MDCTextInputControllerUnderline!

  // MARK: Overrides
  
  override var shouldSetNavBarTransparent: Bool { true }

  override func backButtonTapped(_ sender: AnyObject) {
    super.backButtonTapped(sender)
    logoutTriggerVM.logout()
  }
}

// MARK: - LifeCycle

extension RegistrationNameController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    progressPresenter.dismiss()

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true

    view.endEditing(true)

    inputCache.cacheName(nameField.text)
  }
}

// MARK: - Setup

private extension RegistrationNameController {
  func setup() {
    setupNameField()

    primaryButton.layer.cornerRadius = 8
  }

  func setupNameField() {
    nameField.text = inputCache.name
    nameField.applyAttribute(.givenName)
    nameField.returnKeyType = .continue
    nameField.enablesReturnKeyAutomatically = true

    nameInputController = MDCHelper.underlineInputController(for: nameField)

    nameField.becomeFirstResponder()
  }
}

// MARK: - Bind

private extension RegistrationNameController {
  func bind() {
    nameField.rx.controlEvent(.editingDidEndOnExit)
      .withLatestFrom(nameField.rx.text)
      .filter { !$0.isNilOrEmpty }
      .bind(to: primaryButton.rx.sendActions(for: .touchUpInside))
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Actions

private extension RegistrationNameController {
  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.submit(
      name: nameField.text,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

private extension RegistrationNameController {
  func handleSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.clearFormErrors()
      s.presentNextPage()
    }
  }

  func handleError() -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.nameInputController.setErrorText(
        error.localizedDescription,
        errorAccessibilityValue: nil
      )
    }
  }
}

// MARK: - Utils

private extension RegistrationNameController {
  func clearFormErrors() {
    nameInputController.setErrorText(nil, errorAccessibilityValue: nil)
  }
}

// MARK: - Routing

private extension RegistrationNameController {
  func presentNextPage() {
    let vc = R.storyboard.authSignup.registrationPictureController()!
    vc.viewModel = viewModel.nextPageVM
    vc.inputCache = inputCache
    vc.imagePickerPresenter = vc
    navigationController?.pushViewController(vc, animated: true)
  }
}
