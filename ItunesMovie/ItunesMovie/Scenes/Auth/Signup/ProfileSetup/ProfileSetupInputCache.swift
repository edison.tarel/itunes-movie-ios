//
//  ProfileSetupInputCache.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileSetupInputCacheProtocol {
  var name: String? { get }
  var picture: UIImage? { get }

  func cacheName(_ name: String?)
  func cachePicture(_ picture: UIImage?)
}

class ProfileSetupInputCache: ProfileSetupInputCacheProtocol {
  private(set) var name: String?
  private(set) var picture: UIImage?
}

// MARK: - Methods

extension ProfileSetupInputCache {
  func cacheName(_ name: String?) {
    self.name = name
  }

  func cachePicture(_ picture: UIImage?) {
    self.picture = picture
  }
}
