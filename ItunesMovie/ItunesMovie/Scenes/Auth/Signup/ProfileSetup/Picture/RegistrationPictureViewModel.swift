//
//  RegistrationPictureViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol RegistrationPictureViewModelProtocol {
  func submitPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func skip()
}

class RegistrationPictureViewModel: RegistrationPictureViewModelProtocol {
  private let profileSetup: ProfileSetupServiceProtocol

  init(profileSetup: ProfileSetupServiceProtocol = App.shared.session.profileSetup) {
    self.profileSetup = profileSetup
  }
}

// MARK: - Methods

extension RegistrationPictureViewModel {
  func submitPicture(
    with data: Data,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    profileSetup.setPicture(
      with: data,
      onSuccess: handleSubmissionSuccess(thenExecute: onSuccess),
      onError: onError
    )
  }

  func skip() {
    finish()
  }
}

// MARK: - Utils

private extension RegistrationPictureViewModel {
  func finish() {
    profileSetup.finishSetup()
  }
}

// MARK: - Handlers

private extension RegistrationPictureViewModel {
  func handleSubmissionSuccess(thenExecute handler: @escaping VoidResult) -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.finish()
      handler()
    }
  }
}
