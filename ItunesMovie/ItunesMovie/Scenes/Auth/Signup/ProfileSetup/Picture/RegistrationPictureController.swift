//
//  RegistrationPictureController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class RegistrationPictureController: ViewController, ImagePickerPresenter {
  var viewModel: RegistrationPictureViewModelProtocol!
  var inputCache: ProfileSetupInputCacheProtocol!
  var imagePickerPresenter: ImagePickerPresenter!

  // MARK: ImagePickerPresenter

  var imagePicker: UIImagePickerController! = .init()

  @IBOutlet private(set) var pictureImageView: UIImageView!
  @IBOutlet private(set) var pictureButton: UIButton!
  @IBOutlet private(set) var primaryButton: MDCButton!

  private(set) weak var skipButton: UIBarButtonItem!
  
  override var shouldSetNavBarTransparent: Bool { true }
}

// MARK: - View LifeCycle

extension RegistrationPictureController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    progressPresenter.dismiss()

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true

    inputCache.cachePicture(pictureImageView.image)
  }
}

// MARK: - Setup

private extension RegistrationPictureController {
  func setup() {
    setupNavBar()

    pictureImageView.image = inputCache.picture

    imagePicker.delegate = self

    primaryButton.layer.cornerRadius = 8
  }

  func setupNavBar() {
    let skipButton = UIBarButtonItem(
      title: S.skip(),
      style: .done,
      target: self,
      action: #selector(skipButtonTapped(_:))
    )
    skipButton.tintColor = R.color.black_000000_30()
    navigationItem.rightBarButtonItem = skipButton
    self.skipButton = skipButton
  }
}

// MARK: - Bind

private extension RegistrationPictureController {
  func bind() {
    pictureImageView.rx.observe(UIImage.self, "image")
      .map { $0 == nil }
      .bind(to: primaryButton.rx.isHidden)
      .disposed(by: rx.disposeBag)
  }
}

// MARK: - Actions

private extension RegistrationPictureController {
  @IBAction
  func pictureButtonTapped(_ sender: Any) {
    imagePickerPresenter.presentImagePickerOptions()
  }

  @IBAction
  func continueButtonTapped(_ sender: AnyObject) {
    guard let picture = pictureImageView.image else {
      preconditionFailure("pictureImageView.image should not be nil at this point")
    }

    guard let data = picture.jpegData(compressionQuality: 0.7) else {
      preconditionFailure("pictureImageView.image.jpegData should not be nil at this point")
    }

    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.submitPicture(
      with: data,
      onSuccess: handleSuccess(),
      onError: handleError()
    )
  }

  @objc
  func skipButtonTapped(_ sender: AnyObject) {
    viewModel.skip()
  }
}

// MARK: - UIImagePickerControllerDelegate & UINavigationControllerDelegate

extension RegistrationPictureController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(
    _ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
  ) {
    if let image = info[.editedImage] as? UIImage {
      pictureImageView.image = image
    }

    picker.dismiss(animated: true)
  }
}
