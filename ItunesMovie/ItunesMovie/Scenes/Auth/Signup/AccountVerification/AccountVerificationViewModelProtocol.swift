//
//  AccountVerificationViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AccountVerificationViewModelProtocol: CodeVerificationViewModelProtocol {
  var shouldLogOutOnBackTap: Bool { get }
}
