//
//  EmailPasswordResetViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright (c) 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class EmailPasswordResetViewModel: PasswordResetViewModelProtocol {
  let initialUsername: String

  private let service: PasswordServiceProtocol

  init(
    email: String,
    service: PasswordServiceProtocol = App.shared.guest.password
  ) {
    initialUsername = email
    self.service = service
  }
}

// MARK: - Methods

extension EmailPasswordResetViewModel {
  func sendPasswordResetRequest(
    for username: String?,
    onSuccess: @escaping SingleResult<VerifyPasswordResetViewModelProtocol>,
    onError: @escaping ErrorResult
  ) {
    let result = validate(username)
    switch result {
    case let .success(username):
      service.sendPasswordResetRequest(
        for: username,
        onSuccess: handleSuccess(
          with: username,
          thenExecute: onSuccess
        ),
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension EmailPasswordResetViewModel {
  var usernameAttribute: TextFieldAttributeType { .email }
}

// MARK: - SingleFormInputViewModelProtocol

extension EmailPasswordResetViewModel {
  func validate(_ input: String?) -> Result<String, Error> {
    EmailAddressInputValidator.validate(input).genericResult
  }
}
