//
//  NewPasswordViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class NewPasswordViewModel: NewPasswordViewModelProtocol {
  private let username: String
  private let token: String
  private let service: PasswordServiceProtocol

  init(
    username: String,
    token: String,
    service: PasswordServiceProtocol = App.shared.guest.password
  ) {
    self.username = username
    self.token = token
    self.service = service
  }
}

// MARK: - Methods

extension NewPasswordViewModel {
  func resetPassword(
    with newPassword: String?,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    let result = validate(newPassword)
    switch result {
    case let .success(newPassword):
      let params = UpdatePasswordRequestParams(
        username: username,
        token: token,
        password: newPassword,
        passwordConfirmation: newPassword
      )

      service.resetPassword(
        with: params,
        onSuccess: onSuccess,
        onError: onError
      )
    case let .failure(error):
      onError(error)
    }
  }
}

// MARK: - Getters

extension NewPasswordViewModel {
  var usernameText: String { username }
}
