//
//  VerifyPasswordResetViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol VerifyPasswordResetViewModelProtocol {
  var titleText: String { get }
  var messageText: String { get }
  var subMessageText: String { get }

  func resendCode(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func verify(
    with token: String,
    onSuccess: @escaping SingleResult<NewPasswordViewModelProtocol>,
    onError: @escaping ErrorResult
  )
}

// MARK: - Defaults

extension VerifyPasswordResetViewModelProtocol {
  var titleText: String { S.accountResetPasswordViaUsernameLabelsTitle() }
  var messageText: String { S.accountResetPasswordViaUsernameLabelsDescription() }
}
