//
//  VerifyPasswordResetController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import IQKeyboardManagerSwift
import MaterialComponents.MaterialTextFields
import RxCocoa
import RxSwift
import SVProgressHUD

class VerifyPasswordResetController: ScrollViewController {
  var viewModel: VerifyPasswordResetViewModelProtocol!

  @IBOutlet private(set) var titleLabel: UILabel!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var subMessageLabel: UILabel!
  @IBOutlet private(set) var resendCodeButton: UIButton!

  // Make sure that each field has its own designated Tag value (from 1 to N, left to right.)
  @IBOutlet var textFields: [VerificationCodeField]!

  var inputControllers: [MDCTextInputControllerUnderline] = []
}

// MARK: - View LifeCycle

extension VerifyPasswordResetController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
    bind()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
    IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    textFields.first!.becomeFirstResponder()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    view.endEditing(true)

    IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
  }
}

// MARK: - Setup

private extension VerifyPasswordResetController {
  func setup() {
    setupLabels()
    setupFields()
    setupFieldInputControllers()
  }

  func setupLabels() {
    titleLabel.text = viewModel.titleText
    messageLabel.text = viewModel.messageText
    subMessageLabel.text = viewModel.subMessageText
  }
}

// MARK: - Bind

private extension VerifyPasswordResetController {
  func bind() {
    bindFields()
  }
}

// MARK: - Actions

private extension VerifyPasswordResetController {
  @IBAction
  func resendCodeButtonTapped(_ sender: Any) {
    progressPresenter.presentIndefiniteProgress(from: self)
    viewModel.resendCode(
      onSuccess: handleResendCodeSuccess(),
      onError: handleError()
    )
  }
}

// MARK: - Handlers

private extension VerifyPasswordResetController {
  func handleVerificationSuccess() -> SingleResult<NewPasswordViewModelProtocol> {
    return { [weak self] newPasswordVM in
      guard let s = self else { return }
      s.progressPresenter.dismiss()
      s.presentNewPasswordPage(with: newPasswordVM)
    }
  }

  func handleBackTapSuccess(_ sender: AnyObject) -> VoidResult {
    return { [sender] in
      super.backButtonTapped(sender)
    }
  }
}

// MARK: - Routing

private extension VerifyPasswordResetController {
  func presentNewPasswordPage(with viewModel: NewPasswordViewModelProtocol) {
    let vc = R.storyboard.authForgotPassword.newPasswordController()!
    vc.viewModel = viewModel
    navigationController?.pushViewController(vc, animated: true)
  }
}

// MARK: - CodeVerificationControllerProtocol

extension VerifyPasswordResetController: CodeVerificationControllerProtocol {
  func setupFieldInputControllers() {
    inputControllers = generateInputControllers(for: textFields)
  }

  func handleCodeFillCompletion() -> SingleResult<String> {
    return { [weak self] code in
      guard let s = self else { return }
      s.progressPresenter.presentIndefiniteProgress(from: s)
      s.viewModel.verify(
        with: code,
        onSuccess: s.handleVerificationSuccess(),
        onError: s.handleVerificationError()
      )
    }
  }
}

// MARK: - VerificationCodeFieldDelegate

extension VerifyPasswordResetController: VerificationCodeFieldDelegate {}

// MARK: - UITextFieldDelegate

extension VerifyPasswordResetController: UITextFieldDelegate {
  func textField(
    _ textField: UITextField,
    shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    shouldChangeCharacters(
      for: textField,
      in: range,
      replacementString: string
    )
  }
}
