//
//  ResultHearderView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class ResultHeaderView: UIView {
  @IBOutlet private(set) var titleLabel: UILabel!
}
