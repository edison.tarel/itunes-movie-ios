//
//  MovieListViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieListViewModelProtocol {
  var movies: [Movie] { get }
  var isLoading: Bool { get }
  var lastVisitDate: String { get set }
  var onLoading: VoidResult? { get set }

  func getMovies(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func reloadMovies(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )

  func getMovie(at indexPath: IndexPath) -> Movie?
  var numberOfMovies: Int { get }
}
