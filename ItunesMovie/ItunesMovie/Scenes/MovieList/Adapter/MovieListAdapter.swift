//
//  MovieListAdapter.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class MovieListAdapter: NSObject {
  weak var delegate: MovieListProtocol?

  init(delegate: MovieListProtocol) {
    self.delegate = delegate
  }
}

extension MovieListAdapter: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.movieCell, for: indexPath),
          let movie = self.delegate?.getMovie(at: indexPath)
    else {
      return UITableViewCell()
    }

    cell.viewModel = MovieCellViewModel(movie: movie)
    return cell
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.delegate?.getNumberOfItems() ?? 0
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 80
  }

  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = R.nib.resultHearderView(owner: self)
    headerView?.titleLabel.text = "Last visit : \(self.delegate?.getLastVisitDate() ?? "")"
    return headerView
  }

  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView(frame: CGRect.zero)
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: false)
    self.delegate?.didSelectMovie(at: indexPath)
  }
}
