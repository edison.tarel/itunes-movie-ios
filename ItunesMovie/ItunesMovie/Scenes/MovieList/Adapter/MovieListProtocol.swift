//
//  MovieListProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieListProtocol: class {
  func getMovie(at indexPath: IndexPath) -> Movie?
  func didSelectMovie(at indexPath: IndexPath)
  func getNumberOfSections() -> Int
  func getNumberOfItems() -> Int
  func getLastVisitDate() -> String
}
