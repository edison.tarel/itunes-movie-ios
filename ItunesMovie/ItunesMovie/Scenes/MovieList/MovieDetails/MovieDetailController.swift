//
//  MovieDetailController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class MovieDetailController: ViewController {
  var viewModel: MovieDetailViewModelProtocol!

  @IBOutlet private(set) var trackLabel: UILabel!
  @IBOutlet private(set) var genreLabel: UILabel!
  @IBOutlet private(set) var priceLabel: UILabel!
  @IBOutlet private(set) var artWorkView: UIImageView!
  @IBOutlet private(set) var descriptionLabel: UILabel!

  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}

// MARK: - LifeCycle

extension MovieDetailController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
}

// MARK: - Setup

private extension MovieDetailController {
  private func setup() {
    setupTitle()
    updateDetails()
  }

  private func setupTitle() {
    title = S.movieDetailTitle()
  }

  private func updateDetails() {
    trackLabel.text = viewModel.trackName
    genreLabel.text = viewModel.genre
    descriptionLabel.text = viewModel.description
    priceLabel.text = viewModel.price
    artWorkView.setImageWithURL(
      viewModel.artwork,
      placeholder: R.image.moviePlaceholder()
    )
  }
}

// MARK: - Handlers

private extension MovieDetailController {
  func handleFetchSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.updateDetails()
    }
  }

  func handleFetchError() -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }
      s.infoPresenter.presentErrorInfo(error: error)
    }
  }
}

// MARK: - State Restoration

extension MovieDetailController {
  override func encodeRestorableState(with coder: NSCoder) {
    // save the id of current track
    let trackId = viewModel.movie.trackId
    coder.encode(trackId, forKey: "trackId")

    super.encodeRestorableState(with: coder)
  }

  override func decodeRestorableState(with coder: NSCoder) {
    // fetch the details of last track
    let trackId = coder.decodeInt32(forKey: "trackId")
    viewModel.fetchDetails(
      ofMovie: Int(trackId),
      onSuccess: handleFetchSuccess(),
      onError: handleFetchError()
    )

    super.decodeRestorableState(with: coder)
  }
}
