//
//  MovieDetailViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieDetailViewModelProtocol {
  var movie: Movie { get }
  var trackName: String { get }
  var genre: String { get }
  var description: String { get }
  var price: String { get }
  var artwork: URL? { get }

  func fetchDetails(
    ofMovie trackId: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  )
}
