//
//  MovieDetailViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MovieDetailViewModel: MovieDetailViewModelProtocol {
  private(set) var movie: Movie
  private let service: MoviesServiceProtocol

  init(
    movie: Movie,
    service: MoviesServiceProtocol = App.shared.movies
  ) {
    self.movie = movie
    self.service = service
  }
}

// MARK: - Methods

extension MovieDetailViewModel {
  func fetchDetails(
    ofMovie trackId: Int,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    service.fetchMovie(
      with: trackId,
      onSuccess: { [weak self] movie in
        guard let s = self,
              let movie = movie
        else {
          return
        }

        s.movie = movie
        onSuccess()
      },
      onError: onError
    )
  }
}

// MARK: - Getters

extension MovieDetailViewModel {
  var trackName: String { movie.trackName }
  var genre: String { movie.primaryGenreName }
  var description: String { movie.longDescription }
  var price: String { "$\(movie.trackPrice)" }
  var artwork: URL? { movie.artworkUrl60 }
}
