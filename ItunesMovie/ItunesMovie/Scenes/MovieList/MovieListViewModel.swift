//
//  MovieListViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CoreData
import Foundation
import SwiftDate

class MovieListViewModel: MovieListViewModelProtocol {
  var lastVisitDate: String
  var onLoading: VoidResult?
  var isLoading: Bool = false {
    didSet {
      onLoading?()
    }
  }

  private let service: MoviesServiceProtocol
  private(set) var movies: [Movie] = []

  init(service: MoviesServiceProtocol = App.shared.movies) {
    self.service = service
    self.lastVisitDate = Date().toFormat("yyyy-MM-dd hh:mm:ss a")
  }
}

// MARK: - Methods: Getting Movies

extension MovieListViewModel {
  func reloadMovies(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    fetchMovies(
      fromApi: true,
      onSuccess: onSuccess,
      onError: onError
    )
  }

  func getMovies(
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    self.fetchMovies(
      onSuccess: onSuccess,
      onError: onError
    )
  }

  private func fetchMovies(
    fromApi: Bool = false,
    onSuccess: @escaping VoidResult,
    onError: @escaping ErrorResult
  ) {
    isLoading = true
    let fetchParams = MovieParameter(
      term: "star",
      country: "au",
      media: "movie"
    )

    service.fetchMovies(
      fromApi: fromApi,
      with: fetchParams,
      onSuccess: { [weak self] result in
        guard let s = self,
              let movies = result.results
        else {
          return
        }

        s.movies = movies
        s.isLoading = false
        onSuccess()
      },
      onError: { [weak self] error in
        guard let s = self else { return }

        s.isLoading = false
        onError(error)
      }
    )
  }
}

extension MovieListViewModel {
  func getMovie(at indexPath: IndexPath) -> Movie? {
    guard movies.count > indexPath.row else {
      return nil
    }
    return movies[indexPath.row]
  }

  var numberOfMovies: Int {
    return movies.count
  }
}
