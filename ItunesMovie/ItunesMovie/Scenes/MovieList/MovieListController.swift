//
//  MovieListController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class MovieListController: ViewController {
  lazy var viewModel: MovieListViewModelProtocol! = MovieListViewModel()
  var movieListAdapter: MovieListAdapter!

  @IBOutlet private(set) var tableView: UITableView!
  private(set) var refreshControl: UIRefreshControl!
}

// MARK: - LifeCycle

extension MovieListController {
  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }
}

// MARK: - Setup

private extension MovieListController {
  private func setup() {
    setupVM()
    setupTitle()
    setupTableView()
  }

  private func setupVM() {
    viewModel.onLoading = handleOnLoading()
  }

  private func setupTitle() {
    let titleLabel = UILabel()
    titleLabel.text = S.moviesTitle()
    titleLabel.font = R.font.sfProDisplayBold(size: 28)
    navigationItem.titleView = titleLabel
  }

  private func setupTableView() {
    refreshControl = UIRefreshControl()
    refreshControl?.addTarget(self, action: #selector(onRefreshTableView), for: .valueChanged)

    tableView.register(R.nib.movieCell)
    tableView.addSubview(refreshControl!)
    tableView.sectionHeaderHeight = UITableView.automaticDimension
    tableView.estimatedSectionHeaderHeight = 100
    tableView.canCancelContentTouches = true

    movieListAdapter = MovieListAdapter(delegate: self)
    tableView.dataSource = movieListAdapter
    tableView.delegate = movieListAdapter

    loadMovies()
  }

  private func updateDisplay() {
    refreshControl.endRefreshing()
    tableView.reloadData()
  }
}

// MARK: - Handlers

private extension MovieListController {
  func handleFetchSuccess() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      s.updateDisplay()
    }
  }

  func handleFetchError() -> ErrorResult {
    return { [weak self] error in
      guard let s = self else { return }
      s.updateDisplay()
      s.infoPresenter.presentErrorInfo(error: error)
    }
  }

  func handleOnLoading() -> VoidResult {
    return { [weak self] in
      guard let s = self else { return }
      if s.viewModel.isLoading {
        s.refreshControl.beginRefreshing()
      } else {
        s.refreshControl.endRefreshing()
      }
    }
  }
}

// MARK: - Actions

extension MovieListController {
  func loadMovies() {
    viewModel.getMovies(
      onSuccess: handleFetchSuccess(),
      onError: handleFetchError()
    )
  }

  @objc
  func onRefreshTableView() {
    viewModel.reloadMovies(
      onSuccess: handleFetchSuccess(),
      onError: handleFetchError()
    )
  }
}

extension MovieListController: MovieListProtocol {
  func getMovie(at indexPath: IndexPath) -> Movie? {
    return viewModel.getMovie(at: indexPath)
  }

  func didSelectMovie(at indexPath: IndexPath) {
    guard let movie = viewModel.getMovie(at: indexPath) else {
      return
    }

    if let detailVC = R.storyboard.movieList.movieDetailController() {
      detailVC.viewModel = MovieDetailViewModel(movie: movie)
      navigationController?.pushViewController(detailVC, animated: true)
    }
  }

  func getNumberOfSections() -> Int {
    return 1
  }

  func getNumberOfItems() -> Int {
    return viewModel.numberOfMovies
  }

  func getLastVisitDate() -> String {
    return viewModel.lastVisitDate
  }
}

// MARK: - State Restoration

extension MovieListController {
  override func encodeRestorableState(with coder: NSCoder) {
    // save the current date as last visit date
    let lastVisitdate = Date().toFormat("yyyy-MM-dd hh:mm:ss a")
    coder.encode(lastVisitdate, forKey: "lastVisitdate")

    super.encodeRestorableState(with: coder)
  }

  override func decodeRestorableState(with coder: NSCoder) {
    // get last visit date
    if let lastVisitdate = coder.decodeObject(forKey: "lastVisitdate") as? String {
      viewModel.lastVisitDate = lastVisitdate
    }

    super.decodeRestorableState(with: coder)
  }
}
