//
//  MDButton.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import ChameleonFramework
import Material
import UIKit

class MDButton: Button {
}

/// A button that's intended for use in a form as its main action button. It has a bit of
/// distinct appearance making it stand out in a form possibly crowded with several
/// other components.
///
/// NOTE: You'd normally need only one of this in any given form.
class MDPrimaryButton: MDButton {
  override func prepare() {
    super.prepare()

    cornerRadiusPreset = .cornerRadius8

    backgroundColor = UIColor(
      gradientStyle: .leftToRight,
      withFrame: bounds,
      andColors: Styles.Colors.Form.Button.primaryGradient
    )
  }
}

/// An alternative button that has outline and usually has the same background color
/// as it's parent's background which makes it look like it's hollow.
class MDHollowButton: MDButton {
  override func prepare() {
    super.prepare()

    cornerRadiusPreset = .cornerRadius3
    backgroundColor = Styles.Colors.windowBackground
    borderWidthPreset = .border3
    borderColor = R.color.gray_E5E5E5()
  }
}
