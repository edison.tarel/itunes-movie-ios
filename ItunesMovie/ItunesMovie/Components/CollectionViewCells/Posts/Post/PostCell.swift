//
//  PostCell.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

final class PostCell: UICollectionViewCell {
  var viewModel: PostCellViewModelProtocol! {
    didSet {
      viewModel?.onRefresh = trigger(type(of: self).refresh)
      setupVM()
    }
  }

  // MARK: Outlets

  @IBOutlet private(set) var postAuthorView: PostAuthorView!
  @IBOutlet private(set) var postView: SimplePostItemView!
  @IBOutlet private(set) var mainStackView: UIStackView!

  // MARK: Overrides

  override func systemLayoutSizeFitting(
    _ targetSize: CGSize,
    withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority,
    verticalFittingPriority: UILayoutPriority
  ) -> CGSize {
    var targetSize = targetSize
    targetSize.height = CGFloat.greatestFiniteMagnitude

    let size = super.systemLayoutSizeFitting(
      targetSize,
      withHorizontalFittingPriority: .required,
      verticalFittingPriority: .fittingSizeLevel
    )

    return size
  }
  
  override func prepareForReuse() {
    postView.mainImageView.image = nil
  }
}

// MARK: - Setup

private extension PostCell {
  func setupVM() {
    guard viewModel != nil else { return }
    
    postAuthorView.viewModel = viewModel.authorVM
    
    postView.descriptionLabel.numberOfLines = 2
    postView.viewModel = viewModel.itemVM

    setupAnimation()
    refresh()
  }

  func setupAnimation() {
    postAuthorView.hero.id = HeroAnimation.postAuthorView.createID(withKey: viewModel.id.description)
    postView.hero.id = HeroAnimation.postView.createID(withKey: viewModel.id.description)
  }
}

// MARK: - Refresh

private extension PostCell {
  func refresh() {
    // noop
  }
}
