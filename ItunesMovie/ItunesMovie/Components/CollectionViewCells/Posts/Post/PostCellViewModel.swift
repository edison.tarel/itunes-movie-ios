//
//  PostCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import NSObject_Rx
import SwiftDate

class PostCellViewModel: PostCellViewModelProtocol {
  var onRefresh: VoidResult?
  
  var onToggleLike: VoidResult?

  let itemVM: SimplePostItemViewModelProtocol
  let authorVM: PostAuthorViewModelProtocol

  private(set) var post: Post

  private let service: PostsServiceProtocol

  init(
    post: Post,
    service: PostsServiceProtocol = App.shared.posts
  ) {
    self.post = post
    self.service = service
    
    self.itemVM = SimplePostItemViewModel(post: post)
    self.authorVM = PostAuthorViewModel(post: post)

    bind()
  }
}

// MARK: - Bind

private extension PostCellViewModel {
  func bind() {
    service.bindPost(
      withId: post.id,
      onRefresh: trigger(type(of: self).refreshPost),
      observer: self
    )
  }
}

// MARK: - Refresh

private extension PostCellViewModel {
  func refreshPost() {
    guard let post = service.post(withId: post.id) else { return }
    self.post = post
    onRefresh?()
  }
}

// MARK: - Getters

extension PostCellViewModel {
  var id: Int { post.id }
}
