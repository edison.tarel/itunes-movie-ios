//
//  PostCellViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

protocol PostCellViewModelProtocol: ViewModelProtocol {
  var id: Int { get }

  var itemVM: SimplePostItemViewModelProtocol { get }
  var authorVM: PostAuthorViewModelProtocol { get }
}
