//
//  PostAuthorViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol PostAuthorViewModelProtocol: ViewModelProtocol {
  var onRefresh: VoidResult? { get set }
  
  var id: Int { get }
  var avatarURL: URL? { get }
  var datePostedText: String { get }
  var nameText: String { get }
}
