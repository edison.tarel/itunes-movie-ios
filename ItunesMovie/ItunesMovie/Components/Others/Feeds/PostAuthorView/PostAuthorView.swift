//
//  PostAuthorView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import SkeletonView

class PostAuthorView: BaseView, NibLoadable {
  var viewModel: PostAuthorViewModelProtocol! {
    didSet {
      viewModel?.onRefresh = trigger(type(of: self).refresh)
      setupVM()
    }
  }

  // MARK: Event Callbacks

  var onOptionTap: VoidResult?

  // MARK: Outlets

  @IBOutlet private(set) var contentView: UIView!
  @IBOutlet private(set) var imageView: ImageView!
  @IBOutlet private(set) var nameLabel: UILabel!
  @IBOutlet private(set) var datePostedLabel: UILabel!
  @IBOutlet private(set) var optionButton: UIButton!

  override func prepare() {
    loadNib()
  }
}

// MARK: - Setup

private extension PostAuthorView {
  func setupVM() {
    guard viewModel != nil else { return }
    setupAnimation()
    refresh()
  }

  func setupAnimation() {
    hero.id = HeroAnimation.postAuthorView.createID(withKey: viewModel.id.description)
  }
}

// MARK: - Refresh

private extension PostAuthorView {
  func refresh() {
    guard viewModel != nil else { return }
    imageView.setImageWithURL(
      viewModel.avatarURL,
      placeholder: R.image.profilePicPlaceholder()
    )
    nameLabel.text = viewModel.nameText
    datePostedLabel.text = viewModel.datePostedText
  }
}

// MARK: - Actions

private extension PostAuthorView {
  @IBAction
  func optionButtonTapped(_ sender: Any) {
    onOptionTap?()
  }
}
