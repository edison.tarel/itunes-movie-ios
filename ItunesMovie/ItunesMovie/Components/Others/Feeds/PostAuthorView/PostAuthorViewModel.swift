//
//  PostAuthorViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import NSObject_Rx
import SwiftDate

class PostAuthorViewModel: PostAuthorViewModelProtocol {
  var onRefresh: VoidResult?

  private var post: Post

  private let service: PostsServiceProtocol

  init(
    post: Post,
    service: PostsServiceProtocol = App.shared.posts
  ) {
    self.post = post
    self.service = service

    bind()
    refreshPost()
  }
}

// MARK: - Bind

private extension PostAuthorViewModel {
  func bind() {
    service.bindPost(
      withId: post.id,
      onRefresh: trigger(type(of: self).refreshPost),
      observer: self
    )
  }
}

// MARK: - Getters

extension PostAuthorViewModel {
  var id: Int { post.id }
  var avatarURL: URL? { post.author.avatarThumbURL }
  var datePostedText: String { post.createdAt.toPreciseRelative() }
  var nameText: String { post.author.fullName ?? "" }
}

// MARK: - Refresh

private extension PostAuthorViewModel {
  func refreshPost() {
    guard let post = service.post(withId: post.id) else { return }
    self.post = post
    onRefresh?()
  }
}
