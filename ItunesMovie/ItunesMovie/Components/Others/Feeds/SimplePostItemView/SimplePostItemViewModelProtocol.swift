//
//  SimplePostItemViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

protocol SimplePostItemViewModelProtocol: ViewModelProtocol {  
  var id: Int { get }
  var commentsCountText: String { get }
  var description: String { get }
  var likeCount: Int { get }
  var likeImage: UIImage { get }
  var likeText: String { get }
  var photoURL: URL? { get }
}
