//
//  SimplePostsViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class SimplePostItemViewModel: SimplePostItemViewModelProtocol {
  var onRefresh: VoidResult?
  
  var onToggleLike: VoidResult?

  private var post: Post!

  private let service: PostsServiceProtocol

  init(
    post: Post,
    service: PostsServiceProtocol = App.shared.posts
  ) {
    self.post = post
    self.service = service

    bind()
    refreshPost()
  }
}

// MARK: - Bind

private extension SimplePostItemViewModel {
  func bind() {
    service.bindPost(
      withId: post.id,
      onRefresh: trigger(type(of: self).refreshPost),
      observer: self
    )
  }
}

// MARK: - Refresh

private extension SimplePostItemViewModel {
  func refreshPost() {
    guard let post = service.post(withId: post.id) else { return }
    self.post = post
    onRefresh?()
  }
}

// MARK: - Getters

extension SimplePostItemViewModel {
  var id: Int { post.id }
  var commentsCountText: String { S.numberOfComments(count: post.commentsCount) }
  var description: String { post.body }
  var likeCount: Int { post.favoritesCount }
  var likeImage: UIImage { post.isFavorite ? R.image.heartFilled()! : R.image.heart()! }
  var likeText: String { String(likeCount) }
  var photoURL: URL? { post.photo.url }
}
