//
//  SimplePostItemView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import ImageViewer_swift
import UIKit

class SimplePostItemView: BaseView, NibLoadable {
  var viewModel: SimplePostItemViewModelProtocol! {
    didSet {
      viewModel?.onRefresh = trigger(type(of: self).refresh)
      setupVM()
    }
  }

  // MARK: Event Callbacks

  var onLikeTap: VoidResult?

  // MARK: Inspectable

  /// Whether it can show a large preview of the main image when tapping it
  @IBInspectable var canPreviewImage: Bool = false

  // MARK: Outlets

  @IBOutlet private(set) var contentView: UIView!
  @IBOutlet private(set) var commentsCountLabel: UILabel!
  @IBOutlet private(set) var descriptionLabel: UILabel!
  @IBOutlet private(set) var descriptionMoreLabel: UILabel!
  @IBOutlet private(set) var mainImageView: UIImageView!
  @IBOutlet private(set) var likeButton: UIButton!
  @IBOutlet private(set) var likeCountLabel: UILabel!

  override func prepare() {
    loadNib()
    setup()
  }
}

// MARK: - Setup

private extension SimplePostItemView {
  func setup() {
    descriptionLabel.numberOfLines = 0
  }
  
  func setupVM() {
    guard viewModel != nil else { return }
    
    setupAnimation()
    refresh()
  }
  
  func setupAnimation() {
    hero.id = HeroAnimation.postView.createID(withKey: viewModel.id.description)
  }
}

// MARK: - Refresh

private extension SimplePostItemView {
  func refresh() {
    guard viewModel != nil else { return }
    
    mainImageView.setImageWithURL(viewModel.photoURL)
    if canPreviewImage {
      mainImageView.setupImageViewer(
        options: [.closeIcon(R.image.close()!)]
      )
    }
    
    descriptionLabel.text = viewModel.description
    if descriptionLabel.numberOfLines != 0 {
      descriptionMoreLabel.isHidden = !descriptionLabel.isTruncated
    }
    
    likeButton.setImage(viewModel.likeImage, for: .normal)
    likeCountLabel.text = viewModel.likeCount.description
    commentsCountLabel.text = viewModel.commentsCountText
  }
}

// MARK: - Actions

private extension SimplePostItemView {
  @IBAction
  func likeButtonTapped() {
    onLikeTap?()
  }
}
