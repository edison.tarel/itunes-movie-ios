//
//  GooglePredictedPlaceCellView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import GooglePlaces
import UIKit

class GooglePredictedPlaceCell: UITableViewCell {
  @IBOutlet var placeNameLabel: UILabel!
  @IBOutlet var placeLocationLabel: UILabel!

  var place: GMSAutocompletePrediction? {
    didSet {
      update()
    }
  }

  func update() {
    placeLocationLabel.text = nil

    if let model = place {
      placeNameLabel.attributedText = attributedText(
        for: model.attributedPrimaryText,
        regularFont: UIFont.systemFont(ofSize: 16, weight: .medium),
        boldFont: UIFont.systemFont(ofSize: 16),
        regularFontColor: placeNameLabel.textColor,
        boldFontColor: UIColor.darkGray
      )

      if let text = model.attributedSecondaryText {
        placeLocationLabel.attributedText = attributedText(
          for: text,
          regularFont: UIFont.systemFont(ofSize: 14, weight: .regular),
          boldFont: UIFont.boldSystemFont(ofSize: 14),
          regularFontColor: placeLocationLabel.textColor,
          boldFontColor: UIColor.darkGray
        )
      }
    } else {
      //
    }
  }

  private func attributedText(
    for name: NSAttributedString,
    regularFont: UIFont,
    boldFont: UIFont,
    regularFontColor: UIColor,
    boldFontColor: UIColor
  ) -> NSAttributedString {
    let highlighted = name.mutableCopy() as! NSMutableAttributedString

    highlighted
      .enumerateAttribute(
        NSAttributedString.Key.gmsAutocompleteMatchAttribute,
        in: NSRange(location: 0, length: highlighted.length),
        options: []
      ) { (value, range: NSRange, _: UnsafeMutablePointer<ObjCBool>) -> Void in
        let font = (value == nil) ? regularFont : boldFont
        let color = (value == nil) ? regularFontColor : boldFontColor
        highlighted.addAttribute(.font, value: font, range: range)
        highlighted.addAttribute(.foregroundColor, value: color, range: range)
      }

    return highlighted
  }
}
