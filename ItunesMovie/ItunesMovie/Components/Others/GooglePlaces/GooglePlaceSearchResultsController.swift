//
//  GooglePlaceSearchResultsController.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import GooglePlaces
import PureLayout
import UIKit

protocol GooglePlaceSearchResultsControllerDelegate: NSObjectProtocol {
  func resultsController(
    _ resultsController: GooglePlaceSearchResultsController,
    didSelect place: GMSPlace
  )

  func resultsController(
    _ resultsController: GooglePlaceSearchResultsController,
    didFailWithError error: Error
  )

  func didRequestGooglePlaces(_ viewController: GooglePlaceSearchResultsController)

  func didUpdateGooglePlaces(_ viewController: GooglePlaceSearchResultsController)
}

class GooglePlaceSearchResultsController: UIViewController, ScrollableController {
  var scrollView: UIScrollView! {
    get { return tableView }
    set {}
  }

  var emptyResultsView: UIView?

  weak var delegate: GooglePlaceSearchResultsControllerDelegate?

  private var tableView: UITableView!

  private var fetcher: GMSAutocompleteFetcher!

  /// Only predictions with non-nil PlaceID properties.
  private var predictions: [GMSAutocompletePrediction] = []

  override func viewDidLoad() {
    super.viewDidLoad()

    fetcher = GMSAutocompleteFetcher(bounds: nil, filter: nil)
    fetcher.delegate = self

    tableView = UITableView(frame: CGRect.zero)
    tableView.dataSource = self
    tableView.delegate = self
    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 58

    view.addSubview(tableView)
    tableView.autoPinEdgesToSuperviewEdges()

    tableView.register(
      UINib(nibName: "GooglePredictedPlaceCellView", bundle: nil),
      forCellReuseIdentifier: "GooglePredictedPlaceCell"
    )

    tableView.alwaysBounceVertical = true
    tableView.keyboardDismissMode = .interactive
    addKeyboardVisibilityEventObservers()

    if emptyResultsView == nil {
      emptyResultsView = Bundle.main
        .loadNibNamed("GooglePlaceEmptyResultsView", owner: nil, options: nil)?.first as? UIView
    }
  }
}

extension GooglePlaceSearchResultsController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return predictions.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "GooglePredictedPlaceCell", for: indexPath)
    if let c = cell as? GooglePredictedPlaceCell {
      c.place = predictions[indexPath.row]
    }
    return cell
  }
}

extension GooglePlaceSearchResultsController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let place = predictions[indexPath.row]
    guard let delegate = delegate else { return }

    GMSPlacesClient.shared().lookUpPlaceID(place.placeID) { place, error in
      if let error = error {
        delegate.resultsController(self, didFailWithError: error)
      } else if let place = place {
        delegate.resultsController(self, didSelect: place)
      } else {
        //
      }
    }
  }
}

extension GooglePlaceSearchResultsController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let keyword = searchController.searchBar.text ?? ""

    fetcher.sourceTextHasChanged(keyword)

    if !keyword.isEmpty {
      delegate?.didRequestGooglePlaces(self)
    } else {
      tableView.tableFooterView = nil
    }
  }
}

extension GooglePlaceSearchResultsController: GMSAutocompleteFetcherDelegate {
  func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
    self.predictions = predictions

    tableView.reloadData()

    if self.predictions.isEmpty {
      tableView.tableFooterView = emptyResultsView
    } else {
      tableView.tableFooterView = nil
    }

    delegate?.didUpdateGooglePlaces(self)
  }

  func didFailAutocompleteWithError(_ error: Error) {
    print(error.localizedDescription)
    delegate?.didUpdateGooglePlaces(self)
  }
}
