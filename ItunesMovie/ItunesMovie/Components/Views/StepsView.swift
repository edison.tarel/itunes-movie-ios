//
//  StepsView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import PureLayout

@IBDesignable
class StepsView: BaseView {
  // Indicates how many step
  @IBInspectable var totalSteps: Int = 4

  // Indicates which step
  private var _onStep = 1
  @IBInspectable var onStep: Int {
    get {
      return _onStep
    }
    set {
      if newValue > totalSteps {
        _onStep = totalSteps
      } else if newValue < 1 {
        _onStep = 1
      } else {
        _onStep = newValue
      }
    }
  }

  private var borderWidth: CGFloat = 3

  override func prepare() {
    super.prepare()

    backgroundColor = R.color.grey_78849E()!.withAlphaComponent(0.1)
  }

  override func layoutSubviews() {
    super.layoutSubviews()

    layer.cornerRadius = frame.size.height / 2

    let extraDimen = borderWidth * 2
    let pillWidth = (frame.size.width / CGFloat(totalSteps))
    let pillHeight = frame.size.height
    let stepperWidth = pillWidth + extraDimen
    let stepperHeight = pillHeight + extraDimen

    for index in 0 ..< onStep {
      let stepper = UIView(forAutoLayout: ())
      let pill = UIView(forAutoLayout: ())

      stepper.addSubview(pill)
      addSubview(stepper)

      pill.backgroundColor = Styles.Colors.primaryColor
      pill.layer.cornerRadius = pillHeight / 2

      pill.autoSetDimensions(to: CGSize(width: pillWidth, height: pillHeight))
      pill.autoCenterInSuperview()

      stepper.backgroundColor = Styles.Colors.viewControllerBackground
      stepper.layer.cornerRadius = stepperHeight / 2

      stepper.autoSetDimension(.height, toSize: stepperHeight, relation: .equal)
      stepper.autoSetDimension(.width, toSize: stepperWidth, relation: .equal)
      stepper.autoAlignAxis(toSuperviewAxis: .horizontal)

      let inset = (pillWidth * CGFloat(index)) - borderWidth
      stepper.autoPinEdge(toSuperviewEdge: .leading, withInset: inset)
    }
  }
}
