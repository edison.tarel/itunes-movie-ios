//
//  SkeletonView.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class SkeletonView: UIView {
  var startLocations: [NSNumber] = [-1.0, -0.5, 0.0]
  var endLocations: [NSNumber] = [1.0, 1.5, 2.0]
  var gradientBGColor: CGColor = UIColor.clear.cgColor
  var gradientMovingColor: CGColor = UIColor(white: 1, alpha: 0.3).cgColor
  var movingAnimationDuration: CFTimeInterval = 0.8
  var delayBetweenAnimationLoops: CFTimeInterval = 1.0

  var gradientLayer: CAGradientLayer!

  override func draw(_ rect: CGRect) {
    super.draw(rect)

    addGradientLayer()
  }

  func addGradientLayer() {
    layer.masksToBounds = true

    let gradientLayer = CAGradientLayer()
    gradientLayer.frame = CGRect(
      center: bounds.center,
      size: CGSize(
        width: bounds.width,
        height: bounds.height + 100
      )
    )
    gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
    gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
    gradientLayer.colors = [
      gradientBGColor,
      gradientMovingColor,
      gradientBGColor
    ]
    gradientLayer.locations = startLocations

    /// Rotate gradient 45 degrees
    let angle = 45 * CGFloat.pi / 180
    gradientLayer.transform = CATransform3DMakeRotation(angle, 0, 0, 1)

    layer.mask = self.gradientLayer
    layer.addSublayer(gradientLayer)
    self.gradientLayer = gradientLayer
  }

  func startAnimation() {
    let animation = CABasicAnimation(keyPath: "locations")
    animation.fromValue = startLocations
    animation.toValue = endLocations
    gradientLayer.add(animation, forKey: animation.keyPath)

    let animationGroup = CAAnimationGroup()
    animationGroup.duration = movingAnimationDuration + delayBetweenAnimationLoops
    animationGroup.animations = [animation]
    animationGroup.repeatCount = .infinity
    animationGroup.autoreverses = true
    gradientLayer.add(animationGroup, forKey: animation.keyPath)
    gradientLayer.animation(forKey: animation.keyPath ?? "locations")
  }

  func endAnimation() {
    gradientLayer.stopAnimation(forKey: "locations")
  }
}
