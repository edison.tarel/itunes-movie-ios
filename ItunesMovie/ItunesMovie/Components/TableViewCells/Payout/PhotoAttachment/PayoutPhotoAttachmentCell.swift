//
//  PayoutPhotoAttachmentCell.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import Mantis
import YPImagePicker

class PayoutPhotoAttachmentCell: UITableViewCell {
  var viewModel: PayoutTextInputCellViewModelProtocol! {
    didSet { setupVM() }
  }

  var indexPath: IndexPath!
  var valueUpdated: DoubleResult<IndexPath, PayoutTextInputCellViewModelProtocol>?

  @IBOutlet private var attachment1Label: UILabel!
  @IBOutlet private var attachment2Label: UILabel!
  @IBOutlet private var attachment1ImageButton: FormButton!
  @IBOutlet private var attachment2ImageButton: FormButton!

  private var selectedImageButton: FormButton?
}

// MARK: - Setup

private extension PayoutPhotoAttachmentCell {
  func setupVM() {
    guard viewModel != nil else { return }

    attachment1Label.text = viewModel.input1Placeholder
    attachment2Label.text = viewModel.input2Placeholder

    if viewModel.input2Placeholder == nil {
      attachment2Label.isHidden = true
      attachment2ImageButton.isHidden = true
    }
  }

  func createPicker() -> YPImagePicker {
    var imagePickerConfig = YPImagePickerConfiguration()
    imagePickerConfig.showsPhotoFilters = false
    imagePickerConfig.startOnScreen = YPPickerScreen.photo
    imagePickerConfig.shouldSaveNewPicturesToAlbum = false
    imagePickerConfig.library.mediaType = YPlibraryMediaType.photo
    imagePickerConfig.library.skipSelectionsGallery = true

    return YPImagePicker(configuration: imagePickerConfig)
  }
}

// MARK: - Actions

private extension PayoutPhotoAttachmentCell {
  @IBAction
  func onAttachFirstPhotoTapped() {
    openPhotoLibraryFor(attachment1ImageButton)
  }

  @IBAction
  func onAttachSecondPhotoTapped() {
    openPhotoLibraryFor(attachment2ImageButton)
  }

  func openPhotoLibraryFor(_ button: FormButton) {
    selectedImageButton = button

    let imagePicker = createPicker()
    imagePicker.didFinishPicking { [weak self, imagePicker] items, _ in
      guard let s = self else { return }
      imagePicker.dismiss(animated: true) {
        if let photo = items.singlePhoto {
          s.presentImageCropper(forImage: photo.image)
        }
      }
    }
    let presenter = UIWindow.presentableController
    presenter?.present(imagePicker, animated: true)
  }

  func presentImageCropper(forImage image: UIImage) {
    let cropVC = Mantis.cropViewController(image: image)
    cropVC.delegate = self
    cropVC.modalPresentationStyle = .fullScreen

    let presenter = UIWindow.presentableController
    presenter?.present(cropVC, animated: true)
  }
}

// MARK: - CropViewControllerDelegate

extension PayoutPhotoAttachmentCell: CropViewControllerDelegate {
  func cropViewControllerDidCrop(
    _ cropViewController: CropViewController,
    cropped: UIImage,
    transformation: Transformation
  ) {
    selectedImageButton?.imageView?.contentMode = .scaleAspectFit
    selectedImageButton?.setImage(cropped, for: .normal)
    let photoData = cropped.jpegData(compressionQuality: CGFloat(App.shared.config.defaultPhotoCompression))
    if selectedImageButton == attachment1ImageButton {
      viewModel.input1Value = photoData
    } else {
      viewModel.input2Value = photoData
    }
    cropViewController.dismissModal()
  }
}

// MARK: - Constants

extension PayoutPhotoAttachmentCell {
  static var height: CGFloat { 287.0 }
}
