//
//  PayoutTextInputCellViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

enum PayoutInputType: String {
  case textInput, imageAttachment, date, country, number
}

protocol PayoutTextInputCellViewModelProtocol {
  var input1Placeholder: String { get }
  var input2Placeholder: String? { get }
  var inputType: PayoutInputType { get }
  var inputType2: PayoutInputType { get }
  var input1Value: Any? { get set }
  var input2Value: Any? { get set }
  var input1ParamKey: String { get }
  var input2ParamKey: String? { get }
}
