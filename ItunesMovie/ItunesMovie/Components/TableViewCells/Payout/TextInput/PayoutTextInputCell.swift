//
//  PayoutTextInputCell.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CountryPickerView
import UIKit

class PayoutTextInputCell: UITableViewCell {
  var viewModel: PayoutTextInputCellViewModelProtocol! {
    didSet { setupVM() }
  }

  var indexPath: IndexPath!
  var valueUpdated: DoubleResult<IndexPath, PayoutTextInputCellViewModelProtocol>?

  @IBOutlet private var textField: UITextField!
  @IBOutlet private var secondTextField: UITextField!

  private var countryPicker: CountryPickerView!
  private var countryTextField: UITextField!
  private var doneSetup: Bool = false
}

// MARK: - Setup

private extension PayoutTextInputCell {
  func setupVM() {
    guard viewModel != nil, !doneSetup else { return }
    doneSetup = true
    textField.placeholder = viewModel.input1Placeholder
    secondTextField.isHidden = viewModel.input2Placeholder == nil

    setupInput(with: viewModel.inputType, textField: textField)
    if viewModel.input2Placeholder != nil {
      secondTextField.placeholder = viewModel.input2Placeholder
      setupInput(with: viewModel.inputType2, textField: secondTextField)
    }
  }

  func setupInput(with inputType: PayoutInputType, textField: UITextField) {
    textField.inputView = nil
    textField.leftView = nil

    switch inputType {
    case .date:
      setupDatePicker(for: textField)
    case .country:
      setupCountryPicker(for: textField)
    case .number:
      textField.keyboardType = .numberPad
      fallthrough
    default:
      let value = textField.tag == 1 ? viewModel.input1Value : viewModel.input2Value
      textField.text = value as? String
      textField.autocapitalizationType = .words
    }
  }

  func setupDatePicker(for textField: UITextField) {
    let value = textField.tag == 1 ? viewModel.input1Value : viewModel.input2Value

    let picker = UIDatePicker()
    picker.datePickerMode = .date
    picker.maximumDate = Date()
    picker.tag = textField.tag
    picker.addTarget(self, action: #selector(updateDateField(sender:)), for: .valueChanged)
    textField.inputView = picker

    if let dateValue = value as? Date {
      textField.text = dateString(dateValue)
      picker.setDate(dateValue, animated: false)
    }
  }

  func setupCountryPicker(for textField: UITextField) {
    countryPicker = CountryPickerView(frame: UIScreen.main.bounds)
    countryPicker.showCountryCodeInView = false
    countryPicker.showPhoneCodeInView = false
    countryPicker.delegate = self
    textField.leftView = countryPicker
    textField.leftViewMode = .always
    countryTextField = textField
    let value = textField.tag == 1 ? viewModel.input1Value : viewModel.input2Value
    if let countryValue = value as? Country {
      countryPicker.setCountryByCode(countryValue.code)
      textField.text = countryValue.name
    } else {
      let selectedCountry = countryPicker.selectedCountry
      updateValue(with: textField, value: selectedCountry.code)
      textField.text = selectedCountry.name
    }
  }
}

// MARK: - Actions

extension PayoutTextInputCell {
  @objc
  func updateDateField(sender: UIDatePicker) {
    let selectedDate = sender.date
    if sender.tag == 1 {
      viewModel.input1Value = selectedDate
      textField.text = dateString(selectedDate)
    } else {
      viewModel.input2Value = selectedDate
      secondTextField.text = dateString(selectedDate)
    }
    valueUpdated?(indexPath, viewModel)
  }

  func dateString(_ date: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    return formatter.string(from: date)
  }

  func updateValue(with sender: AnyObject, value: Any) {
    if sender.tag == 1 {
      viewModel.input1Value = value
    } else {
      viewModel.input2Value = value
    }
    valueUpdated?(indexPath, viewModel)
  }
}

// MARK: - Delegates

extension PayoutTextInputCell: UITextFieldDelegate {
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    if viewModel.inputType == .country || viewModel.inputType2 == .country {
      guard let presenter = UIWindow.presentableController else { return true }
      countryTextField = textField
      countryPicker.showCountriesList(from: presenter)
      return false
    }
    return true
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
    let inputType = textField.tag == 1 ? viewModel.inputType : viewModel.inputType2
    guard inputType == .textInput || inputType == .number else { return }

    updateValue(with: textField, value: textField.text as Any)
  }
}

extension PayoutTextInputCell: CountryPickerViewDelegate {
  func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
    countryTextField.text = country.name
    updateValue(with: countryTextField, value: country.code as Any)
  }
}

// MARK: - Constants

extension PayoutTextInputCell {
  static var height: CGFloat { 64.0 }
}
