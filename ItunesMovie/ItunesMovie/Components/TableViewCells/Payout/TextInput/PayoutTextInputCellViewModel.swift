//
//  PayoutTextInputCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class PayoutTextInputCellViewModel: PayoutTextInputCellViewModelProtocol {
  var input1Placeholder: String
  var input2Placeholder: String?
  var inputType: PayoutInputType
  var inputType2: PayoutInputType
  var input1Value: Any?
  var input2Value: Any?
  var input1ParamKey: String
  var input2ParamKey: String?

  init(
    input1Placeholder: String,
    input2Placeholder: String? = nil,
    inputType: PayoutInputType = .textInput,
    inputType2: PayoutInputType = .textInput,
    input1ParamKey: String,
    input2ParamKey: String? = nil,
    input1Value: Any? = nil,
    input2Value: Any? = nil
  ) {
    self.input1Placeholder = input1Placeholder
    self.input2Placeholder = input2Placeholder
    self.inputType = inputType
    self.inputType2 = inputType2
    self.input1ParamKey = input1ParamKey
    self.input2ParamKey = input2ParamKey
    self.input1Value = input1Value
    self.input2Value = input2Value
  }
}
