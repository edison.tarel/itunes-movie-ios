//
//  MovieCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import SwiftDate

class MovieCellViewModel: MovieCellViewModelProtocol {
  private let movie: Movie

  init(movie: Movie) {
    self.movie = movie
  }
}

// MARK: - Getters

extension MovieCellViewModel {
  var trackText: String { movie.trackName }
  var genreText: String { movie.primaryGenreName }
  var artworkURL: URL? { movie.artworkUrl60 }
  var priceText: String { return "$\(movie.trackPrice)" }
}
