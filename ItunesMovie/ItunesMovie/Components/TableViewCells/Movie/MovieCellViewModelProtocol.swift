//
//  MovieCellViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieCellViewModelProtocol {
  var trackText: String { get }
  var genreText: String { get }
  var priceText: String { get }
  var artworkURL: URL? { get }
}
