//
//  MovieCell.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Alamofire
import AlamofireImage
import UIKit

class MovieCell: UITableViewCell {
  @IBOutlet private(set) var trackLabel: UILabel!
  @IBOutlet private(set) var genreLabel: UILabel!
  @IBOutlet private(set) var priceLabel: UILabel!
  @IBOutlet private(set) var artWorkView: UIImageView!

  var viewModel: MovieCellViewModelProtocol! {
    didSet { setupVM() }
  }
}

// MARK: - Setup

private extension MovieCell {
  func setupVM() {
    guard viewModel != nil else { return }

    trackLabel.text = viewModel.trackText
    genreLabel.text = viewModel.genreText
    priceLabel.text = viewModel.priceText
    artWorkView.setImageWithURL(
      viewModel.artworkURL,
      placeholder: R.image.moviePlaceholder()
    )
  }
}
