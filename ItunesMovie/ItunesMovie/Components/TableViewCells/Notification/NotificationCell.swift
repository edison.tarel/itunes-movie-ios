//
//  NotificationCell.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
  var viewModel: NotificationCellViewModelProtocol! {
    didSet { setupVM() }
  }

  @IBOutlet private(set) var mainImageView: ImageView!
  @IBOutlet private(set) var messageLabel: UILabel!
  @IBOutlet private(set) var accessoryImageView: ImageView!
}

// MARK: - Setup

private extension NotificationCell {
  func setupVM() {
    guard viewModel != nil else { return }
    
    mainImageView.setImageWithURL(viewModel.mainImageURL)
    messageLabel.attributedText = generateMessageAttributedText(
      from: viewModel.actorText,
      actionText: viewModel.actionText,
      timeAgoText: viewModel.timeAgoText
    )

    accessoryImageView.setImageWithURL(viewModel.accessoryImageURL)
    accessoryImageView.isHidden = (viewModel.accessoryImageURL == nil)
  }
}

// MARK: - Utils

private extension NotificationCell {
  func generateMessageAttributedText(
    from actorText: String,
    actionText: String,
    timeAgoText: String
  ) -> NSAttributedString {
    let message = "\(actorText) \(actionText) \(timeAgoText)"
    return message
      .attributedString(
        R.font.sfProTextRegular(size: 17.0)!,
        foregroundColor: .black
      )
      .attributedString(
        actorText,
        font: R.font.sfProTextSemibold(size: 17.0)!,
        foregroundColor: .black
      )
      .attributedString(
        actionText,
        font: R.font.sfProTextRegular(size: 17.0)!,
        foregroundColor: .black
      )
      .attributedString(
        timeAgoText,
        font: R.font.sfProTextRegular(size: 11.0)!,
        foregroundColor: .black
      )
  }
}
