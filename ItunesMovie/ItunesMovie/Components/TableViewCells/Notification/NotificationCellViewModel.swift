//
//  NotificationCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

import SwiftDate

class NotificationCellViewModel: NotificationCellViewModelProtocol {
  private lazy var messageComponents: MessageComponents = {
    extractMessageComponents()
  }()

  private let notification: AppNotification

  init(notification: AppNotification) {
    self.notification = notification
  }
}

// MARK: - Utils

private extension NotificationCellViewModel {
  func extractMessageComponents(
  ) -> MessageComponents {
    guard let index = notification.message.index(of: notification.type.keyPhrase) else {
      return MessageComponents(actorText: "", actionText: "")
    }

    return MessageComponents(
      actorText: String(notification.message[..<index]).trimmed,
      actionText: String(notification.message[index...]).trimmed
    )
  }
}

// MARK: - Getters

extension NotificationCellViewModel {
  var mainImageURL: URL? { notification.actor?.avatarThumbURL }
  var actorText: String { messageComponents.actorText }
  var actionText: String { messageComponents.actionText }

  var timeAgoText: String {
    notification.createdAt.toRelative(
      style: RelativeFormatter.twitterStyle()
    )
  }

  var accessoryImageURL: URL? {
    // TODO: Replace with notification.image or something
    if notification.type == .like { return nil }
    return notification.actor?.avatarThumbURL
  }
}

// MARK: - Typealiases

private typealias MessageComponents = (
  actorText: String,
  actionText: String
)
