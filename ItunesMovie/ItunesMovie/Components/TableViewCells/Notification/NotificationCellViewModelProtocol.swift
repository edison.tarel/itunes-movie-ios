//
//  NotificationCellViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol NotificationCellViewModelProtocol {
  var mainImageURL: URL? { get }
  var actorText: String { get }
  var actionText: String { get }
  var timeAgoText: String { get }
  var accessoryImageURL: URL? { get }
}
