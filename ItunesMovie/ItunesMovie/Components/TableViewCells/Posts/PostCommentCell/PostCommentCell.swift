//
//  PostCommentCell.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation
import UIKit

class PostCommentCell: UITableViewCell {
  var viewModel: PostCommentCellViewModelProtocol! {
    didSet { refresh() }
  }

  var onOptionButtonTap: VoidResult?
  var onReplyButtonTap: VoidResult?

  @IBOutlet private(set) var authorImage: ImageView!
  @IBOutlet private(set) var authorNameLabel: UILabel!
  @IBOutlet private(set) var commentBodyLabel: UILabel!
  @IBOutlet private(set) var datePostedLabel: UILabel!
  @IBOutlet private(set) var optionButton: UIButton!
  @IBOutlet private(set) var replyButtonContainer: UIView!

  @IBOutlet private(set) var mainStackViewLeadingConstraint: NSLayoutConstraint!
  @IBOutlet private(set) var authorImageWidthConstraint: NSLayoutConstraint!
}

// MARK: - Refresh

private extension PostCommentCell {
  func refresh() {
    guard viewModel != nil else { return }

    authorImage.setImageWithURL(
      viewModel.authorAvatarURL,
      placeholder: R.image.profilePicPlaceholder()
    )
    authorNameLabel.text = viewModel.authorName
    datePostedLabel.text = viewModel.datePostedText
    commentBodyLabel.text = viewModel.commentText

    let isLevel1 = (viewModel.level == .level1)
    let leadingWidth: CGFloat = isLevel1 ? 16 : 56
    let imageWidth: CGFloat = isLevel1 ? 40 : 24
    
    mainStackViewLeadingConstraint.constant = leadingWidth
    authorImageWidthConstraint.constant = imageWidth
    authorImage.layer.cornerRadius = imageWidth / 2
    
    optionButton.isHidden = true // TODO: Show when we support comment options
    replyButtonContainer.isHidden = true //TODO: Show when we support comment in comments
  }
}


// MARK: - Actions

private extension PostCommentCell {
  @IBAction
  func optionButtonTapped(_ sender: Any) {
    onOptionButtonTap?()
  }

  @IBAction
  func replyButtonTapped(_ sender: Any) {
    onReplyButtonTap?()
  }
}
