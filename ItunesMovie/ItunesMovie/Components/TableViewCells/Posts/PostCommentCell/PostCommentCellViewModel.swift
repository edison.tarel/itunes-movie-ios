//
//  PostCommentCellViewModel.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

import SwiftDate

class PostCommentCellViewModel: PostCommentCellViewModelProtocol {
  private let comment: Comment

  init(comment: Comment) {
    self.comment = comment
  }
}

extension PostCommentCellViewModel {
  var authorAvatarURL: URL? { comment.author?.avatarThumbURL }
  var authorName: String { comment.author?.fullName ?? S.anonymous() }
  var datePostedText: String { comment.createdAt.toPreciseRelative() }
  var commentText: String { comment.body }
  var level: Comment.Level { comment.level }
}
