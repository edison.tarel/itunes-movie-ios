//
//  PostCommentCellViewModelProtocol.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

protocol PostCommentCellViewModelProtocol {
  var authorAvatarURL: URL? { get }
  var authorName: String { get }
  var datePostedText: String { get }
  var commentText: String { get }
  var level: Comment.Level { get }
}
