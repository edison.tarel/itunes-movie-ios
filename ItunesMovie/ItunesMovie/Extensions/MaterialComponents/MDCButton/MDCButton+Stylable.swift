//
//  MDCButton+Stylable.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import MaterialComponents
import UIKit

extension MDCButton: Styleable {
  func applyStyle(_ style: MDCButtonStyleType) {
    style.apply(to: self)
  }
}

enum MDCButtonStyleType: ViewStyleType {
  case primary
  case black
  case white

  func apply(to button: MDCButton) {
    switch self {
    case .primary: applyPrimaryStyle(to: button)
    case .black: applyBlackStyle(to: button)
    case .white: applyWhiteStyle(to: button)
    }
  }
}

private extension MDCButtonStyleType {
  private func applyBaseStyle(to button: MDCButton) {
    button.layer.cornerRadius = 8
    button.setTitleFont(R.font.sfProTextRegular(size: 20)!, for: .normal)
  }

  func applyPrimaryStyle(to button: MDCButton) {
    applyBaseStyle(to: button)
  }

  func applyBlackStyle(to button: MDCButton) {
    applyBaseStyle(to: button)
    button.setBackgroundColor(.black)
    button.setTitleColor(.white, for: .normal)
  }

  func applyWhiteStyle(to button: MDCButton) {
    button.applyOutlinedTheme(withScheme: MDCHelper.shared.containerScheme)

    button.setTitleColor(R.color.blue_2C66D5(), for: .normal)
    button.setBackgroundColor(.white)

    applyBaseStyle(to: button)

    button.applyShadow(
      color: Styles.Colors.Form.normalColor,
      opacity: 0.1,
      offSet: CGSize(width: 0, height: 4),
      radius: 6
    )
  }
}
