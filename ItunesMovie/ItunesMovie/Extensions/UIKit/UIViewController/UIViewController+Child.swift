//
//  UIViewController+Child.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  func addAsChild(to parent: UIViewController) {
    parent.addChild(self)
    parent.view.addSubview(view)
    didMove(toParent: parent)
  }

  func removeAsChild() {
    // Just to be safe, we check that this view controller
    // is actually added to a parent before removing it.
    guard parent != nil else {
      return
    }

    willMove(toParent: nil)
    view.removeFromSuperview()
    removeFromParent()
  }
}
