//
//  CropViewControllerDelegate+Defaults.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit

import Mantis

extension CropViewControllerDelegate {
  func cropViewControllerDidFailToCrop(
    _ cropViewController: CropViewController,
    original: UIImage
  ) {
    // noop
  }

  func cropViewControllerDidCancel(
    _ cropViewController: CropViewController,
    original: UIImage
  ) {
    cropViewController.dismissModal()
  }

  func cropViewControllerDidBeginResize(
    _ cropViewController: CropViewController
  ) {
    // noop
  }

  func cropViewControllerDidEndResize(
    _ cropViewController: CropViewController,
    original: UIImage,
    cropInfo: CropInfo
  ) {
    // noop
  }

  func cropViewControllerWillDismiss(
    _ cropViewController: CropViewController
  ) {
    // noop
  }
}
