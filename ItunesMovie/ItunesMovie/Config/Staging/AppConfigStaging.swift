//
//  AppConfigStaging.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct AppConfigStaging: AppConfigProtocol {}

// MARK: - App URLs

extension AppConfigStaging {
  var baseUrl: String {
    "https://itunes.apple.com"
  }

  var termsOfServiceUrl: String {
    "https://www.tryplaceholder.com/terms/"
  }

  var privacyPolicyUrl: String {
    "https://www.tryplaceholder.com/privacy/"
  }

  var secrets: AppSecretsProtocol { AppSecretsStaging() }
}

// MARK: - Default Parameters

extension AppConfigStaging {
  var defaultPageSize: Int { 10 }

  var defaultPhotoCompression: Float { 0.7 }
}
