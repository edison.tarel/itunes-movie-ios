//
//  AppConfig.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol AppConfigProtocol {
  /// Backend server URL
  var baseUrl: String { get }

  /// URL string for the terms of service page for the app
  var termsOfServiceUrl: String { get }

  /// URL string for the privacy policy  page for the app
  var privacyPolicyUrl: String { get }

  /// Default pagination items per page
  var defaultPageSize: Int { get }

  /// Default compression quality for UIImage before sending to backend
  var defaultPhotoCompression: Float { get }
  
  var secrets: AppSecretsProtocol { get }
}

struct AppConfig: AppConfigProtocol {}

// MARK: - App URLs

extension AppConfig {
  var baseUrl: String {
    "https://itunes.apple.com"
  }

  var termsOfServiceUrl: String {
    "https://www.tryplaceholder.com/terms/"
  }

  var privacyPolicyUrl: String {
    "https://www.tryplaceholder.com/privacy/"
  }
  
  var secrets: AppSecretsProtocol { AppSecrets() }
}

// MARK: - Default Parameters

extension AppConfig {
  var defaultPageSize: Int { 10 }

  var defaultPhotoCompression: Float { 0.7 }
}
