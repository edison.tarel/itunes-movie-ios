//
//  AppNotification.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct AppNotification: APIModel, Codable {
  let id: String
  let type: Type
  let actorId: Int
  let message: String
  let createdAt: Date
  let actor: User?
}

extension AppNotification {
  enum `Type`: String, Codable {
    case follow
    case comment
    case like

    var keyPhrase: String {
      switch self {
      case .follow:
        return S.notificationFollowKeyPhrase()
      case .comment:
        return S.notificationCommentKeyPhrase()
      case .like:
        return S.notificationLikeKeyPhrase()
      }
    }
  }
}
