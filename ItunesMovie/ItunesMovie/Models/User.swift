//
//  User.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct User: APIModel, Codable, Identifiable, Equatable {
  let id: Int
  let email: String?
  let phoneNumber: String?
  let fullName: String?
  let gender: Gender?

  // Email verification status
  let emailVerified: Bool

  // Phone verification status
  let phoneNumberVerified: Bool

  let avatarPermanentUrl: URL
  let avatarPermanentThumbUrl: URL

  let avatar: Photo?
  
  let birthdate: String?
  let description: String?
}

extension User {
  var isEmailVerified: Bool { emailVerified }
  var isPhoneNumberVerified: Bool { phoneNumberVerified }
  var hasName: Bool { !fullName.isNilOrEmpty }
  var hasAvatar: Bool { avatar != nil }

  var needsToVerifyEmail: Bool {
    guard let email = email else { return false }
    return !email.isEmpty && !isEmailVerified
  }

  var needsToVerifyPhoneNumber: Bool {
    !phoneNumber.isNilOrEmpty && !isPhoneNumberVerified
  }
  
  var birthdateValue: Date? {
    guard let bd = birthdate else { return nil }
    return Constants.Formatters.birthdateFormatter.date(from: bd)
  }
  
  var avatarThumbURL: URL {
    avatar?.thumbUrl ?? avatarPermanentThumbUrl
  }
  
  var avatarURL: URL {
    avatar?.url ?? avatarPermanentUrl
  }
}

// MARK: - Other Init

extension User {
  init(user: User, avatar: Photo?) {
    self.init(
      id: user.id,
      email: user.email,
      phoneNumber: user.phoneNumber,
      fullName: user.fullName,
      gender: user.gender,
      emailVerified: user.emailVerified,
      phoneNumberVerified: user.phoneNumberVerified,
      avatarPermanentUrl: user.avatarPermanentUrl,
      avatarPermanentThumbUrl: user.avatarPermanentThumbUrl,
      avatar: avatar,
      birthdate: user.birthdate,
      description: user.description
    )
  }
}
