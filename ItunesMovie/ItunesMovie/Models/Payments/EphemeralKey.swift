//
//  EphemeralKey.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct EphemeralKey: APIModel, Codable {
  let id: String
  let object: String
  let secret: String
  let associatedObjects: [AssociatedObject]
  let created: Int
  let expires: Int
  let livemode: Bool
}

// MARK: - AssociatedObject

extension EphemeralKey {
  struct AssociatedObject: Codable {
    let id: String
    let type: String
  }
}
