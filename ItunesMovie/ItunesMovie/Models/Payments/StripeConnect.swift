//
//  StripeConnectParams.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import CountryPickerView
import Foundation

struct StripeConnect: APIModel, Codable {
  let individual: IndividualAccount
  let externalAccount: String?
  let externalAccounts: ExternalAccounts?
}

extension StripeConnect {
  struct ExternalAccounts: Codable {
    let data: [BankDetails]
  }

  struct IndividualAccount: Codable {
    let firstName: String?
    let lastName: String?
    let dob: DateOfBirth?
    let address: Address?
    let verification: VerificationDocument?
  }

  struct DateOfBirth: Codable {
    let day: Int?
    let month: Int?
    let year: Int?
  }

  struct Address: Codable {
    let city: String?
    let line1: String?
    let state: String?
    let country: String?
    let postalCode: String?
  }

  struct VerificationDocument: Codable {
    let document: Document?
  }

  struct Document: Codable {
    let front: String?
    let back: String?
  }
}

// MARK: Utils

extension StripeConnect.DateOfBirth {
  var date: Date? {
    guard
      let year = year,
      let month = month,
      let day = day
    else { return nil }

    return Date(
      year: year,
      month: month,
      day: day,
      hour: 0,
      minute: 0
    )
  }
}

extension StripeConnect.Address {
  var countryObject: Country {
    CountryPickerView().getCountryByCode(country ?? "")!
  }
}
