//
//  BankDetails.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct BankDetails: APIModel, Codable {
  private(set) var id: String?
  private(set) var accountHolderName: String?
  private(set) var last4: String?
  private(set) var routingNumber: String?
}

extension BankDetails {
  var accountNumber: String { "*" + (last4 ?? "") } // TODO: Check for actual account number length for * placement
  var bsb: String { routingNumber ?? "" }
}
