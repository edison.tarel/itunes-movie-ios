//
//  ReportCategory.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct ReportCategory: APIModel, Codable, Equatable {
  let id: Int
  let label: String
}
