//
//  Report.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct Report: APIModel, Codable, Equatable {
  let reasonId: Int
  let description: String
  let attachments: [Data]

  var customDictionary: JSONDictionary {
    [
      "reason_id": reasonId,
      "description": description,
      "attachments": attachments
    ]
  }
}
