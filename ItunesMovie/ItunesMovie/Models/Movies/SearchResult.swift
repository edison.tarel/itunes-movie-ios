//
//  SearchResult.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct SearchResult<T: Codable>: Codable {
  var resultCount: Int?
  var results: [T]?
}
