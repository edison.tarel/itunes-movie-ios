//
//  Movie.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct Movie: Codable {
  let trackId: Int32
  let trackName: String
  let primaryGenreName: String
  let artworkUrl60: URL?
  let trackPrice: Float
  let longDescription: String
}

extension Movie: Equatable {
  static func == (lhs: Movie, rhs: Movie) -> Bool {
    lhs.trackId == rhs.trackId
  }
}
