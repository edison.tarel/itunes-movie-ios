//
//  Post.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

struct Post: APIModel, Codable, Identifiable {
  let id: Int
  let body: String
  let commentsCount: Int
  let favoritesCount: Int
  let isFavorite: Bool
  let author: User
  let photo: Photo
  let updatedAt: Date
  let createdAt: Date
}

extension Post: Hashable {
  func hash(into hasher: inout Hasher) {
    hasher.combine(id)
  }
}

extension Post: Equatable {
  static func == (lhs: Post, rhs: Post) -> Bool {
    lhs.id == rhs.id
  }
}

extension Post {
  func toggleLikeCopy() -> Post {
    Post(
      id: id,
      body: body,
      commentsCount: commentsCount,
      favoritesCount: favoritesCount + (isFavorite ? -1 : 1),
      isFavorite: !isFavorite,
      author: author,
      photo: photo,
      updatedAt: updatedAt,
      createdAt: createdAt
    )
  }
  
  func incrementCommentCountCopy() -> Post {
    Post(
      id: id,
      body: body,
      commentsCount: commentsCount + 1,
      favoritesCount: favoritesCount,
      isFavorite: isFavorite,
      author: author,
      photo: photo,
      updatedAt: updatedAt,
      createdAt: createdAt
    )
  }
}
