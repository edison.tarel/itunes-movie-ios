//
//  Comment.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//  

import Foundation

struct Comment: APIModel {
  let id: Int
  let author: User?
  let authorId: Int
  let body: String
  let createdAt: Date
  let updatedAt: Date
  let responses: [Comment]?

  var level: Level = .level1
}

extension Comment: Codable {
  enum CodingKeys: String, CodingKey {
    case id, author, authorId, body, createdAt, updatedAt, responses
  }
}

extension Comment {
  enum Level: Int, Codable {
    case level1 = 1, level2
  }
}
