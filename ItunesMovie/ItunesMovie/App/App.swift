//
//  App.swift
//  ItunesMovie
//
//  Created by Team Appetiser ( https://appetiser.com.au )
//  Copyright © 2018 Appetiser Pty Ltd. All rights reserved.
//

import Firebase
import GooglePlaces
import GoogleSignIn
import UIKit
import Valet

/// This is our main application object. This holds instances of all the services available
/// in the app like the APIClient, SessionService, etc.
///
/// IMPORTANT:
/// - Defer creation of service instance up to the point where it's first needed.
///
class App {
  enum Environment: String {
    case staging
    case production
  }

  static let shared = App()

  static var environment: Environment {
    // These values are set in their corresponding Targets.
    // See: Target > Build Settings > Other Swift Flags.
    #if STAGING
      return .staging
    #else
      return .production
    #endif
  }

  static let valet = Valet.valet(
    with: Identifier(nonEmpty: App.bundleIdentifier!)!,
    accessibility: .whenUnlocked
  )

  private(set) var config: AppConfigProtocol!
  private(set) var api: APIClient!
  private(set) var session: SessionService!
  private(set) var guest: GuestService!
  private(set) var deepLink: DeepLinkServiceProtocol!
  private(set) var pushNotif: PushNotificationService!
  private(set) var notifications: NotificationsService!
  private(set) var report: ReportServiceProtocol!
  private(set) var payment: PaymentService!
  private(set) var posts: PostsServiceProtocol!
  private(set) var movies: MoviesServiceProtocol!
  private(set) var errorHandling: ErrorHandlingServiceProtocol!

  // MARK: Initialization

  init() {
    debugLog("env: \(App.environment.rawValue)")
  }

  func bootstrap(
    with application: UIApplication,
    launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) {
    config = (App.environment == .production) ? AppConfig() : AppConfigStaging()
    api = APIClient(baseURL: URL(string: config.baseUrl)!, version: "")

    session = SessionService(
      api: api,
      register: RegisterService(api: api),
      login: LoginService(api: api),
      socialAuth: SocialAuthService(api: api),
      accountVerification: AccountVerificationService(api: api),
      profileSetup: ProfileSetupService(api: api),
      profileRefresh: ProfileRefreshService(api: api),
      editProfile: EditProfileService(api: api),
      credentials: CredentialsService(
        authChangeAPI: api,
        authVerificationAPI: api
      ),
      accountDeletion: AccountDeletionService(api: api),
      logout: LogoutService(api: api)
    )

    guest = GuestService(
      usernameChecker: UsernameCheckerService(api: api),
      password: PasswordService(api: api)
    )

    deepLink = DeepLinkService(
      launchOptions: launchOptions
    )
    pushNotif = PushNotificationService(
      api: api,
      session: session,
      deepLink: deepLink
    )

    notifications = NotificationsService(api: api)
    report = ReportService(api: api)
    payment = PaymentService(api: api)
    posts = PostsService(api: api)
    movies = MoviesService(
      api: api,
      dataStorage: MovieStorage()
    )

    errorHandling = ErrorHandlingService(session: session)

    GIDSignIn.sharedInstance().clientID = config.secrets.googleClientID
  }
}

// MARK: - App Info

extension App {
  static var bundleIdentifier: String? {
    return Bundle.main.bundleIdentifier
  }

  /// A dictionary, constructed from the bundle’s Info.plist file.
  static var info: [String: Any] {
    return Bundle.main.infoDictionary ?? [:]
  }

  static var displayName: String {
    return (info["CFBundleDisplayName"] as? String) ?? "ItunesMovie"
  }

  /// Alias for `CFBundleShortVersionString`.
  static var releaseVersion: String {
    return (info["CFBundleShortVersionString"] as? String) ?? "1.0"
  }

  /// Alias for `CFBundleVersion`.
  static var buildNumber: String {
    return (info["CFBundleVersion"] as? String) ?? "1"
  }
}

/// Use this for all App-level errors.
// TODO: Add conformance to CustomNSError.
enum AppError: Error {
  case unauthorized(_ reason: String)
  case unknown
}

extension AppError: LocalizedError {
  var errorDescription: String? {
    switch self {
    case .unauthorized:
      return S.errorDevAuthorization()
    default:
      return S.errorDevSomethingWrong()
    }
  }

  var failureReason: String? {
    switch self {
    case let .unauthorized(reason):
      return reason
    default:
      return S.errorDevUnknown()
    }
  }
}
