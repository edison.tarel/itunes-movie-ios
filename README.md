# Project name : iTunesMovie
### Description: This project demonstrates the usage of iTunes Search API and displaying the fetched items on a master-detail.

#### Persistence
All fetched item from iTunes Search API are stored to sqlite database. 
CoreData was used on this application for object graph management. It was chosen on this project as it was easy to use and setup, no other dependency needed.

The app saves the last visit time and display on the top of searched results as a header.
State restoration is also applied on this project, every time the user leaves the app, the state was preserved and restored on next app interaction.

#### Architecture
The project uses MVVM Pattern to separate class responsibilities, this pattern also helps in creating unit tests.
  
 #### Screenshots
[![Simulator-Screen-Shot-i-Phone-12-Pro-Max-2020-12-23-at-09-23-00.png](https://i.postimg.cc/VdjjF61r/Simulator-Screen-Shot-i-Phone-12-Pro-Max-2020-12-23-at-09-23-00.png)](https://postimg.cc/VdjjF61r)
[![Simulator-Screen-Shot-i-Phone-12-Pro-Max-2020-12-23-at-09-23-04.png](https://i.postimg.cc/SjYLJdvG/Simulator-Screen-Shot-i-Phone-12-Pro-Max-2020-12-23-at-09-23-04.png)](https://postimg.cc/SjYLJdvG)

### ToDo
Add more Tests
